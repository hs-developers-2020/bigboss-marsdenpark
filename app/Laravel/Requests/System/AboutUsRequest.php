<?php

namespace App\Laravel\Requests\System;

use Session, Auth;
use App\Laravel\Requests\RequestManager;

class AboutUsRequest extends RequestManager
{

    public function rules()
    {
        return [
            'title'    => "nullable",
            'description' => "required",
        ];
    }

    public function messages()
    {
        return [
            'required'    => "Field is required.",
        ];
    }
}
