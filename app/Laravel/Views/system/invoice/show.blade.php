@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
		<!-- header starts -->
	
			<!-- Begin page content -->
			<div class="container ">
				<div class="row">
					<div class="col-lg-9  ">
                        <h6 class="text-uppercase text-xsmall spacing-1 text-color-gray">Audit Log: #{{$invoice_details->invoice_number}}</h6>
                    </div>
                    <div class="col-md-3 mb-3" >
                    
                        <h6 class="text-uppercase text-xsmall spacing-1 text-color-gray">Invoice Status:<b>{{Str::title($invoice_details->status)}}</b></h6>
                    </div>
                </div>  
                  
                      <div class="row">
   
                            <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 ">
                                <div class="card rounded-0 mb-3 border-0">
                                    <div class="card-body p-4">
                                        <div class="row mb-3">
                                            <div class="col-lg-7">
                                                 <p class="text-xsmall text-color-blue font-semibold text-uppercase">{{$invoice_details->created_at->format('l m/d/Y - g:i a')}} Added by <b>{{$invoice_details->user_info->full_name}}</b></p>     
                                            </div>
                                            <div class="col-md-5">
                                                <a style="float: right;" class="btn bg-color-blue text-white text-xxsmall text-uppercase mr-2" href=" {{ route('system.invoice.print',[$invoice_details->id]) }}" ><i class="fas fa-print mr-2" ></i>Print log
                                                </a>
                                                @if($invoice_details->status == "paid")
                                                <a  style="float: right;" class="btn bg-color-blue text-white text-xxsmall text-uppercase mr-2" href="{{ route('system.invoice.send',[$invoice_details->booking_id]) }}" ><i class="fas fa-share mr-2" ></i>Email Invoice
                                                </a>
                                                @endif
                                               
                                                    <a style="float: right;" class="btn bg-color-blue text-white text-xxsmall text-uppercase btn-update mr-2" data-toggle="modal" data-target=".update" data-url="{{ route('system.invoice.update',[$invoice_details->id]) }}" data-target="#confirm-changes" href="#"><i class="fas fa-print mr-2" ></i>Update Invoice
                                                </a>
                                                
                                                
                                            </div>
                                             
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 pl-4 pr-4">
                                                <div class="d-flex justify-content-between mt-2">
                                                    <p class="text-color-blue font-semibold text-uppercase">Type:</p>
                                                    <p class="text-color-gray font-semibold ">Invoice</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 pl-4 pr-4">
                                                <div class="d-flex justify-content-between mt-2">
                                                    <p class="text-color-blue font-semibold text-uppercase">Invoice Number:</p>
                                                    <p class="text-color-gray font-semibold">#{{$invoice_details->invoice_number}}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 pl-4 pr-4">
                                                <div class="d-flex justify-content-between mt-2">
                                                    <p class="text-color-blue font-semibold text-uppercase">Amount: </p>
                                                    <p class="text-color-gray font-semibold">$ {{Helper::amount($invoice_details->amount)}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 pl-4 pr-4">
                                                <div class="d-flex justify-content-between">
                                                    <p class="text-color-blue font-semibold text-uppercase">Date: </p>
                                                    <p class="text-color-gray font-semibold">{{Helper::date_only($invoice_details->created_at)}}</p>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 pl-4 pr-4">
                                                <div class="d-flex justify-content-between">
                                                    <p class="text-color-blue font-semibold text-uppercase">Invoice to: </p>
                                                    <p class="text-color-gray font-semibold">{{Str::title($invoice_details->invoice_to)}}</p>
                                                </div>
                                            </div>
                                           
                                        </div>
                                       
                                        <table class="table bg-color-white mt-4 mb-0 footable">
                                            <thead>
                                                <tr>
                                                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 font-semibold text-color-gray text-left">Date</th>
                                                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 font-semibold text-color-gray text-left" data-breakpoints="xs">User</th>
                                                    <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 font-semibold text-color-gray text-left" data-breakpoints="xs sm">Invoice To</th>
                                                    <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 font-semibold text-color-gray text-left" data-breakpoints="xs sm">Account</th>
                                                    <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 font-semibold text-color-gray text-left" data-breakpoints="xs sm">Invoice Amount</th>
                                                </tr>
                                                </thead>
                                            <tbody>
                                                    <tr>
                                                        <td><h5 class="text-color-blue p-1 text-xsmall text-uppercase font-semibold">{{Helper::date_format($invoice_details->created_at)}}</h5></td>
                                                        <td><h5 class="text-color-gray p-1 text-xsmall text-uppercase">Added Invoice Entry <span class="text-color-blue font-semibold">#{{$invoice_details->invoice_number}}</span></h5></td>
                                                        <td><h5 class="text-color-blue font-semibold p-1 text-xsmall text-uppercase">{{Str::title($invoice_details->invoice_to)}}</h5></td>
                                                        <td><h5 class="text-color-blue font-semibold p-1 text-xsmall text-uppercase">{{Str::title($invoice_details->business_info->business_name)}}</h5></td>
                                                        <td><h5 class="text-color-blue  p-1 text-xsmall text-uppercase">+$ {{Helper::amount($invoice_details->amount)}}</h5></td>
                                                    </tr> 
                                          
                           
                                            </tbody>
                                        </table>                                 
                                    </div>
                                </div>
                            </div>
                  
                       
                    </div>
                
            
        </div>
        <!-- main container ends -->       
</div>
@stop



<div class="modal fade update"  tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Update Invoice</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="" id="form_location">
      <div class="modal-body">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="recipient-name" class="col-form-label">Invoice Status:</label>
            {!!Form::select('status',$invoice_status,old('status'),['class' => "form-control  text-color-blue font-semibold pl-3 pr-3", 'id' => "status"])!!}  
        </div>
      </div>
      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Removing record ..." class="btn btn-primary text-white btn-loading" id="btn-confirm-changes" >Save Changes</a>
      </div>
    </div>
  </div>
</div>

@section('page-styles')
<style type="text/css">
    footer {
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
#pie-chart{
  min-height: 250px;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
.text-color-yellow {
    color: #D8A009;
}
</style>
@stop

@section('page-scripts')

<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">

<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

     @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
@endif


   $(".btn-update").on("click",function(){
      var btn = $(this);
      $("#btn-confirm-changes").attr({"href" : btn.data('url')});
      $("#btn-confirm-changes").attr({"data-url" : btn.data('url')});
      $('#status').change();
    });

    $('#btn-confirm-changes').on('click', function() {
      $('.btn-link').hide();
      $('.btn-loading').button('loading');
      var status = $('#status').val()
      console.log(status)
      if (status == "") {
        alert("You need to select status")
        return false
      }else{
        $('#target').submit();
      }
      
     });

    $('#status').on('change', function(){
      console.log($(this).val())
      var url = $("#btn-confirm-changes").data('url') + "?status=" + $(this).val()
      $("#btn-confirm-changes").attr({"href" : url});
    });
});
 

</script>   

@stop