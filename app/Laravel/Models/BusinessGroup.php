<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon, Helper,Str;

class BusinessGroup extends Model
{
    use Notifiable, SoftDeletes, DateFormatterTrait;

    protected $table = "business_group";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'business_name','group','business_address', 'file_name', 'file_label','path','directory','filename'
    ];
    
    public $timestamps = true;

    public function business_file() {
        return $this->hasMany("App\Laravel\Models\BusinessGroupFile", "group_id" ,"id");
    }

    public function userinfo() {
        return $this->belongsTo("App\Laravel\Models\UserInfo", "user_id" ,"user_id");
    }

    public function scopeKeyword($query,$keyword){
        if($keyword){
            $key = Str::lower($keyword);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("business_name LIKE '%{$key}%'");
                         
            });

        }
    }
}
