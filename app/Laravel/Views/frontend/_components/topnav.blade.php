<nav class="navbar navbar-expand-md navbar-dark pb-3 pt-3 bg-color-white ">
<div class="container">
   <img src="{{ asset('frontend/images/main-logo.png') }}"  class="img-fluid logo-size">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-list text-color-red"></i>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item p-2 mr-5 pr-5">
        <a class="nav-link text-color-red text-xsmall font-semibold" href="#"><i class=" fas fa-phone-square"></i>  Call 1800 131 599</a>
      </li>
      <li class="nav-item  p-2 pr-4">
        <a class="nav-link text-color-gray text-xsmall font-semibold" href="#">How it works</a>
      </li>
      <li class="nav-item  p-2 pr-4">
        <a class="nav-link text-color-gray text-xsmall font-semibold" href="#">Services</a>
      </li>
      <li class="nav-item  p-2 pr-4">
        <a class="nav-link text-color-gray text-xsmall font-semibold" href="#">FAQS/Support</a>
      </li>
    </ul>
     <a href="{{route('frontend.login')}}" style="text-decoration: none;"> <button class="btn my-2 my-sm-0 btn-color-red text-xsmall ml-lg-3 ml-md-3" type="submit">Log in</button></a>
  </div>
</div>
</nav>
