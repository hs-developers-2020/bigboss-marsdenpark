@extends('frontend._layout.main')
@section('style-script')
<style type="text/css">
body {
background-color: #F4F4F4 !important;
}

@media only screen and (max-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center; 
    background-size: cover; \
    background-position: center;
}
.picture-size {
height: 15%; width: 30%;
}

}

@media only screen and (min-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center; 
    background-size: cover; 
     background-position: center;
}
.picture-size {
height:5%; width: 7%;
}

  }
  a:hover{
    color: white !important;
  }
</style>
@stop
@section('content')

@include('frontend._components.topnav-main')

<div class="container mt-4 text-center" id="gradient-background"  style="margin-top: 170px !important; padding:100px;">
    <h3 class="text-white text-uppercase">Terms of Use</h3>
</div>

<div class="container mt-2" style="background-color: white; ">         
    <div class="contentz">
      <div class="">
        <div class="card p-4" style="border: 0px solid rgba(0,0,0,.125);">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row">
                  <div class="col-lg-12 pl-5 pr-5 pt-5" style="background-color: white;">
                    <div class="row">
                      <p class="text-justify text-color-gray" style="font-size: 17px">
                        Please take the time to read these terms and conditions. By using BIG BOSS GROUP
                        Website and the Services and information offered on BIG BOSS GROUP Website, you are
                        agreeing to these terms and conditions.
                        <br><br>
                        If you purchase services through our Website, there will be additional terms and conditions
                        relating to the purchase. Please make sure you agree with these terms and conditions,
                        which you will be directed to read prior to making your purchase.
                          <br><br>    
                        <strong style="font-size:20px;">Definitions</strong>
                        <br><br>
                        <strong>Services</strong> means cleaning services for BIG BOSS CLEANING, Foreign Exchange for BIG
                        BOSS FX, accounting services for BIG BOSS ACCOUNTANTS and IT Services for BIG
                        BOSS INFOTECH.
                        <br><br>  
                        <strong>the Website</strong> means the website www.bigbossgroup.com.au
                        <br><br>  
                        <strong>We / Us</strong> etc. means BIG BOSS GROUP and any subsidiaries, affiliates, employees, officers,
                        agents or assigns.
                          <br><br>
                        <strong style="font-size:20px;">Accuracy of content</strong>
                        <br><br>
                        We have taken proper care and precautions to ensure that the information we provide on
                        this Website is accurate. However, we cannot guarantee, nor do we accept any legal liability
                        arising from or connected to, the accuracy, reliability, currency or completeness of anything
                        contained on this Website or on any linked site. The information contained on this Website
                        should not take the place of professional advice.
                          <br><br>  
                        <strong style="font-size:20px;">Use</strong>
                        <br><br>  
                        The Website is made available for your use on your acceptance and compliance with these
                        terms and conditions. By using this Website, you are agreeing to these terms and conditions.
                          <br><br>  
                        You agree that you will use this website in accordance with all applicable local, state,
                        national and international laws, rules and regulations.
                        <br>
                        You agree that you will not use, nor will you allow or authorise any third party to use, the
                        Website for any purpose that is unlawful, defamatory, harassing, abusive, fraudulent or
                        obscene way or in any other inappropriate way or in a way which conflicts with the Website
                        or the Services.
                        <br>
                        If you contribute to our forum (if any) or make any public comments on this Website which
                        are, in our opinion, unlawful, defamatory, harassing, abusive, fraudulent or obscene or in any
                        other way inappropriate or which conflict with the Website or the Services offered, then we
                        may at our discretion, refuse to publish such comments and/or remove them from the
                        Website. We reserve the right to refuse or terminate service to anyone at any time without
                        notice or reason.
                          <br><br>  
                        <strong style="font-size:20px;">Passwords and logins</strong>
                        <br><br>    
                        You are responsible for maintaining the confidentiality of your passwords and login details
                        and for all activities carried out under your password and login.
                        <br><br>
                        <strong style="font-size:20px;">Indemnification for loss or damage</strong>
                        <br><br>  
                        You agree to indemnify the BIG BOSS GROUP and hold THE BIG BOSS GROUP harmless
                        from and against any and all liabilities or expenses arising from or in any way related to your
                        use of this Website or the Services or information offered on this Website, including any
                        liability or expense arising from all claims, losses, damages (actual and consequential),
                        suits, judgments, litigation costs and solicitors fees of every kind and nature incurred by you
                        or any third parties through you.
                        <br><br>
                        <strong style="font-size:20px;">Intellectual property and copyrights</strong>
                        <br><br>  
                        We hold the copyright to the content of this Website, including all uploaded files, layout
                        design, data, graphics, articles, file content, codes, news, tutorials, videos, reviews, forum
                        posts and databases contained on the Website or in connection with the Services. You must
                        not use or replicate our copyright material other than as permitted by law. Specifically, you
                        must not use or replicate our copyright material for commercial purposes unless expressly
                        agreed to by Us, in which case we may require you to sign a Licence Agreement.
                        <br><br>  
                        If you wish to use content, images or other of our intellectual property, you should submit
                        your request to us at the following email address: contact@bigbossgroup.com.au
                        <br><br>
                        <strong style="font-size:20px;">Trademarks</strong>
                        <br>
                        The trademarks and logos contained on this Website are trademarks of THE BIG BOSS
                        GROUP. Use of these trademarks is strictly prohibited except with Our express, written
                        consent.
                        <br><br>
                        <strong style="font-size:20px;">Links to external websites</strong>
                        <br>
                        This Website may contain links that direct you outside of this Website. These links are
                        provided for your convenience and are not an express or implied indication that we endorse
                        or approve of the linked Website, its contents or any associated website, product or service.
                        We accept no liability for loss or damage arising out of or in connection to your use of these
                        sites.
                        <br><br>
                        You may link to our articles or home page. However, you should not provide a link which
                        suggests any association, approval or endorsement on our part in respect to your website,
                        unless we have expressly agreed in writing. We may withdraw our consent to you linking to
                        our site at any time by notice to you.
                          <br><br>
                        <strong style="font-size:20px;">Limitation of Liability</strong>
                        <br><br>  
                        We take no responsibility for the accuracy of any of the content or statements contained on
                        this Website or in relation to our Services. Statements made are by way of general
                        comment only and you should satisfy yourself as to their accuracy. Further, all of our
                        Services are provided without a warranty with the exception of any warranties provided by
                        law. We are not liable for any damages whatsoever, incurred as a result of or relating to the
                        use of the Website or our Services.
                        <br><br>
                        <strong style="font-size:20px;">Information Collection</strong>
                        <br><br>  
                        Use of information you have provided us with, or that we have collected and retained relating
                        to your use of the Website and/or our Services, is governed by our Privacy Policy. By using
                        this Website and the Services associated with this Website, you are agreeing to the Privacy
                        Policy. To view our Privacy Policy and read more about why we collect personal information
                        from you and how we use that information, click here (LINK TO YOUR PRIVACY POLICY
                        PAGE).
                        <br><br>
                        <strong style="font-size:20px;">Confidentiality</strong>
                        <br><br>
                        All personal information you give us will be dealt with in a confidential manner in accordance
                        with our Privacy Policy. However, due to circumstances outside of our control, we cannot
                        guarantee that all aspects of your use of this Website will be confidential due to the potential
                        ability of third parties to intercept and access such information.
                          <br><br>
                        <strong style="font-size:20px;">Governing Law</strong>
                        <br><br>  
                        These terms and conditions are governed by and construed in accordance with the laws of
                        NSW, Australia. Any disputes concerning this website are to be resolved by the courts
                        having jurisdiction in NSW. We retain the right to bring proceedings against you for breach of
                        these Terms and Conditions, in your country of residence or any other appropriate country or
                        jurisdiction.

                      </p>
                      
                    </div>
                  </div>         
             </div>
          </div>
        </div>  
   </div>
  </div>
    </div>

@include('frontend._components.footer')


@stop