<?php 
namespace App\Laravel\Listeners;

use App\Laravel\Events\SendEmailLink;

class SendEmailListenerLink{

	public function handle(SendEmailLink $email){
		$email->job();

	}
}