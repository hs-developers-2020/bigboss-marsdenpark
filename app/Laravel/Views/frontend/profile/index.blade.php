@extends('frontend._layout.main')
<style type="text/css">
	footer {
        position: absolute;
		margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 9% !important;
    background-color: #ECF5F5 !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('frontend._components.topnav-main')
<div class=" mt-5 pt-5 mt-lg-5 pt-lg-0 mt-xl-0 pt-xl-0">
	<div class="container bg-white mb-5 p-5">
        <form action="" method="POST">
            {{ csrf_field()}}
		<div class="row">
            <div class="col-lg-4 mt-1 text-center p-4">
                <img src="{{ asset('frontend/images/default.png') }}" style="border-radius: 50%; background-color: red" alt="Generic placeholder image" class="">
                <h5 class="text-uppercase mt-3">{{ strtoupper( auth()->user()->firstname) }} {{ auth()->user()->lastname }}</h5>
            </div>
            <div class="col-lg-8">
                <div class="">
                    <div class="row">
                        {{-- <div class="col-lg-12 mt-4">
                            <label class="font-semibold text-color-gray text-small text-uppercase">Personal Details</label>
                        </div>
                        <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Title: </label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="title" placeholder="">
                        </div> --}}

                         <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3 ">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">First Name:​</label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0 {{$errors->first('firstname') ? 'has-error' : NULL}}">
                            <input type="text" class="form-control" name="firstname" value="{{ auth()->user()->firstname }}">
                            @if($errors->first('firstname'))
                                <span class="help-block" style="color: red;">{{$errors->first('firstname')}}</span>
                            @endif  
                        </div>

                         <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3 ">
                           <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall"> Last Name:​</label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0 {{$errors->first('lastname') ? 'has-error' : NULL}}">
                            <input type="text" class="form-control" name="lastname" value="{{ auth()->user()->lastname  }}">
                            @if($errors->first('lastname'))
                                <span class="help-block" style="color: red;">{{$errors->first('lastname')}}</span>
                            @endif
                        </div>

                         {{-- <div class="col-lg-4 mt-lg-3 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Date of Birth:​</label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="date" class="form-control" name="">
                        </div> --}}

                        <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Email:</label>​
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0 {{$errors->first('email') ? 'has-error' : NULL}}">
                            <input type="email"  class="form-control" name="email"  value="{{ auth()->user()->email }}" >
                            @if($errors->first('email'))
                                <span class="help-block" style="color: red;">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                        <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Contact Number:</label>​
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
                            <input type="text"  class="form-control" name="contact_number"  value="{{ auth()->user()->contact_number }}" >
                            @if($errors->first('contact_number'))
                                <span class="help-block" style="color: red;">{{$errors->first('contact_number')}}</span>
                            @endif
                        </div>
                       {{--  <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Primary Address:</label>​
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text"  class="form-control" name="contact_number">
                        </div>
                        <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Secondary Address:</label>​
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text"  class="form-control" name="contact_number">
                        </div> --}}

                         {{-- <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">New Password: </label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="">
                        </div>

                         <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Re-confirm password:​</label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="">
                        </div> --}}

                        <div class="col-lg-12 mt-4">
                             <label class="font-semibold text-color-gray text-small text-uppercase">Business Details</label>
                        </div>

                        <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">ABN: </label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="abn" value="{{ old('abn',auth()->user()->abn ) }}">
                        </div>

                         <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Entity Name:​</label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="entity_name" value="{{ old('entity_name',auth()->user()->entity_name) }}">
                        </div>

                         <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                           <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall"> Company Name:​</label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="company_name" value="{{ old('company_name',auth()->user()->company_name) }}" >
                        </div>

                         <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Type of business:​</label>
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="business_type" value="{{ old('business_type',auth()->user()->business_type) }}">
                        </div>

                        <div class="col-lg-4 mt-lg-2 mt-md-0 mt-sm-3 mt-xs-3">
                            <label class="text-color-gray font-semibold text-uppercase spacing-1 text-xxsmall">Job Title:</label>​
                        </div>
                        <div class="col-lg-8 mt-lg-2 mt-md-2 mt-sm-0 mt-xs-0">
                            <input type="text" class="form-control" name="job_title" value="{{ old('business_type',auth()->user()->job_title) }}">
                        </div>

                       
                        <div class="col-lg-12 text-right mt-3">
                            <button type="submit" class="btn bg-color-red text-color-white text-uppercase text-xxxsmall spacing-1 font-semibold">Update Information</button>
                        </div>                 

                    </div>
                </div>
            </div>
                       
		</div>
        <div class="col-lg-12 mt-4">
            <a class="text-color-gray font-semibold no-decoration text-uppercase text-xxsmall" data-toggle="modal" data-target="#addAddress" href="#"><i class="fas fa-plus mr-2"></i>Add Address</a>
            <table class="table bg-color-white mt-3 mb-0 footable">
                <thead>
                    <tr>
                        <th class="p-3 text-uppercase text-xxxsmall spacing-1 text-left text-color-blue font-weight-bold">ID</th>
                        <th class="p-3 text-uppercase text-xxxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="sm">Address</th>
                        <th class="p-3 text-uppercase text-xxxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">Action</th>   
                    </tr>
                    <tbody>
                        <tr>
                            <td class="text-color-gray font-semibold">1</td>
                            <td class="text-color-gray font-semibold text-nowrap">Lorem ipsum dummy text</td>
                            <td><button class="btn btn-secondary text-xxsmall">Active</button></td>
                        </tr>
                        <tr>
                            <td class="text-color-gray font-semibold">2</td>
                            <td class="text-color-gray font-semibold text-nowrap">Lorem ipsum dummy text</td>
                            <td><button class="btn btn-primary text-xxsmall">Primary</button></td>
                        </tr>
                                       
                    </tbody>
                </thead>
                                           
            </table>      
         </div>

        </form>
	</div>
</div>
  {{-- Modals --}}
  <div class="modal fade" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12 mt-2">
                <label class="text-color-gray font-semibold text-uppercase text-xxsmall" >Address</label>
                <input type="text" class="form-control" name="address" placeholder="Unit No./StreetNo./StreetName/Suburb/Postcode/State">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save Changes</button>
      </div>
    </div>
  </div>
</div>
@stop



@section('page-styles')
<style type="text/css">
    .custom-nav {
    -webkit-box-shadow: 2px 2px 2px #212121 !important; 
    -moz-box-shadow:    2px 2px 2px #212121 !important; 
    box-shadow:         2px 2px 2px #212121 !important; 
    z-index:999;
}
</style>
@stop
