@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper pb-5">    
      <!-- Begin page content -->
  <div class="container">
      <h6 class="text-xsmall text-color-gray text-uppercase pb-3">Add new contract: <a href="{{ route('system.contract-group.list',[$business_file->group_id]) }}" class="text-color-blue font-semibold p-3 text-xsmall">{{$business_file->business_group->business_name}}</a></h6>
  </div>
      <div class="container pb-5 mt-2">       
          <div class="row">

              <div class="col-lg-3">
                <div class="card mb-4 ">
                        <div class="card-body">
                            <div class="row">
                                <!-- <div class="col-lg-12 text-right">
                                    <i class="fas fa-pen text-color-light mr-3"></i><i class="fas fa-trash-alt text-color-light"></i>
                                </div> -->
                                <div class="col-lg-12 text-center pt-3 pb-3">
                                    <i class="fas fa-file-alt fa-7x text-color-blue"></i>
                                </div>
                                <div class="col-lg-12 text-center pt-1">
                                    <p class="text-color-gray">{{$business_file->name}} - {{$business_file->label}}<br>
                                    <span class="text-color-blue text-xsmall"><i>{{str_replace("_", " ", $business_file->file_type)}}</i></span></p>
                                </div>
                                <div class="col-lg-12 mt-4 w-100 mb-4">
                                  <div class=" d-flex justify-content-center text-center w-100">
                                    @if($business_file->file_type=="image_file")
                                      <a href="#" class="btn bg-color-blue text-color-white text-xxsmall pop">
                                        <i class="fas fa-angle-double-down text-color-white mr-2"></i>Preview Image
                                      </a>
                                    @else
                                       <a href="{{ route('system.contract-group.download',[$business_file->id]) }}" class="btn bg-color-blue text-color-white text-xxsmall">
                                        <i class="fas fa-angle-double-down text-color-white mr-2"></i>Download File
                                      </a>
                                    @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              </div>
              <div class="col-lg-9 ">
                <div class="card">
                  <div class="card-body p-4">
                    <div class="row">
                    <div class="col-lg-8">
                      <div class="row">
                        <div class="col-md-3 pt-2">
                          <h6 class="text-xsmall spacing-1 text-color-gray">File Details</h6>
                        </div>
                        <div class="col-md-9  mb-3" >
                          <button class="btn btn-warning rounded-3 text-color-white text-xxsmall">{{$business_file->is_approved == "yes" ? Str::title($status) : "Pending"}} Contract</button>
                        </div>
                      </div>
                    </div>
                    @if(in_array(Str::lower(Auth::user()->type), ['area_head','head','super_user']))
                      @if($business_file->is_approved == "no" and $status == "active")
                      <div class="col-lg-4 mb-3 text-right">
                      <a class="btn-approve btn bg-color-blue text-white text-xsmall" data-url="{{ route('system.contract-group.approved',[$business_file->id]) }}" >Approve</a>
                        
                      </div>
                      @endif
                    @endif
                    </div>
                  
                    <div class="row">
                      <div class="col-lg-12 mt-2">
                        <p class="text-color-blue text-xsmall ml-2 font-semibold">Uploaded by</p>
                        <div class="d-flex flex-row">
                          <figure class="avatar150 rounded-circle  border" style="width: 10%;height: 100%;min-width: 1%">
                            @if($business_file->user->directory)
                              <img src="{{asset($business_file->user->directory.'/'.$business_file->user->filename)}}" alt="user image" >
                            @else
                              <img src="{{ asset('frontend/images/user.jpg') }}" alt="user image" >
                            @endif
                          </figure>  
                          <p class="text-color-gray font-semibold text-xsmall mt-3 ml-2">{{Str::title($business_file->user->full_name) ?: "N/A"}} <br>{{Helper::date_format($business_file->updated_at)}}</p>
                        </div>
                      </div>
                    </div>
                    <div class="row mt-2">
                      <div class="col-lg-4 d-flex flex-row mt-2">
                        <p class="text-color-blue text-xsmall mt-1 ml-2 font-semibold">File Name</p>
                        <p class="text-color-gray text-xsmall font-semibold mt-1 ml-2">{{Str::title($business_file->name)}}</p>
                      </div>
                      <div class="col-lg-4 d-flex flex-row mt-2">
                        <p class="text-color-blue text-xsmall mt-1 ml-2 font-semibold">File Label</p>
                        <p class="text-color-gray text-xsmall font-semibold mt-1 ml-2">{{$business_file->label}}</p>
                      </div>
                      <div class="col-lg-4 d-flex flex-row mt-2">
                        <p class="text-color-blue text-xsmall mt-1 ml-2 font-semibold">File Type</p>
                        <p class="text-color-gray text-xsmall font-semibold mt-1 ml-2">{{Str::title(str_replace("_", " " , $business_file->file_type))}}</p>
                      </div>
                     
                    </div>
                    <div class="row mt-2">
                      <div class="col-lg-4 d-flex flex-row mt-2">
                        <p class="text-color-blue text-xsmall mt-1 ml-2 font-semibold">Business Group</p>
                        <p class="text-color-gray text-xsmall font-semibold mt-1 ml-2">{{Str::title($business_file->business_group->business_name)}}</p>
                      </div>
                      <div class="col-lg-4 d-flex flex-row mt-2">
                        <p class="text-color-blue text-xsmall mt-1 ml-2 font-semibold">Reference Number</p>
                        <p class="text-color-gray text-xsmall font-semibold mt-1 ml-2">{{$business_file->reference}}</p>
                      </div>
                      <div class="col-lg-4 d-flex flex-row mt-2">
                        <p class="text-color-blue text-xsmall mt-1 ml-2 font-semibold">File Expiry</p>
                        <p class="text-color-gray text-xsmall font-semibold mt-1 ml-2">{{Helper::date_only($business_file->date_expiry)}}</p>
                      </div>
                    </div>
                    <div class="row">
                      @if($business_file->is_high_risk)
                      <div class="col-lg-4 d-flex flex-row mt-2">
                        <p class="text-color-blue text-xsmall mt-1 ml-2 font-semibold">High Risk?</p>
                        <p class="text-color-gray text-xsmall font-semibold mt-1 ml-2">{{Str::title(str_replace("_", " " , $business_file->is_high_risk))}}</p>
                      </div>
                      @endif
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
      </div>
    <!-- main container ends -->
  </div>
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">              
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <img src="{{asset($business_file->directory.'/'.$business_file->filename)}}" class="imagepreview" style="width: 100%;" >
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

@stop

@section('page-styles')
<style type="text/css">
  footer {
    position: absolute;
    margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
  }
body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@stop

@section('page-scripts')
<script type="text/javascript">
  $(function() {
    $('.pop').on('click', function() {
      $('#imagemodal').modal('show');   
    });   
});


$(".btn-approve").on('click', function(){
        var url = $(this).data('url');
        var self = $(this)
        swal({
            text: "You will not be able to recover this record.",   
            type: "warning", 
            input: 'select',
            inputOptions: {
              'yes':'Yes',
              'no': 'No'
            },
            inputPlaceholder: "High Risk Contract",
            showCancelButton: true,
            confirmButtonText: 'Yes, Approve it!',
            showLoaderOnConfirm: true,
        }).then((remarks) => {
            if (remarks === false) return false;
          
            if (remarks === "") {
                alert("You need to choose one")
                return false
            }
            console.log(remarks);
            window.location.href = url + "?remarks="+remarks;
        });
    });
</script>

@stop