<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCleaningServiceAddFieldRoomType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cleaning_services', function($table){
            $table->text('room_type')->nullable()->after('unit');
            $table->text('service_type_amount')->nullable()->after('service_type');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cleaning_services', function($table){
            $table->dropColumn(['room_type','service_type_amount']);
        });
    }
}
