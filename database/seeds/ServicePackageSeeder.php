<?php
use App\Laravel\Models\ServicePackage;
use Illuminate\Database\Seeder;

class ServicePackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ServicePackage::create(
            ['service_name' => "oven",'service_type'=>"regular",'property_type'=>"home", 'price' => "170"]);
        ServicePackage::create(
            ['service_name' => "carpet",'service_type'=>"regular",'property_type'=>"home", 'price' => "40"]);
         ServicePackage::create(
            ['service_name' => "window",'service_type'=>"regular",'property_type'=>"home", 'price' => "10"]);
         ServicePackage::create(
            ['service_name' => "lawn",'service_type'=>"regular",'property_type'=>"home", 'price' => "80"]);

          ServicePackage::create(
            ['service_name' => "rubbish",'service_type'=>"regular",'property_type'=>"home", 'price' => "80"]);


          ServicePackage::create(
            ['service_name' => "oven",'service_type'=>"once-off",'property_type'=>"home", 'price' => "80"]);
        ServicePackage::create(
            ['service_name' => "carpet",'service_type'=>"once-off",'property_type'=>"home", 'price' => "40"]);
         ServicePackage::create(
            ['service_name' => "window",'service_type'=>"once-off",'property_type'=>"home", 'price' => "10"]);
         ServicePackage::create(
            ['service_name' => "lawn",'service_type'=>"once-off",'property_type'=>"home", 'price' => "80"]);

          ServicePackage::create(
            ['service_name' => "rubbish",'service_type'=>"once-off",'property_type'=>"home", 'price' => "80"]);


ServicePackage::create(
            ['service_name' => "oven",'service_type'=>"lease",'property_type'=>"home", 'price' => "80"]);
        ServicePackage::create(
            ['service_name' => "carpet",'service_type'=>"lease",'property_type'=>"home", 'price' => "40"]);
         ServicePackage::create(
            ['service_name' => "window",'service_type'=>"lease",'property_type'=>"home", 'price' => "10"]);
         ServicePackage::create(
            ['service_name' => "lawn",'service_type'=>"lease",'property_type'=>"home", 'price' => "80"]);

          ServicePackage::create(
            ['service_name' => "rubbish",'service_type'=>"lease",'property_type'=>"home", 'price' => "80"]);



    }
}
