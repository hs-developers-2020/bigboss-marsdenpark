<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Link extends Model
{
    use DateFormatterTrait,SoftDeletes;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'main_link', 'ask_me_how', 'facebook', 'instagram', 'twitter', 'own_this_business',
    ];

    public $timestamps = true;
}
