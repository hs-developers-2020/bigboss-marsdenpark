<?php 

namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class FeedbackRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;
		$media_type = $this->request->get('media_type')?:"";
		
	
		$rules = [
			'name'	=> "required",
            'stars' =>"required",
            'description' =>"required",
			'file' =>"required",
		];
		
		if($id){
			$rules['file'] = "nullable|image";
		}

		return $rules;
	}

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
		];
	}
}