<?php 
namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use Mail,Str,Helper,Carbon;

class SendBooking extends Event {


	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->data = $form_data;
		// $this->email = $form_data['insert'];

	

	}

	public function job(){	
		
		
		foreach($this->data as $index =>$value){
			$mailname = "Booking Details";
			$emails = ['sprtn40@gmail.com'];
			$user_email = $value['email'];
			$this->data['new_email'] = $user_email;
			$this->data['booking_id'] = $value['booking_id'];
			// dd($this->data);
			Mail::send('emails.booking_ref', $this->data, function($message) use ($mailname,$emails,$user_email){
				$message->from('no_reply@highlysucceed.com');
				$message->to($user_email);
				$message->subject("Booking Details");
			});
		}


		
		
		
	}
}
