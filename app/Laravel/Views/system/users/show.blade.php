@extends('system._layouts.main')
<style type="text/css">
	footer {
		margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 9% !important;
}
#pie-chart{
  min-height: 250px;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
.text-color-yellow {
    color: #D8A009;
}
</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
		<!-- header starts -->
	
			<!-- Begin page content -->
			<div class="container ">
				<div class="row">
					<div class="col-lg-12  pb-3">
                        <h6 class="text-uppercase text-xsmall spacing-1 text-color-gray">User Profile</h6>
                    </div>

                        <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="card rounded-0 border-0 mb-3">
                            <div class="card-header rounded-0">
                              
                              
                            </div>
                            <div class="card-body  userlist_large text-center pb-5">
                                <div class="media mt-0">
                                    <figure class="avatar150 rounded-circle  border">
                                        @if($show_user->directory)
                                            <img src="{{"{$show_user->directory}/resized/{$show_user->filename}"}}" alt="user image">
                                        @else
                                            <img src="{{asset('frontend/images/default.png')}}" alt="user image">
                                        @endif
                                    </figure>
                                    <div class="media-body">
                                        <h4 class="mt-0 text-color-blue font-semibold">{{$show_user->full_name}}</h4>
                                        <p class="mb-2">Big Boss {{Str::title(str_replace("_"," ",$show_user->type))}}</p> 
                                         <p class="mb-2">{{$show_user->info->state ?: "N/A"}}</p> 
                                         <p class="text-color-yellow font-weight-bold">
                                            @for($i = 0; $i < round($rating ,0 ,PHP_ROUND_HALF_DOWN); $i++)
                                            <i class="fas fa-star fa-2x mr-2 text-color--yellow"></i>
                                            @endfor
                                           

                                        </p>                                                  
                                    </div>
                                </div>                                                                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-9 ">
                        <div class="card rounded-0 mb-3 border-0">
                            <div class="card-body p-4">
                                <div class="row">
                                    <div class="col-lg-12 text-right">
                                        <p class="text-color-Blue text-xsmall mb-2 ml-2 font-semibold"><a class="text-color-blue" href="{{ route('system.users.work-load',[$show_user->id]) }}" style="text-decoration: none;">View All</a></p>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between">
                                     <p class="text-xsmall text-color-gray font-semibold">Work Availability</p>
                                      <p class="text-xsmall text-color-blue text-right font-semibold">{{$startDate->format('M d')}} - {{$endDate->format('d , Y')}}</p>
                                </div>
                                <table class="table bg-color-white  mb-0 footable-availability">
                                    <thead>
                                        <tr>
                                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue text-left">Day</th>
                                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue text-left" data-breakpoints="xs">Work Details</th>
                                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Max. Leads</th>
                                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Time In</th>
                                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Time Out</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse($availability as $value)
                                            <tr>
                                            <td><h5 class="text-color-blue p-0 text-xxsmall">{{Helper::show_date($value->date)}}</td>
                                            <td><h5 class="text-color-gray p-0 text-xxsmall">{{Str::title(str_replace("_", " ", $value->work_stated))}}</td>
                                            <td><h5 class="text-color-gray p-0 text-xxsmall">{{$value->max_leads}}</h5></td>
                                            <td><h5 class="text-color-gray p-0 text-xxsmall">{{Helper::time_only($value->time_in)}}</h5></td>
                                            <td><h5 class="text-color-gray p-0 text-xxsmall">{{Helper::time_only($value->time_out)}}</h5></td>
                                            </tr> 
                                           
                                        @empty
                                        <tr>
                                           <td> <h5>No Record</h5></td>
                                        </tr>
                                        @endforelse
                                    </tbody>
                                </table>                                 
                            </div>
                        </div>
                    </div>
                    {{-- <div class="col-lg-6">
                        <div class="card rounded-0 mb-3 border-0">
                            <div class="card-body p-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <p class="text-color-gray text-xsmall mt-2 ml-2 font-semibold">Profile Details</p>
                                    </div>
                                    <div class="col-lg-6 d-flex flex-row mt-3">
                                        <span class="fa-stack fa-md text-color-blue">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-user text-xxsmall fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <p class="text-color-blue text-xxsmall mt-1 ml-2 font-semibold">Username</p>
                                    </div>
                                    <div class="col-lg-6 text-right mt-3">
                                        <p class="text-color-gray text-xxsmall mt-2 ml-2 font-semibold">Shonda-areahead</p>
                                    </div>

                                     <div class="col-lg-6 d-flex flex-row mt-3">
                                        <span class="fa-stack fa-md text-color-blue">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-phone text-xxsmall fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <p class="text-color-blue text-xxsmall mt-1 ml-2 font-semibold">Contact Number</p>
                                    </div>
                                    <div class="col-lg-6 text-right mt-3">
                                        <p class="text-color-gray text-xxsmall mt-2 ml-2 font-semibold">+61 456 456 435</p>
                                    </div>  

                                    <div class="col-lg-6 d-flex flex-row mt-3">
                                        <span class="fa-stack fa-md text-color-blue">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-envelope text-xxsmall fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <p class="text-color-blue text-xxsmall mt-1 ml-2 font-semibold">Email address</p>
                                    </div>
                                    <div class="col-lg-6 text-right mt-3">
                                        <p class="text-color-gray text-xxsmall mt-2 ml-2 font-semibold">Shondalee@gmail.com</p>
                                    </div>

                                    <div class="col-lg-6 d-flex flex-row mt-3">
                                        <span class="fa-stack fa-md text-color-blue">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-birthday-cake text-xxsmall fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <p class="text-color-blue text-xxsmall mt-1 ml-2 font-semibold">Birthdate</p>
                                    </div>
                                    <div class="col-lg-6 text-right mt-3">
                                        <p class="text-color-gray text-xxsmall mt-2 ml-2 font-semibold">September 10, 1975</p>
                                    </div>

                                    <div class="col-lg-6 d-flex flex-row mt-3">
                                        <span class="fa-stack fa-md text-color-blue">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-home text-xxsmall fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <p class="text-color-blue text-xxsmall mt-1 ml-2 font-semibold">Address</p>
                                    </div>
                                    <div class="col-lg-6 text-right mt-3">
                                        <p class="text-color-gray text-xxsmall mt-2 ml-2 font-semibold">12 The Avenue, North Sydney, NSW</p>
                                    </div>

                                    <div class="col-lg-6 d-flex flex-row mt-3">
                                        <span class="fa-stack fa-md text-color-blue">
                                            <i class="fa fa-circle fa-stack-2x"></i>
                                            <i class="fas fa-map text-xxsmall fa-stack-1x fa-inverse"></i>
                                        </span>
                                        <p class="text-color-blue text-xxsmall mt-1 ml-2 font-semibold">Post Code</p>
                                    </div>
                                    <div class="col-lg-6 text-right mt-3">
                                        <p class="text-color-gray text-xxsmall mt-2 ml-2 font-semibold">2060</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="col-lg-6">
                         <div class="card rounded-0 mb-3 border-0">
                            <div class="card-body p-4">
                                <div class="d-flex justify-content-between">
                                    <p class="text-color-gray font-semibold">User Territory</p>
                                    @if(in_array(Str::lower(Auth::user()->type), ['super_user','area_head','head']))
                                    <p class="text-color-blue font-semibold pointer"  data-toggle="modal" data-target=".map">Change</p>
                                    @endif
                                </div>
                               <div id="map" style="width: 100%; height: 300px;margin-top: 20px;"></div>
                                <p></p>
                            </div>
                        </div>
                    </div>

                   

                    {{--  Performance Chart--}}
                    @if(in_array(Str::lower(Auth::user()->type), ['super_user','franchisee']))
                        @if($show_user->type == "franchisee")
                            <div class="col-lg-6">
                                 <div class="card rounded-0 mb-3 border-0">
                                    <div class="card-body p-4">
                                        <div class="d-flex justify-content-between">
                                            <p class="text-color-gray font-semibold">Performance Chart</p>
                                            <p class="text-color-blue font-semibold">{{Helper::date_only($startDate)}} - {{Helper::date_only($endDate)}}</p>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div id="pie-chart" ></div>
                                            </div>
                                            <div class="col-lg-5 mt-5">
                                                <div class="d-flex flex-row">
                                                <i class="fas fa-circle mt-3 mr-3" style="color: #60B266;"></i>
                                                <p class="mt-1"> Converted <br> Leads 
                                                    <h1 class="ml-3" style="color: #60B266;">{{$converted_leads}}</h1></p>
                                                </div>
                                                 <div class="d-flex flex-row">
                                                <i class="fas fa-circle mt-3 mr-3" style="color: #A84047;"></i>
                                                <p class="mt-1"> Unconverted <br> Leads 
                                                    <h1 class="ml-3" style="color: #A84047;">{{$unconverted_leads}}</h1></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endif 

                  
                    {{-- Profile Bigboss Head --}}
                    @if(in_array(Str::lower(Auth::user()->type), ['super_user' ,'head']))
                        @if($show_user->type == "area_head" or $show_user->type == "head")
                        <div class="col-lg-6">
                            <div class="card rounded-0 mb-3 border-0">
                                <div class="card-body p-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="text-color-gray text-xsmall mt-2 ml-2 font-semibold">
                                        @if($show_user->type == "area_head")
                                        Franchisees</p>
                                        @endif
                                        @if($show_user->type == "head")
                                        Big Boss Area Head</p>
                                        @endif
                                        <p class="text-color-Blue text-xsmall mt-2 ml-2 font-semibold"><a class="text-color-blue" href="{{route('system.users.index')}}" style="text-decoration: none;">View All</a></p>
                                    </div>
                                    <table class="table bg-color-white  mb-0 footable">
                                <thead>
                                    <tr>
                                        <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall font-semibold spacing-1 text-color-blue text-left">Avatar</th>
                                        @if($show_user->type == "area_head")
                                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall font-semibold spacing-1 text-color-blue text-left" data-breakpoints="xs">User ID</th>
                                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall font-semibold create.blade.php spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Name</th>
                                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall font-semibold spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Designation</th>
                                        @endif
                                        @if($show_user->type == "head")
                                            <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall font-semibold spacing-1 text-color-blue text-left" data-breakpoints="xs">Name</th>
                                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall font-semibold create.blade.php spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Business Name</th>
                                            <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall font-semibold spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Region</th>
                                        @endif
                                        
                                    </tr>
                                    </thead>

                                        <tbody>
                                              @forelse($show as $index => $value)
                                                 <tr>
                                                <td>
                                                    <div class="figure avatar40 border rounded-circle align-self-start">
                                                        @if($value->directory)
                                                        <img src="{{"{$value->directory}/resized/{$value->filename}"}}" alt="Generic placeholder image" class="">      
                                                        @else
                                                        <img src="{{asset('frontend/images/default.png')}}" alt="Generic placeholder image" class="">
                                                        @endif
                                                    </div>
                                                </td>
                                                @if($show_user->type == "area_head")
                                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{$value->id}}</h5></td>
                                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{$value->full_name}}</h5></td>
                                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{$value->info->user_designation}}</h5></td>
                                                    </tr> 
                                                @endif
                                                @if($show_user->type == "head")
                                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{$value->full_name}}</h5></td>
                                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{$value->info->business_name}}</h5></td>
                                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{$value->info->state}}</h5></td>
                                                    </tr> 
                                                @endif
                                                @empty
                                                <tr>
                                                    <td>No Records</td>
                                                </tr> 
                                                @endforelse
                       
                                        </tbody>
                                    </table>                       
                                </div>
                            </div>
                        </div>
                        @endif
                    @endif
                    @if(in_array(Str::lower(Auth::user()->type), ['area_head']))
                        <div class="col-lg-6">
                            <div class="card rounded-0 mb-3 border-0">
                                <div class="card-body p-4">
                                    <div class="d-flex justify-content-between">
                                        <p class="text-color-gray text-xsmall mt-2 ml-2 font-semibold">Jobs</p>
                                      
                                        <p class="text-color-Blue text-xsmall mt-2 ml-2 font-semibold"><a class="text-color-blue" href="{{ route('system.jobs.index') }}" style="text-decoration: none;">View All</a></p>
                                    </div>
                                    <table class="table bg-color-white  mb-0 footable">
                                        <thead>
                                            <tr>
                                                <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Username</th>
                                                <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs">Service Type</th>
                                                <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">Service Fee</th>
                                                <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left" data-breakpoints="xs sm">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($all_jobs as $index => $value)
                                            <tr>
                                                <td class="userlist p-4" width="50%">
                                                    <a class=" text-color-white" style="text-decoration: none; cursor: hand;" href="{{ route('system.jobs.details',[$value->id]) }}">
                                                        <div class="media">
                                                            <div class="figure avatar40 border rounded-circle align-self-start">
                                                                <label class="checkbox-user-check">
                                                                <input type="checkbox">
                                                                <i class="fa fa-check"></i>
                                                                </label>
                                                                <img src="{{ asset('frontend/images/user.jpg') }}" alt="Generic placeholder image" class="">
                                                            </div>
                                                            <div class="media-body">
                                                                <h5 class="text-color-gray">{{Str::title($value->full_name)}}
                                                                <span class="float-right text-muted"></span>
                                                                </h5>
                                                                <p class="mb-0">{{$value->location}}</p>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <h5 class="text-color-gray p-0">{{$value->service_type}}</h5>
                                                    <p class="mb-0">{{Str::title($value->property_type)}}</p>
                                                </td>
                                                <td>
                                                    <h5 class="text-color-gray p-0">$ {{Helper::amount($value->booking_total)}}</h5>
                                                </td>
                                                <td>
                                                    <button class="btn btn-warning rounded-3 text-color-white text-xxsmall">{{Str::title($value->status)}}</button>
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td>No Records</td>
                                            </tr> 
                                            @endforelse
                                        </tbody>
                                    </table>                       
                                </div>
                            </div>
                        </div>
                    @endif  
                    </div>
                </div>
            </div>
        </div>
        <!-- main container ends -->       
</div>


<div class="modal fade map" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <form method="POST" action="" id="form_location">
            {{ csrf_field() }}
            <div id="map1" style="width: 100%; height: 500px;"></div>
            <div class="row">
                <div class="col-lg-2 text-left  pt-4 pb-4">
                    <button class="btn btn-danger ml-4 text-xsmall " data-dismiss="modal">CANCEL</button>
                </div>
                <div class="col-lg-7 text-center pt-4 pb-4">
                  <p class="text-color-gray text-xsmall">User can only accept job orders from selected territory</p>
                  <input type="hidden" name="coordinate" id="coordinates">
                  <input type="hidden" name="lat" id="lat">
                  <input type="hidden" name="lng" id="lng">
                </div>
                <div class="col-lg-3 text-right pt-4 pb-4">
                    <button type="button" class="btn btn-primary mr-3 text-xsmall" id="add_user" >ADD TERRITORY</button>
                </div>
            </div>
        </form>
    </div>
  </div>
</div>

@stop

@section('page-scripts')

<script src="{{asset('assets/lib/countup/countUp.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.pie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/curvedLines.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/chartjs/Chart.min.js')}}" type="text/javascript"></script>
<!-- <script src="{{asset('assets/lib/raphael/raphael-min.js')}}" type="text/javascript"></script> -->
<script src="{{asset('assets/lib/morrisjs/morris.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.js"></script>
<!-- 
<script>
    "use strict";
    /* Footable */
    $('.footable1').footable({
        "paging": {
            "enabled": true,
            "size": 7
        }
    }, function(ft){
        $('.checkbox-user-check').on('change',function(){                
            $(this).closest('.media').toggleClass("selected");
        });
        $('.selectallticket').on('change',function(){
            if( $(this).is(":checked") == true){
                $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
             }else{
                $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
             }
        });
    });
    
    Morris.Donut({
      element: 'pie-chart',
      data: [
        {label: "Converted ", value: {{$converted_leads}}},
        {label: "Unconverted ", value: {{$unconverted_leads}}},

      ]
    });         
</script> -->
<script type="text/javascript">     
        
    var poly;
    var map;
    var bermudaTriangle;
    var infoWindow;

    function initMap() {

        var map1 = new google.maps.Map(document.getElementById('map1'), {
            zoom:7,
            center: {lat: -25.2743988, lng: 133.7751312} ,
            mapTypeId: 'roadmap' 
        });

     
        poly = new google.maps.Polygon({
            drawingMode: google.maps.drawing.OverlayType.MARKER,
            strokeOpacity: 1.0,
            strokeWeight: 3,
            @if($show_user->type == "franchisee")
                editable:false,
            @else
                editable:true, 
            @endif
            strokeColor: '#5E65B8',
            path: {!! $coords !!},
        });
        
        @if($show_user->type == "franchisee"){
            poly.addListener('click', function (event) {
                if (marker && marker.setPosition) {
                       marker.setPosition(null);
                        marker = new google.maps.Marker({position: event.latLng,map:map1});
                } else {
                        marker = new google.maps.Marker({position: event.latLng,map:map1});
                }
                
                $('#lat').val(marker.position.lat());
                $('#lng').val(marker.position.lng());
                // infowindow.setContent("Hello, world.");
                // var anchor = new google.maps.MVCObject();
                // anchor.set("position", event.latLng);
                // infowindow.open(map, marker);
            });
            
        }
        @else{
            map1.addListener('click', addLatLng);
        }
        @endif
        poly.setMap(map1);
       

        var path = poly.getPath()
        var coordinates = [];

        for (var i = 0 ; i < path.length ; i++) {

            coordinates.push({
                lat: path.getAt(i).lat(),
                lng: path.getAt(i).lng()
            });

            @if ($show_user->type == "franchisee")
                @if($user_marker)
                    var marker = new google.maps.Marker({
                    position: {lat: {{$user_marker->latitude}}, lng: {{$user_marker->longitude}}},
                    map: map1,
                    title: 'marker title',
                    icon: {
                        
                        fillColor: '#00F',
                        fillOpacity: 0.6,
                        strokeColor: '#00A',
                        strokeOpacity: 0.9,
                        strokeWeight: 1,
                        scale: 7
                    }
                });
                @endif
            @else
                var marker = new google.maps.Marker({
                    position: {lat: path.getAt(i).lat(), lng: path.getAt(i).lng()},
                    map: map1,
                    title: 'marker title',
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        fillColor: '#00F',
                        fillOpacity: 0.6,
                        strokeColor: '#00A',
                        strokeOpacity: 0.9,
                        strokeWeight: 1,
                        scale: 7
                    }
                });
            @endif  
        }



        var map = new google.maps.Map(document.getElementById('map'), {  
            zoom:7,
            center: {lat: -25.2743988, lng: 133.7751312},
            mapTypeId: 'roadmap'
        });


        var bermudaTriangle = new google.maps.Polygon({
            path: {!! $coords !!},
            strokeColor: '#5E65B8',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#00F',
            fillOpacity: 0.35
        });
        bermudaTriangle.setMap(map);

        var path = bermudaTriangle.getPath()
        var coordinates = [];

        for (var i = 0 ; i < path.length ; i++) {

                coordinates.push({
                lat: path.getAt(i).lat(),
                lng: path.getAt(i).lng()
            });
            @if ($show_user->type == "franchisee")
                
                @if($user_marker)
                var marker = new google.maps.Marker({
                    position: {lat: {{$user_marker->latitude}}, lng: {{$user_marker->longitude}} },
                    map: map,
                    title: 'marker title',
                    icon: {
                    
                        fillColor: '#00F',
                        fillOpacity: 0.6,
                        strokeColor: '#00A',
                        strokeOpacity: 0.9,
                        strokeWeight: 1,
                        scale: 7
                    }
                });
                @endif
            @else
                var marker = new google.maps.Marker({
                    position: {lat: path.getAt(i).lat(), lng: path.getAt(i).lng()},
                    map: map,
                    title: 'marker title',
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        fillColor: '#00F',
                        fillOpacity: 0.6,
                        strokeColor: '#00A',
                        strokeOpacity: 0.9,
                        strokeWeight: 1,
                        scale: 7
                    }
                });
            @endif
          
        }
        if (coordinates === undefined || coordinates.length == 0) {
            
        }else{

            var bounds = new google.maps.LatLngBounds();
            bermudaTriangle.getPath().forEach(function (path, index) {
                bounds.extend(path);
            });
            map.fitBounds(bounds);
        }





    $('.map').on('shown.bs.modal', function () {
       
        if (coordinates === undefined || coordinates.length == 0) {
            
        }else{

            var bounds1 = new google.maps.LatLngBounds();
            poly.getPath().forEach(function (path, index) {
                bounds1.extend(path);
            });

            map1.fitBounds(bounds1);

            google.maps.event.trigger(map1, 'resize');
        }

    });

}

        var marker;
        
    function addMarker(event) {
        console.log(event.latLng)
        if (marker && marker.setPosition) {
            marker.setPosition(event.latLng);
        } else {
            marker = new google.maps.Marker({position: event.latLng,map:map1});
        } 
     }


    function addLatLng(event) {
      var path = poly.getPath();
      path.push(event.latLng);
    }

    $('#add_user').on('click',function(){
        var path = poly.getPath();
        var contentString ="";
        var coordinates = [];
        // Iterate over the vertices.
        for (var i =0; i < path.getLength(); i++) {
            var xy = path.getAt(i);
            var coordinate = path.getAt(i).lat() + '|' + path.getAt(i).lng()
            coordinates.push(coordinate)
            contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' +
                xy.lng();
        }

        console.log(coordinates)
        $('#coordinates').val(coordinates)
        $('#form_location').submit()
    });


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOzUJWdUgmPii5xcVAsz2-mBj3V2zHQPM&libraries=drawing&callback=initMap"
         async defer></script>
@stop