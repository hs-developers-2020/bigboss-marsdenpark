@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper" style="margin-top: -30px;">
    <div class="container">
        <form action="" id="search_filter" >
            <div class="row">
                 <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="text-color-gray mb-2">  
                                    <span class="ml-2 text-xxsmall font-semibold text-uppercase">From</span>
                                    <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                    <input type="text" class="form-control input-sm datepick" name="from" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$from}}" id="date_range_from">
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="text-color-gray mb-2">
                                    <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                    <span class="ml-2 text-xxsmall font-semibold text-uppercase">To</span>
                                    <input type="text" class="form-control input-sm datepick" name="to" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$to}}" id="date_range_to">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="text-color-gray mb-2">
                                    <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                    <span class="ml-2 text-xxsmall font-semibold text-uppercase">Type</span>
                                    {!!Form::select('type',$category_type,old('type',$type),['class' => "form-control text-xsmall input-sm select2", 'id' => "type"])!!}
                                </div>
                            </div>
                        </div>
                      
                        <div class="col-md-2">
                            <div class="btn-group pt-4" role="group" aria-label="Basic example" >
                                <a  class="w-100 btn bg-color-blue text-xxsmall text-color-white" id="search_button"><i class="mdi mdi-search"></i> FILTER </a>
                                <a  class="w-100 btn bg-color-blue text-xxsmall text-color-white" href="{{ route('system.finance.finance-history') }}"><i class="mdi mdi-search"></i> CLEAR </a>
                            </div>     
                        </div>
                    </div> 
                </div>      
                <div class="col-lg-3 mt-4">
                    <a href="{{ route('system.finance.print') }}?&from={{$from}}&to={{$to}}&type={{$type}}" class="btn bg-color-blue text-xxsmall text-color-white" style="float: right;"><i class="fas fa-file mr-2 text-color-white"></i>Generate Report (csv)</a>
                </div>
            </div>
        </form>
        <table class="table bg-color-white  mb-0 footable-finance">
            <thead>
                <tr>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Transaction #</th>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold spacing-1 text-left">Booking ID</th>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue font-semibold text-left" data-breakpoints="xs">Description</th>         
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue font-semibold text-left" data-breakpoints="xs sm">Type</th>                                                                   
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue font-semibold text-left" data-breakpoints="xs sm">Date</th>
                    <th  class="p-4 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue font-semibold text-left" data-breakpoints="">Amount</th>
                    <th  class="p-4 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-color-blue font-semibold text-left" data-breakpoints="">Added By</th>
                </tr>
            </thead>
            <tbody>
                @forelse($finance as $index => $value)
                <tr>
                    <td><h5 class="text-color-gray p-3 text-xsmall">{{str_pad($value->id, 6, "0", STR_PAD_LEFT)}}</h5></td>
                     <td><h5 class="text-color-gray p-3 text-xsmall">{{$value->booking_id ?: "N/A"}}</h5></td>
                    @if($value->type == "Income")
                    <td><h5 class="text-color-gray p-3 text-xsmall">{{Str::title($value->property_type)}} ({{Str::title($value->details)}})</h5></td>
                    @else
                     <td><h5 class="text-color-gray p-3 text-xsmall">{{Str::title($value->details)}}</h5></td>
                    @endif
                    <td><h5 class="text-color-gray p-3 text-xsmall">{{Str::title($value->type)}}</h5></td>
                    <td><h5 class="text-color-gray p-3 text-xsmall">{{Helper::date_only($value->created_at)}}</h5></td>
                    <td><h5 class="text-color-gray p-3 text-xsmall">${{Helper::amount($value->amount)}}</h5></td>
                    <td><h5 class="text-color-gray p-3 text-xsmall">{{Str::title($value->user ? $value->user->full_name:"N/A")}}</h5></td>
                </tr>
                @empty
                <tr>
                  <td> <h5>No Available Record</h5></td>
                </tr>
                @endforelse                        
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="7">
                        <div class="float-right">
                            {{ $finance->appends(['from' => $from, 'to' => $to,'type'=> $type ])->render() }}
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>                    
    </div>
</div>

@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/daterangepicker/css/daterangepicker.css')}}"/>
<style type="text/css">
    footer {
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}

</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/daterangepicker/js/daterangepicker.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    $('#date_range_from, #date_range_to').daterangepicker({
        singleDatePicker: true,
        autoApply: true,
        timePicker: false,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
});

$('#search_button').on('click', function(){
    console.log("akhjdhjd")
    $('#search_filter').get(0).submit();
});
</script>
@stop