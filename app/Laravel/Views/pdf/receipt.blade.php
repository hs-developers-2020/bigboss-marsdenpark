<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>

<table width="100%" cellpadding="3" cellspacing="0"  style="padding-top: 10px;">
	<TBODY>
		
	 <tr>
      <th colspan="20" style="font-weight: normal; padding-bottom: 20px; padding-left: 20px;">{{$payment->created_at->format("m-d-Y")}}</th>  
    </tr>

    <tr>
      <th colspan="20" style="font-weight: normal; padding-bottom: 20px; padding-left: 20px;">OR Number: {{$payment->receipt_number}}</th>  
    </tr>

    <tr>
      <th colspan="20" style="font-weight: normal; padding-left: 20px;">{{ $payment->reference ? $payment->reference->business_name : 'N/A' }}</th>  
    </tr>

    <tr>
      <th colspan="20" style="font-weight: normal; padding-left: 20px;">{{ $payment->reference ? $payment->reference->unit_floor_no : 'N/A' }} {{ $payment->reference ? $payment->reference->street : 'N/A' }} Barangay San Antonio, Pasig City</th>  
    </tr>

    {{-- <tr>
      <th colspan="20" style="font-weight: normal; padding-left: 50px; padding-top: 20px;">One Thousan Five Hundred Seventy</th>  
    </tr> --}}

    {{-- <tr>
      <th colspan="20" style="font-weight: normal;" align="center">{{ Helper::amount($payment->amount) }}</th>  
    </tr> --}}
    <tr>
      <th></th>
    </tr>
    <tr>
      <th></th>
    </tr>
    <tr>
      <th></th>
    </tr>
	  <tr>
      <th colspan="19" style="font-weight: normal;"align="right">Clearance Fee:</th>  
      <th style="font-weight: normal; padding-right: 350px;" align="right"><b>{{ $payment->reference->amount ? Helper::amount($payment->reference->amount) : '0.00' }}</b></th>
    </tr>
    <tr>
      <th colspan="19" style="font-weight: normal;"align="right">Documentary Stamp:</th>  
      <th style="font-weight: normal; padding-right: 350px;" align="right"><b>{{ $payment->reference->docs ? Helper::amount($payment->reference->docs) : '0.00' }}</b></th>
    </tr>
    <tr>
      <th colspan="19" style="font-weight: normal;"align="right">Business Plate:</th>  
      <th style="font-weight: normal; padding-right: 350px;" align="right"><b>{{ $payment->reference->business_plate ? Helper::amount($payment->reference->business_plate) : '0.00' }}</b></th>
    </tr>
    <tr>
      <th colspan="19" style="font-weight: normal;"align="right">Penalty:</th>  
      <th style="font-weight: normal; padding-right: 350px;" align="right"><b>{{ $payment->reference->penalty ? Helper::amount($payment->reference->penalty) : '0.00' }}</b></th>
    </tr>
    <tr>
      <th></th>
    </tr>
    <tr>
      <th colspan="19" style="font-weight: normal;"align="right">Total:</th>  
      <th style="font-weight: normal; padding-right: 350px;" align="right"><b>{{ $payment->amount }}</b></th>
    </tr>
	</TBODY>


</table>


</body>
</html>


<style type="text/css">
  
  h4{
  
  font-weight: normal;
  }
  
  h3{

    font-size: 20px;
  }

.code {
    text-decoration: underline;
    font-weight: bold;
  }

 .underline{
  border-bottom:1px solid #000000; 
  width:520px; 
 
}

.underline2{
  border-bottom:1px solid #000000; 
  width:200px; 
 
}

 .underline3{
  border-bottom:1px solid #000000; 
  width:550px; 
 
}

.date{
  border-bottom:1px solid #000000; 
  width:150px; 
 
}


 </style>

