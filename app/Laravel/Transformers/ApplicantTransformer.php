<?php 

namespace App\Laravel\Transformers;

use Input;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\Applicant;
use App\Laravel\Mode\ApplicantImage;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

use Str;

class ApplicantTransformer extends TransformerAbstract{

	protected $applicant,$auth;

	protected $availableIncludes = [
        'government_ids', 'licenses', 'land_title', 'store_photo', 'id_photo'
    ];

    public function __construct() {
    	$this->auth = Auth::user();
    }

	public function transform(Applicant $applicant) {
		$this->applicant = $applicant;
	    return [
	     	'id' => $applicant->id ?:0,
	     	'name' => $applicant->name,
	     	'firstname' => $applicant->firstname,
	     	'lastname' => $applicant->lastname,
	     	'contact' => $applicant->contact,
	     	'age' => $applicant->age,
	     	'gender' => $applicant->gender,
	     	'marital_status' => $applicant->marital_status,
	     	'number_of_children' => $applicant->number_of_children,
	     	'educational_attainment' => $applicant->educational_attainment,
	     	'ownership_of_house' => $applicant->ownership_of_house,
	     	'length_of_stay' => $applicant->length_of_stay,
	     	'has_id' => $applicant->has_id,
	     	'government_id' => $applicant->government_id,
	     	'has_bank_account' => $applicant->has_bank_account,
	     	'bank_name' => $applicant->bank_name,
	     	'has_outstanding_loan' => $applicant->has_outstanding_loan,
	     	'outstanding_loan' => $applicant->outstanding_loan,
	     	'weekly_payment' => $applicant->weekly_payment,
	     	'years_in_business' => $applicant->years_in_business,
	     	'store_size' => $applicant->store_size,
	     	'ownership_of_store' => $applicant->ownership_of_store,
	     	'average_monthly_sales' => $applicant->average_monthly_sales,
	     	'others_business' => $applicant->others_business,
	     	'others' => $applicant->others,
	     	'has_business_license' => $applicant->has_business_license,
	     	'business_license' => $applicant->business_license,
	     	'has_online_payment' => $applicant->has_online_payment,
	     	'online_payment_gateway' => $applicant->online_payment_gateway,
	     	'status' => $applicant->status
	     ];
	}

	public function includeGovernmentId(Applicant $applicant){
	   	if($applicant->government_ids->count()){
        	return $this->collection($applicant->government_id, new ApplicantImageTransformer);
	   	}else{
	   		$new_image = new ApplicantImage;
        	return $this->collection([$new_image], new ApplicantImageTransformer);
	   	}
	}

	public function includeLandTitle(Applicant $applicant){
	   	if($applicant->land_title->count()){
        	return $this->collection($applicant->land_title, new ApplicantImageTransformer);
	   	}else{
	   		$new_image = new ApplicantImage;
        	return $this->collection([$new_image], new ApplicantImageTransformer);
	   	}
	}

	public function includeLicenses(Applicant $applicant){
	   	if($applicant->licenses->count()){
        	return $this->collection($applicant->licenses, new ApplicantImageTransformer);
	   	}else{
	   		$new_image = new ApplicantImage;
        	return $this->collection([$new_image], new ApplicantImageTransformer);
	   	}
	}

	public function includeStorePhoto(Applicant $applicant){
	   	if($applicant->store_photo->count()){
        	return $this->collection($applicant->store_photo, new ApplicantImageTransformer);
	   	}else{
	   		$new_image = new ApplicantImage;
        	return $this->collection([$new_image], new ApplicantImageTransformer);
	   	}
	}

	public function includeIdPhoto(Applicant $applicant){
	   	if($applicant->id_photo->count()){
        	return $this->collection($applicant->id_photo, new ApplicantImageTransformer);
	   	}else{
	   		$new_image = new ApplicantImage;
        	return $this->collection([$new_image], new ApplicantImageTransformer);
	   	}
	}
}