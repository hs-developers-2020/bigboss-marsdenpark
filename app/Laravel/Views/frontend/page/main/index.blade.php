@extends('frontend._layout.main')

@section('content')
@include('frontend._components.topnav-main')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<div class="video-container" style="height: auto;">
  <video loop muted autoplay  width="1280" height="720" style="background-color: rgba(0,0,0,.5) !important;">
    <source src="{{ asset('frontend/Video/main4.mp4') }}" type="video/mp4" >
  </video>
  <div class="bg-masked"></div>
  <div class="container mobile-margin">
    <div class="row">
      <div class="col-lg-12 text-center pt-1">
        <h2 class="text-color-white text-uppercase text-header font-weight-bold text-headerspacing-2">Be Treated Like a Boss</h2>
        <p class="text-color-white spacing-1 mt-1 text-sub-header font-weight-bold">Select service type you need</p>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 mt-2 col-6 col-xs-3 col-main-button pointer">
        <a href="{{route('frontend.cleaning.index')}}" style="text-decoration: none;">
        <div class="container border-dash-white dash-size text-center  main-button--hover">
          <img src="{{ asset('frontend/button-icons/main/cleaning1.svg')}}" class="col-image-size">
          <div class="text-center pb-1">
          <p class="text-color-white mt-2 font-weight-bold text-size  spacing-1" style="padding: 0%;"> Cleaning</p>
          </div>
        </div>
      </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-6 col-xs-3 col-main-button mt-2 pointer">
         <a href="{{route('frontend.accounting.index')}}" style="text-decoration: none;">
        <div class="container border-dash-white dash-size text-center  main-button--hover">
          <img src="{{ asset('frontend/button-icons/main/accounting.svg')}}" class="col-image-size">
          <div class="text-center pb-1">
          <p class="text-color-white mt-2 text-size  spacing-1 font-weight-bold" style="padding: 0%;"> Accounting</p>
          </div>
        </div>
      </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-6 col-xs-3  col-main-button mt-2 pointer">
         <a href="{{route('frontend.forex.index')}}" style="text-decoration: none;">
        <div class="container border-dash-white dash-size text-center  main-button--hover">
          <img src="{{ asset('frontend/button-icons/main/forex.svg')}}" class="col-image-size">
          <div class="text-center pb-1">
          <p class="text-color-white mt-2 text-size  spacing-1 font-weight-bold" style="padding: 0%;"> ForEx</p>
          </div>
        </div>
      </a>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-6 col-xs-3 col-main-button mt-2 pointer">
         <a href="{{route('frontend.infotech.index')}}" style="text-decoration: none;">
        <div class="container border-dash-white dash-size text-center  main-button--hover">
          <img src="{{ asset('frontend/button-icons/main/itservice.png')}}" class="col-image-size">
          <div class="text-center pb-1">
          <p class="text-color-white mt-2 text-size  spacing-1 font-weight-bold" style="padding: 0%;"> InfoTech</p>
          </div>
        </div>
      </a>
      </div>
    </div>

{{-- @extends('frontend._components.footer-main')
 --}}
 </div>
</div>

@stop

@section('style-script')
<style type="text/css">
  footer {
    background-color: transparent !important;
    margin-top: 300px !important;
    position: fixed !important;
  }
  .position {
    overflow: hidden;
  position: fixed !important;
  top: 0 !important;
  width: 100%;
  }

  @media only screen and (max-width: 1000px) {
      .col-main-button {
          padding-left: 0px !important;
          padding-right: 0px !important;

      }
      .image-size1 {
        height: 100% !important; 
        width: 89% !important;
      }

      .dash-size {
        width: 90% !important;
      }
  }

  @media only screen and (min-width: 1000px) {
      .text-header {
          font-size: 40px !important;
      }
      .text-sub-header  {
        font-size: 20px !important;
      }
      .text-size {
        font-size: 25px !important;
      }

  }
   @media only screen and (max-width: 1000px) {
      .text-header {
          font-size: 27px !important;
      }
      .text-sub-header  {
        font-size: 15px !important;
        margin-top: 5px !important;
      }
      .text-size {
        font-size: 17px !important;
      }
  }

</style>
@stop