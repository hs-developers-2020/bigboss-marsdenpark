<?php 
namespace App\Laravel\Requests\System;

use Session,Auth,Input;
use App\Laravel\Requests\RequestManager;

class SendLoginLinkRequest extends RequestManager{
	protected $data = array();

		public function __construct(){
	        $this->data = [];
	    }

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'email' => 'required',
			'password' => 'required|confirmed',

		];

		if($id != 0){
			$rules['password'] = "confirmed";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'email.required'		=> "Please indicate your email address",
			'email.email'	=> "Invalid email address format.",
			'email.unique'  => "*This email is already used. Try another",
			'password.confirmed' => "*Password mismatched"
		];
	}

	public function response(array $errors)
	{
		if ($this->ajax() || $this->wantsJson())
		{
			return new JsonResponse($errors, 422);
		}
		
		$trash = 'Page Moved To Trash';
		Session::flash('notification-status','failed');
		Session::flash('notification-msg','Opps! Some fields are not accepted.');
		// dd($this->getRedirectUrl());


		return $this->redirector->to($this->getRedirectUrl()."#send_login_link")
		->withInput($this->except($this->dontFlash))
		->withErrors($errors, $this->errorBag);
	}
}