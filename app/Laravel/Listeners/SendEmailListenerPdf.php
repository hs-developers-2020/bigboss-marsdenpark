<?php 
namespace App\Laravel\Listeners;

use App\Laravel\Events\SendEmailPdf;

class SendEmailListenerPdf{

	public function handle(SendEmailPdf $email){
		$email->job();

	}
}