<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>

  <style type="text/css">

    *{
      
      font-family: 'Montserrat', sans-serif;

    }
    h4{

      font-weight: normal;

    }

    h3{
      text-transform: uppercase;
      font-size: 20px;
    }

    .code {
      text-decoration: underline;
      font-weight: bold;
    }

    .underline{
      border-bottom:1px solid #000000; 
      width:520px; 

    }

    .underline2{
      border-bottom:1px solid #000000; 
      width:200px; 

    }

    .underline3{
      border-bottom:1px solid #000000; 
      width:550px; 

    }

    .date{
      border-bottom:1px solid #000000; 
      width:150px; 

    }
    .border-color td{
      border:1px solid #ADD0FB;
    }
    .color{
      background-color: #2047A4;
      color: #fff;
    }
    .border-color{
      border-color: #F2F9FA;
    }
    .color2{
      background-color: #ADD0FB;
    }
    .color3{
      background-color: #F2F9FA;
    }
    th {
      color: #323639;
    }
    p {
      color: #323639;
      line-height: 2px;
    }
  </style>

</head>
<body style="margin-left: 25px; margin-right: 25px">
  <img src="{{'frontend/images/main-logo.png'}}" style="position:absolute;top:3%;width:170px; height:0px;float: right;" >

 
    <p style="color: #2047A4;font-weight: bold;">{{Str::title($invoice->business_info->business_name)}}</p>
    <p style="font-size: 14px;">{{Str::title($invoice->franchisee_info->street)}}</p>
    <p style="font-size: 13px;">{{$invoice->franchisee_info->invoice_address}}</p>
    <p style="font-size: 13px;">M:{{$invoice->user->contact_number}}</p>
    <p style="font-size: 13px;">E:{{$invoice->user->email}}</p>
    <p style="font-size: 13px;padding-top: 10px;">ABN:{{$invoice->business_info->business_number}}</p> 
    <h2 style="padding-top: -45px; text-align: center;">Tax Invoice</h2>
    
   
 
   
    <table border="0" style="font-size: 12px;padding-top: -10px;" width="75%" cellpadding="0" cellspacing="0">
      <tr>
        <td align="center" class="color3">INVOICE TO</td>
        <td width="20%" style="text-align: right;" class="color">INVOICE NO</td>
        <td width="20%" style="text-align: right;" class="color2">{{$invoice->invoice_number}}</td>
      </tr>
      <tr>
        <td rowspan="3">{{$invoice->booking_info->location}}</td>
        <td style="text-align: right;" class="color">DATE</td>
        <td style="text-align: right;">{{Helper::date_only($invoice->created_at)}}</td>
      </tr>
      <tr>
        <td style="text-align: right;" class="color">TOTAL DUE</td>
        <td style="text-align: right;" class="color2">$ {{Helper::amount($invoice->amount)}}</td>
      </tr>
      <tr>
        <td style="text-align: right;" class="color">DUE DATE</td>
        <td style="text-align: right;">{{Helper::date_only($invoice->created_at->addDays(7))}}</td>
      </tr>
    </table><br>

<div  style="border: 2px solid #ADD0FB;height: 500px;" >
  <table  width="100%"  cellpadding="3" cellspacing="0" class="border-color">
    
    <tr>
      <td style="text-align: center;width:55%">Description</td>
      <td style="text-align: center;width:15%">QTY</td>
      <td style="text-align: center;width:15%">RATE</td>
      <td style="text-align: center;width:15%">AMOUNT</td>
    </tr>
    <tr>
      <td>{{$booking_data->service_type}}</td>
      <td></td>
      <td></td>
      <td>$ {{Helper::amount($booking_data->service_type_amount)}}</td>
    </tr>
    <tr>
      <td>{{Helper::get_subservice_name($booking_data->number_of_rooms)}}</td>
      <td></td>
      <td></td>
      <td>$ {{Helper::amount(Helper::get_subservice_price($booking_data->number_of_rooms))}}</td>
    </tr>
    @forelse($others as $index => $value)
    <tr>
      <td>{{$value->services}}</td>
      <td></td>
      <td></td>
      <td>$ {{Helper::amount($value->price)}}</td>
    </tr>
    @empty
    <tr>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    @endforelse
    @if($services)
      @forelse($services as $value)
      <tr>
        <td>{{$value->name}}</td>
        <td></td>
        <td></td>
        <td>$ {{Helper::amount($value->price)}}</td>
      </tr>
      @empty
      @endforelse
    @endif                     
    
  </table>
</div><br>
 
<div style="border: 1px solid black;height: 100px;width:300px;">
  <p style="font-size: 12px;text-align: center;">Please pay by Direct Debit to</p>
   <table cellpadding="3" cellspacing="0" border="0" align="center" style="font-size: 12px;" width="100%">
    <tr>
      <td rowspan="3"></td>
      <td>Account Name</td>
      <td>: Big Boss Group</td>
    </tr>
    <tr>
      <td>BSB</td>
      <td>: 012 464</td>
    </tr>
     <tr>
      <td>Account No</td>
      <td>: 420633015</td>
    </tr>
  </table>
</div> 

<table cellpadding="3" cellspacing="0" border="0" align="right" style="font-size: 12px;padding-top: -90px;" width="40%">
    <tr>
      <td class="color">GST TOTAL</td>
      <td class="color2">$ {{Helper::amount($gst)}}</td>
    </tr>
    <tr>
      <td class="color">TOTAL</td>
      <td > $ {{Helper::amount($gst_total)}}</td>
    </tr>
     <tr>
      <td class="color">BALANCE DUE</td>
      <td class="color2">420633015</td>
    </tr>
  </table>

</body>
</html>


