<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notes extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "call_out_notes";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    public $timestamps = true;

    public function author(){
        return $this->belongsTo('App\Laravel\Models\User', 'user_id', 'id');
    }
}

