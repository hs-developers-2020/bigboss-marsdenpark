<?php

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Service;
use App\Laravel\Models\Regions;
use App\Laravel\Models\ExtraService;
use App\Laravel\Models\Addons;


/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\ServiceRequest;
use App\Laravel\Requests\System\ExtraServiceRequest;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, Auth, ImageUploader,Input,PDF,FileUploader,File,DB;

class SubServiceController extends Controller
{
    protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"Choose Service",'cleaning' => "Cleaning","information_technology" => "Information Technology","accounting" => "Accounting"];
		$this->data['service_type'] = [ ''=>"Choose Type",'home' => "House","office" => "Office"];
		$this->data['has_addons'] = [ ''=>"Choose Type",'yes' => "yes","no" => "No"];
		$this->data['reg'] =  Regions::pluck('name', 'code')->toArray();
		
	}

	public function index(PageRequest $request,$id = NULL){
  		$this->data['group'] = $request->get('group',false);
  		$this->data['main_service'] = Service::find($id);
  		$this->data['services'] = ExtraService::where('service_id',$id)->orderBy('created_at' , "DESC")->paginate(10);
  		$this->data['service_id'] = $id;
  		
		return view ('system.sub-service.index',$this->data);
	}
  
	 public function create($service_id = NULL){
	 	$this->data['main_id'] = $service_id;
	 	$this->data['service_info'] = Service::find($service_id);
		return view ('system.sub-service.create',$this->data);

	}



	public function store(ExtraServiceRequest $request,$service_id = NULL){
		try {

			$new_subservice = new ExtraService;
			$new_subservice->fill($request->all());
			$new_subservice->service_id = $service_id;
			
			$main_service = Service::find($service_id);
			$new_subservice->regions = implode(', ', $request->get('region'));
			$temp_id = time();
			if ($main_service->group == "information_technology") {
				if ($request->get('has_addon')=="yes") {
					foreach ($request->get('addons') as $key => $value) {

						$new_addons = new Addons;
						$new_addons->name = $request->get('addons')[$key];
						$new_addons->price = $request->get('addons_price')[$key];
						
						$new_addons->subservice_id = $temp_id;
						$new_addons->save();
					}
				}
				
			}

			

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/user_profile");
			    $new_subservice->path = $image['path'];
			    $new_subservice->directory = $image['directory'];
			    $new_subservice->filename = $image['filename'];
			}

			if($new_subservice->save()) {
				Addons::where('subservice_id',$temp_id)->update(['subservice_id' => $new_subservice->id]);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.sub_service.index',$service_id);
			}
			session()->flash('notification-status','failed');
			session()->flaPcsh('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}

	}

	public function edit ($id = NULL, $service_id = NULL) {
		$this->data['main_id'] = $service_id;
		$this->data['service_info'] = Service::find($service_id);
		$sub_service = ExtraService::find($id);

		$this->data['addon'] =Addons::where('subservice_id' , $id)->get();
		$this->data['price_addon'] =explode(',', $sub_service->addons_price);

		if (!$sub_service) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.sub-service.edit');
		}

		$this->data['sub_service'] = $sub_service;
		return view('system.sub-service.edit',$this->data);
	}
	public function update (ExtraServiceRequest $request, $id = NULL,$service_id = NULL) {
			//dd($request->all());
		try {
			$update_service = ExtraService::find($id);
			$update_service->add_ons()->delete();
			$temp_id = time();

			if ($request->get('has_addon')=="yes") {
				foreach ($request->get('addons') as $key => $value) {

					$new_addons = new Addons;
					$new_addons->name = $request->get('addons')[$key];
					$new_addons->price = $request->get('addons_price')[$key];
					
					$new_addons->subservice_id = $temp_id;
					$new_addons->save();
				}
			}


			$update_service->regions = implode(', ', $request->get('region'));
			
			

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/user_profile");
			    $update_service->path = $image['path'];
			    $update_service->directory = $image['directory'];
			    $update_service->filename = $image['filename'];
			}
			if (!$update_service) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.sub_service.index',$service_id);
			}

			$update_service->fill($request->all());

			if($update_service->save()) {
				Addons::where('subservice_id',$temp_id)->update(['subservice_id' => $update_service->id]);
				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.sub_service.index',$service_id);
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


	public function update_day (PageRequest $request, $id = NULL,$service_id = NULL) {
		try {
			
			
			$services = ExtraService::find($id);

			if (!$services) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.services.index');
			}

			//dd($request->input('regions'));

			
			if($request->get('days_available')){
			$services->days_available = implode(',', $request->get('days_available'));
			}

			if($services->save()) {
				if($request->get('days_available')){
					session()->flash('notification-status','success');
					Session()->flash('alert-type','success');
					session()->flash('notification-msg',"Record has been modified successfully.");
				}else{
					Session()->flash('alert-type','failed');
					session()->flash('notification-msg',"Something went wrong.");
				}
				return redirect()->route('system.sub_service.index',$service_id);
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL, $service_id = NULL) {
		try {
			$extra_service = ExtraService::find($id);

			if (!$extra_service) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.sub_service.index');
			}

			if($extra_service->delete()) {
				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.sub_service.index',$service_id);
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
