<?php

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\UserImage;
use App\Laravel\Models\UserInfo;
use App\Laravel\Models\CleaningService;
use App\Laravel\Models\UserTerritory;
use App\Laravel\Models\Regions;
use App\Laravel\Models\WorkAvailability;

use App\Laravel\Models\CustomerFeedback;
use App\Laravel\Models\BusinessGroup;

use App\Laravel\Events\SendEmailLink;
/**
*
* Requests used for validating inputs
*/
use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Requests\System\UserRequest;
use App\Laravel\Requests\System\SendLoginLinkRequest;
use App\Laravel\Requests\System\WorkAvailabilityRequest;
use App\Laravel\Requests\System\ProfileRequest;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, Auth, ImageUploader,Input,PDF,FileUploader,File,DB,Event,Crypt,AuditRequest,Carbon\CarbonPeriod;

class UsersController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"All Services",'cleaning' => "Cleaning","information_technology" => "Information Technology"];
		$this->data['users_type'] = [ ''=>"Choose User Type",'head' => "Big Boss Head",'area_head' => "Big Boss Area Head",'franchisee' => "Big Boss Franchisee"];
		$this->data['reg'] = ['' => "Choose Regions"] + Regions::pluck('name', 'name')->toArray();
		$this->data['types'] = [''=>'Select Type' , 'home' => "Home" ,'office'=>"Office"];
		$this->data['work_code'] = ['all_areas' => "A-All Areas","zero_working" => "0-Working. Will take message but no new leads","territory" => "T-Territory" , "local" => "L-Local" , "holidays" => "H-Holidays" ];
		$this->data['business_name'] = ['' => "Choose Big Boss Head"] + User::select("id", DB::raw("CONCAT(id,'-',firstname,' ',lastname) AS user_name"))->where('type','head')->pluck('user_name','id')->toArray();
		$this->data['business_area_head'] = ['' => "Choose Big Boss Area Head"] + User::select("id", DB::raw("CONCAT(id,'-',firstname,' ',lastname) AS user_name"))->where('type','area_head')->pluck('user_name','id')->toArray();
		
	}

  	public function index(PageRequest $request){
  		$this->data['user_type'] = $request->get('user_type',false);
  		$this->data['keyword'] = $request->get('keyword',NULL);
  	
  		if (in_array(Str::lower(Auth::user()->type), ['super_user'])) {
  			$this->data['users_type'] = [ ''=>"Choose User Type",'head' => "Big Boss Head",'area_head' => "Big Boss Area Head",'franchisee' => "Big Boss Franchisee"];
  			$this->data['users'] = User::keyword($this->data['keyword'])->whereNotIn('type', ['user','super_user'])->type($this->data['user_type'])->orderBy('created_at' , "DESC")->paginate(10);
  		}
  		else{
  			$this->data['users_type'] = [ ''=>"Choose User Type",'area_head' => "Big Boss Area Head",'franchisee' => "Big Boss Franchisee"];
  			$head= User::where('head_id' , Auth::user()->id)->type($this->data['user_type'])->pluck('id');
  			$this->data['users'] = User::whereIn('head_id', $head)->orWhereIn('id', $head)->paginate(10);
  		}
		return view ('system.users.index',$this->data);
	}

	public function create(){

		return view ('system.users.create',$this->data);
	}



	public function store (UserRequest $request) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$new_user = new User;
			$new_user->fill($request->except('password'));

			if(Str::lower(Auth::user()->type) == 'head'){
				$new_user->type = 'area_head';
				$new_user->head_id = Auth::user()->id;	
			}elseif (Str::lower(Auth::user()->type) == 'area_head') {
				$new_user->type = 'franchisee';
				$new_user->head_id = Auth::user()->id;	
			}else{
				$new_user->type = $request->get('user_type');
			}

			if ($request->get('user_type') == 'area_head') {
				$new_user->head_id = $request->get('user_head');	
			}

			if ($request->get('user_type') == 'franchisee') {
				$new_user->head_id = $request->get('franchisee_head');	
			}
			$temp_id = time();

			if($request->hasFile('police_check')){
				//dd("dsads");
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){ 
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){ //gif,bmp
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="police_check";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('awareness_certificate')){
				//dd("dsads");
				foreach ($request->awareness_certificate as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="awareness_certificate";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('first_aid_license')){
				//dd("dsads");
				foreach ($request->first_aid_license as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="first_aid_license";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('whitecard')){
				//
				foreach ($request->whitecard as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="whitecard";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('insurance')){
				//dd("dsads");
				foreach ($request->insurance as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="insurance";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('other_documents')){
				//dd("dsads");
				foreach ($request->other_documents as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="other_documents";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('utility_vehicle')){
				//dd("dsads");
				foreach ($request->utility_vehicle as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="utility_vehicle";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('staff_uniform')){
				//dd("dsads");
				foreach ($request->staff_uniform as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="staff_uniform";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}
			if($request->hasFile('business_avatar')){
				//dd("dsads");
				foreach ($request->business_avatar as $key => $image) {

					$ext = $image->getClientOriginalExtension();
				if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
					$type = 'file';
					$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
				} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
					$type = 'image';
					$upload_image = ImageUploader::upload($image, "uploads/images/users/{$new_user->id}/users");
				}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="business_avatar";
					$new_image->user_id = $temp_id;
					$new_image->save();

					$new_business = new BusinessGroup;
					$new_business->file_label ="business_avatar";
					$new_business->path = $upload_image['path'];
					$new_business->directory = $upload_image['directory'];
					$new_business->filename = $upload_image['filename'];

					$new_business->user_id = $temp_id;
					$new_business->save();
					
					
				}
			}
			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/user_profile");
			    $new_user->path = $image['path'];
			    $new_user->directory = $image['directory'];
			    $new_user->filename = $image['filename'];
			}
			//dd($request->all());

			if($new_user->save()) {

				$position = str_replace("_", " ", $new_user->type);
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "USER_CREATED", 'remarks' => Auth::user()->full_name." has successfully created {$new_user->full_name} information. [{$position}]",'ip' => $ip , 'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);
				UserImage::where('user_id',$temp_id)->update(['user_id' => $new_user->id]);
				$new_info = new UserInfo;
				$new_info->fill($request->all());
				$new_info->head_id = $request->get('user_head') ?: $request->get('franchisee_head');
				$new_info->user_id = $new_user->id;
				$new_info->save();

				$insert[] = [
			                'email' => $new_user->email,
			                'firstname' => $new_user->firstname,
			                'lastname' => $new_user->lastname,
			                'name' => $new_user->firstname . " " . $new_user->lastname
			                ];	
				$notification_data = new SendEmailLink($insert);
			    Event::fire('send-email-link', $notification_data);

				
				if(!$request->hasFile('business_avatar')){
					if ($new_user->type == "area_head") {
						$new_business = new BusinessGroup;
						$new_business->user_id = $new_user->id;
						$new_business->save();
						}
					
				}

				if ($new_user->type == "area_head") {
					BusinessGroup::where('user_id',$new_user->id)->update(['business_name' => $new_info->business_name,'group' => $new_info->business_group,'business_address' => $new_info->full_address]);
				}
				session()->flash('notification-status','success');
				Session::flash('alert-type','success');
				session()->flash('notification-msg',"New account has been added.");
				return redirect()->route('system.users.index');
			}
			session()->flash('notification-status','failed');
			Session::flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
	
		$edit_user = User::where('type', '<>', 'user')->find($id);
		//$edit_head = User::where('type', '<>', 'user')->where('id' , $edit_user->head_id)->first();

		
	
	

		if (!$edit_user) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.users.index');
		}
		// dd($edit_user->load('police_check'));
		$this->data['edit_user'] = $edit_user;
		//$this->data['edit_head'] = $edit_head;
	
		return view('system.users.edit',$this->data);
	}

	public function update (UserRequest $request, $id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$update_user = User::where('type', '<>', 'user')->find($id);

			if (!$update_user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.users.index');
			}

			if ($update_user->type == "area_head") {
				$update_user->head_id = $request->get('user_head');
			}
			if ($update_user->type == "franchisee") {
				$update_user->head_id = $request->get('franchisee_head');
			}
		
		
			$temp_id = time();

			$update_user->fill($request->all());

			$user_info = UserInfo::where('user_id', $update_user->id)->first();

			if($request->hasFile('police_check')){
				//dd("dsads");
				$update_user->police_check()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="police_check";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('awareness_certificate')){
				//dd("dsads");
				$update_user->awareness_certificate()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="awareness_certificate";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('insurance')){
				//dd("dsads");
				$update_user->insurance()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="insurance";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('first_aid_license')){
				//dd("dsads");
				$update_user->first_aid_license()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="first_aid_license";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('whitecard')){
				//dd("dsads");
				$update_user->whitecard()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="whitecard";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('other_documents')){
				//dd("dsads");
				$update_user->other_documents()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="other_documents";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('staff_uniform')){
				//dd("dsads");
				$update_user->staff_uniform()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="staff_uniform";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('utility_vehicle')){
				//dd("dsads");
				$update_user->utility_vehicle()->delete();
				foreach ($request->police_check as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="utility_vehicle";
					
					$new_image->user_id = $temp_id;
					$new_image->save();
				}
			}

			if($request->hasFile('business_avatar')){
				//dd("dsads");
				$update_user->business_avatar()->delete();
				foreach ($request->business_avatar as $key => $image) {
					$ext = $image->getClientOriginalExtension();
					if($ext == 'pdf' || $ext == 'docx' || $ext == 'doc'){
						$type = 'file';
						$upload_image = FileUploader::upload($image, 'storage/documents/users/{$new_user->id}');
					} elseif($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png'){
						$type = 'image';
						$upload_image = ImageUploader::upload($image, "uploads/images/users/{$update_user->id}/users");
					}
			
					$new_image = new UserImage;
					$new_image->path = $upload_image['path'];
					$new_image->directory = $upload_image['directory'];
					$new_image->filename = $upload_image['filename'];
					$new_image->file_type = $type;
					$new_image->type ="business_avatar";
					
					$new_image->user_id = $temp_id;
					$new_image->save();


				}
					$business_group = BusinessGroup::where('user_id', $id)->first();
					$business_group->file_label ="business_avatar";
					$business_group->path = $upload_image['path'];
					$business_group->directory = $upload_image['directory'];
					$business_group->filename = $upload_image['filename'];
					$business_group->save();
			}

			if($request->hasFile('file')){
				$upload = ImageUploader::upload($request->file('file'), "uploads/user_profile");
				if($upload){	
					if (File::exists("{$update_user->directory}/{$update_user->filename}")){
						File::delete("{$update_user->directory}/{$update_user->filename}");
					}
					if (File::exists("{$update_user->directory}/resized/{$update_user->filename}")){
						File::delete("{$update_user->directory}/resized/{$update_user->filename}");
					}
					if (File::exists("{$update_user->directory}/thumbnails/{$update_user->filename}")){
						File::delete("{$update_user->directory}/thumbnails/{$update_user->filename}");
					}
				}
				
				$update_user->path = $upload["path"];
				$update_user->directory = $upload["directory"];
				$update_user->filename = $upload["filename"];
			}


			

			if($update_user->save()) {
				$position = str_replace("_", " ", $update_user->type);
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "USER_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$update_user->full_name} information. [{$position}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				UserImage::where('user_id',$temp_id)->update(['user_id' => $update_user->id]);

				if($user_info) {
					$user_info->fill($request->all());
					$user_info->head_id = $request->get('user_head');
					$user_info->save();
				}

				session()->flash('notification-status','success');
				Session::flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.users.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}


	public function show($id = NULL){

		$rating = 0;

		$this->data['show_user'] = User::where('type', '<>', 'user')->find($id);
	
		$this->data['show'] = User::where('type', '<>', 'user')->where('head_id',$id)->paginate(5);

		if($this->data['show_user']->type == "franchisee"){
			$coordinates = [];
			foreach ($this->data['show_user']->territory as $key => $coordinate) {
				array_push($coordinates, ['lat' => (float)$coordinate->latitude, 'lng' => (float)$coordinate->longitude]);
			}
		}else{
			$coordinates = [];
			foreach ($this->data['show_user']->coordinates as $key => $coordinate) {
				array_push($coordinates, ['lat' => (float)$coordinate->latitude, 'lng' => (float)$coordinate->longitude]);
			}
		}

		$this->data['all_jobs'] = CleaningService::where('franchisee_id',$id)->orderBy("created_at", "DESC")->get();
		$this->data['user_rate'] = CustomerFeedback::where('franchisee_id', $id)->count();
		$this->data['rate'] = CustomerFeedback::where('franchisee_id', $id)->sum('rate');

		if ($this->data['user_rate']) {
			$rating = $this->data['rate'] / $this->data['user_rate'] ;
		}
		$startDate = Carbon::now()->startOfWeek();
      	$endDate = Carbon::now()->endOfWeek();
		$this->data['converted_leads'] = CleaningService::whereBetween('updated_at' , [$startDate ,$endDate])->where('franchisee_id',$id)->where("status" , "done")->count();
		$this->data['unconverted_leads'] = CleaningService::whereBetween('updated_at' , [$startDate ,$endDate])->where('franchisee_id',$id)->where("status" , "pending")->count();

		$this->data['coords'] = json_encode($coordinates);
		$this->data['rating'] = $rating;
		$this->data['user_marker'] = UserTerritory::where('user_id', $id)->where('latitude' ,'<>', NULL)->first();
		$this->data['startDate'] = $startDate;
		$this->data['endDate'] = $endDate;
		$this->data['availability'] = WorkAvailability::where('user_id',$id)->whereBetween('date',[$startDate ,$endDate])->get();

		return view ('system.users.show',$this->data);
	}

	public function update_location(PageRequest $request, $id = NULL){
		$user = User::where('type', '<>', 'user')->find($id);
		

		if(null !== ($request->get('coordinate')) AND $user->type != "franchisee"){

			$user->coordinates()->delete();
			$coordinates = explode(',', $request->get('coordinate'));
			
			foreach ($coordinates as $key => $coordinate) {
				$lat = explode('|', $coordinate)[0];
				$lng = explode('|', $coordinate)[1];

				$new_territory = new UserTerritory;
				$new_territory->user_id = $id;
				$new_territory->latitude = $lat;
				$new_territory->longitude = $lng;
				$new_territory->save();
			}
		} else {
			$new_territory = UserTerritory::where('user_id', $id)->first();

			if(!$new_territory) {
				$new_territory = new UserTerritory;
			}

			$new_territory->user_id = $id;
			$new_territory->latitude = $request->get('lat');
			$new_territory->longitude = $request->get('lng');
			$new_territory->save();
		}
		
		return redirect()->route('system.users.user-profile', [$id]);
	}


	public function showWorkload($id = NULL){

	

		$this->data['first_date'] = Carbon::now()->format("d");
		$this->data['second_date'] =  Carbon::now()->endOfWeek()->addDays(7)->format("d");


		$startDate = Carbon::now();
		$startDate2 = Carbon::now()->subdays(1);
  		$endDate = Carbon::now()->endOfWeek()->addDays(7);
  		$this->data['availability'] = WorkAvailability::where('user_id',$id)->whereBetween('date',[$startDate2 ,$endDate])->get();

  		if ($this->data['availability']) {
  			$this->data['type'] = WorkAvailability::where('user_id',$id)->orderBy('date', "DESC")->first();
  		}
  		

  		$this->data['startDate'] = $startDate;
  		$this->data['endDate'] = $endDate;

		return view ('system.users.workload.index',$this->data);
	}

	public function update_workload(WorkAvailabilityRequest $request,$id= null){
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		$startDate = Carbon::now()->startOfWeek();
  		$endDate = Carbon::now()->endOfWeek()->addDays(7);
  		$period = CarbonPeriod::create($startDate, $endDate);
  		$first_date = Carbon::now()->startOfWeek();
  		$date_value = $request->get('date');
  		$this->data['startDate'] = $startDate;
  		$this->data['endDate'] = $endDate;

  		$flag = false;

  		$franchisee = User::find($id);
  		$exists = WorkAvailability::where('user_id',$id)->whereBetween('date',[$startDate,$endDate])->get();
  		//dd($request->all());
  			foreach ($request->get('date') as $key => $value) {
  				foreach ($exists as $index => $value_exists) {
	  				if ($value_exists->date == Carbon::parse($value)->format("Y-m-d")) {
	  					if ($value_exists->max_leads != $request->get('max_leads')[$key] || 
	  						$value_exists->work_stated != $request->get('work_stated')[$key] || 
	  						$value_exists->time_in != $request->get('time_in')[$key] || 
	  						$value_exists->time_out != $request->get('time_out')[$key] || 
	  						$value_exists->work_type != $request->get('work_type')) {
	  						$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "WORK_AVAILABILITY_UPDATE", 'remarks' => Auth::user()->full_name." has successfully updated work availability {$franchisee->full_name} information - date ({$request->get('date')[$key]})",'ip' => $ip,'franchisee_id' => $id]);
							Event::fire('log-activity', $log_data);
	  					}
	  					$value_exists->date = $request->get('date')[$key];
						$value_exists->work_stated = $request->get('work_stated')[$key];
						$value_exists->max_leads = $request->get('max_leads')[$key];
						$value_exists->time_in = $request->get('time_in')[$key];
						$value_exists->time_out = $request->get('time_out')[$key];
						$value_exists->user_id = $id;
						$value_exists->work_type = $request->get('work_type');
						$value_exists->save();
						
	  				}
  				}
  				if ($exists->isEmpty()) {
  					$flag = true;
  					$new_work_availability = new WorkAvailability;
					$new_work_availability->date = $request->get('date')[$key];
					$new_work_availability->work_stated = $request->get('work_stated')[$key];
					$new_work_availability->max_leads = $request->get('max_leads')[$key];
					$new_work_availability->time_in = $request->get('time_in')[$key];
					$new_work_availability->time_out = $request->get('time_out')[$key];
					$new_work_availability->user_id = $id;
					$new_work_availability->work_type = $request->get('work_type');
					$new_work_availability->save();
					
  				}
			}

			if($flag){
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "WORK_AVAILABILITY_UPDATE", 'remarks' => Auth::user()->full_name." has successfully updated work availability {$franchisee->full_name} information",'ip' => $ip,'franchisee_id' => $id]);
						Event::fire('log-activity', $log_data);
			}
		

		return redirect()->route('system.users.user-profile', [$id]);

	}

	public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$delete_user = User::where('type', '<>', 'user')->find($id);

			$delete_business = BusinessGroup::where('user_id' , $id);
			if (!$delete_user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.users.index');
			}

			if($delete_user->delete() and $delete_business->delete()) {
				$position = str_replace("_", " ", $delete_user->type);
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "USER_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$delete_user->full_name} information. [{$position}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session::flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.users.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function profile ($id = NULL){
		$this->data['user_profile'] = User::find($id);
		return view ('system.users.profile',$this->data);
	}
	
	
	public function update_profile(ProfileRequest $request,$id = NULL) {

		try {
			$user = User::find($id);

			if (!$user) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.user.index');
			}

			$user->fill($request->except('password'));
			$user->username = $request->get('username');
			
			if($request->has('password')){
				$user->password = bcrypt($request->get('password'));
			}

			if($user->save()) {
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"Account has been modified successfully.");
				return redirect()->route('system.dashboard');
			}

			session()->flash('alert-type','success');
			session()->flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function send_login_link($email = NULL){

        try {
            $this->data['page_title'] = "";
            $_email = Crypt::decrypt($email);
            $this->data['user'] = User::where('email',$_email)->first();

            if($this->data['user']->email){
                return view('system.users.send-login-link',$this->data);
            }else{
                session()->flash('notification-status', 'failed');
                session()->flash('notification-msg', 'Email no longer exist!');
                return redirect()->route('system.login');

            }
            
        } catch (DecryptException $e) {}
        
    }

    public function send_login_link_confirm($redirect_uri = NULL,SendLoginLinkRequest $request){

       try {
            $user= User::where('email',$request->email)->first();
            $user->password = bcrypt($request->get('password'));
            $user->save();

             

               return redirect()->route('system.login');             

        } catch (DecryptException $e) {}
        
    }
	
}
