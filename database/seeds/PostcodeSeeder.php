<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\Postcode;
class PostcodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/json/au_postcodes.json";
    	$code= json_decode(file_get_contents($path), true);

    	for ($i=0; $i < count($code) ; $i++) { 
    		
    		$data = array(
    						"postcode" => $code[$i]['postcode'],
    						"place_name" => $code[$i]['place_name'],
                            "state_name" => $code[$i]['state_name'],
                            "state_code" => $code[$i]['state_code'],
                            "latitude" => $code[$i]['latitude'],
                            "longitude" => $code[$i]['longitude'],
                            "accuracy" => $code[$i]['accuracy'],
    						"created_at" => date('Y-m-d'),
    						);
            Postcode::insert($data);
    	}

    	

    }
}
