<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableUserAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function($table){
            
            $table->string('abn')->nullable();
            $table->string('entity_name')->nullable();
            $table->string('company_name')->nullable();
            $table->string('business_type')->nullable();
            $table->string('job_title')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         
        Schema::table('user', function($table){
            $table->dropColumn(['abn','entity_name','company_name','business_type','job_title']);
        });
    }
}
