<?php 
namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use Mail,Str,Helper,Carbon;

class SendEmailPdf extends Event {


	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->data = $form_data;
		// $this->email = $form_data['insert'];

		

	}

	public function job(){	
		
		
		foreach($this->data as $index =>$value){
			$mailname = "Institutional Subscription";
			$emails = ['sprtn40@gmail.com'];
			$user_email = $value['email'];
			$this->data['new_email'] = $user_email;
			$this->data['invoice'] = $value['id'];
			
			Mail::send('emails.send-pdf', $this->data, function($message) use ($mailname,$emails,$user_email){
				$message->from('no_reply@highlysucceed.com');
				$message->to($user_email);
				$message->subject("Invoice PDF");
			});
		}


		
		
		
	}
}
