<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBusinessGroupFileAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('business_group_file', function($table){
            $table->date('date_issue')->nullable()->after('file_type');
            $table->date('date_expiry')->nullable()->after('date_issue');
            $table->text('reference')->nullable()->after('date_expiry');
            $table->text('is_high_risk')->nullable()->after('reference');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_group_file', function($table){
            $table->dropColumn(['date_issue','date_expiry','reference','is_high_risk']);
        });
    }
}
