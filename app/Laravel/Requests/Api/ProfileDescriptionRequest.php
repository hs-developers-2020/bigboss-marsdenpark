<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;
use Str;

class ProfileDescriptionRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'description'   => "required",
        ];
        return $rules;
    }

    public function messages() {

        return [
            'required'                  => "Please describe your profession or specialty.",
        ];
    }
}
