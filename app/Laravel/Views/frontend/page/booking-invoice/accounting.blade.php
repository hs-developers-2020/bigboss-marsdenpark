@extends('frontend._layout.main')

@section('content')

@include('frontend._components.topnav-main')

<div class="container"  style="margin-top: 170px !important;">
  <div class="d-flex justify-content-between">

  </div>
</div>

<div class="container  card-shadow" style="background-color: white; ">         
  <div class="content">
    <div class="">
      <div class="card" style="border-color: white!important;">
        <div class="card-body" style="padding: 0px !important;">
          <div class="row">
            <div class="col-lg-6 pl-5 pr-5 pt-5" style="background-color: white;">
              <div class="row">
                      {{-- <div class="col-lg-12">
                        <p class="text-color-gray font-semibold text-small text-uppercase spacing-1">Invoice Number: 032132</p>
                      </div> --}}
                      <div class="col-lg-12">
                        <p class="text-color-gray font-weight-bold text-small text-uppercase"><i class="fas fa-receipt mr-2 text-color-blue"></i>Quote</p>
                      </div>
                      <div class="col-lg-12 col-md-6 mt-2">
                        <p class="text-color-gray text-xsmall spacing-1 text-uppercase font-weight-bold">Customer Name</p>
                      </div>
                      <div class="col-lg-12 col-md-6 mt-md-0 mt-sm-0 mt-xs-0">

                        <p class="text-color-blue font-semibold">{{ Str::title($info->name) }} {{ Str::title($info->lastname) }}</p>

                      </div>
                      <div class="col-lg-12 col-md-6 mt-3">
                        <p class="text-color-gray text-xsmall spacing-1  font-weight-bold">Contact Number</p>
                      </div>
                      <div class="col-lg-12 col-md-6 mt-md-0 mt-sm-0 mt-xs-0">
                        <p class="text-color-blue font-semibold">{{ $info->contact }}</p>
                      </div>
                      <div class="col-lg-12 col-md-6 mt-3">
                        <p class="text-color-gray text-xsmall spacing-1 text-uppercase font-weight-bold">Email Address</p>
                      </div>
                      <div class="col-lg-12 col-md-6  mt-md-0 mt-sm-0 mt-xs-0">
                       <p class="text-color-blue font-semibold">{{ $info->email }}</p>
                     </div>
                     <div class="col-lg-12 col-md-6 mt-3">
                      <p class="text-color-gray text-xsmall spacing-1 text-uppercase font-weight-bold">Address</p>
                    </div>
                    <div class="col-lg-12 col-md-6 mt-md-0 mt-sm-0 mt-xs-0">
                      <p class="text-color-blue font-semibold">{{ Str::title($info->location) }}</p>
                    </div>
                    <div class="col-lg-12 col-md-6 mt-3">
                      <p class="text-color-gray text-xsmall spacing-1 font-weight-bold">POST</p>
                    </div>
                    <div class="col-lg-12 col-md-6 mt-md-0 mt-sm-0 mt-xs-0">
                      <p class="text-color-blue font-semibold text-uppercase">{{ $info->postcode }}</p>
                    </div>
                    <div class="col-lg-12 col-md-6 mt-3">
                      <p class="text-color-gray font-weight-bold text-xsmall spacing-1 text-uppercase">Payment Method</p>
                    </div>
                    <div class="col-lg-12 col-md-6 mt-md-0 mt-sm-0 mt-xs-0">
                      <p class="text-color-blue font-semibold">{{ Str::title($info->payment) }}</p>
                    </div>


                  </div>
                </div>
                <div class="col-lg-6 p-5" id="gradient-background">               
                  <div class="row">              
                    <div class="col-lg-12 text-lg-right text-md-right text-sm-left text-xs-left">
                      <p class="text-color-white font-semibold text-uppercase">Booking ID :  {{ $info->booking_id }}</p>
                    </div>                 
               
                    <div class="col-lg-12 mt-2">
                      <p class="text-color-white font-semibold mt-2 text-uppercase spacing-1"><i class="fas fa-user text-color-white mr-2"></i>Payment Details</p>
                    </div>
                    <div class="col-lg-12 mt-2">

      

                    <table class="table" style="border: none;">
                      <thead class="text-uppercase text-xxsmall spacing-1">
                        <tr class="text-color-white">
                          <th scope="col">Service Type</th>
                          <th scope="col">Service Package</th>
                          <th scope="col">Service Price</th>
                          <th scope="col" class="text-right"></th>
                        </tr>
                      </thead>
                      <tbody>
                      

                        <tr class="text-color-white font-semibold">
                          @forelse($services as $value)
                           <tr class="text-color-white font-semibold">
                             <td class="text-xxsmall spacing-1">{{Str::title($value->group)}}</td>
                            <td class="text-xxsmall spacing-1">{{Str::title($value->name)}}</td>
                            <td class="text-xxsmall spacing-1">${{Helper::amount($value->price)}}</td>
                            <td class="text-xxsmall spacing-1">${{Helper::amount($value->price)}}</td>
                          </tr>
                          @empty
                          <tr class="text-color-white font-semibold">
                            <td class="text-xxsmall spacing-1">N/A</td>
                            <td class="text-xxsmall spacing-1">N/A</td>
                            <td class="text-xxsmall spacing-1">N/A</td>
                          </tr>
                          @endforelse
                        </tr>
                                           
                      </tbody>
                    </table>
                  
                 
                  </div>
                  <div class="col-lg-12">
                    <hr class="text-color-white" style="height: 2px; background-color: white;">
                  </div>
                  <div class="col-lg-6 col-6">
                    <p class="text-color-white font-semibold text-small text-uppercase">Booking Total</p>
                  </div>
                  <div class="col-lg-6 col-6 text-right">

                    {{-- <p class="text-color-white font-semibold text-small">${{ Helper::amount($info->booking_total)}}</p> --}}
                    <p class="text-color-white font-semibold text-small">${{ Helper::amount($info->booking_total)}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>      
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-lg-12 d-flex flex-row">
        <div class="mr-3">
          <a name="next" class="spacing-1" style="text-decoration: none;">
            <a href="{{ route('frontend.print',[$info->booking_id]) }}" target="_blank"class="btn text-color-white mt-3 text-xxsmall text-uppercase font-semibold pl-4 pr-4 pt-3 pb-3" style="background-color: #2047A4;">Get a PDF Version</a>
          </a>
          @if($info->status != "pending" AND Auth::check())
          <a href="{{route('frontend.rate-service', [$info->booking_id])}}" class="" style="text-decoration: none;">
            <button class="btn text-color-white mt-3 text-uppercase font-semibold bg-color-red text-xsmall pl-5 pr-5 pt-3 pb-3">Rate
            </button>
          </a>
          @endif
        </div>    
      </div>     
    </div>
  </div>


  @include('frontend._components.footer')


  @stop

  @section('style-script')
  <style type="text/css">
    .hidden
    {
      display: none;
    }
    body {
      background-color: #F4F4F4 !important;
    }
    .table td, .table th {
      border-top: 0px solid #e9ecef !important;
    }
    @media only screen and (max-width: 1000px) {

     #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
    }
    .picture-size {
      height: 15%; width: 30%;
    }

  }

  @media only screen and (min-width: 1000px) {

   #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
  }
  .picture-size {
    height:5%; width: 7%;
  }

}
a:hover{
  color: white !important;
}
</style>
@stop