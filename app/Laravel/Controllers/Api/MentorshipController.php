<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader,Carbon;
use App\Laravel\Models\User;
use App\Laravel\Models\Chat;
use App\Laravel\Models\Views\Mentorship as MentorshipView;
use App\Laravel\Models\ChatParticipant;
use App\Laravel\Models\ChatConversation;

use App\Laravel\Models\Mentorship;
use App\Laravel\Models\MentorshipParticipant;
use App\Laravel\Models\MentorshipConversation;


use Illuminate\Http\Request;
use App\Laravel\Requests\Api\ChatRequest;
use App\Laravel\Requests\Api\ChatNameRequest;
use App\Laravel\Requests\Api\ChatIconRequest;
use App\Laravel\Requests\Api\MentorshipReviewRequest;

use App\Laravel\Transformers\ChatTransformer;
use App\Laravel\Transformers\ArticleTransformer;
use App\Laravel\Transformers\MentorshipTransformer;
use App\Laravel\Transformers\Views\MentorshipTransformer as MentorshipViewTransformer;


use App\Laravel\Transformers\TransformerManager;

class MentorshipController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}



	public function index(Request $request, $format = '') {

        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        $user = $request->user();
        $keyword = $request->get('keyword');

        $sort_by = Str::lower($request->get('sort_by','date_posted'));
        $sort_order = Str::lower($request->get('sort_order','desc'));
    
        $this->response['msg'] = "List of Mentorship";
        $this->response['status_code'] = "MENTORSHIP_LIST";

        switch($sort_order){ 
            case 'desc'  : $sort_order = 'desc'; break;
            default: $sort_order = 'asc';
        }

        switch($sort_by){ 
            case 'category'  : $sort_by = 'category_id'; break;
            default: $sort_by = 'latest_message_created_at';
            // default: $sort_by = 'created_at';

        }
        $mentorship_ids = MentorshipParticipant::where('user_id',$user->id)->pluck('mentorship_id')->toArray();
        if(!$mentorship_ids){ $mentorship_ids = "0";}
        else{$mentorship_ids = implode(",", $mentorship_ids);}

        $mentorships = MentorshipView::keyword($keyword)
                    ->whereRaw("id IN ($mentorship_ids)")
                    // ->where(function($query) use($request,$user){
                    //     if($request->has('type') AND Str::lower($request->get('type')) == "own"){
                    //         $this->response['msg'] = "List of My Group Chat";
                    //         return $query->where('owner_user_id',$user->id);
                    //     }
                    // })
                    ->orderBy($sort_by,$sort_order)->paginate($per_page);
        

        if($request->has('keyword')){
            $this->response['msg'] = "Search result: '{$keyword}'";
            $this->response['status_code'] = "SEARCH_RESULT";
        }

        $this->response['status'] = TRUE;
        $this->response['has_morepages'] = $mentorships->hasMorePages();
        $this->response['data'] = $this->transformer->transform($mentorships, new MentorshipViewTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function ongoing(Request $request, $format = '') {

        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        $user = $request->user();
        $keyword = $request->get('keyword');

        $sort_by = Str::lower($request->get('sort_by','date_posted'));
        $sort_order = Str::lower($request->get('sort_order','desc'));
    
        $this->response['msg'] = "List of Mentorship";
        $this->response['status_code'] = "MENTORSHIP_LIST";

        switch($sort_order){ 
            case 'desc'  : $sort_order = 'desc'; break;
            default: $sort_order = 'asc';
        }

        switch($sort_by){ 
            case 'category'  : $sort_by = 'category_id'; break;
            default: $sort_by = 'latest_message_created_at';
            // default: $sort_by = 'created_at';

        }
        $mentorship_ids = MentorshipParticipant::where('user_id',$user->id)->pluck('mentorship_id')->toArray();
        if(!$mentorship_ids){ $mentorship_ids = "0";}
        else{$mentorship_ids = implode(",", $mentorship_ids);}

        if($user->type == "mentee"){
            $mentorships = MentorshipView::keyword($keyword)
                    ->where('transaction_status','ongoing')
                    ->whereRaw("id IN ($mentorship_ids)")
                    // ->where(function($query) use($request,$user){
                    //     if($request->has('type') AND Str::lower($request->get('type')) == "own"){
                    //         $this->response['msg'] = "List of My Group Chat";
                    //         return $query->where('owner_user_id',$user->id);
                    //     }
                    // })
                    ->orderBy($sort_by,$sort_order)->paginate($per_page);
        }else{
            $mentorships = MentorshipView::keyword($keyword)
                    ->whereRaw("id IN ($mentorship_ids)")
                    // ->where(function($query) use($request,$user){
                    //     if($request->has('type') AND Str::lower($request->get('type')) == "own"){
                    //         $this->response['msg'] = "List of My Group Chat";
                    //         return $query->where('owner_user_id',$user->id);
                    //     }
                    // })
                    ->orderBy($sort_by,$sort_order)->paginate($per_page);
        }
        

        if($request->has('keyword')){
            $this->response['msg'] = "Search result: '{$keyword}'";
            $this->response['status_code'] = "SEARCH_RESULT";
        }

        $this->response['status'] = TRUE;
        $this->response['has_morepages'] = $mentorships->hasMorePages();
        $this->response['data'] = $this->transformer->transform($mentorships, new MentorshipViewTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function search(Request $request,$format = ''){
        $busy_mentors = Mentorship::where('status','active')->pluck('mentor_user_id')->toArray();

        $exclude_ids = "0";

        if(count($busy_mentors) > 0){
            $exclude_ids = implode(",", $busy_mentors);
        }

        $random_mentor = User::types(['mentor'])->whereRaw("id NOT IN ($exclude_ids)")->inRandomOrder()->first();
        if(!$random_mentor){
            $this->response['msg'] = "No available mentors found. All mentors are currently busy. You may try again later or keep trying.";
            $this->response['status_code'] = "MENTOR_NOT_FOUND";
            $this->response['status'] = false;
            $this->response_code = 404;
            goto callback;
        }

        $user = $request->user();

        /*
        Sample Transaction Code
        MTR-20180102-2-01 

        MTR - Mentorship,  
        20180102 - Year Month Day of Registration, 
        2 - Mentee , 1 - Mentor , 
        01 - Sequence Number
        */

        $next_sequence = Mentorship::where('owner_user_id',$user->id)->count()+1;
        $transaction_code = "MTR-{$user->created_at->format("Ymd")}-2-".str_pad($next_sequence, 2, "0", STR_PAD_LEFT);

        $mentorship = new Mentorship;
        $mentorship->owner_user_id = $user->id;
        $mentorship->mentor_user_id = $random_mentor->id;
        $mentorship->mentee_user_id = $user->id;
        $mentorship->status = "active";
        $mentorship->transaction_status = "ongoing";
        $mentorship->code = $transaction_code;
        $mentorship->save();

        $particpant = new MentorshipParticipant;
        $particpant->user_id = 1;
        $particpant->mentorship_id = $mentorship->id;
        $particpant->role = "moderator";
        $particpant->nickname = "Administrator";
        $particpant->save();

        $particpant = new MentorshipParticipant;
        $particpant->user_id = $user->id;
        $particpant->mentorship_id = $mentorship->id;
        $particpant->role = "member";
        $particpant->save();

        $particpant = new MentorshipParticipant;
        $particpant->user_id = $random_mentor->id;
        $particpant->mentorship_id = $mentorship->id;
        $particpant->role = "member";
        $particpant->save();

        $message = new MentorshipConversation;
        $message->mentorship_id = $mentorship->id;
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "Mentorship initiated";
        $message->save();

        $this->response['msg'] = "Mentorship has been created.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "MENTORSHIP_CREATED";
        $this->response['data'] = $this->transformer->transform($mentorship, new MentorshipTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }


    }

    public function show(Request $request, $format = '') {

        $mentorship = $request->get('mentorship_data');

        $this->response['msg'] = "Mentorship detail.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "MENTORSHIP_DETAIL";
        $this->response['data'] = $this->transformer->transform($mentorship, new MentorshipTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function complete(Request $request,$format = ""){
        $user = $request->user();
        $mentorship = $request->get('mentorship_data');
        $mentorship_participant = MentorshipParticipant::where('mentorship_id',$request->get('mentorship_id'))->where('user_id',$user->id)->first();
        
        if(!$mentorship_participant){
            $this->response['msg'] = "Unable to process request. You are not part of the conversation.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "UNAUTHORIZED";
            $this->response_code = 403;
            goto callback;
        }

        if($mentorship->transaction_status != "ongoing"){
            $this->response['msg'] = "Unable to process request. No more action needed.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "BAD_REQUEST";
            $this->response_code = 400;
            goto callback;
        }

        $mentorship->transaction_status = "completed";
        $mentorship->status = "inactive";
        $mentorship->completed_user_id = $user->id;
        $mentorship->completed_at = Carbon::now();
        $mentorship->save();

        $opposite_role = "mentee";
        if($user->type == "mentee") $opposite_role = "mentor";

        $this->response['msg'] = "Mentorship successfully complete. Please send review to your {$opposite_role}.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "MENTORSHIP_COMPLETED";
        $this->response_code = 200;


        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function send_review(MentorshipReviewRequest $request,$format = ""){
        $user = $request->user();
        $mentorship = $request->get('mentorship_data');
        $mentorship_participant = MentorshipParticipant::where('mentorship_id',$request->get('mentorship_id'))->where('user_id',$user->id)->first();
        
        if(!$mentorship_participant){
            $this->response['msg'] = "Unable to process request. You are not part of the conversation.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "UNAUTHORIZED";
            $this->response_code = 403;
            goto callback;
        }

        if($mentorship->transaction_status == "pending"){
            $this->response['msg'] = "Unable to process request. Complete mentorship first before sending user review.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "BAD_REQUEST";
            $this->response_code = 400;
            goto callback;
        }

        if($user->type == "mentor"){
            $mentorship->mentor_rating = $request->get('rating');
        }

        if($user->type == "mentee"){
            $mentorship->mentee_rating = $request->get('rating');
        }

        $mentorship->save();
        $this->response['msg'] = "Your user review has been stored.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "USERREVIEW_SAVED";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update_name(ChatNameRequest $request, $format = '' ){
        $mentorship = $request->get('mentorship_data');

        $mentorship->title = $request->get('title');
        $mentorship->save();

        $this->response['msg'] = "Mentorship name has been modified.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "MENTORSHIPNAME_MODIFIED";
        $this->response['data'] = $this->transformer->transform($mentorship, new MentorshipTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update_icon(ChatIconRequest $request, $format = '' ){
        $mentorship = $request->get('mentorship_data');

        if($request->hasFile('file')) {
            $image = ImageUploader::upload($request->file('file'), "uploads/images/mentorship/{$mentorship->id}");
            $mentorship->path = $image['path'];
            $mentorship->directory = $image['directory'];
            $mentorship->filename = $image['filename'];
        }

        $mentorship->save();


        $this->response['msg'] = "Mentorship icon has been modified.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "MENTORSHIPICON_MODIFIED";
        $this->response['data'] = $this->transformer->transform($mentorship, new MentorshipTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update(ChatRequest $request, $format = '') {

        $article = $request->get('article_data');
        $user = $request->user();

        $article->fill($request->only('title','video_url','content','category_id'));

        if($request->hasFile('file')) {
            $image = ImageUploader::upload($request->file('file'), "uploads/images/users/{$user->id}/articles");
            $article->path = $image['path'];
            $article->directory = $image['directory'];
            $article->filename = $image['filename'];
        }

        $article->save();

        $this->response['msg'] = "Article has been updated.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_MODIFIED";
        $this->response['data'] = $this->transformer->transform($article, new ArticleTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function destroy(Request $request, $format = '') {

        $article = $request->get('article_data');

        $article->delete();

        $this->response['msg'] = "Article has been removed.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_DELETED";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}