<?php namespace App\Laravel\Requests\System;

use App\Laravel\Requests\RequestManager;

class PaymentRequest extends RequestManager
{
	public function rules()
	{
		$id = $this->route('transaction_id') ?: 0;

		$rules = [
			'receipt_number'		=> "required_if:payment_status,paid|unique:payment,receipt_number,{$id}|nullable",
			'amount'		=> "required",
			'payment_status' => "required",
			'payment_method' => "required",
			"bank_name" => "required_if:payment_method,check",
			"cheque_no" => "required_if:payment_method,check"

		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "Field is required",
			'receipt_number.required_if' => "Field is required",
			'receipt_number.unique' => "OR already used."
		];
	}
}