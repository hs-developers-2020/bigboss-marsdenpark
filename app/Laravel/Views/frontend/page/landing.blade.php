@extends('frontend._layout.main') 

@section('content') 
{{-- <div class="fixed-top" style="background:#a1000a; margin: 0 auto;" >
    <div class="container">
        <p class="text-white font-weight-bold mb-0 first-top">Visit Us on booking.bigbossgroup.com.au/cleaning</p>
    </div>
</div> --}}
@include('frontend._components.topnav-main')
<div class="landing-background">
    <div class="container first-contianer">
        <div class="content d-flex flex-column justify-content-center" style="height: 100%;">
            <div class="row" >
                <div class="col-12 col-lg-2">
                   &nbsp;
                </div>
                <div class="col-12 col-lg-10 parent-div">
                    <h3 class="text-color-white  text-header font-weight-bold text-headerspacing-2 ml-5 header-title">
                        @if($title) {{ $title->title }} @else BIG BOSS CLEANING Marsden Park @endif 
                    </h3>
                    <div class="header-video" style="height: 25rem; width: 100%;">
                        {{-- @if ($video_content)
                            <video  autoplay="autoplay" muted onloadeddata="this.play();" preload="preload" style="width: auto; height: 100%;" id="myVideoPlayer" controls>
                                <source src="{{asset($video_content->directory.'/'.$video_content->filename)}}" type="video/mp4" style="width: 100%; height: 100%">
                                Your browser does not support the video tag.
                            </video>
                        @else
                            <img src="{{ asset('frontend/images/landing/vid.png') }}" alt="Big Boss" style="width: 100%; height: 100%;"/>
                        @endif --}}
                        <video  autoplay="autoplay" muted onloadeddata="this.play();" preload="preload" style="width: auto; height: 100%;" id="myVideoPlayer" controls>
                            <source src="{{ asset('frontend/Video/BOSS CLEANING FINAL.mp4')}}" type="video/mp4" style="width: 100%; height: 100%">
                            Your browser does not support the video tag.
                        </video>
                    </div>
                   
                </div>
            </div> 
            
            <div>
                {{-- <div class="floating_btn" style="position: fixed;  width: auto;  height: 40px;  bottom: 26px;
                right: 120px; z-index: 1">
                    <a href="https://bigbosscleaning.com.au/big-boss-group.php"><div class="floating_div "> <span>Want to own this business? Click here</span> <i class="fa fa-star float_icon"></i></div></a>
                </div> --}}

                <div style="position: fixed;  width: 50px;   bottom: -32px;
                right: -20px; z-index: 1">
                <div class="fab">
                    <div class="mainop">
                        <i id="addIcon" ><img src="{{ asset('frontend/Icons/floating_icon.png') }}" alt="" style="width:100%"></i>
                    </div>
                    <div id="forms" class="minifab op1">
                        <span style="float:left; margin-left:10px; margin-top:15px;color:white">Want to own this business? <a style="color:white" href="{{ $links->own_this_business??'https://bigbosscleaning.com.au/own-business-now.php' }}"><br> Click here</a></span>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="third-background py-3">
    <div class="container">
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="svg-wrapper">
                    <a href="{{route('frontend.cleaning.index')}}" style="text-decoration: none;">
                        <svg height="60" width="320" xmlns="http://www.w3.org/2000/svg" style="border-radius:.25rem">
                        <rect class="shape" height="60" width="320" />
                        <div class="text text-color-white font-weight-bold text-size spacing-1" style="text-shadow: 2px 2px 12px #000000;">Book Service Here</div>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
        <div class="content about-section" >
            
            <div class="row d-flex justify-content-start align-items-start">
                <div class="col-12 col-lg-6 text-justify">
                    <h3 class="text-green-10 " style="font-weight: 900">ABOUT US</h3>
                    <p class="my-5" style="line-height: 1.5;">

                        @if ($about_us)
                            {{ $about_us->description }}
                        @else 
                        Meet Oscar Nazar Jr. and Klein Nazar - Business Owner of Big Boss Cleaning Marsden Park.  Jr and Klein joined the Big Boss Family on November 2020.  In just three months after rigid training, getting used to and learning the ins and outs of the business, they already have regular domestic and commercial clients, local real estate clients that give them regular end of lease cleans and plus NDIS accredited provider clients that keep them so busy.  
                        Big Boss Cleaning Marsden Park caters the local areas of Marsden Park, and surrounding suburbs like Shalvey, Colebee, Schofields and Riverstone.  
                        Be astounded and be inspired on how their life has changed by taking the risk, by overcoming their fear starting their own business, and of getting out of their comfort zone and how Big Boss Family has helped them turned their life around for the better.
                        @endif
                    </p>
                    {{-- <a href="">Read More <i class="fas fa-chevron-right"></i></a> --}}
                </div>
                <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-center cleaning-head">
                    {{-- <img src="{{ asset('frontend/images/landing/sir-medan-pangilinan.png') }}" alt="Sir Maden Panglinan" class="img-avatar" /> --}}
                    <img src="{{ asset('frontend/images/landing/Profile pictures/jr_cropped.jpg') }}" alt="Sir Oscar Nazar Jr"  style="height:12em; width:auto; border-radius:50%; border: 5px solid #A40A13;""/>
                    <h4 class="text-red-10 font-weight-light mt-3">Mr. Oscar Nazar Jr.</h4>
                    <h6 class="font-weight-light">Big Boss Cleaning Marsden Park</h6>
                    <p class="text-center px-5 mt-3 mb-5">
                        Oscar Nazar Jr (simply called "JR") is the Business Owner of Big Boss Cleaning Marsden Park. Before joining the Big Boss Family, he used to work as Assistant Manager in Harris Farm Markets and Forklift Driver in Woolworths.
                        {{-- Medan is our Big Boss Cleaning Head and responsible for overseeing many of our cleaning jobs. He also continues to provide hands-on cleaning ratings as well.  --}}
                    </p>
                    {{-- <a href="">Read More <i class="fas fa-chevron-right"></i></a> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-background">
    <div class="container">
        <div class="content">
            <h3 class="text-green-10 text-center font-weight-bold">GALLERY</h3>
            <div class="row">
                <div class="col-12">
                    <div class="d-flex justify-content-center align-items-center gallery py-5">
                        @foreach ($gallery as $image)
                            <img src="{{ asset($image->directory.'/resized/'.$image->filename) }}" alt="Big Boss Cleaning" class="mx-3 mb-3"  width="280" height="200" />
                        @endforeach 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-background">
    <div class="container my-5">
        <h3 class="text-green-10 font-weight-bold">WHAT OUR CLIENTS SAY</h3>
        <div class="row">
            <div class="col-12">
                <div class="ratings">
                    @foreach ($feedback as $user)
                        <div class="slider--ratings mt-5 text-center px-4">
                            <div class="d-flex justify-content-center pb-3">
                                <img src="{{ asset($user->directory.'/resized/'.$user->filename)  }}" style="height:12em;width:auto;" />
                            </div>
                            @for ($i = 0; $i < $user->stars; $i++)
                                <i class="fas fa-star text-yellow-10"></i>
                            @endfor
                            <h6 class="pt-2 font-weight-bold text-uppercase">{{ $user->name }}</h6>
                            <p class="user-description" style="text-align: justify;">{{ $user->description }}</p>
                            <button class="more btn btn-link" value="1">Show More </button>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@include('frontend._components.footer-landing') 
@stop

@section('style-script')
<style type="text/css">
    body {
        background: #f9f9f9;
    }
    .fab {
        background-color: transparent;
        height: 64px;
        width: 64px;
        border-radius: 32px;
        transition: width 300ms;
        transition-timing-function: ease;
        position: absolute;
        right: 50px;
        bottom: 50px;
        text-align: center;
        overflow: hidden;
    }
    .fab:hover {
        width: 300px;
    }
    .fab:hover .mainop {
        cursor: pointer
    }

    .mainop {
        margin: auto;
        width: 64px;
        height: 64px;
        position: absolute;
        bottom: 0;
        right: 0;
        transition: transform 300ms;
        background-color: #f44336;
        border-radius: 32px;
        z-index: 6;
    }
    .minifab {
        position: relative;
        width: 344px;
        height: 64px;
        border-radius: 24px;
        z-index: 5;
        float: left;
        margin-bottom: 8px;
        margin-left: 8px;
        margin-right: 8px;
        background-color: blue;
        transition: box-shadow 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    }

    .minifab:hover {
        box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
    }

    .minifabIcon {
        height: 24px;
        width: 24px;
        margin-top: 12px;
    }

    .op1 {
        background-color: #2f82fc;
    }
    .floating_btn {
        list-style: none;
        margin:0;
        padding:0;
        max-width: 75px;
    }
    .floating_btn:after{
        content:"";
        display: block;
        clear: both;
    }

    .float_icon {
        /* background-image: linear-gradient(to right, rgba(255,255,255,0) 0%, rgba(255,255,255,5) 33%, rgba(255,255,255,1) 33%); */
        background-color: #2D3C8A ;
        border-radius: 50%;
        box-sizing: border-box;
        padding: 15px;
        position: relative;
        margin:-20px;
        text-decoration: none;
        color:white; 
        position: absolute;
        z-index: 20;
        bottom: 11px;
        right: -4px;
        font-size: 26px !important;
        text-align: center;
        line-height: 28px !important;
        box-shadow: 1px 1px 8px #000000;
    }

    .floating_btn div {
        background: #2D3C8A ;
        padding:0px;
        width: auto;
        border-radius: 50px;
        text-align: center;
        overflow:hidden;
        float:right;
        margin-bottom: 10px;
        box-shadow: 1px 1px 8px #000000;
    }
    .floating_btn div a{
        position:relative;
        display: block;
        min-width: 27px;
        padding:15px;
    }
    .floating_btn span {
        position: relative;
        z-index: 1;
        white-space: nowrap;
        margin-left: 10px;
        display: inline-block;
        vertical-align: top;
        position: relative;
        visibility: none;
        width: auto;
        max-width:0px; /* This is the fixed unit value */
        opacity: 0;
        text-align: center;
        transition:all 1s;
        box-sizing:border-box;
        padding-top:10px;
        padding-bottom:10px;
        text-decoration:none;
        color:white;
    }

    .floating_div:hover {
        cursor: pointer;
    }
    .floating_div:hover span {
        margin-right:0px;
        opacity: 1;
        width: auto;
        max-width:500px;
        padding-right:38px;
    }
    p {
        color: #000000 !important;
    }

    .btn-circle.btn-xl {
        width: 60px;
        height: 60px;
        padding: 10px 16px;
        border-radius: 35px;
        font-size: 24px;
        line-height: 1.33;
        /* box-shadow: 0 0 0 3px #000000; */

    }

    .btn-circle {
        width: 30px;
        height: 30px;
        padding: 6px 0px;
        border-radius: 15px;
        text-align: center;
        font-size: 12px;
        line-height: 1.42857;
    }


    .expand_button {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%)
    }

    .e-button {
        color: #fff;
        background-color: #000;
        border: 1px solid #000;
        padding-left: 10px;
        padding-right: 10px;
        position: relative;
        float: left;
        overflow: hidden;
        max-width: 30px;
        -webkit-transition: max-width 0.3s ease-in-out;
        -moz-transition: max-width 0.3s ease-in-out;
        -o-transition: max-width 0.3s ease-in-out;
        transition: max-width 0.3s linear
    }

    .e-button-text {
        display: block;
        white-space: nowrap;
        color: #fff
    }

    .e-button:hover {
        background-color: #000;
        color: #fff;
        max-width: 280px
    }
    
    .svg-wrapper {
        /* position: relative; */
        /* top: 50%; */
        transform: translateY(-50%);
        margin: 25px auto;
        width: 320px;  
    }
    .shape {
        stroke-dasharray: 140 540;
        stroke-dashoffset: -474;
        stroke-width: 8px;
        fill: #2D3C8A;
        stroke: blue ;
        border-bottom: 5px solid black;
        transition: stroke-width 1s, stroke-dashoffset 1s, stroke-dasharray 1s, background 1s;
    }
    .text {
        font-size: 28px;
        line-height: 32px;
        letter-spacing: 2px;
        color: #FFF;
        top: -52px;
        position: relative;
        left: 20px;
    }
    .text:hover{
        /* color: #C4272D; */
    }
    .svg-wrapper:hover .shape {
        stroke-width: 5px;
        stroke-dashoffset: 0;
        stroke-dasharray: 760;
        fill:#2D3C8A;
    }
    
    .float{
        position:fixed;
        width: auto;
        height:40px;
        bottom:28px;
        right:90px;
        background-color: rgba(0, 0, 0, 0.5);
        color:#FFF;
        text-align:center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float{
        margin-top:22px;
}

    .custom-nav {
        /* margin-top: 22px; */
    }
    .landing-background {
        background: url("frontend/background/header.png") ;
        background-repeat: no-repeat;
        background-position: top;
        background-size: cover;
        height: 90vh !important;
        /* position: relative; */
        margin-top: 100px
    }
    .section-background {
        background: url("frontend/background/section-bg.png") ;
        width: 100%;
    }
    .third-background {
        /* background-image: url("frontend/background/third_bg.png") no-repeat center center / cover;  */
        background-image: url("frontend/background/third_bg.png"); 
        /* background-color: blue;  */
        background-repeat: no-repeat;
        background-size: 120%;
        background-position: center;;
    }
    .text-green-10 { color: #189819  }
    .text-red-10 { color: #A40A13 }
    .text-yellow-10 { color: #F6B421 }
    .text-blue-10 { color: #1944A6 }
    
    .img-avatar {
        height: 12rem;
        width: auto;
        border-radius: 100px;
        border: 5px solid #A40A13
    }
    .prev_btn {
        position: absolute;
        left: 0;
        top: 0;
        color: #04A234;
        padding-top: 10rem;
    }
    
    .fa-chevron-circle-right {
        position: absolute;
        right: 0;
        top: 0;
        color: #04A234;
        padding-top: 10rem;
    }
    .header-video {
        width: 100% !important;
    }

    .first-contianer {
        height: 100%; 
    }
    @media only screen and (max-width: 768px) {
        .col-main-button {
            padding-left: 0px !important;
            padding-right: 0px !important;
        }
        .cleaning-head {
            padding-top: 100px;
        }
        .gallery {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }
        .header-video {
            height: 13rem !important;
            display: flex;
            justify-content: center
        }
        .header-title {
            padding-top: 10px !important
        }

        .about-section {
            margin-top: -30px
        }

        .landing-background { 
            height: 67vh !important;
        }
        .first-contianer {
            height: 85%; 
        }

        .text-header {
            margin-left: 0px !important;
        }
        .first-top {
            font-size: 12px !important;
        }
        .third-background {
            background-size: cover !important;
        }
    }

    @media only screen and (max-width: 1280px) {
        .header-video {
            /* height: 60% !important;
            width: 88% !important;
            margin-left: 40px */
        }

        .parent-div {
            /* margin-top: 45px !important; */
        }

        .landing-background{
            margin-top: 100px
        }
    }

    .user-description{
        height:70px;
        display:block;
        overflow:hidden;
    }
</style>
@stop 

@section('page-scripts')
<script>
var video = document.getElementById("myVideoPlayer");
function stopVideo(){
    video.pause();
    video.currentTime = 0;
}

$(document).on('click', '.more', function(){
    var value = parseInt($(this).val());
    if ( value % 2 == 0) {
        $(this).text('Show More')
        $(this).siblings('p').css({ 'height': '70px' })
    }else{
        $(this).text('Show Less')
        $(this).siblings('p').css({ 'height': 'auto' })
    }
    $(this).val(value + 1)
});

//     $('.owl-carousel').owlCarousel({
//     loop:true,
//     margin:10,
//     nav:true,
//     responsive:{
//         0:{
//             items:1
//         },
//         600:{
//             items:3
//         },
//         1000:{
//             items:5
//         }
//     }
// })
</script>
@stop