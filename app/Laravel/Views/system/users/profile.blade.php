@extends('system._layouts.main')
<style type="text/css">
    footer {
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@section('content')
@include('system._components.topnav')

<div class=" mt-5 pt-5 mt-lg-5 pt-lg-0 mt-xl-0 pt-xl-0">
    <div class="container bg-white mb-5 p-5">
        <form action="" method="POST">
            {{ csrf_field()}}
        <div class="row">
            <div class="col-lg-4 mt-1 text-center p-4">
                <img src="{{ asset('frontend/images/default.png') }}" style="border-radius: 50%; background-color: red" alt="Generic placeholder image" class="">
                <h5 class="text-uppercase mt-3">{{ strtoupper( auth()->user()->firstname) }}  {{ auth()->user()->lastname }}</h5>
            </div>
            <div class="col-lg-8">
                    <div class="row">
                        <div class="col-lg-12 mt-lg-0 mt-md-0">
                            <div class="text-color-gray mb-2 {{$errors->first('firstname') ? 'has-error' : NULL}}">
                                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">First Name</span>
                                <input type="text"  placeholder="Enter Last name" class="form-control text-xsmall" name="firstname" value="{{$user_profile->firstname}}">
                            </div>
                            @if($errors->first('firstname'))
                            <span class="help-block" style="color: red;">{{$errors->first('firstname')}}</span>
                            @endif 
                        </div>
                        <div class="col-lg-12 mt-lg-0 mt-md-0">
                            <div class="text-color-gray mb-2 {{$errors->first('lastname') ? 'has-error' : NULL}}">
                                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Last Name</span>
                                <input type="text"  placeholder="Enter Last name" class="form-control text-xsmall" name="lastname" value="{{$user_profile->lastname}}">
                            </div>
                            @if($errors->first('lastname'))
                            <span class="help-block" style="color: red;">{{$errors->first('lastname')}}</span>
                            @endif 
                        </div>
                        <div class="col-lg-12 mt-lg-0 mt-md-0">
                            <div class="text-color-gray mb-2 {{$errors->first('username') ? 'has-error' : NULL}}">
                                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Username</span>
                                <input type="text"  placeholder="Enter Last name" class="form-control text-xsmall" name="username" value="{{ $user_profile->username}}">
                            </div>
                            @if($errors->first('username'))
                            <span class="help-block" style="color: red;">{{$errors->first('username')}}</span>
                            @endif 
                        </div>
                        <div class="col-lg-12 mt-lg-0 mt-md-0">
                            <div class="text-color-gray mb-2 {{$errors->first('email') ? 'has-error' : NULL}}">

                                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Email</span>
                                <input type="text"  placeholder="Enter Last name" class="form-control text-xsmall" name="email" value="{{$user_profile->email}}">
                            </div>
                            @if($errors->first('email'))
                            <span class="help-block" style="color: red;">{{$errors->first('email')}}</span>
                            @endif 
                        </div>
                        <div class="col-lg-12 mt-lg-0 mt-md-0">
                            <div class="text-color-gray mb-2 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
                                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Contact Number</span>
                                <input type="text"  placeholder="Enter Last name" class="form-control text-xsmall" name="contact_number" value="{{$user_profile->contact_number}}">
                            </div>
                            @if($errors->first('contact_number'))
                            <span class="help-block" style="color: red;">{{$errors->first('contact_number')}}</span>
                            @endif 
                        </div>
                       
                        <div class="col-lg-12 mt-3 ">
                             <code>Note: If you want to <strong>reset account password</strong>. Fill up the password below.</code>
                            <div class="text-color-gray mb-2 {{$errors->first('password') ? 'has-error' : NULL}}">
                                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Password</span>
                                <input type="password" placeholder="Password" class="form-control" name="password" value="{{ old('password') }}">
                            </div>
                            @if($errors->first('password'))
                            <span class="help-block" style="color: red;">{{$errors->first('password')}}</span>
                            @endif 
                        </div>
                        <div class="col-lg-12 mt-3 ">
                            <div class="text-color-gray mb-2">
                                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Verify Password</span>
                                <input type="password" placeholder="Verify Password" class="form-control" name="password_confirmation">
                            </div>
                        </div>
                    
                   
                        <div class="col-lg-12 text-right mt-3">
                            <button type="submit" class="btn bg-color-blue text-color-white text-xxsmall mr-2 text-uppercase">Update Information</button>
                        </div>                 

                    </div>
                
            </div>
                       
        </div>
      
        </form>
    </div>
</div>

@stop



@section('page-styles')
<style type="text/css">
    .custom-nav {
    -webkit-box-shadow: 2px 2px 2px #212121 !important; 
    -moz-box-shadow:    2px 2px 2px #212121 !important; 
    box-shadow:         2px 2px 2px #212121 !important; 
    z-index:999;
}
</style>
@stop
