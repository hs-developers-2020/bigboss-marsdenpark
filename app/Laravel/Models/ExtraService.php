<?php

namespace App\Laravel\Models;


use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon, Helper,Str;

class ExtraService extends Model
{
    use DateFormatterTrait,SoftDeletes;

    protected $table = "extra_services";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','group','price','regions' ,'days_available','service_type','path', 'directory', 'filename','has_addon'
    ];

    public $timestamps = true;

    public function scopeGroup($query,$group = NULL){
        $key = Str::lower($group);
        if ($key) {
            return $query->where(function($query) use ($key){
                    $query->where('group', $key);
            });
        }
       

    }

    public function add_ons(){
        return $this->hasMany('App\Laravel\Models\Addons','subservice_id','id');
        
    }



}
