<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.9.0/css/all.css" integrity="sha384-i1LQnF23gykqWXg6jxC2ZbCbUMxyw5gLZY6UiUS98LYV5unm8GWmfkIS6jqJfb4E" crossorigin="anonymous">  
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}"/>
<!-- Custome scrollbar CSS -->
<link rel="stylesheet" href="{{asset('system/vendor/jquery-custom-scrollbar-0.5.5/jquery-custom-scrollbar-0.5.5/jquery.custom-scrollbar.css')}}" type="text/css">
  
<!-- Footable CSS -->
<link rel="stylesheet" href="{{asset('system/vendor/footable-bootstrap.latest/css/footable.bootstrap.min.css')}}"  type="text/css">
  
  <!-- jvectormap CSS -->
<link href="{{asset('system/vendor/jquery-jvectormap/jquery-jvectormap-2.0.3.css')}}"  rel="stylesheet">
<link href="{{asset('system/vendor/sweet-alert2/sweetalert2.min.css')}}" rel="stylesheet" type="text/css">
<!-- framework CSS -->
<link id="theme" rel="stylesheet" href="{{asset('system/css/style-helpdesk.css')}}?v=1.0" type="text/css">


@yield('page-styles')


<style type="text/css">

 
  li.dropdown>a img {
    max-width: 32px;
    max-height: 32px;
    border-radius: 50%;
  }
.dropdown-menu {
    border-radius: 3px;
    margin-top: 12px;
}
.dropdown-menu:after{
 
    border-bottom-color: #4285f4;
}

.dropdown:hover > .dropdown-menu {
    display: block;
}
.dropdown > .dropdown-toggle:active {
    /*Without this, clicking will make it sticky*/
    pointer-events: none;
    
}
.dropdown-menu.account{
      min-width: 15rem !important;
}

.dropdown-menu .user-info {
    background-color: #4285f4;
    color: #fff;
    overflow: hidden;
    padding: 20px 15px 14px;
    border-radius: 2px 2px 0 0;
    margin-bottom: 7px;
}

.navbar-default .navbar-nav > li.dropdown:hover > a, 
.navbar-default .navbar-nav > li.dropdown:hover > a:hover,
.navbar-default .navbar-nav > li.dropdown:hover > a:focus {
    background-color: rgb(231, 231, 231);
    color: rgb(85, 85, 85);
}
li.dropdown:hover > .dropdown-menu {
    display: block;
}
.dropdown-menu .dropdown-item {
  padding: 5px !important;
  
}
.dropdown-menu {
  margin-top: 0px;
}

.text-xxxsmall {
  font-size: 0.75rem !important;
}
.text-xxsmall {
  font-size: 0.8rem !important;
}
.text-xsmall {
  font-size: .9rem !important;
}
.text-header-small {
  font-size: 1.1rem !important;
}
.text-small {
  font-size: 1.2rem !important;
}
.text-medium {
  font-size: 1.3rem !important;
}
.text-xmedium {
  font-size: 1.7rem !important;
}
.text-large {
  font-size: 2rem !important;
}
.text-xlarge {
  font-size: 2.3rem !important;
}

/*text-spacing*/
.spacing-1 {
	letter-spacing: 1px;
}
.spacing-2 {
	letter-spacing: 2px;
}
.spacing-3 {
	letter-spacing: 7px;
}
.spacing-4 {
	letter-spacing: 10px;
}

/*text height*/
.text-height-1 {
line-height: 25px;
}
.text-height-2 {
line-height: 40px;
}

.font-semibold {font-weight: 500;}
.text-color-red {
	color: #a1000a !important;
}
.text-color-dark-red {
	color: #9A171F !important;
}
.text-color-blue {
	color: #1944a6 !important;
}
.text-color-gray {
	color: #3C3C3C !important;
}

.text-color-green {
	color: #00a131;
}
.text-color-light {
	color: #8596ae !important;
}
.text-color-white {
	color: white !important;
}
.text-color-yellow {
	color: #f4a91c;
}
.nav-link:hover {
	color: #9A171F !important;
}
.btn-color-red {
		background-color: #a1000a;
		border-color: #a1000a;
		color: white;
}
.btn-color-red:hover {
		background-color:  #a1000a;
		border-color: #a1000a;
		color: white;
}
.bg-color-red {
	background-color: #a1000a;
}
.bg-color-dark-red {
	background-color: #9A171F;
}
.bg-color-green {
	background-color: #00a131 !important;
}
.bg-color-blue {
	background-color: #1944a6 !important;
}
.bg-color-white {
	background-color: white;
}

.pointer {
	cursor: pointer;
}
.logo-blue {
	color: #1944a6;
	font-family: 'Ubuntu';
}
.logo-red {
	color: #a1000a;
	font-family: 'Ubuntu';
}
.logo-green {
	color: #00a131;
	font-family: 'Ubuntu';
}
.font-semibold {font-weight: 500 !important;}
.decoration-none {
	text-decoration: none !important;
}

.active {
  color: #9A171F !important;
}
.input-icons i { 
  position: absolute; 
  } 
.input-icons { 
  width: 100%; 
  margin-bottom: 10px; 
} 
.icon { 
  padding: 10px; 
  min-width: 40px; 
} 
.position-absolute {
	position: absolute;
}
.input-field { 
  width: 100%; 
  padding: 15px; 
  text-align: left;
  padding-left: 50px;
   }

.date-icon {
right:28px;
position:absolute;
top:38px;
color: #2047A4;
}

.icon-input {
padding-right:60px !important;
}

.dropdown {
  position: relative;
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: white;
  min-width: 100px;
  box-shadow: 0px 5px 16px 0px rgba(0,0,0,0.2);
  padding: 12px 16px;
  z-index: 1;
}

.dropdown:hover .dropdown-content {
  display: block;
}

#toast-container  {
    
  padding-top:120px;
  padding-right: 100px
}

.inputfile {
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
}

.inputfile + label {
    font-size: 1.25em;
    font-weight: 700;
    color: white;
    background-color: black;
    display: inline-block;
}
	@media only screen and (max-width: 1000px) {


.picture-size {
height: 15%; width: 30%;
}
.input-size {
  width: 90% !important;
}
.logo-size {
  width: 17%;
  }
  .image-size {
    height: 50%;
  }
  .text-mobile {
  font-size: 30px;
}
}

@media only screen and (min-width: 1000px) {


.picture-size {
height:15%; width: 17%;
}
.input-size {
  width: 30% !important;
}
.logo-size {
  width:9%;
  }
.text-mobile {
  font-size: 20px;
}
}
.inputfile:focus + label,
.inputfile + label:hover {
    background-color: red;
}

.inputfile + label {
	cursor: pointer; /* "hand" cursor */
}

.searchTerm {
  width: 100%;
  border: 1px solid #1944a6;
  border-right: none;
  padding: 5px;
  height: 32px;
  border-radius: 5px 0 0 5px;
  outline: none;
  color: #000;
}

.searchTerm:focus{
  color: #000;
}

.searchButton {
  width: 40px;
  height: 32px;
  border: 1px solid #1944a6;
  background: #1944a6;
  text-align: center;
  color: #fff;
  border-radius: 0 5px 5px 0;
  cursor: pointer;
  font-size: 20px;
}

body::-webkit-scrollbar-track
{
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  background-color: #F5F5F5;
}

body::-webkit-scrollbar
{
  width: 12px;
  background-color: #F5F5F5;
}

body::-webkit-scrollbar-thumb
{
 
  -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
  background-color: #a1000a;
}
.card-shadow {
    -webkit-box-shadow: 2px 2px 2px #212121;
    -moz-box-shadow:    2px 2px 2px #212121;
    box-shadow:         2px 2px 2px #212121;
}
.no-decoration {
  text-decoration: none !important;
}
</style>