<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class WaybillRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			// 'delivery_date'	=> "date",
			// 'received_date'	=> "date",
			// 'sorting_date'	=> "date",
		];

		return $rules;
	}

	public function messages(){
		return [
			'delivery_date.date'	=> "Invalid date format for delivery date.",
			'sorting_date.date'	=> "Invalid date format for sorting date.",
			'received_date.date'	=> "Invalid date format for received date.",


		];
	}
}