@extends('frontend._layout.main')

@section('content')

@include('frontend._components.topnav-main')

<div class="container mt-4" style="margin-top: 160px !important;">
  <div class="d-flex justify-content-between">
 
</div>
</div>

<div class="container mt-2" style="background-color: white; ">         
    <div class="content">
      <div class="">
        <div class="card" style="border-color: white !important;">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row">
                  <div class="col-lg-7 p-5" style="background-color: white;">
                    <div class="row">
                      <div class="col-lg-12">
                         <p class="text-color-gray font-semibold text-uppercase text-small">Booking Status: <span class="text-color-green">Ongoing</span></p>
                      </div>
                      
                       <div class="col-lg-12 mt-2">
                        <p class="text-color-gray font-weight-bold text-uppercase"><i class="fas fa-list-alt mr-2 text-color-blue"></i>Service Details</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold">Service Type</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold">House of Standard</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold">Service Package</p>
                      </div>
                      <div class="col-lg-6">
                       <p class="text-color-blue font-semibold">One Off</p>
                      </div>
                      <div class="col-lg-12 mt-2">
                        <p class="text-color-gray font-weight-bold text-uppercase"><i class="fas fa-calendar-alt text-color-blue mr-2"></i>Booking Time/Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold">Booking Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold">27/02/2019</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold">Secondary Booking Date</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold">27/02/2019</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold">Service Time</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold">10:00 am - 01:00 pm</p>
                      </div>
                       <div class="col-lg-12 mt-2">
                        <p class="text-color-gray font-weight-bold text-uppercase"><i class="fas fa-map-marker-alt mr-2 text-color-blue"></i>Service Location</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold">Address</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold">Level 4/235 Macquarie St., Sydney, NSQ</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-gray font-semibold">Post</p>
                      </div>
                      <div class="col-lg-6">
                        <p class="text-color-blue font-semibold">2000</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-5 p-5">
                    <div class="row">
                      <div class="col-lg-12">
                        <h3 class="text-color-blue">Booking</h3>
                        <h3 class="text-color-red">Confirmed</h3>
                      </div>
                      <div class="col-lg-12 mt-3">
                        <p class="text-color-gray font-semibold">Here's the details of your cleaing expert:</p>
                      </div>
                      <div class="col-lg-12">
                        <div class="d-flex flex-row">
                          <img src="{{ asset('frontend/images/user.jpg') }}" class="rounded-circle mr-3 mt-2 picture-size">
                          <p class="text-xsmall text-color-blue mt-3 pt-2 font-weight-bold ml-2 spacing-1 text-uppercase">Emmet Brown<br>
                            <span class="font-semibold text-color-gray text-xxsmall text-uppercase">Da Cleaners</span>
                          </p>
                        </div>
                      </div>
                      <div class="col-lg-12 mt-3">
                        <p class="text-color-gray font-semibold">Our cleaning expert will arrive on:</p>
                        <p class="text-color-blue font-semibold">September 17,2019 / 10:00 am</p>
                      </div>
                       <div class="col-lg-12">
                        <a class="text-color-white" href="{{route('frontend.dashboard')}}" class="spacing-1" style="text-decoration: none;">
                        <button class="btn text-color-white mt-3 text-xxsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #1F46A3;">CONTINUE
                          </button>
                          </a>
                      </div>
                    </div>
                  
                  </div>
                </div>
              </div>
                 
            </div>
          </div>
        </div>      
      </div>
    </div>
  </div>
  

@include('frontend._components.footer')


@stop

@section('style-script')
<style type="text/css">
body {
background-color: #F4F4F4 !important;
}
@media only screen and (max-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 450px; 
    background: linear-gradient(-90deg,white,white, white, white), url("frontend/images/bg1.jpg") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}

}

@media only screen and (min-width: 1000px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 450px; 
    background: linear-gradient(-90deg,transparent,transparent, white, white), url("frontend/images/booking.png") no-repeat center; 
    background-size: cover; 
}

.picture-size {
height:5%; width: 7%;
}

  }
  a{
    color: white
  }
  a:hover{
    color: white !important;
  }
  footer {
    padding-top: 0px !important;
    margin-top: 0px !important;
  }
</style>
@stop