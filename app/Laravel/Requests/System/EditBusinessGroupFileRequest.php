<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class EditBusinessGroupFileRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'name'	=> "required",
			'label' => "required",
			'date_issue' => "required",
			'date_expiry' => "required",
			'reference' => "required",
			'is_high_risk' => "required",
			

		];

		return $rules;
	}

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
		];
	}
}