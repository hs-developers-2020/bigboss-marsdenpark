<?php


namespace App\Laravel\Controllers\System;

/**
 *
 * Models used for this controller
 */

use Illuminate\Http\Request;




/**
 *
 * Requests used for validating inputs
 */

use App\Laravel\Models\AboutUs;
use App\Http\Requests\PageRequest;

use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Requests\System\IconRequest;

use App\Laravel\Requests\System\AboutUsRequest;
use Helper, Carbon, Str, Auth, ImageUploader, FileUploader, File, DB, AuditRequest, Event;


class AboutUsController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data = [];
        parent::__construct();
        array_merge($this->data, parent::get_data());
    }

    public function index(PageRequest $request)
    {
        // $this->data['group'] = $request->get('group',false);
        $this->data['keyword'] = $request->get('keyword', NULL);

        $this->data['about_us'] = AboutUs::orderBy('created_at', "DESC")->paginate(10);

        return view('system.cleaning.about-us.index', $this->data);
    }

    public function create()
    {
        return view('system.cleaning.about-us.create', $this->data);
    }

    public function store (AboutUsRequest $request) {

		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$about_us = new AboutUs;
			$about_us->fill($request->all());

			if($about_us->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ABOUT_US_CREATED", 'remarks' => Auth::user()->full_name." has successfully created new about us {$about_us->title}.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.about-us.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function edit($id = NULL)
    {
        $about_us = AboutUs::find($id);
	
		if (!$about_us) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.about-us.index');
		}

		$this->data['about_us'] = $about_us;

		return view('system.cleaning.about-us.edit',$this->data);
    }

    public function update(AboutUsRequest $request, $id = NULL)
    {
        $ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$edit_about = AboutUs::find($id);
			$edit_about->fill($request->all());
		
			if (!$edit_about) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.about-us.index');
			}


			if($edit_about->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ABOUT_US_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$edit_about->title} about us.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.about-us.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
    }

    public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$icons = AboutUs::find($id);

			if (!$icons) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.about-us.index');
			}

			if($icons->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ABOUT_US_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$icons->name} about us.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.about-us.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
