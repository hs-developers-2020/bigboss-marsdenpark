<?php

namespace App\Laravel\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Laravel\Services\ResponseManager;

use App\laravel\Models\User;
use App\Http\Requests\PageRequest;

use App\Laravel\Requests\Frontend\ProfileRequest;

use DB,Request,PDF;

class ProfileController extends Controller
{
  public function index(){
		return view ('frontend.profile.index');
	}

public function update(ProfileRequest $request)
{
	
	$user = User::find(auth()->user()->id);

	if(!$user)
	{
		session()->flash('notification-status','warning');
		session()->flash('notification-msg','No Data Found.');
		return redirect()->back();
	}
	

	 User::where('id',auth()->user()->id)
	 			->update([ 
	 					"firstname"=>$request->firstname,
	 					"lastname"=>$request->lastname,
	 					"email"=>$request->email,
	 					"contact_number"=>$request->contact_number,
	 					"abn"=>$request->abn,
	 					"entity_name"=>$request->entity_name,
	 					"company_name"=>$request->company_name,
	 					"business_type"=>$request->business_type,
	 					"job_title"=>$request->job_title,
	 				 ]);

	
// return 	$users->fill($request->only('firstname','lastname','email','contact_numnber','abn','entity_name','company_name','business_type','job_title'));
// 	$users->save();
		session()->flash('notification-status','success');
		session()->flash('notification-msg','Feedback was successfully save.');
		return redirect()->route('frontend.profile');
	
}

	

}
