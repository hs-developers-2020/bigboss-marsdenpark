@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
<div class="wrapper ">
	<div class="container ">
		<div class="row">
    		<div class="col-lg-6 pt-2 mb-3">
             <h6 class="text-xsmall spacing-1 text-color-gray">Task Details</h6>
            </div>
            
            <div class="col-lg-6 mb-3 text-right">
                <button class="btn btn-warning rounded-3 text-color-white text-xxsmall">{{Str::title($jobs->status)}}</button>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 text-right">
                <div id="map" style="width: 100%; height: 200px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 bg-color-white w-100 p-4" >
                <div class="row">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-between">
                            <p class="text-color-gray font-semibold">Job ID #{{$jobs->booking_id}}</p>
                                @if($jobs->status == 'pending')
                                <p class="text-color-light font-semibold"><i class="fas fa-times-circle mr-2"></i>Cancel Request</p>
                                @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mt-3">
                        <div class="d-flex flex-row">
                            <figure class="avatar150 rounded-circle border" style="min-width: 50px !important;height: 60px !important;width: 60px !important;">
                              <img src="{{ asset('frontend/images/user.jpg') }}" alt="user image">
                            </figure>
                            <p class="text-color-gray font-semibold ml-3 mt-2  w-75">{{Str::title($jobs->full_name)}} | +61 {{$jobs->contact}} | {{$jobs->email}} <br>#{{$jobs->location}}</p>
                        </div>
                     </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-4">
                        <div class="row mb-3">
                            <div class="col-lg-3">
                                <span class="text-color-gray font-semibold">Service Type</span> 
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->property_type ?: 'No Data')}}</p>
                            </div>
                            <div class="col-lg-3">
                                <span class="text-color-gray font-semibold">Service Package</span>
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->service_type) ?: 'No Data'}}</p>
                            </div>
                            @if($jobs->services)
                             <div class="col-lg-3">
                                <span class="text-color-gray font-semibold">Extras Services</span>
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->services) ?: 'No Data'}}</p>
                            </div>
                            @endif
                            <div class="col-lg-3 mb-3">
			                    <span class="text-color-gray font-semibold">Storey Type</span>
			                    <p class="text-color-blue font-semibold">{{Str::title($jobs->unit) ?: "N/A"}}</p>                 
			                </div>
                        </div>
                        <div class="row mb-3">
                        	<div class="col-lg-3 ">
			                    <span class="text-color-gray font-semibold">Other Services</span>
			                    <p class="text-color-blue font-semibold">@forelse($others as $index => $value)
			                    	{{$value->services}} - ${{$value->price}} 
			                    	@empty No data @endforelse
			                    </p>
			                </div>
                        	<div class="col-lg-3 mb-3">
			                    <span class="text-color-gray font-semibold">Facility</span>
			                    <p class="text-color-blue font-semibold">{{($jobs->subservice ? $jobs->subservice->name :"N/A")}}</p>                 
			                </div>
                            <div class="col-lg-3 ">
                                <span class="text-color-gray font-semibold">Region</span>
                                <p class="text-color-blue font-semibold">{{$region}}</p>
                            </div>
                            <div class="col-lg-3 ">
                                <span class="text-color-gray font-semibold">Booking Date</span>
                                <p class="text-color-blue font-semibold">{{Helper::date_only($jobs->booking_date) }}</p>
                            </div>
                           
                        </div>
                        <div class="row">
                        	<div class="col-lg-3 ">
                                <span class="text-color-gray font-semibold">Service Time</span>
                                @if($jobs->time_start)

                                    <p class="text-color-blue font-semibold">{{Helper::time_only($jobs->time_start) }} - {{Helper::time_only($jobs->time_end)}}</p>
                                @else
                                     <p class="text-color-blue font-semibold">{{$jobs->time_start ?: 'No Time Selected'}} - {{$jobs->time_end ?: 'No Time Selected'}}</p>
                                @endif

                            </div>
                            <div class="col-lg-3 mb-3">
                                <span class="text-color-gray font-semibold">Estimated Total</span>
                                <p class="text-color-blue font-semibold">${{Helper::amount($jobs->amount)}}</p>                 
                            </div>
                            <div class="col-lg-3 mb-3">
                                <span class="text-color-gray font-semibold">Payment Method</span>
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->payment)}}</p>                 
                            </div>
                            <div class="col-lg-3 mb-3">
                                <span class="text-color-gray font-semibold">Booking Total</span>
                                <p class="text-color-blue font-semibold">${{Helper::amount($jobs->booking_total)}}</p>                 
                            </div>
                        </div>
                    </div> 
                </div>
            </div> 
        </div>  
        <hr>
        @if($jobs->status =="cancelled")
            <div class="row">
                <div class="col-lg-12 bg-color-white w-100 p-4" >
                    <div class="row">
                        <div class="col-lg-6 pt-2 mb-3">
                            <h6 class="text-xsmall spacing-1 text-color-gray">Call Out Notes</h6>
                        </div>
                        <div class="col-lg-6 mb-3 text-right">
                            <a href="#" data-toggle="modal" data-target="#addNotes" class="btn btn-primary rounded-3 text-color-white text-xxsmall">Add Notes</a>
                        </div>
                        <div class="col-lg-12">
                        <table class="table">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Notes</th>
                              <th scope="col">Call By</th>
                            </tr>
                          </thead>
                          <tbody>
                            @forelse($jobs->notes as $index => $value)
                            <tr>
                              <th scope="row">{{ $index + 1 }}</th>
                              <td>{{ $value->notes }}</td>
                              <td>{{ $value->author ? $value->author->full_name : '' }}</td>
                            </tr>
                            @empty
                                <tr><td colspan="3">No available data</td></tr>
                            @endforelse
                          </tbody>
                        </table>
                        </div>
                    </div>
                </div>  
            </div>
        @endif
        @if($jobs->franchisee_id)
        <div class="row mt-3">
            <div class="col-lg-6 bg-color-white">
                <div class="row">
                    <div class="col-lg-1 mb-3"> 
                        <figure class="avatar150 rounded-circle border mt-3" style="min-width: 50px !important;height: 60px !important;width: 60px !important;">
                            @if($jobs->staff->directory)
                            <img src="{{asset($jobs->staff->directory.'/'.$jobs->staff->filename)}}" alt="user image">
                            @else
                            <img src="{{ asset('frontend/images/default.png') }}" alt="Generic placeholder image" class="">
                            @endif
                        </figure>
                    </div>
                    <div class="col-lg-6 mt-3">
                        <p class="text-color-gray font-semibold ml-3 mt-3">{{$jobs->staff->full_name}} 
                            <span class="mr-2 ml-2">|</span>
                            <span class="text-color-light">Assigned Staff</span>
                        </p>
                    </div>
                     <div class="col-lg-5 mt-3">
                        <p class="text-color-gray font-semibold ml-3 mt-3">
                        	 {{Str::title($jobs->status)}} 
                            <span class="mr-2 ml-2">|</span>
                           	<span class="text-color-light">Job Status</span>
                            
                        </p>
                    </div>
                </div>

            </div>
        </div>
        @endif
    </div>            
</div>          
@stop

<form method="POST" action="{{ route('system.jobs.save_notes') }}">
    {{ csrf_field() }}
    <div class="modal" tabindex="-1" role="dialog" id="addNotes">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Add Call Out Notes</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-color-gray mb-2 {{$errors->first('notes') ? 'has-error' : NULL}}">
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall font-semibold text-uppercase">Notes
                <span class="text-color-red ml-1">*</span>
              </span>
              <input type="hidden" name="job_id" value="{{ $jobs->id }}">
              <textarea class="form-control" name="notes" rows="3" required></textarea>
              @if($errors->first('notes'))
                <span class="help-block">{{$errors->first('notes')}}</span>
              @endif
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Save changes</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
</form>

@section('page-scripts')
<script type="text/javascript">

function initMap() {
  // The location of Uluru
  var uluru = {lat: {{$jobs->latitude}}, lng: {{$jobs->longitude}}};


  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 15, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}

</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOzUJWdUgmPii5xcVAsz2-mBj3V2zHQPM&callback=initMap"></script>

@stop

@section('page-styles')
<style type="text/css">
    footer {
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@stop