<?php 

namespace App\Laravel\Transformers;

use Input,Str;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\CreditScore;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

class OptionTransformer extends TransformerAbstract{

	protected $availableIncludes = [

    ];


	public function transform(CreditScore $score) {

	    return [
	     	'id' => $score->id ?: 0,
	     	'value' => $score->value
	     ];
	}
}