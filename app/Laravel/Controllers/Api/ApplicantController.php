<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Controller;
use App\Laravel\Events\UserAction;
use App\Laravel\Models\UserDevice;
use App\Laravel\Models\Applicant;
use App\Laravel\Models\ApplicantImage;


use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\ApplicantTransformer;
use Auth, JWTAuth, Helper, Str, ImageUploader;

/**
AuditTrail
**/

use App\Laravel\Events\AuditTrailActivity;

use Illuminate\Http\Request;

class ApplicantController extends Controller {

	protected $response = array();

	public function __construct() {
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

    public function index(Request $request, $format = '') {
        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "List of Applicants";

        $applicants = Applicant::orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "APPLICANT_LIST";
        $this->response['has_morepages'] = $applicants->hasMorePages();
        $this->response['data'] = $this->transformer->transform($applicants, new ApplicantTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

	public function store(Request $request, $format = '') {
        // dd($request->all());
    	$user = $request->user();

        $applicant = new Applicant;
        $applicant->fill($request->all());
        $applicant->account_manager_id = $user->id;
        $applicant->status = 'pending';
        $temp_user_id = time();

        if($request->hasFile('ids') && is_array($request->file('ids'))){
            foreach($request->file('ids') as $index => $file) {

                $image = ImageUploader::upload($file, "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'government_id';
                $new_image->save();

                $image_uploaded = true;
            }
        }else{
            if($request->hasFile('ids')) {
                $image = ImageUploader::upload($request->file('ids'), "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'government_id';
                $new_image->save();
            }
        }

        if($request->hasFile('land_title') && is_array($request->file('land_title'))){
            foreach($request->file('land_title') as $index => $file) {

                $image = ImageUploader::upload($file, "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'land_title';
                $new_image->save();

                $image_uploaded = true;
            }
        }else{
            if($request->hasFile('land_title')) {
                $image = ImageUploader::upload($request->file('land_title'), "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'land_title';
                $new_image->save();
            }
        }

        if($request->hasFile('licenses') && is_array($request->file('licenses'))){
            foreach($request->file('licenses') as $index => $file) {

                $image = ImageUploader::upload($file, "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'business_license';
                $new_image->save();

                $image_uploaded = true;
            }
        }else{
            if($request->hasFile('licenses')) {
                $image = ImageUploader::upload($request->file('licenses'), "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'business_license';
                $new_image->save();
            }
        }

        if($request->hasFile('store_photo') && is_array($request->file('store_photo'))){
            foreach($request->file('store_photo') as $index => $file) {

                $image = ImageUploader::upload($file, "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'store_photo';
                $new_image->save();

                $image_uploaded = true;
            }
        }else{
            if($request->hasFile('store_photo')) {
                $image = ImageUploader::upload($request->file('store_photo'), "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'store_photo';
                $new_image->save();
            }
        }

        if($request->hasFile('id_photo') && is_array($request->file('id_photo'))){
            foreach($request->file('id_photo') as $index => $file) {

                $image = ImageUploader::upload($file, "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'id_photo';
                $new_image->save();

                $image_uploaded = true;
            }
        }else{
            if($request->hasFile('id_photo')) {
                $image = ImageUploader::upload($request->file('id_photo'), "uploads/images/users");

                $new_image = new ApplicantImage;
                $new_image->user_id = $temp_user_id;
                $new_image->path = $image['path'];
                $new_image->directory = $image['directory'];
                $new_image->filename = $image['filename'];
                $new_image->reference_type = 'id_photo';
                $new_image->save();
            }
        }

        $applicant->save();

        ApplicantImage::where('user_id',$temp_user_id)->update(['user_id' => $applicant->id]);

        $this->response['msg'] = "Added a new applicant.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CREATE_APPLICANT";
        $this->response['data'] = $this->transformer->transform($applicant, new ApplicantTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

}