<?php

namespace App\Laravel\Controllers\Frontend;

use App\Http\Controllers\Controller;

use App\Laravel\Services\ResponseManager;
use App\Laravel\Models\CleaningService;
use App\Laravel\Models\ServicePackage;
use App\Laravel\Models\OtherService;
use App\Laravel\Models\Postcode;
use App\Laravel\Models\User;
use App\Laravel\Models\Finance;
use App\Laravel\Requests\Frontend\BookingRequest;
use App\Mail\BookingNumber;


use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, DB,Input,Event,PDF,Curl,Signature,Log,Auth,Mail;
class DashboardController extends Controller
{
	// public function __construct () {
	// 	
	// 	parent::__construct();

	// }
  public function index(){
  		$this->data = [];


  		$this->data['cancelled'] = CleaningService::where('status',"cancelled")->where('customer_id',auth()->user()->id)->count();
  		
  		$this->data['ongoing'] = CleaningService::whereIn('status',["ongoing","pending"])->where('customer_id',auth()->user()->id)->count();
  		$this->data['completed'] = CleaningService::where('customer_id',auth()->user()->id)->where('status',"completed")->count();

  		$this->data['booking'] = CleaningService::where('customer_id',auth()->user()->id)->get();
		return view ('frontend.dashboard.index',$this->data);
	}
	

}
