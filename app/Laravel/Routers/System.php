<?php

$this->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "System", 
	'as' => "system.", 
	'prefix'	=> "admin",
	// 'middleware' => "", 

], function(){

	$this->group(['middleware' => ["web","backoffice.guest"]], function(){
		$this->get('register/{_token?}',['as' => "register",'uses' => "AuthController@register"]);
		$this->post('register/{_token?}',['uses' => "AuthController@store"]);
		$this->get('login/{redirect_uri?}',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login/{redirect_uri?}',['uses' => "AuthController@authenticate"]);

		$this->get('sample', ['uses' => "AuthController@sample"]);
		$this->post('sample', ['uses' => "AuthController@sample_store"]);
	});

	$this->get('users/send-login-link/{user_email?}',['as'=>'users.send_login_link','uses' => "UsersController@send_login_link"]);
	$this->post('users/send-login-link-confirm/{redirect_uri?}',['as'=>'users.send_login_link_confirm','uses' => "UsersController@send_login_link_confirm"]);

	$this->group(['middleware' => ["web","backoffice.auth","backoffice.super_user"]], function(){
		
		$this->get('lock',['as' => "lock", 'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		// $this->group(['as' => "account."],function(){
		// 	$this->get('p/{username?}',['as' => "profile",'uses' => "AccountController@profile"]);
		// 	$this->group(['prefix' => "setting"],function(){
		// 		$this->get('info',['as' => "edit-info",'uses' => "AccountController@edit_info"]);
		// 		$this->post('info',['uses' => "AccountController@update_info"]);
		// 		$this->get('password',['as' => "edit-password",'uses' => "AccountController@edit_password"]);
		// 		$this->post('password',['uses' => "AccountController@update_password"]);
		// 	});
		// });

		$this->group([], function() {
			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);


			// $this->group(['prefix' => "subscriber", 'as' => "subscriber."], function () {
			// 	$this->get('/',['as' => "index", 'uses' => "NewsletterSubscriptionController@index"]);
			// });

			// $this->group(['prefix' => "system-account", 'as' => "user."], function () {
			// 	$this->get('/',['as' => "index", 'uses' => "UserController@index"]);
			// 	$this->get('create',['as' => "create", 'uses' => "UserController@create"]);
			// 	$this->post('create',['uses' => "UserController@store"]);
			// 	$this->get('edit/{id?}',['as' => "edit", 'uses' => "UserController@edit"]);
			// 	$this->post('edit/{id?}',['uses' => "UserController@update"]);
			// 	$this->any('delete/{id?}',['as' => "destroy", 'uses' => "UserController@destroy"]);
			// });

			
			$this->group(['prefix'=> "customer-feedback",'as' => 'customer-feedback.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "CustomerFeedbackController@index"]);
				$this->get('/feedback-details/{id?}', ['as' => "feedback-details",'uses' => "CustomerFeedbackController@show"]);
			});

			$this->group(['prefix'=> "users",'as' => 'users.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "UsersController@index"]);
				$this->get('create-user', ['as' => "create-user",'uses' => "UsersController@create"]);
				$this->post('create-user',['uses' => "UsersController@store"]);
				$this->get('edit-user/{id?}',['as' => "edit", 'uses' => "UsersController@edit"]);
				$this->post('edit-user/{id?}',['uses' => "UsersController@update"]);
				$this->get('user-profile/{id?}', ['as' => "user-profile",'uses' => "UsersController@show"]);
				$this->any('delete/{id?}', ['as' => "destroy",'uses' => "UsersController@destroy"]);
				$this->post('user-profile/{id?}', ['as' => "update_location",'uses' => "UsersController@update_location"]);
				$this->get('work-load/{id?}', ['as' => "work-load",'uses' => "UsersController@showWorkload"]);
				$this->post('work-load/{id?}',['uses' => "UsersController@update_workload"]);
				$this->get('profile/{id?}', ['as' => "profile",'uses' => "UsersController@profile"]);
				$this->post('profile/{id?}',['uses' => "UsersController@update_profile"]);
			});

			$this->group(['prefix'=> "finance",'as' => 'finance.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "FinanceController@index"]);
				$this->get('finance-history', ['as' => "finance-history",'uses' => "FinanceController@show"]);
				$this->get('create', ['as' => "create",'uses' => "FinanceController@create"]);
				$this->post('create',['uses' => "FinanceController@store"]);
				$this->get('print', ['as' => "print",'uses' => "FinanceController@print"]);
			});

			$this->group(['prefix'=> "media-library",'as' => 'media-library.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "MediaLibraryController@index"]);
				$this->get('create',['as' => "create", 'uses' => "MediaLibraryController@create"]);
				$this->post('create',['uses' => "MediaLibraryController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "MediaLibraryController@edit"]);
				$this->post('edit/{id?}',['uses' => "MediaLibraryController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "MediaLibraryController@destroy"]);
				$this->get('download',['as' => "download", 'uses' => "MediaLibraryController@download"]);
				$this->get('image',['as' => "image", 'uses' => "MediaLibraryController@view_image"]);
			});

			$this->group(['prefix'=> "audit-trail",'as' => 'audit-trail.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "AuditTrailController@index"]);
				$this->get('show/{id?}', [ 'as' => "show",'uses' => "AuditTrailController@show"]);
				$this->get('print/{id?}', [ 'as' => "print",'uses' => "AuditTrailController@print"]);
			});

			$this->group(['prefix'=> "invoice",'as' => 'invoice.' ],function(){
				$this->get('/', [ 'as' => "index",'uses' => "InvoiceController@index"]);
				$this->get('show/{id?}', [ 'as' => "show",'uses' => "InvoiceController@show"]);
				$this->get('print/{id?}', [ 'as' => "print",'uses' => "InvoiceController@print"]);
				$this->get('send/{id?}', [ 'as' => "send",'uses' => "InvoiceController@send_invoice"]);
				$this->get('download/{id?}', [ 'as' => "download",'uses' => "InvoiceController@download"]);
				$this->get('update/{id?}', [ 'as' => "update",'uses' => "InvoiceController@update_status"]);
				$this->get('excel/{id?}', [ 'as' => "excel",'uses' => "InvoiceController@excel"]);
			});

			$this->group(['prefix'=> "jobs",'as' => 'jobs.' ],function(){
			$this->get('/', [ 'as' => "index",'uses' => "JobsController@index"]);
			$this->get('cancelled', [ 'as' => "cancelled",'uses' => "JobsController@cancelled"]);
			$this->get('show/{jobs?}', [ 'as' => "show",'uses' => "JobsController@show"]);
			$this->get('details/{id?}', [ 'as' => "details",'uses' => "JobsController@details"]);
			$this->post('details/{id?}', [ 'as' => "save_details",'uses' => "JobsController@save_details"]);
			$this->get('assigned/{id?}', [ 'as' => "assigned",'uses' => "JobsController@assigned"]);
			$this->get('remove/{id?}', [ 'as' => "remove",'uses' => "JobsController@remove_staff"]);
			$this->get('unserviced/{id?}', [ 'as' => "unserviced",'uses' => "JobsController@unserviced"]);
			//$this->post('assigned/{id?}', [ 'as' => "save_assigned",'uses' => "JobsController@save_assigned"]);
			$this->get('completed/{id?}', [ 'as' => "completed",'uses' => "JobsController@complete_booking"]);
			$this->get('cancel/{id?}', [ 'as' => "cancel",'uses' => "JobsController@cancel_quote"]);
			$this->any('get-room',['as' => "get_rooms", 'uses' => "JobsController@get_rooms"]);
			$this->any('invoice/{id?}',['as' => "invoice", 'uses' => "JobsController@generate_invoice"]);
			$this->post('notes', [ 'as' => "save_notes",'uses' => "JobsController@save_notes"]);
			});

			$this->group(['prefix'=> "services",'as' => 'services.' ],function(){
				$this->any('/', [ 'as' => "index",'uses' => "ServicesController@index"]);
				
				$this->get('create',['as' => "create", 'uses' => "ServicesController@create"]);
				$this->post('create',['uses' => "ServicesController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ServicesController@edit"]);
				$this->post('edit/{id?}',['uses' => "ServicesController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ServicesController@destroy"]);
				$this->any('save/{id?}',['as' => "save", 'uses' => "ServicesController@update_day"]);
			

			});

			$this->group(['prefix'=> "sub-service",'as' => 'sub_service.' ],function(){
				$this->any('/{id?}', [ 'as' => "index",'uses' => "SubServiceController@index"]);
				$this->get('create/{service_id?}',['as' => "create", 'uses' => "SubServiceController@create"]);
				$this->post('create/{service_id?}',['uses' => "SubServiceController@store"]);
				$this->get('edit/{id?}/{service_id?}',['as' => "edit", 'uses' => "SubServiceController@edit"]);
				$this->post('edit/{id?}/{service_id?}',['uses' => "SubServiceController@update"]);
				$this->any('delete/{id?}/{service_id?}',['as' => "destroy", 'uses' => "SubServiceController@destroy"]);
				$this->any('save/{id?}/{service_id?}',['as' => "save", 'uses' => "SubServiceController@update_day"]);
			

			});
	
			$this->group(['prefix'=> "contract",'as' => 'contract.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "ContractsController@index"]);	
				$this->get('create', ['as' => "create",'uses' => "ContractsController@create"]);
				$this->post('create',['uses' => "ContractsController@store"]);
			});

			$this->group(['prefix'=> "contract-group",'as' => 'contract-group.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "ContractGroupController@index"]);	
				$this->get('list/{id?}', ['as' => "list",'uses' => "ContractGroupController@contactList"]);	
				$this->get('show/{id?}', ['as' => "show",'uses' => "ContractGroupController@show"]);
				$this->get('create/{id?}', ['as' => "create",'uses' => "ContractGroupController@create"]);
				$this->post('create/{id?}',['uses' => "ContractGroupController@store"]);
				$this->get('edit/{id?}',['as' => "edit", 'uses' => "ContractGroupController@edit"]);
				$this->post('edit/{id?}',['uses' => "ContractGroupController@update"]);
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ContractGroupController@destroy"]);
				$this->any('download/{id?}',['as' => "download", 'uses' => "ContractGroupController@download"]);
				$this->any('approved/{id?}',['as' => "approved", 'uses' => "ContractGroupController@approved_contract"]);

			});


			$this->group(['prefix'=> "about-us",'as' => 'about-us.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "AboutUsController@index"]);	
				$this->get('create', ['as' => "create",'uses' => "AboutUsController@create"]);	
				$this->post('create', ['uses' => "AboutUsController@store"]);	
				$this->get('edit/{id}', ['as' => "edit",'uses' => "AboutUsController@edit"]);	
				$this->post('edit/{id}', ['uses' => "AboutUsController@update"]);	
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "AboutUsController@destroy"]);
			});

			$this->group(['prefix'=> "gallery",'as' => 'gallery.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "GalleryController@index"]);	
				$this->get('create', ['as' => "create",'uses' => "GalleryController@create"]);	
				$this->post('create', ['uses' => "GalleryController@store"]);	
				$this->get('edit/{id}', ['as' => "edit",'uses' => "GalleryController@edit"]);	
				$this->post('edit/{id}', ['uses' => "GalleryController@update"]);	
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "GalleryController@destroy"]);
			});

			$this->group(['prefix'=> "feedback",'as' => 'feedback.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "FeedbackController@index"]);	
				$this->get('create', ['as' => "create",'uses' => "FeedbackController@create"]);	
				$this->post('create', ['uses' => "FeedbackController@store"]);	
				$this->get('edit/{id}', ['as' => "edit",'uses' => "FeedbackController@edit"]);	
				$this->post('edit/{id}', ['uses' => "FeedbackController@update"]);	
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "FeedbackController@destroy"]);
			});

			$this->group(['prefix'=> "video-content",'as' => 'video-content.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "VideoContentController@index"]);	
				$this->get('create', ['as' => "create",'uses' => "VideoContentController@create"]);	
				$this->post('create', ['uses' => "VideoContentController@store"]);	
				$this->get('edit/{id}', ['as' => "edit",'uses' => "VideoContentController@edit"]);	
				$this->post('edit/{id}', ['uses' => "VideoContentController@update"]);	
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "VideoContentController@destroy"]);
			});

			$this->group(['prefix'=> "title",'as' => 'title.' ],function(){
				$this->get('/', ['as' => "index",'uses' => "TitleController@index"]);	
				$this->get('create', ['as' => "create",'uses' => "TitleController@create"]);	
				$this->post('create', ['uses' => "TitleController@store"]);	
				$this->get('edit/{id}', ['as' => "edit",'uses' => "TitleController@edit"]);	
				$this->post('edit/{id}', ['uses' => "TitleController@update"]);	
				$this->any('delete/{id?}',['as' => "destroy", 'uses' => "TitleController@destroy"]);
			});
		
			$this->group(['prefix'=> "link",'as' => 'link.' ],function(){
				$this->get('create', ['as' => "create",'uses' => "LinkController@create"]);	
				$this->post('create', ['uses' => "LinkController@store"]);	
			});
		});
	});
});