@extends('frontend._layout.main')
<style type="text/css">
	footer {
        position: absolute;
		margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 9% !important;
    background-color: #ECF5F5 !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('frontend._components.topnav-main')
<div class="wrapper mt-5 pt-5 mt-lg-5 pt-lg-0 mt-xl-0 pt-xl-0">
	<div class="container">
		<div class="row">
            <div class="col-lg-4">
                <div class="bg-color-white card-shadow p-3">
                    <div class="row">
                        <i class="fas fa-check-circle fa-2x text-success" style="position: absolute; top: 50%;right: 10%;"></i>
                        <div class="col-lg-12">
                            <span class="text-xsmall font-semibold">Total Done Transaction</span>
                            <p class="font-weight-bold text-color-red text-xlarge">{{$completed}}</p>
                        </div>

                    </div>
                </div>
            </div>
             <div class="col-lg-4">
                <div class="bg-color-white card-shadow p-3">
                    <div class="row">
                        <i class="fas fa-exclamation-circle fa-2x text-warning" style="position: absolute; top: 50%;right: 10%;"></i>
                        <div class="col-lg-12">
                            <span class="text-xsmall font-semibold">Total Pending Transaction</span>
                            <p class="font-weight-bold text-color-red text-xlarge">{{$ongoing}}</p>
                        </div>

                    </div>
                </div>
            </div>
             <div class="col-lg-4">
                <div class="bg-color-white card-shadow p-3">
                    <div class="row">
                        <i class="fas fa-times-circle fa-2x text-danger" style="position: absolute; top: 50%;right: 10%;"></i>
                        <div class="col-lg-12">
                            <span class="text-xsmall font-semibold">Total Cancel Transaction</span>
                            <p class="font-weight-bold text-color-red text-xlarge">{{$cancelled}}</p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-12 mt-5">
                <p class="font-weight-bold spacing-1 text-color-gray text-small">Booking Transaction</p>
            </div>
            <div class="col-lg-12 mb-5">
    			<table class="table bg-color-white card-shadow1 table table-hover">
                    <thead>
                        <tr>
                            <th class="p-4 text-xsmall spacing-1 text-color-gray text-left font-weight-bold">Booking ID</th>
                            <th class="p-4 text-xsmall spacing-1 text-color-gray text-left font-weight-bold" data-breakpoints="xs">Date Created</th> 
                            <th class="p-4 text-xsmall spacing-1 text-color-gray text-left font-weight-bold" data-breakpoints="xs">Status</th>   
                        </tr>
                        <tbody>

                            @forelse($booking as $data)
                            <tr>
                                <td class="text-color-gray font-semibold p-4">{{ $data->booking_id }}</td>
                                <td class="text-color-gray font-semibold p-4 text-nowrap">{{ date('M d,Y ', strtotime($data->created_at)) }}</td>
                                <td class="p-4">
                                @if($data->status == "completed")
                                    <span class="text-color-white bg-success p-2 font-semibold rounded text-xsmall">{{Str::title($data->status)}}</span>
                                @elseif($data->status == "pending")
                                    <span class="text-color-white bg-warning p-2 font-semibold rounded text-xsmall">{{Str::title($data->status)}}</span>
                                @elseif($data->status == "ongoing")
                                    <span class="text-color-white bg-secondary p-2 font-semibold rounded text-xsmall">{{Str::title($data->status)}}</span>
                                @else
                                    <span class="text-color-white bg-danger p-2 font-semibold rounded text-xsmall">{{Str::title($data->status)}}</span>
                                @endif  
                                  
                                </td>
                            </tr>
                            @empty
                            <tr>
                         
                            <td class="text-left p-4 font-semibold">No Transaction</td>
                          
                                </tr>
                            @endforelse
                           
                             
                        </tbody>
                    </thead>
                               
                </table>      
                </div>              
		</div>
	</div>
</div>
         
@stop


@section('page-styles')
<style type="text/css">
    .custom-nav {
    -webkit-box-shadow: 2px 2px 2px #212121 !important; 
    -moz-box-shadow:    2px 2px 2px #212121 !important; 
    box-shadow:         2px 2px 2px #212121 !important; 
    z-index:999;
}
</style>
@stop


