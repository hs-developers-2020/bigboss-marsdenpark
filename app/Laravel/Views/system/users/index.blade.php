@extends('system._layouts.main')
<style type="text/css">
    footer {
        margin-top: 25px !important;
        
    }
    body {
    	padding-top: 9% !important;
    }
    .f-10 {
        font-size: 8px;
    }

</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
	<div class="container ">
        <form action="" id="search_filter" >
        	<div class="row">
        		<div class="col-lg-2 pt-2">
                    <h6 class="text-uppercase text-xsmall text-color-gray">Users 
                        @if(Auth::user()->type == 'super_user')
                        (Big Boss Franchisor)
                        @else
                        (Big Boss {{Str::title(str_replace("_" , " " ,Auth::user()->type))}})
                        @endif
                    </h6>
                </div>
                
                <div class="col-lg-3">     
                    @if(in_array(Auth::user()->type, ['head','super_user']))
                    {!!Form::select('user_type',$users_type,old('user_type',$user_type),['class' => "form-control text-color-blue font-semibold pl-3 pr-3 input-xs", 'id' => "select_group"])!!}
                    @endif
                </div>
                
                <div class="col-lg-7 d-flex flex-row mt-1">   
                    <input type="text" class="searchTerm" name="keyword" value="{{$keyword}}" placeholder="Search keyword such as name">
                    <a href="#"  class="btn bg-color-blue text-color-white searchButton" id="button_search">
                        <i class="fa fa-search"></i>
                    </a>
                    <a  href="{{ route('system.users.create-user') }}" style="padding-left: 10px;" class="no-decoration"><button class="btn bg-color-blue text-xxxsmall text-color-white text-uppercase" type="button"><i class="fas fa-plus text-color-white mr-2"></i>Add user</button></a>
                </div>       
        	</div>
        </form>        
          <table class="table bg-color-white  mb-0 ">
            <thead>      
                <tr>
                    <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Avatar</th>
                    <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">User ID</th>
                    @if(in_array(Str::lower(Auth::user()->type), ['super_user','head']))
                    <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">Position</th>
                    @endif
                    <th class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm" >Name</th>
                    <th  class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Region</th>                                                                   
                    <th  class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Email address</th>
                    <th  class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Contact Number</th>
                     <th  class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Rating</th>
                    <th  class="p-3 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($users as $index => $value)
                <tr>
                    <td class="userlist p-4">
                       <div class="media">
                       
                        <a href="{{ route('system.users.user-profile',[$value->id]) }}">
                            <div class="figure avatar40 border rounded-circle align-self-start">
                                @if($value->directory)
                                <img src="{{asset($value->directory.'/'.$value->filename)}}" alt="Generic placeholder image" class="">
                                @else
                                 <img src="{{ asset('frontend/images/default.png') }}" alt="Generic placeholder image" class="">
                                @endif
                            </div>
                        </a>                                                                             
                        </div>
                    </td>
                    <td>
                        <h5 class="text-color-gray p-0">{{$value->id}}</h5>
                    </td>
                    @if(in_array(Str::lower(Auth::user()->type), ['super_user','head']))
                    <td>
                        <h5 class="text-color-gray p-0">{{Str::Title(str_replace("_"," ",$value->type))}}</h5>
                    </td>
                    @endif
                     <td>
                        <h5 class="text-color-gray p-0">{{$value->full_name}}</h5>
                    </td>
                    <td>
                         <h5 class="text-color-gray p-0">{{$value->info ? $value->info->state : ''}}</h5>
                    </td>
                    <td>
                        <h5 class="text-color-gray p-0">{{$value->email}}</h5>
                    </td>
                    <td>
                        <h5 class="text-color-gray p-0">{{$value->contact_number}}</h5>
                    </td>
                    <td></td>
                    <td>
                       <div class="dropdown">
                          <span>
                            <i class="fas fa-circle f-10"></i>
                            <i class="fas fa-circle f-10"></i>
                            <i class="fas fa-circle f-10"></i></span>
                          <div class="dropdown-content">
                            <div class="d-flex flex-column mt-1">
                                <a href="{{ route('system.users.edit',[$value->id]) }}" style="font-size: 16px;text-decoration: none;" class="mb-2 text-color-light font-semibold">EDIT</a>
                                <a class="btn-destroy mb-2 text-color-light font-semibold" data-url="{{ route('system.users.destroy',[$value->id]) }}" href="#"><i class="mdi mdi-delete"></i> <span>REMOVE</span></a>
                               
                            </div>
                          </div>
                        </div>
                    </td> 
                </tr> 
                @empty
                <tr>
                  <td>No Record</td>
                </tr>
                @endforelse       
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="9">
                        <div class="float-right">
                           {{ $users->appends(['keyword' => $keyword,'user_type' => $user_type])->render() }}
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>                   
	</div>
</div>
		<!-- main container ends -->

		
		<!-- sidebar right ends -->


@stop



@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
    @endif

    $("#button_search").on("click",function(e){
      $("#search_filter").submit();
    });

    $('#select_group').on('change', function(){
        $('#search_filter').get(0).submit();
    });

    $(".table")
        .delegate('.btn-destroy','click', function(){
            var url = $(this).data('url');
            swal({   
                title: "Are you sure?",   
                text: "You will not be able to recover this record.",   
                type: "warning",   
                showCancelButton: true,   
                confirmButtonColor: "#3085d6",   
                confirmButtonText: "Yes, delete it!"
            }).then(function(result) {
                window.location.href = url;
            });
        });


});


</script>

@stop
