<div class="footer-bottom" style=" position: relative;  width: 100%">
<footer class="container "style="background-color: transparent;">
  <div class="row">
      <div class="col-lg-6 col-sm-12">
        <h4 class="pt-lg-4 mt-lg-4 pt-md-4 mt-md-4 mobile-margin-footer-reserved  text-xsmall">
        <script>document.write(new Date().getFullYear());</script> Big Boss Group. All rights reserved | <a class="no-decoration text-color-gray" href="{{route('frontend.terms')}}">Terms of Use</a> | <a class="no-decoration text-color-gray" href="{{route('frontend.privacy')}}">Privacy Policy</a></h4>
      </div>
      <div class="col-lg-6 col-sm-12 text-lg-right text-center text-md-right">
          <h4 class=" pt-lg-4 mt-lg-4 pt-md-4 mt-md-4 text-color-gray text-xsmall">Connect with us
            <a href="https://www.facebook.com/BigBossGroupAU/" class="ml-2 fab fa-facebook" style="text-decoration:none;font-size: 20px; color: #4C60A2 !important;" target="_blank"></a>
            <a href="https://www.instagram.com/BigBossGroupAU/" class="fab fa-instagram ml-2" style="text-decoration:none;font-size: 20px; color: #ED7EAC !important;" target="_blank"></a>
            <a href="https://twitter.com/BigBossGroupAU1" class="fab fa-twitter ml-2" style="text-decoration:none;font-size: 20px; color: #127CC2 !important;" target="_blank"></a>
{{--                 <i class="fas fa-phone-square ml-2" style="font-size: 20px; color: #9A171F;"></i>
 --}}        <a class="fas fa-envelope-square ml-2 mr-2" href="mailto:info@bigbossgroup.com.au?Subject" style="text-decoration:none;font-size: 20px; color: #9A171F !important;" target="_blank"></a>
              </h4>
      </div>
  </div>
</footer>
</div>