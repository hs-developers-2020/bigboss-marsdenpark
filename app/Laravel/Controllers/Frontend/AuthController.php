<?php

namespace App\Laravel\Controllers\Frontend;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Laravel\Models\User;
use Illuminate\Http\Request;

use App\Laravel\Services\ResponseManager;
use App\Laravel\Models\PasswordReset;
use App\Laravel\Requests\Frontend\CustomerUserRequest;
use App\Laravel\Requests\Frontend\ForgotRequest;
use App\Laravel\Notifications\SendEmailVerificationToken;
use App\Laravel\Notifications\SendResetToken;

use Session, Input, Auth,Carbon,Helper, Image,Str;
class AuthController extends Controller
{
	public function __construct(Guard $auth){
		$this->auth = $auth;
	}

  	public function index(){
		return view ('frontend.auth.index');
	}

	public function create(){

		return view ('frontend.auth.create');
	}

	public function verify(){
		return view ('frontend.page.verify-account.index');
	}

	public function success(){
		return view ('frontend.page.successful-verify.index');
	}

	public function store(CustomerUserRequest $request,$_token = NULL){
		// dd($request->all());

		$new_user = new User;
		$new_user->fill($request->only('firstname', 'lastname','contact_number', 'username', 'email'));
		$new_user->password = bcrypt($request->get('password'));
		if($new_user->save()){
			// $this->auth->loginUsingId($new_user->id,true);
			session()->flash('notification-status','success');
			session()->flash('notification-msg',"Welcome {$new_user->name}!");
			return redirect()->route('frontend.login');
		}else{
			session()->flash('notification-status','success');
			session()->flash('notification-msg','Successfully registered.');
			goto callback;
		}
/*=======
		// if($_token){
			// $_token = base64_decode($_token);
			// if(Helper::date_db(Carbon::now()) <= $_token){
				// MjAxOC0wMS0wOQ==
				$new_user = new User;
			 $new_user->fill($request->only('name','lastname','contact_number','username','email'));
				$new_user->password = bcrypt($request->get('password'));
				// $new_user->is_employee = "yes";
				$new_user->save();

				if($request->has('auto_login')){
					$this->auth->loginUsingId($new_user->id,true);
					session()->flash('notification-status','success');
					session()->flash('notification-msg',"Welcome {$new_user->name}!");
					return redirect()->route('frontend.login');
				}else{
					session()->flash('notification-status','success');
					session()->flash('notification-msg','Successfully registered.');
					goto callback;
				}
			// }
		// }
>>>>>>> f82ada3a3a7285c918d3537dcf7469a0e1373c71*/


		callback:
		return redirect()->route('frontend.login');
	}


	public function authenticate($redirect_uri = NULL){
		try{
			$this->data['page_title'] =  "Login";
			$username = Input::get('username');
			$password = Input::get('password');
			$remember_me = Input::get('remember_me',0);
			$field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
			
			$user = User::where($field,$username)
					->whereIn('type', ['user'])
					->first();

			if(!$user){
				session()->flash('notification-status', "failed");
				session()->flash('notification-msg', "Invalid username/password");
				return redirect()->back();
			}	

			if($this->auth->attempt([$field => $username,'password' => $password], $remember_me)){
				session()->flash('notification-status','success');
				session()->flash('notification-title',"It's nice to be back");
				session()->flash('notification-msg',"Welcome {$this->auth->user()->name}!");
				$this->auth->user()->save();
				if($redirect_uri AND session()->has($redirect_uri)){
					return redirect( session()->get($redirect_uri) );
				}
				

				return redirect()->route('frontend.dashboard');
			}	

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Wrong username or password.');
			return redirect()->back();

		}catch(Exception $e){
			abort(500);
		}
	}

	public function forgot_password(ForgotRequest $request, $email = NULL){

		$existing = User::where("email",$request->get('reset_email'))->where('type',"user")->first();

		if(!$existing){
			session()->flash('notification-status',"warning");
			session()->flash('error','failed');
			session()->flash('notification-msg',"Email does not exist.");
			goto callback;
    	}

		$token = Str::upper($this->_generateEmailVerificationToken());

        $existing->notify(new SendResetToken($token, ['email' => $existing->email]));
        $this->_saveResetToken($existing, $token);

        session()->flash('notification-status',"success");
		session()->flash('notification-msg',"Reset password request successfully sent.");
		goto callback;

        callback:
		return redirect()->back();
	}

	 private function _saveResetToken(User $user, $token) {
		PasswordReset::where('email', $user->email)->delete();

		$password_reset = new PasswordReset;
		$password_reset->email = $user->email;
		$password_reset->token = $token;
		$password_reset->created_at = Carbon::now();
		$password_reset->save();
	}

	private function _generateEmailVerificationToken() {
        $key = config('app.key');
        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }
        return Str::random(6);
    }
}
