<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableServicePackageAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_packages', function($table){
            $table->string('service_type')->nullable()->after('service_name');
            $table->string('property_type')->nullable()->after('service_type');
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_packages', function($table){
            $table->dropColumn(['service_type','property_type']);
        });
    }
}
