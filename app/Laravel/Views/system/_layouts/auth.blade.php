<!DOCTYPE html>
<html lang="en">
  
<head>
    @include('system._components.metas')
    @include('system._components.styles')
  </head>
  <body>
      
      @yield('content')

    @include('system._components.scripts')
  </body>
</html>