@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper" style="margin-top: -30px;">
	<div class="container ">
        <form action="" id="search_filter" >
            <div class="row">
                 <div class="col-lg-9">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="text-color-gray mb-2">  
                                    <span class="ml-2 text-xxsmall font-semibold text-uppercase">From</span>
                                    <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                    <input type="text" class="form-control input-sm datepick" name="from" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$from}}" id="date_range_from">
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="text-color-gray mb-2">
                                    <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                    <span class="ml-2 text-xxsmall font-semibold text-uppercase">To</span>
                                    <input type="text" class="form-control input-sm datepick" name="to" data-min-view="2" data-date-format="yyyy-mm-dd"  value="{{$to}}" id="date_range_to">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 ">
                            <div class="form-group">
                                <div class="text-color-gray mb-2">
                                    <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                    <span class="ml-2 text-xxsmall font-semibold text-uppercase">Type</span>
                                    {!!Form::select('status',$payment_status,old('status',$status),['class' => "form-control text-xsmall input-sm select2", 'id' => "status"])!!}
                                </div>
                            </div>
                        </div>
                      
                        <div class="col-md-2">
                            <div class="btn-group pt-4" role="group" aria-label="Basic example" >
                                <a  class="w-100 btn bg-color-blue text-xxsmall text-color-white" id="search_button"><i class="mdi mdi-search"></i> FILTER </a>
                                <a  class="w-100 btn bg-color-blue text-xxsmall text-color-white" href="{{ route('system.invoice.index') }}"><i class="mdi mdi-search"></i> CLEAR </a>
                            </div>     
                        </div>
                    </div> 
                </div>      
                <div class="col-lg-3 mt-4">
                    <a href="{{ route('system.invoice.excel') }}?&from={{$from}}&to={{$to}}&status={{$status}}" class="btn bg-color-blue text-xxsmall text-color-white" style="float: right;"><i class="fas fa-file mr-2 text-color-white"></i>Generate Report (csv)</a>
                </div>
            </div>
        </form>
      
  
		 
        <table class="table bg-color-white  mb-0 footable">
            <thead>      
                <tr>
                    <th class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Invoice Number</th>
                    <th class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">Date</th>
                    <th class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm" >Customer Name</th>                                                                  
                    <th  class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">Amount</th>
                    <th  class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="">Status</th>
                    <th  class="p-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($invoice as $index)
                <tr>
                    <td>
                        <h5 class="text-color-blue p-3"><span class="text-color-blue font-semibold">#{{$index->invoice_number}}</span></h5>
                    </td>
                    <td>
                        <h5 class="text-color-gray p-3">{{Helper::date_format($index->created_at)}} </h5>
                    </td>
                     <td>
                         <h5 class="text-color-blue p-3">{{Str::title($index->invoice_to)}}</h5>
                    </td>
                   
                    <td>
                        <h5 class="text-color-blue p-3">$ {{Helper::amount($index->amount)}}</h5>
                    </td>
                     <td>
                        <h5 class="text-color-blue p-3">{{Str::title($index->status)}}</h5>
                    </td>
                    <td>
                        <a href="{{ route('system.invoice.show',[$index->id]) }}" class="text-color-white text-uppercase" style="text-decoration: none;"><button class="btn bg-color-blue text-color-white text-xxsmall mr-2 text-uppercase">View</button></a>
                         
                    </td>
                   
                </tr>
                @empty
                <tr>
                    <td>No Record Available</td>
                </tr>
                @endforelse
            </tbody>
        </table>                    
	</div>
</div>
@stop

@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/daterangepicker/css/daterangepicker.css')}}"/>
<style type="text/css">
    footer {
         margin-top: 25px !important;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1030;
        }
    body {
        padding-top: 9% !important;

    }
    .pull-right{
        float: right!important;
    }

    .daterangepicker.drop-menu{
        width: 0px !important;
    }


</style>
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/daterangepicker/js/daterangepicker.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>

<script>
 $(function(){
    

    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
    @endif
     $('#search_button').on('click', function(){
      console.log("akhjdhjd")
      $('#search_filter').get(0).submit();
    });


    $('#date_range_from, #date_range_to').daterangepicker({
        singleDatePicker: true,
        autoApply: true,
        timePicker: false,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
});

</script>
@stop