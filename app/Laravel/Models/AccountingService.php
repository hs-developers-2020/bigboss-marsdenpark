<?php

namespace App\laravel\Models;

use Illuminate\Database\Eloquent\Model;

class AccountingService extends Model
{
    protected $fillable = [
    					'postcode',
    					'location',
    					'property_type',
    					'services',
    					'booking_id',
    					'name',
    					'lastname',
    					'email',
    					'contact',
    					'booking_date',
    					'time_start',
    					'time_end',
    					'booking_date_secondary',
    					'secondary_time_start',
    					'secondary_time_end',
                        'longtitude',
                        'latitude',
                        'region'
    					];
                        public $timestamps = true;
}
