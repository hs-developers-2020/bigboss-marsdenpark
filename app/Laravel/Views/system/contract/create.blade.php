@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
  <div class="container">
    <h6 class="text-xsmall text-color-gray text-uppercase pb-3">Add new group</h6>
  </div>
  <form action="" class="with-confirmation" method="POST" enctype="multipart/form-data">
    {!!csrf_field()!!}
    <div class="container pb-5 pl-5 pr-5 pt-4 mt-2" style="background-color: white; ">       
      <div class="row">

        <div class="col-lg-12 mt-2 mb-2">
          <p class="text-color-gray text-xsmall font-semibold text-uppercase">Group Information</p>
        </div>

        <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('business_name') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Business Name</span>
            <input type="text"  placeholder="Enter business name" class="form-control text-xsmall" name="business_name" value="{{ old('business_name') }}">
          </div>
          @if($errors->first('business_name'))
            <span class="help-block" style="color:red;">{{$errors->first('business_name')}}</span>
          @endif
        </div>  

        <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('business_address') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Business Address</span>
            <input type="text"  placeholder="Enter business address" class="form-control text-xsmall" name="business_address" value="{{ old('business_address') }}">
          </div>
          @if($errors->first('business_address'))
            <span class="help-block" style="color:red;">{{$errors->first('business_address')}}</span>
          @endif
        </div>
        <div class="col-lg-12 mt-4 mb-2">
          <p class="text-color-gray text-xsmall font-semibold text-uppercase">Contract Information</p>
        </div>
        <div class="col-lg-12 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('file') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">User File 
              <span class="text-color-red">*</span>
            </span>
          </div>
          <div class="d-flex flex-row mb-3 mt-2">
            <input type="file" id="file" name="file"/> 
          </div>
          @if($errors->first('file'))
            <span class="help-block" style="color:red;">{{$errors->first('file')}}</span>
          @endif
        </div>

        <div class="col-lg-4 mt-1 ">
          <div class="text-color-gray mb-2 {{$errors->first('file_name') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">File Name</span>
            <input type="text"  placeholder="Enter file name" class="form-control text-xsmall" name="file_name" value="{{ old('file_name') }}">
          </div>
          @if($errors->first('file_name'))
            <span class="help-block" style="color:red;">{{$errors->first('file_name')}}</span>
          @endif
        </div>

        <div class="col-lg-4 mt-1 ">
          <div class="text-color-gray mb-2 {{$errors->first('file_label') ? 'has-error' : NULL}}">
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">File Label</span>
            <input type="text"  placeholder="Enter file label" class="form-control text-xsmall" name="file_label" value="{{ old('file_label') }}">
          </div> 
          @if($errors->first('file_label'))
            <span class="help-block" style="color:red;">{{$errors->first('file_label')}}</span>
          @endif
        </div>
        
        <div class="col-lg-12 mt-4">
          <button type="submit" class="btn bg-color-blue text-xxsmall text-color-white p-2 pl-3 pr-3"> ADD NEW GROUP
              <i class="ml-2 fas fa-chevron-right text-color-white" style="font-size: 10px;"></i>
              <i class="fas fa-chevron-right text-color-white" style="font-size: 10px;"></i> 
          </button>
        </div> 
      </div>
    </div>
  </form>
</div>

@include('system._components.footer')     
@stop

@section('page-styles')
<style type="text/css">
  footer {
   position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
  }
body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@stop