<?php


namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\MediaLibrary;




/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\IconRequest;
use App\Laravel\Events\AuditTrailActivity;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use Helper, Carbon, Str, Auth, ImageUploader,FileUploader,File,DB,AuditRequest,Event;


class MediaLibraryController extends Controller
{	
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"Choose Service",'cleaning' => "Cleaning","information_technology" => "Information Technology","accounting" => "Accounting"];
	
		
	}

   	public function index(PageRequest $request){
  		$this->data['group'] = $request->get('group',false);
  		$this->data['keyword'] = $request->get('keyword', NULL);

  		$this->data['icons'] = MediaLibrary::group($this->data['group'])->orderBy('created_at' , "DESC")->paginate(10);

		return view ('system.media-library.index',$this->data);
	}

	 public function create(){
		return view ('system.media-library.create',$this->data);

	}

	public function store (IconRequest $request) {

		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$new_icon = new MediaLibrary;
			$new_icon->fill($request->all());
			

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/media-library");
			    $new_icon->path = $image['path'];
			    $new_icon->directory = $image['directory'];
			    $new_icon->filename = $image['filename'];
			}

			if($new_icon->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ICON_CREATED", 'remarks' => Auth::user()->full_name." has successfully created {$new_icon->name} icon information. [{$new_icon->group}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.media-library.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}

	public function edit ($id = NULL) {
	
		$icons = MediaLibrary::find($id);
	
		
		if (!$icons) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.media-library.index');
		}

		$this->data['icons'] = $icons;
		return view('system.media-library.edit',$this->data);
	}

	public function update (IconRequest $request, $id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$edit_icons = MediaLibrary::find($id);
			$edit_icons->fill($request->all());

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/media-library");
			    $edit_icons->path = $image['path'];
			    $edit_icons->directory = $image['directory'];
			    $edit_icons->filename = $image['filename'];
			}
			if (!$edit_icons) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.media-library.index');
			}

			

			if($edit_icons->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ICON_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$edit_icons->name} service information. [{$edit_icons->group}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.media-library.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$icons = MediaLibrary::find($id);

			if (!$icons) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.media-library.index');
			}

			if($icons->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "ICON_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$icons->name} icon information. [{$icons->group}]",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.media-library.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	public function download () {
        $file_path = "frontend/button-icons/icon_guide.psd";
        return response()->download($file_path);
	}
	public function view_image () {
		
        $file_path = "frontend/button-icons/icon_guide.png";
        return response()->file($file_path);
	}

}
