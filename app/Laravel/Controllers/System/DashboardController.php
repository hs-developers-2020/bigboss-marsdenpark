<?php

namespace App\Laravel\Controllers\System;

use App\Laravel\Models\CustomerFeedback;
use App\Laravel\Models\User;
use App\Laravel\Models\CleaningService;


use App\Laravel\Services\ResponseManager;



use Helper, Carbon, Session, Str,Input,Auth,Curl;

class DashboardController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

 	public function index(){


      $startDate = Carbon::now()->startOfWeek();
      $endDate = Carbon::now()->endOfWeek();

      $month = Carbon::now()->subMonth(1)->toDateTimeString();
      $day = Carbon::now()->subDay(1)->toDateTimeString();
      $hour = Carbon::now()->subHours(2)->toDateTimeString();
      $minutes = Carbon::now()->subMinutes(10)->toDateTimeString();

      $this->data['two_hours']  = CleaningService::where('created_at' , '>' ,$hour)->count();

      $this->data['minutes']  = CleaningService::where('created_at' , '>' ,$minutes)->count();
      $this->data['call_out'] = CleaningService::where('status', "cancelled")->count();
      $this->data['last_day']  = CleaningService::where('created_at' , '>' ,$day)->count();
      $this->data['on_progress'] = CleaningService::where('status',"ongoing")->count();
      $this->data['pending'] = CleaningService::where('status',"pending")->count();
      $this->data['completed'] = CleaningService::where('status',"completed")->count();
      $this->data['boss_regent'] = User::where('type',"area_head")->count();
      $this->data['new_boss_regent'] = User::where('type',"head")->where('created_at','>' , $month)->count();
      $this->data['franchisee'] = User::where('type',"franchisee")->count();
      $this->data['new_area_head'] = User::where('type',"area_head")->where('created_at','>' , $month)->count();

      $this->data['total_job'] = CleaningService::whereBetween('created_at' , [$startDate ,$endDate])->count();
      $this->data['unconverted'] = CleaningService::where('status' ,"pending")->whereBetween('created_at' , [$startDate ,$endDate])->count();

      $this->data['converted'] = CleaningService::where('status' ,"on_going")
                              ->orWhere('status' , "completed")
                              ->whereBetween('created_at' , [$startDate ,$endDate])->count();

      if ($this->data['total_job']) {
         $percent = $this->data['converted'] / $this->data['total_job'] * 100;
         $unconverted_percent = $this->data['unconverted'] / $this->data['total_job'] * 100;
      
      }else{
         $percent = 0;
         $unconverted_percent = 0;
        
      }


 		$this->data['tickets'] = [];
      $this->data['feedbacks'] = CustomerFeedback::orderBy('created_at', 'DESC')->paginate(5);
 		// $client_id = '645319672849-s4ouse27q81db9k3ff23101mo94730po.apps.googleusercontent.com';
   //  	$client_secret = 'mNSZzWF7gWRKJZm0BbTz33D8';
   //  	$refresh_token = '1/Ws89gkSBponz0vTAxzBB-cTCW3c6mbb_gXX2LoKeFfE';

   //   	$response = Curl::to("https://ronniecastro.freshdesk.com/api/v2/tickets?filter=new_and_my_open")
   //   	->withHeader('Authorization: Bearer ' . base64_encode('IUAoQZEL4IBYm5SKLOUs'))
   //   	->returnResponseObject()
   //   	->get();

   //   	if($response->status == 200) {
   //   		$this->data['tickets'] = json_decode($response->content);
   //   	}

      $this->data['startDate'] = $startDate;
      $this->data['percent'] = $percent;
      $this->data['unconverted_percent'] = $unconverted_percent;
      $this->data['endDate'] = $endDate;
		return view ('system.dashboard.index', $this->data);
	}

}
