<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class VideoContent extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "video_content";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','group','path', 'directory', 'filename'
    ];

    public $timestamps = true;
}
