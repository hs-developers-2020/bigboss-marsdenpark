<?php


namespace App\Laravel\Controllers\System;

/**
 *
 * Models used for this controller
 */

use Illuminate\Http\Request;




/**
 *
 * Requests used for validating inputs
 */

use App\Laravel\Models\Title;
use App\Laravel\Models\AboutUs;

use App\Http\Requests\PageRequest;
use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Requests\System\IconRequest;

use App\Laravel\Requests\System\TitleRequest;
use App\Laravel\Requests\System\AboutUsRequest;
use Helper, Carbon, Str, Auth, ImageUploader, FileUploader, File, DB, AuditRequest, Event;


class TitleController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data = [];
        parent::__construct();
        array_merge($this->data, parent::get_data());
    }

    public function index(PageRequest $request)
    {
        // $this->data['group'] = $request->get('group',false);
        $this->data['keyword'] = $request->get('keyword', NULL);

        $this->data['title'] = Title::orderBy('created_at', "DESC")->paginate(10);

        return view('system.cleaning.title.index', $this->data);
    }

    public function create()
    {
        return view('system.cleaning.title.create', $this->data);
    }

    public function store (TitleRequest $request) {

		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$title = new Title;
			$title->fill($request->all());

			if($title->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "TITLE_CREATED", 'remarks' => Auth::user()->full_name." has successfully created new Title {$title->title}.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.title.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function edit($id = NULL)
    {
        $title = Title::find($id);
	
		if (!$title) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.title.index');
		}

		$this->data['title'] = $title;

		return view('system.cleaning.title.edit',$this->data);
    }

    public function update(TitleRequest $request, $id = NULL)
    {
        $ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$edit_title = Title::find($id);
			$edit_title->fill($request->all());
		
			if (!$edit_title) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.title.index');
			}


			if($edit_title->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "TITLE_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$edit_title->title} Title.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.title.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
    }

    public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$icons = Title::find($id);

			if (!$icons) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.title.index');
			}

			if($icons->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "TITLE_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$icons->title} Title.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.title.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
