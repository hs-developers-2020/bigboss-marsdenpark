<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Helper, Str;
class CleaningService extends Model
{
    protected $fillable = [
    					'postcode',
    					'location',
    					'property_type',
    					'services',
    					'booking_id',
    					'name',
    					'lastname',
    					'email',
    					'contact',
    					'booking_date',
    					'time_start',
    					'time_end',
    					'booking_date_secondary',
    					'secondary_time_start',
    					'secondary_time_end',
                        'longtitude',
                        'latitude',
                        'region',
                        'customer_id',
                        'service_type_amount'
    					];
                        public $timestamps = true;

    protected $appends = ['full_name'];

    public function getFullNameAttribute(){

        $name = "{$this->name} {$this->lastname}";

        if($this->suffix) $name.= " {$this->suffix}";
        
        return $name;
    }

    public function scopeKeyword($query,$keyword){
        if($keyword){
            $key = Str::lower($keyword);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("name LIKE '%{$key}%'")
                          ->orWhereRaw("lastname LIKE '%{$key}%'")
                          ->orWhereRaw("LOWER(email) LIKE '%{$key}%'")
                          ->orWhereRaw("LOWER(location) LIKE '%{$key}%'");
                         
            });

        }
    }

    public function scopeProgress($query,$progress = NULL){
        $key = Str::lower($progress);
        if ($key) {
            return $query->where(function($query) use ($key){
                    $query->where('status', $key);
            });
        }
       

    }



    

    public function franchisee() {
        return $this->BelongsTo("App\Laravel\Models\UserTerritory", 'franchisee_id' , 'id');
    }
    public function subservice() {
        return $this->BelongsTo("App\Laravel\Models\SubService", 'number_of_rooms' , 'id');
    }

    public function service() {
        return $this->BelongsTo("App\Laravel\Models\Service", 'standard_services' , 'id');
    }

    public function staff() {
        return $this->belongsTo("App\Laravel\Models\User", 'franchisee_id' , 'id');
    }

 

    public function others() {
        return $this->belongsTo("App\Laravel\Models\OtherService", 'booking_id' , 'booking_id');
    }

    public function notes(){
        return $this->hasMany('App\Laravel\Models\Notes', 'job_id', 'id');
    }

    public function scopeWithinTeritory($query,$user_teritory = NULL){
        if($user_teritory){
            return $query->whereRaw("ST_WITHIN(
                ST_GEOMFROMTEXT(
                    CONCAT('POINT(',latitude,' ',longitude,')')
                ),
                ST_GEOMFROMTEXT('POLYGON(({$user_teritory}))')
                ) = 1");
        }
    }
}
