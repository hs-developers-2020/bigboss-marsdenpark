<?php



namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\CleaningService;
use App\Laravel\Models\ServicePackage;
use App\Laravel\Models\OtherService;
use App\Laravel\Models\Postcode;
use App\Laravel\Models\User;
use App\Laravel\Models\Finance;
use App\Laravel\Requests\Frontend\BookingRequest;
use App\Mail\BookingNumber;

use App\Http\Controllers\Controller;

use App\Laravel\Services\ResponseManager;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, DB,Input,Event,PDF,Curl,Signature,Log,Auth,Mail;


class SubpageController extends Controller
{
	protected $data;
	function __construct($foo = null)
	{
		$this->data = [];
	}
	public function cleaning(Request $request){
		
		$request->session()->forget('booking_info');
		$request->session()->forget('current_step');
		$request->session()->forget('clean');
		$request->session()->forget('percent');
	$this->data['message_alert'] =  '';
		return view ("frontend.subpage.cleaning.step1.index",$this->data);
	}

	
	public function accounting(){
		return view ('frontend.subpage.accounting.index');
	}
	public function forex(){
		return view ('frontend.subpage.forex.index');
	}
	public function itService(){
		return view ('frontend.subpage.it-service.index');
	}

	public function back(Request $request)
	{
		$current_step = $request->session()->get('current_step');
		$request->session()->get('current_step') - 2;
		 // return $request->session()->get('current_step');
		return redirect()->route('frontend.cleaning.index');
	}

	public function create(BookingRequest $request){
		
		
		}
	public function search(Request $request)
	{
		$postcode = $request->get('term');

		$result = Postcode::where('place_name','LIKE','%'.$postcode."%")
		->orWhere('postcode','LIKE','%'.$postcode."%")	
		->get();

		return $x = json_decode($result,true);

	}


	public function step_index(Request $request)
	{

		$request->session()->put('current_step',1);
		$this->data['message_alert'] =  '';

		return view('frontend.subpage.cleaning.step1.index',$this->data);
	}
	public function step_two(BookingRequest $request)
	{
		$validate = Postcode::where('postcode',$request->get('postcode'))->count();

		if($validate > 0)
		{
			$request->session()->put('clean.postcode',$request->postcode);
			$request->session()->put('clean.longitude1',$request->longitude);
			$request->session()->put('clean.latitude2',$request->latitude);
			$request->session()->put('current_step',2);
			// $request->session()->put('percent',10);

			return view('frontend.subpage.cleaning.step2.index',$this->data);
		}
		else
		{
			session()->flash('notification-status','error');
			session()->flash('notification-msg','no post code found Please try again.');
			return redirect()->back();
		}


		
	} 




	public function step_three(BookingRequest $request)
	{
		$request->session()->put('clean.location',$request->location);
		$request->session()->put('clean.longitude', $request->get('geo_long'));
		$request->session()->put('clean.latitude', $request->get('geo_lat'));
		$request->session()->put('clean.state', $request->get('state'));
		$request->session()->put('clean.sub', $request->get('sub'));
		// return $request->session()->get('clean');
		$request->session()->put('current_step',3);
		// $request->session()->put('percent',20);
		return view('frontend.subpage.cleaning.step3.index',$this->data);
	} 

	public function step_four(BookingRequest $request)
	{

		$request->session()->put('clean.property_type',$request->only('property'));
		$request->session()->put('current_step',4);
		// $request->session()->put('percent',30);
		return view('frontend.subpage.cleaning.step4.index',$this->data);
	} 
	public function step_five(BookingRequest $request)
	{
		
     	// return $request->session()->get('clean');
		$request->session()->put('current_step',5);
		// $request->session()->put('percent',50);
		$request->session()->put('clean.type',$request->only('type'));
		$this->data['oven'] = ServicePackage::where('service_name','oven')->first();
		$this->data['carpet'] = ServicePackage::where('service_name','carpet')->first();
		$this->data['window'] = ServicePackage::where('service_name','window')->first();
		$this->data['rubbish'] = ServicePackage::where('service_name','rubbish')->first();
		$this->data['lawn'] = ServicePackage::where('service_name','lawn');
		 $this->data['servcices_count'] = ServicePackage::all()->count();
		return view('frontend.subpage.cleaning.step5.index',$this->data);
	} 

	public function step_six(BookingRequest $request)
	{
	
		
		
	
		if (Auth::check()) {
			
			$request->session()->put('clean.services',$request->only('svc'));
			$request->session()->put('booking_info.booking_id',rand(1111111111,9999999999));
			$request->session()->put('booking_info.fname',Auth::user()->name);
			$request->session()->put('percent',60);
			$request->session()->put('clean.fname', Auth::user()->name);
			$request->session()->put('clean.email', Auth::user()->email);
			$request->session()->put('clean.contact',Auth::user()->contact);
			$this->data['user'] = Auth::user();
			$request->session()->put('current_step',6);

			return view('frontend.subpage.cleaning.step6.index',$this->data);
		}
		else
		{
			
			$request->session()->put('clean.services',$request->only('svc'));
			$request->session()->put('percent',60);
			$request->session()->put('current_step',6);
			return view('frontend.subpage.cleaning.step6.index',$this->data);
		}
		
	} 

	public function step_seven(BookingRequest $request)
	{
		if(!$request->session()->has('clean'))
		{
			return redirect()->route('frontend.booking.index');
		}
		// return $request->session()->put('clean.type',$request->only('type'));
		// return $request->session()->get('clean');
		$request->session()->put('booking_info.booking_id',rand(1111111111,9999999999));
		$request->session()->put('booking_info.fname', $request->fname);

		
		$request->session()->put('clean.fname', $request->fname);
		$request->session()->put('clean.lname', $request->lname);
		$request->session()->put('clean.email', $request->email);
		$request->session()->put('clean.contact', $request->contact);
		
		
		// return $request->session()->get('clean');
		$request->session()->put('current_step',7);
		// $request->session()->put('percent',90);
		return view('frontend.subpage.cleaning.step7.index',$this->data);
	} 

	public function step_eight(BookingRequest $request)
	{
		// return $request->session()->get('clean');
		if(!$request->session()->has('clean'))
		{
			return redirect()->route('frontend.booking.index');
		}
		
		
		$request->session()->put('clean.booking_date', $request->booking_date);
		$request->session()->put('clean.time_start', $request->time_start);
		$request->session()->put('clean.time_end', $request->time_end);
		$request->session()->put('clean.booking_date_secondary', $request->booking_date_secondary);
		$request->session()->put('clean.secondary_time_start', $request->secondary_time_start);
		$request->session()->put('clean.secondary_time_end', $request->secondary_time_end);
		
			// return $request->session()->get('clean');
		
		$request->session()->put('current_step',8);

		
		return view('frontend.subpage.cleaning.step8.index',$this->data);
	}

	public function step_nine(BookingRequest $request)
	{
		if(!$request->session()->has('clean'))
		{
			return redirect()->route('frontend.booking.index');
		}
		// $request->session()->put('current_step',9);
		// $request->session()->put('percent',100);
		return view('frontend.subpage.cleaning.step9.index',$this->data);
	} 

	public function finish(BookingRequest $request)
	{
		if(!$request->session()->has('clean'))
		{
			return redirect()->route('frontend.cleaning.index');
		}
		$request->session()->put('clean.payment', $request->payment);
		// return $request->session()->get('clean');


		   $services =  new CleaningService;

		   $services->postcode = $request->session()->get('clean.postcode');
		   $services->location = $request->session()->get('clean.location');
		   $services->service_type = implode(',',$request->session()->get('clean.type'));
		   $services->property_type = $request->session()->get('clean.property_type.property');
		   $services->property_type = $request->session()->get('clean.property_type.property');
		   $services->services = implode(",",$request->session()->get('clean.services.svc'));
		   $services->name = $request->session()->get('clean.fname');
		   $services->lastname = $request->session()->get('clean.lname');
		   $services->email = $request->session()->get('clean.email');
		   $services->contact = $request->session()->get('clean.contact');
		   $services->booking_id = $request->session()->get('booking_info.booking_id');
		   $services->booking_date = $request->session()->get('clean.booking_date');
		   $services->time_start =  $request->session()->get('clean.time_start');
		   $services->time_end =  $request->session()->get('clean.time_end');
		   $services->booking_date_secondary =   $request->session()->get('clean.booking_date_secondary');
		   $services->secondary_time_end =    $request->session()->get('clean.secondary_time_end');
		   $services->secondary_time_start =    $request->session()->get('clean.secondary_time_start');
		   $services->payment =  $request->session()->get('clean.payment');
		   $services->longitude =  $request->session()->get('clean.longitude');
		   $services->latitude =  $request->session()->get('clean.latitude');
		   $services->region =  $request->session()->get('clean.state');
		   $services->amount =   $request->session()->get('clean.service_price');
		   $services->booking_total =   $request->session()->get('clean.service_price');
		   $services->status =   "pending";


		if ($services->save()) {

			Mail::to($request->session()->get('clean.email'))->send(new BookingNumber());
			

			$request->session()->forget('current_step');
			$request->session()->forget('clean');
			$request->session()->forget('percent');
		}
			// $request->session()->put('current_step',9);
		$this->data['message_alert'] =  '';
		return view('frontend.page.booking-submitted.index',$this->data);
	}





	public function finish2(BookingRequest $request)
	{
            if(!$request->session()->has('clean.postcode'))
		{
			return redirect()->route('frontend.cleaning.index');
		}
		$request->session()->put('clean.payment', $request->payment);
		
		$request->session()->put('booking_info.booking_id',rand(1111111111,9999999999));
		$request->session()->put('booking_info.fname', $request->get('fname'));
		$request->session()->put('booking_info.lname', $request->get('lname'));
		


		$services =  new CleaningService;

		   $services->postcode = $request->session()->get('clean.postcode');
		   $services->location = $request->session()->get('clean.location');
		   $services->service_type = implode(',',$request->session()->get('clean.type'));
		   $services->property_type = $request->session()->get('clean.property_type.property');
		   $services->property_type = $request->session()->get('clean.property_type.property');
		   $services->services = implode(",",$request->session()->get('clean.services.svc'));
		   $services->name =$request->get('fname');
		   $services->lastname = $request->get('lname');
		   $services->email = $request->get('email');
		   $services->contact = $request->get('contact');
		   $services->booking_id = $request->session()->get('booking_info.booking_id');
		   $services->booking_date = $request->session()->get('clean.booking_date');
		   $services->time_start =  $request->session()->get('clean.time_start');
		   $services->time_end =  $request->session()->get('clean.time_end');
		   $services->booking_date_secondary =   $request->session()->get('clean.booking_date_secondary');
		   $services->secondary_time_end =    $request->session()->get('clean.secondary_time_end');
		   $services->secondary_time_start =    $request->session()->get('clean.secondary_time_start');
		   $services->payment =  $request->session()->get('clean.payment');
		   $services->longitude =  $request->session()->get('clean.longitude');
		   $services->latitude =  $request->session()->get('clean.latitude');
		   $services->region =  $request->session()->get('clean.state');
		   $services->amount =   $request->session()->get('clean.service_price');
		   $services->booking_total =   $request->session()->get('clean.service_price');
		   $services->status =   "pending-sched";

		if ($services->save()) {

			$request->session()->forget('current_step');
			$request->session()->forget('clean');
			$request->session()->forget('percent');
		}
			// $request->session()->put('current_step',9);
		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "congratulations you have successfully booked",
			showConfirmButton: false,
			timer: 3000
			})
			</script>';
		
		return view('frontend.page.booking-submitted.index',$this->data);
	}

	public function cancel(Request $request)
	{

		if(!$request->session()->has('clean.postcode'))
		{	
			return redirect()->route('frontend.booking.index');
		}
           $request->session()->put('clean.payment', $request->payment);
	

		   $services =  new CleaningService;

		   $services->postcode = $request->session()->get('clean.postcode');
		   $services->location = $request->session()->get('clean.location');
		   $services->service_type = implode(',',$request->session()->get('clean.type'));
		   $services->property_type = $request->session()->get('clean.property_type.property');
		   $services->property_type = $request->session()->get('clean.property_type.property');
		   $services->services = implode(",",$request->session()->get('clean.services.svc'));
		   $services->name = $request->session()->get('clean.fname');
		   $services->lastname = $request->session()->get('clean.lname');
		   $services->email = $request->session()->get('clean.email');
		   $services->contact = $request->session()->get('clean.contact');
		   $services->booking_id = $request->session()->get('booking_info.booking_id');
		   $services->booking_date = $request->session()->get('clean.booking_date');
		   $services->time_start =  $request->session()->get('clean.time_start');
		   $services->time_end =  $request->session()->get('clean.time_end');
		   $services->booking_date_secondary =   $request->session()->get('clean.booking_date_secondary');
		   $services->secondary_time_end =    $request->session()->get('clean.secondary_time_end');
		   $services->secondary_time_start =    $request->session()->get('clean.secondary_time_start');
		   $services->payment =  $request->session()->get('clean.payment');
		   $services->longitude =  $request->session()->get('clean.longitude');
		   $services->latitude =  $request->session()->get('clean.latitude');
		   $services->region =  $request->session()->get('clean.state');
		   $services->amount =   $request->session()->get('clean.service_price');
		   $services->booking_total =   $request->session()->get('clean.service_price');
		   $services->status =   "cancelled";


		if ($services->save()) {
			
			$request->session()->forget('current_step');
			$request->session()->forget('clean');
			$request->session()->forget('percent');
		}
			
	
		
		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "Your booking has been cancelled",
			showConfirmButton: false,
			timer: 3000
			})
			</script>';

		return view ("frontend.subpage.cleaning.step1.index",$this->data); 
			// return redirect()->route('frontend.booking.index',$this->data);
		
	}



	public function destroy($value='')
	{
		$request->session()->flush();
		return redirect()->route('frontend.cleaning.index');
	}


	public function mails(Request $request)
	{
		// $title = $request->input('title');
  //       $content = $request->input('content');

        Mail::to('markwil.abiera0518@gmail.com')->send(new BookingNumber());
	}
}



