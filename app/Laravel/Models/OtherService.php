<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class OtherService extends Model
{
    protected $fillable = ['services','booking_id'];
}
