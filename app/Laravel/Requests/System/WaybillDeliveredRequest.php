<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class WaybillDeliveredRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'rider_id'	=> "required",
			'delivery_date'	=> "required|date"
		];

		return $rules;
	}

	public function messages(){
		return [
			'delivery_date.date'	=> "Invalid date format for delivery date.",
			'delivery_date.required'	=> "Delivery date is required.",
			'rider_id.required'	=> "Please choose a rider before you continue.",
		];
	}
}