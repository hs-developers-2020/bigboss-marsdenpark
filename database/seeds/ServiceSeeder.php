<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\SubService;
class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = storage_path() . "/json/services.json";
        $code= json_decode(file_get_contents($path), true);

        for ($i=0; $i < count($code) ; $i++) { 
            
            $data = array(
                            "name" => $code[$i]['name'],
                            "type" => $code[$i]['type'],
                            "price" => $code[$i]['price'],
                            "storey" => $code[$i]['storey'],
                            );
            SubService::insert($data);
        }

        

    }
}
