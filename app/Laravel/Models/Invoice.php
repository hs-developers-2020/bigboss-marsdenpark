<?php

namespace App\Laravel\Models;


use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon, Helper,Str;

class Invoice extends Model
{
    use DateFormatterTrait,SoftDeletes;

    protected $table = "invoice";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_number','franchisee_id','type','invoice_type' ,'invoice_to','amount','booking_id','user_id'
    ];

    public $timestamps = true;

    public function scopeGroup($query,$group = NULL){
        $key = Str::lower($group);
        if ($key) {
            return $query->where(function($query) use ($key){
                    $query->where('group', $key);
            });
        }
       

    }

    public function scopeDateRange($query,$from,$to){
        return $query->where(function($query) use($from){
            if($from){
                $_from = Helper::date_db($from); 
                return $query->orWhereRaw("DATE(created_at) >= '{$_from}'");
                }
            })->where(function($query) use ($to){
            if($to){
                $_to = Helper::date_db($to); 
                return $query->orWhereRaw("DATE(created_at) <= '{$_to}'");
            }
        });
    }
    public function scopeStatus($query,$status){
        if($status){
            $key = Str::lower($status);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("LOWER(status) LIKE '%{$key}%'");
                                            
            });

     
        }
    }

    public function user() {
        return $this->belongsTo("App\Laravel\Models\User", "franchisee_id" ,"id");
    }

    public function user_info() {
        return $this->belongsTo("App\Laravel\Models\User", "user_id" ,"id");
    }

    public function booking_info() {
        return $this->belongsTo("App\Laravel\Models\CleaningService", "booking_id" ,"booking_id");
    }

    public function business_info() {
        return $this->belongsTo("App\Laravel\Models\UserInfo", "head_id" ,"user_id");
    }

    public function franchisee_info() {
        return $this->belongsTo("App\Laravel\Models\UserInfo", "franchisee_id" ,"user_id");
    }

    public function area_head() {
        return $this->belongsTo("App\Laravel\Models\User", "head_id" ,"id");
    }


}
