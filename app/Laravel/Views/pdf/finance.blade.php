<!DOCTYPE>
<html>
<head>
	<title></title>
</head>
<body>
	<table width="100%"  cellspacing="0" cellpadding="3" border="1">
		<thead>
			<tr>
				<td>Transaction #</td>
				<td>Booking ID</td>
				<td>Description</td>
				<td>Type</td>
				<td>Date</td>
				<td>Amount</td>
			</tr>

		</thead>
		<tbody>

			@forelse($finance as $value)
			<tr>
				<td>{{str_pad($value->id, 6, "0", STR_PAD_LEFT)}}</td>
				<td>{{$value->booking_id ?: "N/A"}}</td>
				@if($value->type == "Income")
                    <td><h5 class="text-color-gray p-3 text-xsmall">{{Str::title($value->property_type)}} ({{Str::title($value->details)}})</h5></td>
                @else
                    <td><h5 class="text-color-gray p-3 text-xsmall">{{Str::title($value->details)}}</h5></td>
                @endif
				<td>{{Str::title($value->type)}}</td>
                <td>{{Helper::date_only($value->created_at)}}</td>
                <td>${{Helper::amount($value->amount)}}</td>
               
			</tr>
			@empty
			<tr>
				<td>No Record</td>
			</tr>
			@endforelse
		</tbody>
	</table>
</body>
</html>