<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ServiceRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;
		$type = $this->request->get('group')?:"";
		$media_type = $this->request->get('media_type')?:"";
		
	
		$rules = [
			'name'	=> "required",
			'group' => "required",
			'price'	=> "required|numeric",
			'service_type'	=> "required",
			'file' =>"required",
		];

		if($type == "accounting"){
		
			unset($rules['service_type']);
			
		}

		if($type == "information_technology"){
			unset($rules['service_type']);
			unset($rules['price']);

			
		}

		if ($media_type == 2) {
			$rules['file'] = "nullable|image";
			$rules['media_library'] = "required";
		}
		if($id){
			$rules['file'] = "nullable|image";
		}


		return $rules;
	}

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
		];
	}
}