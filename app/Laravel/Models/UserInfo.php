<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth,Helper,Str;

class UserInfo extends Model
{
    use DateFormatterTrait;

    protected $table = "user_info";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'head_id','business_group', 'alternate_email', 'street', 'city', 'state', 'post_code', 'business_name', 'business_number','tax_number', 'business_street', 'business_city', 'business_state', 'business_post_code', 'user_designation','business_group'
    ];
    
    public $timestamps = true;
    protected $appends = ['full_address','invoice_address'];

    public function scopeKeyword($query,$keyword){
        if($keyword){
            $key = Str::lower($keyword);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("business_name LIKE '%{$key}%'");
                         
            });

        }
    }

    public function getFullAddressAttribute(){
        $address = "";

        if($this->business_street) $address .= "{$this->business_street} ";
        if($this->business_city) $address .= "{$this->business_city} ";
        if($this->business_state) $address .= "{$this->business_state}, ";

        return $address;
    }
     public function getInvoiceAddressAttribute(){
        $address = "";

        switch ($this->state) {
            case 'Victoria':
                $state = "VIC";
                break;
            case 'New South Wales':
                $state = "NSW";
                break;
            case 'Queensland':
                $state = "QLD";
                break;
            case 'Northern Territory':
                $state = "NT";
                break;
            case 'South Australia':
                $state = "SA";
                break;
            case 'Tasmania':
                $state = "TAS";
                break;
            case 'Western Australia':
                $state = "WA";
                break;
           
            default:
               $state ="N/A";
                break;
        }

        if($this->city) $address .= "{$this->city} ";
        if($this->state) $address .= "{$state}, ";
        if($this->post_code) $address .= "{$this->post_code} ";
        return $address;
    }

    public function area_head() {
        return $this->belongsTo("App\Laravel\Models\User", "head_id" ,"id");
    }

    
}
