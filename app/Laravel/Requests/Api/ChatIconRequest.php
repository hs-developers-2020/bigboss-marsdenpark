<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class ChatIconRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'file' => "required|image",
        ];

        return $rules;
    }

    public function messages() {

        return [
            'file.image' => "Please attach an image file.",
            'file.required' => "Please attach an image file.",
        ];
    }
}
