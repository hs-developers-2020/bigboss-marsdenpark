<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\CleaningService;
use App\Laravel\Models\OtherService;
use App\Laravel\Models\Postcode;
use App\Laravel\Models\Service;
use App\Laravel\Models\SubService;
use App\Laravel\Requests\Frontend\BookingRequest;


use App\Http\Controllers\Controller;

use App\Laravel\Services\ResponseManager;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, DB,Input,Event,PDF,Curl,Signature,Log,Auth,Mail;

class TrackController extends Controller
{
    public function index(Request $request)
    {
        $total_amount=0;
    	$this->data['info'] = CleaningService::where('booking_id',$request->booking_id)->first();
    	$this->data['postcode'] = Postcode::all();

        if (!$this->data['info']) {
            return view('frontend.page.booking-invoice.no-result',$this->data);
        }

        $service_standard = explode(",",$this->data['info']->service_id); 

        foreach ($service_standard as $key) {
            $service = Service::find($key);

            if($service) {
                $total_amount += $service->price;
            }


        }

        $this->data['services'] = Service::whereIn('id', explode(',', $this->data['info']->service_id))->get();
        $this->data['subservice'] = SubService::find($this->data['info']->number_of_rooms);
        $this->data['others'] = OtherService::where('booking_id', $this->data['info']->booking_id)->get();
    	if($this->data['info']) 
    	{
         
            $this->data['total_amount'] = $total_amount;
            if ($this->data['info']->type == "accounting") {
                return view('frontend.page.booking-invoice.accounting',$this->data);
            }else{
                return view('frontend.page.booking-invoice.index',$this->data);
            }
    		
    	}
    	else
    	{
    		return view('frontend.page.booking-invoice.no-result',$this->data);
    	}
    }
}
