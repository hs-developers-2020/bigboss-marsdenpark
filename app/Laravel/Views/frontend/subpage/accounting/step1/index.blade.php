@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
{{ Session::put('current_progress',1) }}
{{ Session::put('percent',0) }}
<form action="" method="post">
  {{ csrf_field() }}
    <div class="content" style="padding-bottom:7%;">
      <div class="container text-center" style="padding-top: 190px;">
        <div class="row">   
          <div class="col-lg-12 text-center">
           
            <h6 class="text-color-white font-semibold letter-height-1 spacing-1 mt-3">We Work Best When We Work With You. Find your IT Experts.</h6>
             <p class="text-white font-semibold mt-3">Enter Postcode or Suburb</p>
          </div>
          <div class="col-lg-2"></div>
          <div class="col-lg-8" >
            <input type="hidden" name="latitude" id="lat" placeholder="2">
            <input type="hidden" name="longitude" id="long">
            <div class="ui-widget d-flex justify-content-center">
              <div class="input-group w-75">
                   <input id="postcode" name="postcode" autocomplete="off" type="text" class="p-3 card-shadow  form-control text-center" />
              </div>
              
            </div>
            <div class="d-flex justify-content-center">
              <div id="result" onclick="myFunction()" class="text-uppercase text-left w-75 postcode--dropdown">
              </div>
            </div>
          </div>
          <div class="col-lg-2"></div>
        </div>    
        <div class="col-lg-12 text-center">
          <div class="">
            <a name="next"  class="nextone spacing-1" style="text-decoration: none; ">
            <button type="submit" class="card-shadow next btn text-color-white mt-3 font-semibold spacing-1 text-xsmall pl-5 pr-5 pt-3 pb-3 bg-color-blue">CONTINUE</button></a>
            

          </div>
        </div>
      </div>
    </div>
  
</form>
  


@include('frontend._components.footer-sub')
@stop

@section('style-script')
<style type="text/css">

  a{
    cursor: pointer; 
  }
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
#result a:hover {
  color: white !important;
  background-color: #212121;
  padding: 3px;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Accounting.jpg') }}") !important;
  background-repeat:no-repeat !important;
  background-size:cover !important;
  overflow-x: hidden !important;
  background-color: rgba(0,0,0,.9) !important;
} 

@media only screen and (max-width: 1500px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
 .footer-bottom{
  position: absolute;
  bottom:0;

}
}

@media only screen and (min-width: 1500px) {

   #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
  }
  .picture-size {
  height:5%; width: 7%;
  }
  .footer-bottom{
  position: absolute;
  bottom:0;


}
  }
  a:hover{
    color: white !important;
  }
</style>

@stop

@section('scripts')
<script type="text/javascript">
@if ($errors->first('postcode') ? 'has-error' : NULL) {
 Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Invalid or No Selected Postcode, Please Select One to Continue',
})
e.preventDefault()
}
@endif
</script>
@stop