<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTableCleaningServicesAddColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cleaning_services', function($table){
            $table->text('longitude')->nullable();
            $table->text('latitude')->nullable();
            $table->text('region')->nullable();
            $table->text('amount')->nullable();
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cleaning_services', function($table){
            // $table->dropColumn('longitude');
            // $table->dropColumn('latitude');
            // $table->dropColumn('region');
            // $table->dropColumn('amount');

        });
    }
}
