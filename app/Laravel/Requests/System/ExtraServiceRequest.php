<?php 
namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ExtraServiceRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;
		$type = $this->request->get('has_addon')?:"";
		$rules = [
			'name'	=> "required",
			'price'	=> "required|numeric",
			'region'	=> "required",
			'file' => "required|image",
			'has_addon' => "required",
		];

		if($type == "yes"){
			// $rules['addons_price'] = "required";
			// $rules['addons'] = "required";
			unset($rules['price']);
			foreach(range(1,count($this->get('addons'))) as $index => $value){
				$rules["addons.{$index}"] = "required";
				$rules["addons_price.{$index}"] = "required";

			}
		}
		if($id){
			$rules['file'] = "nullable|image";
		}


		return $rules;
	}

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
		];
	}
}