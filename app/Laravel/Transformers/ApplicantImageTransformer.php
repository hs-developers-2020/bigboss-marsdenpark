<?php 

namespace App\Laravel\Transformers;

use Input;
use JWTAuth, Carbon, Helper;
use App\Laravel\Models\ApplicantImage;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Laravel\Transformers\MasterTransformer;

use Str;

class ApplicantImageTransformer extends TransformerAbstract{

	protected $availableIncludes = [
    ];

    public function __construct() {
    }

	public function transform(ApplicantImage $image) {
	    return [
			'id'	=> $image->id ? $image->id : 0,
			'user_id'	=> $image->user_id ? $image->user_id : 0,
			'filename' => $image->filename ? $image->filename : '',
			'path' => $image->path ? $image->path : '',
			'directory' => $image->directory,
			'full_path' => "{$image->directory}/resized/{$image->filename}",
			'thumb_path' => "{$image->directory}/thumbnails/{$image->filename}"
		];
	}
}