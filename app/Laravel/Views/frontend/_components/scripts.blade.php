
<script src="{{asset('frontend/js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/script.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/combodate.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/js/slick.js')}}" type="text/javascript"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="http://weareoutman.github.io/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

  @yield('scripts')

<script type="text/javascript">
$('.services').slick({
  prevArrow: '<div class="prev_btn"><i class="fas fa-caret-square-left fa-2x text-color-blue"></i></div>',
  nextArrow: '<i class="fas fa-caret-square-right fa-2x text-color-red"></i>',
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
   fade: false,


  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
        infinite: true,
        dots: true,

      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$('.ratings').slick({
  prevArrow: '<div class="prev_btn"><i class="fas fa-chevron-circle-left fa-2x"></i></div>',
  nextArrow: '<i class="fas fa-chevron-circle-right fa-2x"></i>',
  dots: true,
  infinite: true,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 3,
   fade: false,


  responsive: [
    {
      breakpoint: 1504,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,

      }
    },
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true,

      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
});

$("#booking_date").datepicker({
 minDate:'0'
});

   $('#t1').timepicker({
  timeFormat: 'hh:mm a',
  interval: 30,
  minTime: '7',
  maxTime: '07:00 PM',
  startTime: '07:00 AM',
  dynamic: false,
  dropdown: true,
  scrollbar: true
});

$('#t1')
  .timepicker('option', 'change', function(time) {
    var later = new Date(time.getTime() + (2 * 60 * 60 * 1000));
    $('#t2').timepicker('option', 'minTime', time);
    $('#t2').timepicker('setTime', later);
  });

$('#t2').timepicker({
  timeFormat: 'hh:mm a',
  interval: 30,
  maxTime: '07:00 PM',
  startTime: '07:00 AM',
  dynamic: false,
  dropdown: true,
  scrollbar: true
});      

      $('input.datepicker').focus(function(){

        $(this).datetimepicker({
          autoclose: true,
          format: "yyyy-mm-dd",
           minView: "month",
          language: "fr"});

        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });

$('#track').on('click',function(){

// Swal.fire({
//   title: 'Sweet!',
//   text: 'Modal with a custom image.',
//   imageWidth: 400,
//   imageHeight: 200,
//   imageAlt: 'Custom image',
//   animation: false,

// })

})

$('#back').on('click',function(){
  
 history.go(-1);
// window.location=document.referrer;
 // window.location=document.referrer;
})

  $(document).ready(function() {




// end of sweet alert



    $('#result').css("display","none");
    $( "#postcode" ).on('keyup',function(){
      var l =$(this).val();
      if(l.length <= 2){
        $('#result').html("");
        $('#result').css("display","none");
      }$
    })
    


    $( "#postcode" ).autocomplete({


      source: function(request, response) {
        $.ajax({
          url: "{{url('searchpostcode')}}",
          data: {
            term : request.term
          },
          
          success: function(data){
           var resp = $.map(data,function(obj){

            
            var table = "<table><th></th>"
            for (var i = 0; i < data.length; i++) {

              table += "<tr>"
              table += "<td><a data-role=getdata data-id="+data[i].postcode+" data-long="+data[i].longitude+" data-lat="+data[i].latitude+">"+ data[i].place_name + ", " +data[i].postcode +"</a></td>"
              table += "</tr>" 
            }

            table += "</table>";

            
            $('#result').html(table).fadeIn('fast')
          
            
          }); 
           
           // response(resp);
         }
       });
      },
      minLength: 3
    });



    $(document).on('click','a[data-role=getdata]',function(){

      var code = $(this).data('id');
       $('#postcode').val(code)

       $('#result').html("");
       $('#result').css("display","none");
       $('#long').val($(this).data('long'));
       $('#lat').val($(this).data('lat'));

    })
});
$(function(){
    $('#time').combodate({
        firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
        minuteStep: 1
    });  
});
$(function(){
    $('#datetime24').combodate();  
});


  setInterval(function(){
        $('.alert').fadeOut('slow');
     },2000)


</script>

<script type="text/javascript">
      
  
$( function() {
  $( "#booking_date" ).datepicker({
    dateFormat: "yy-mm-dd"
    , duration: "fast"
  });
} );
$( function() {
  $( "#booking_date_secondary" ).datepicker({
    dateFormat: "yy-mm-dd"
    , duration: "fast"
  });
} );

// Time Picker 
var input = $('.time_picker');
input.clockpicker({
    autoclose: true
});


</script>
@yield('scripts-content')

