@extends('frontend._layout.main')

@section('content')
@include('frontend._components.topnav-main')

  <video loop muted autoplay id="myVideo">
    <source src="{{ asset('frontend/Video/main1.mp4') }}" type="video/mp4">
  </video>

  <div class="container" style="margin-top: 9rem;">
    <div class="row ">
      <div class="col-lg-12">
        <div class="d-flex flex-row text-color-white pointer">
          <a class="d-flex pt-2 flex-row text-color-white spacing-1" style="text-decoration: none;" href="{{route('frontend.main')}}">
          <i class="fas fa-caret-left mt-1 mr-2"></i><p class="text-color-white font-semibold">GO BACK</p></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-center pt-1">
        <h2 class="text-color-white text-uppercase text-header font-weight-bold text-headerspacing-2">FOREIGN EXCHANGE</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12">
        <div class="row"> 
          <div class="col-lg-1"></div>
          <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 text-center pt-4 pb-2">
            <div class="card rounded card-1" style="background-color: transparent;">
              <div class="card-body" style="background-color: rgba(0,0,0,.5);">
                <img src="{{ asset('frontend/Logos/fx.png')}}" class=" img-fluid w-75 p-3 rounded">
                <p class="text-xsmall text-color-white text-justify font-semibold text-limit-5">Partnership with Australian forex broker, we pride ourselves on our ability to provide the most transparent forex trading environment and personal support, giving our clients a forex trading edge.<br><br>
                  </p>
              </div>
            </div>
          </div>
          <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 text-center pt-4">
            <div class="card rounded card-1" style="background-color: transparent;">
              <div class="card-body" style="background-color: rgba(0,0,0,.5);">
                <img src="{{ asset('frontend/images/forex.svg')}}" class=" img-fluid p-3 rounded" style="width: 55% !important;">
                <p class="text-xsmall text-color-white text-justify font-semibold text-limit-5">IFS Markets is an online FX and CFD broker based in Sydney, Australia regulated by the Australian Securities and Investments Commission.
                We do not just provide a trading platform. Instead, we aim to provide you with our best level of customer service whether you are a beginner looking to learn how to trade the markets or seasoned professional trader that demands high calibre in execution, technology and support.

                IFS Markets trading solution integrates exceptional spreads, continuous streaming liquidity, and instant trade execution functionality. When you trade with us, we connect you to our pool of renowned global investment banks and brokers, providing direct access to institutional pricing and liquidity in over 50 currencies. IFS Markets technology bridges the gap between trading and technology, enabling you to leverage our institutional relationships, and streamlined infrastructure to maximise trading effectiveness. Our focus is providing tight spreads, low commissions and the latest technology to provide you with a high-quality trading environment.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-1"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-center mt-3 mb-5">
        <a href="http://bigbossfx.com.au/" class="btn bg-color-blue text-white font-semibold pt-3 pb-3" target="_blank" style="padding-left: 60px;padding-right: 60px;">BOOK NOW</a>
      </div>
    </div>
  </div>  


@stop

@section('style-script')
<style type="text/css">
  
.bg-color-white {
  background-color: white !important;
}

#myVideo {
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%; 
  min-height: 100%;
}

.container {
 
  bottom: 0;
 
  color: #f1f1f1;
  width: 100%;
 
}

</style>
@stop
