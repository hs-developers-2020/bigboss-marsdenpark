@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
<div class="wrapper" style="margin-top: -30px;">
    <div class="container">
        <!-- <div class="row">
            <div class="col-lg-12 pb-2 text-right mb-1 mt-4">
               
                <a href="{{ route('system.finance.create') }}" class="text-uppercase btn bg-color-blue text-xxsmall text-color-white pl-3 pr-3 mr-2" style="float: right;"> Add Expense</a>
               
                 <a href="{{ route('system.invoice.index') }}" class="text-uppercase btn bg-color-blue text-xxsmall text-color-white pl-3 pr-3 mr-2" style="float: right;">View Invoice</a>
        
            </div>
        </div> -->
               
        <div class="row">
            <div class=" col-sm-12 col-md-6 col-lg-8 col-xl-4">
                <h4>Top Income</h4>
                @forelse($top_income as $index => $value)
                <div class="card mb-3 border-0 rounded-0  bg-color-green" style="border-radius: 5px !important;  height: 110px !important;">
                        <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                        <div style="background-color:rgba(0,161,49, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                        <div class="card-body position-absolute w-100">
                            <div class="media mb-2">
                                <div class="media-body">
                                    <div class="row">
                                        <div class="col-9">
                                            <p class="text-color-white text-xsmall font-semibold">{{Str::title($value->property_type)}} ({{Str::title($value->details)}})
                                            <span class="text-color-white text-xxsmall"><i>{{Str::title($value->group)}}</i></span></p>
                                            <p class="text-color-white text-small font-semibold round">$ {{$value->amount}}</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="fas fa-calculator fa-2x text-color-white"></i>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>   
                @empty
                <div class="card mb-3 border-0 rounded-0  bg-color-green" style="border-radius: 5px !important;  height: 110px !important;">
                    <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                    <div style="background-color:rgba(0,161,49, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                    <div class="card-body position-absolute w-100">
                        <div class="media mb-2">
                            <div class="media-body">
                                <div class="row">
                                    <div class="col-9">
                                        <p class="text-color-white text-xsmall font-semibold">N/A
                                        <p class="text-color-white text-small font-semibold round">$ 00.00</p>
                                    </div>
                                    <div class="col-3 text-right">
                                        <i class="fas fa-calculator fa-2x text-color-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-3 border-0 rounded-0  bg-color-green" style="border-radius: 5px !important;  height: 110px !important;">
                    <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                    <div style="background-color:rgba(0,161,49, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                    <div class="card-body position-absolute w-100">
                        <div class="media mb-2">
                            <div class="media-body">
                                <div class="row">
                                    <div class="col-9">
                                        <p class="text-color-white text-xsmall font-semibold">N/A
                                        <p class="text-color-white text-small font-semibold round">$ 00.00</p>
                                    </div>
                                    <div class="col-3 text-right">
                                        <i class="fas fa-calculator fa-2x text-color-white"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                @endforelse
                <h4>Top Expense</h4>
                @forelse($top_expense as $index => $value)    
                <div class="card mb-3 border-0 rounded-0  bg-color-green" style="border-radius: 5px !important;  height: 110px !important;">
                    <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                    <div style="background-color:rgba(161,0,10, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                    <div class="card-body position-absolute w-100">
                        <div class="media mb-2">
                            <div class="media-body">
                                <div class="row">
                                    <div class="col-9">
                                        <p class="text-color-white text-xsmall font-semibold">{{Str::title($value->category)}}</p>
                                        <p class="text-color-white text-small font-semibold round">$ {{Helper::amount($value->total)}}</p>
                                    </div>
                                    <!-- <div class="col-3 text-right">
                                        <i class="fas fa-car fa-2x text-color-white"></i>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                      
                @empty
                
                <div class="card mb-3 border-0 rounded-0  bg-color-green" style="border-radius: 5px !important;  height: 110px !important;">
                    <img src="{{ asset('frontend/images/bg1.jpg') }}" class="img-fluid" style="height: 100%; background-repeat: no-repeat; background-position: center;" >
                    <div style="background-color:rgba(161,0,10, 0.7); position: absolute; width: 100%; height: 100%;"></div>
                    <div class="card-body position-absolute w-100">
                        <div class="media mb-2">
                            <div class="media-body">
                                <div class="row">
                                    <div class="col-9">
                                        <p class="text-color-white text-xsmall font-semibold">Equipment</p>
                                        <p class="text-color-white text-small font-semibold round">$140,000.00</p>
                                    </div>
                                    <div class="col-3 text-right">
                                        <i class="fas fa-file fa-2x text-color-white"></i>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            
                @endforelse
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-8 " >
                <div class="card rounded-0 mb-3 border-0" >
                    <div class="card-body p-4">
                        <div class="d-flex justify-content-between">
                           <p class="text-xsmall text-color-gray font-semibold text-uppercase">{{$business_name ? $business_name->info->business_name : "Big Boss Group Expense"}}/Income Report</p>
                           <p class="text-xsmall text-color-blue text-right font-semibold text-uppercase">{{$StartDate}} - {{$weekEndDate}}</p>
                       </div>
                        <div class="row">
                            <div class="offset-1 col-lg-5">
                                <div class="container">
                                    <div class="">
                                        <p class="text-color-gray text-uppercase" style="margin-bottom: 0px !important;">Total Expenses </p>
                                        <h2 class="text-color-red">${{$total_expense}}</h2>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <div class="container">
                                    <div class="">
                                        <p class="text-color-gray text-uppercase" style="margin-bottom: 0px !important;">Total Income </p>
                                        <h2 class="text-color-green">${{$total_income}}</h2>
                                    </div>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div  class="col-sm-12 text-center">
                                    <div id="bar-chart" style="height: 10% !important;"></div>
                                </div>
                            </div>
                        </div>
                    </div>                            
                </div>
            </div> 
        </div>        
        <div class="row">
            <div class="col-lg-12 mt-3">
                <div class="card rounded-0 mb-3 border-0">
                    <div class="card-body p-4">
                        <div class="d-flex justify-content-between">
                            <p class="text-color-gray text-xsmall mt-2 ml-2 font-semibold text-uppercase">Finance History</p>
                            <p class="text-color-Blue text-xsmall mt-2 ml-2 font-semibold text-uppercase"><a class="text-color-blue" href="{{ route('system.finance.finance-history') }}" style="text-decoration: none;">View All</a></p>
                        </div>
                        <table class="table bg-color-white  mb-0 ">
                            <thead>
                                <tr>
                                    <th class="p-3 pl-4 pr-4 text-uppercase font-semibold text-xxsmall spacing-1 text-color-blue text-left">Transaction #</th>
                                    <th class="p-3 pl-4 pr-4 text-uppercase font-semibold text-xxsmall spacing-1 text-color-blue text-left" data-breakpoints="xs">Description</th>
                                    <th  class="p-3 pl-4 pr-4 text-uppercase font-semibold text-xxsmall spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Date</th>
                                    <th  class="p-3 pl-4 pr-4 text-uppercase font-semibold text-xxsmall spacing-1 text-color-blue text-left" data-breakpoints="xs sm">Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($finance as $index => $value)
                                <tr>
                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{str_pad($value->id, 6, "0", STR_PAD_LEFT)}}</h5></td>
                                     @if($value->type == "Income")
                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{Str::title($value->property_type)}} ({{Str::title($value->details)}})</h5></td>
                                    @else
                                     <td><h5 class="text-color-gray p-0 text-xxsmall">{{Str::title($value->details)}}</h5></td>
                                    @endif
                                    <td><h5 class="text-color-gray p-0 text-xxsmall">{{Helper::date_only($value->created_at)}}</h5></td>
                                    <td><h5 class="text-color-gray p-0 text-xxsmall">${{Helper::amount($value->amount)}}</h5></td>
                                </tr>
                                 @empty
                                <tr>
                                    <td>No Record Available</td>
                                </tr> 
                                @endforelse
                            </tbody>
                        </table>                       
                    </div>
                </div>
            </div>
        </div>   
    </div>   
</div>


@stop
@section('page-styles')
<style type="text/css">
    footer {
        margin-top: 25px !important;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1030;
    }
    body {
        padding-top: 9% !important;
    }

    .font-small {
        font-size: 12px;
    }
    .upload-btn-wrapper input[type=file] {
      font-size: 100px;
      position: absolute;
      left: 0;
      top: 0;
      opacity: 0;
  }
  .input-icons i { 
      position: absolute; 
  } 
  .input-icons { 
      width: 100%; 
      margin-bottom: 10px; 
  } 
  .icon { 
      padding: 10px; 
      min-width: 40px; 
  } 
  .input-field { 
      width: 100%; 
      padding: 15px; 
      text-align: left;
      padding-left: 50px;
  }

  .date-icon-finance {
    right:28px;
    position:absolute;
    color: #2047A4;
}
</style>
@stop
@section('page-scripts')

<script src="{{asset('assets/lib/countup/countUp.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.pie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/curvedLines.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/chartjs/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/raphael/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/morrisjs/morris.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script type="text/javascript">
    $(function(){
    

    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
    @endif
   
    });
    "use strict";
    /* Footable */
    $('.footable1').footable({
        "paging": {
            "enabled": true,
            "size": 7
        }
    }, function(ft){
        $('.checkbox-user-check').on('change',function(){                
            $(this).closest('.media').toggleClass("selected");
        });
        $('.selectallticket').on('change',function(){
            if( $(this).is(":checked") == true){
                $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
            }else{
                $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
            }
        });
    });

    Morris.Bar({
      element: 'bar-chart',
      data: {!!$finance_chart!!},
      xkey: 'date',
      ykeys: ['expense','income'],
      labels: ['Total Expenses','Total Income'],
      barColors: ["#a1000a ", "#00a131"]
  });




</script>
@stop   