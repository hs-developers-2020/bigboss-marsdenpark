<?php namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class BusinessGroupRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'business_name'	=> "required",
			'business_address' => "required",
			'file_name'	=> "required",
			'file_label'=> "required",
			'file' =>"required"

		];

		return $rules;
	}

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
		];
	}
}