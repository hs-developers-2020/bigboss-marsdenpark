<footer class="footer-background ">
    <div class="container my-5 text-white">
        <div class="row">
            <div class="col-12 col-lg-6">
                <h4 class="text-white">WANT TO OWN BIG BOSS</h4>
                <h4 class="text-white">CLEANING BUSINESS ?</h4>
                <div class="col-xs-10 col-8 col-lg-6 col-main-button pointer mt-5">
                    <a href="{{$links->ask_me_how??'https://bigbosscleaning.com.au/own-business-now.php'}}" style="text-decoration: none;">
                        <div class="container border-dash-white dash-size text-center  main-button--hover">
                            <h5 class="text-color-white my-2 font-weight-bold text-size spacing-1" style="font-weight: 100;">ASK ME HOW</h5>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <h6 class="text-white newsletter">Newsletter</h6>
                <p class="text-white">Please sign up to receive Big Boss Newsletter and updates.</p>

                <form action="" method="POST">
                    <div class="row">
                        <div class="col-12 col-lg-8">
                            <div class="input-group form" style="background: #FFF; border-radius: 50px; padding: 5px 10px">
                                <input type="text" name="email" class="form-control form-custom" placeholder="Email Addess" aria-label="email" aria-describedby="email" style="border-radius: 50px;">
                                <div class="input-group-append" style="background: #A40A13; border-radius: 50px;">
                                    <button class="input-group-text btn btn-link" type="submit"><i class="fas fa-paper-plane text-white"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="mt-3">
                    <a href="{{ $links->facebook??'https://www.facebook.com/BigBossGroupAU/' }}" target="_blank">
                        <img src="{{ asset('frontend/Icons/facebook.png') }}" class="mx-3" style="height:2rem; width:auto; border-radius: 20px; background: #FFF" />
                    </a>
                    <a href="{{ $links->twitter??'https://twitter.com/BigBossGroupAU1' }}" target="_blank">
                        <img src="{{ asset('frontend/Icons/twitter.png') }}" class="mx-3" style="height:2rem; width:auto; border-radius: 20px; background: #FFF" />
                    </a>
                    <a href="{{ $links->instagram??'https://www.instagram.com/BigBossGroupAU/' }}" target="_blank">
                        <img src="{{ asset('frontend/Icons/instagram.png') }}" class="mx-3" style="height:2rem; width:auto; border-radius: 20px; background: #FFF" />
                    </a>
                   
                   
                </div>
            
                
            </div>
        </div>
    </div>
    <div class="text-center text-white pb-2">{{date('Y')}} Big Boss Group. All rights reserved | Terms of Use | Privacy Policy</div>
</footer>

<style>
.footer-background {
    background: url("frontend/background/footer.png") no-repeat center center / cover; 
    width: 100%;
}
.text-red-10 { color: #A40A13 }
.form-custom {
    appearance: none; 
    background: transparent; 
    border: none;
    font-size: 1rem;
}
@media only screen and (max-width: 768px) {
  .newsletter {
    padding-top: 50px
  }
}
</style>