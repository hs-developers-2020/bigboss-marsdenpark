 @extends('frontend._layout.main')
 @section('content')
 @include('frontend._components.topnav-main')



 <div class="content" style="padding-bottom:7%;">


    <div class="container" style="padding-top: 155px;">
     
        <div class="col-lg-12">
        <div class="text-center">
          <img src="{{ asset('frontend/Icons/emptyData.svg')}}" class=" img-fluid w-25 p-3 rounded">
          <h2 class="mt-3">No results found</h2>
          <p class="mt-4">Try adjusting your search or filter to find what you're looking for.</p>
          <a  href="{{ route('frontend.main') }}" class="btn bg-color-red text-white text-small p-2 pl-4 pr-4">Book Now</a>
        </div>
      </div>
      </div>
    </div>
  </div>
  @stop