@extends('system._layouts.main')
<style type="text/css">
footer {
    margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
    <div class="container ">
        <form action="" id="search_filter" >
            <div class="row">
                <div class="col-lg-2 pt-2">
                    <h6 class="text-xsmall spacing-1 text-color-gray">Current Jobs</h6>
                </div>
                <div class="col-lg-3">     
                    {{-- {!!Form::select('group',$service_group,old('group'),['class' => "form-control text-color-blue font-semibold pl-3 pr-3 ", 'id' => "select_group"])!!} --}}
                </div>
                <div class="col-lg-7 mb-2 d-flex flex-row ">   
                    <input type="text" class="searchTerm" name="keyword" value="{{$keyword}}" placeholder="Search keyword such as name">
                    <a href="#"  class="btn bg-color-blue text-color-white searchButton" id="button_search">
                        <i class="fa fa-search"></i>
                    </a>
                    <a  href="{{route('system.gallery.create')}}" style="padding-left: 10px;" class="no-decoration"><button class="btn bg-color-blue text-xxxsmall text-color-white text-uppercase" type="button"><i class="fas fa-plus text-color-white mr-2"></i>Add Gallery</button></a>
                </div>       
            </div>
        </form>        
    
        <table class="table bg-color-white  mb-0 jobs-show">
            <thead>
                <tr>
                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" width="15%">Image</th>
                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold">Name</th>
                    <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs" width="10%">Action</th>
                </tr>
            </thead>
            <tbody>

                @forelse($about_us as $index => $value)
                <tr>
                    <td>
                        <img src="{{asset($value->directory.'/'.$value->filename)}}" alt="" class="img-thumbnail" width="100" height="100">
                    </td>
                    <td>
                        <p class="font-semibold text-xsmall">{{$value->name ?: "N/A"}}</p>
                    </td>
                    <td>
                        <a href="{{route('system.gallery.edit',[$value->id])}}"  title="Edit Record"  class="btn bg-color-blue text-white text-xsmall"><i class="fas fa-xs fa-edit"></i></a>
                        <a class="btn-destroy btn bg-color-blue text-white text-xsmall" data-url="{{route('system.gallery.destroy',[$value->id])}}" href="#"><i class="fas fa-xs fa-trash-alt"></i></a>
                    </td> 
                </tr>
                @empty
                <tr>
                    <td colspan="3">No Record Available</td>
                </tr>
                @endif

            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3">
                        <div class="float-right">
                            {{-- {{ $services->appends(['keyword' => $keyword, 'group' => $group ])->render() }} --}}
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>                    
    </div>
</div> 
@stop


@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script>
$(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
    @endif
    $("#action-delete").on("click",function(){
        var btn = $(this);
        $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });


});

$('#btn-confirm-delete').on('click', function() {
    console.log("aaa")
    $('.btn-link').hide();
    $('.btn-loading').button('loading');
    $('#target').submit();
 });


$('#select_group').on('change', function(){
    $('#search_filter').get(0).submit();
});
$("#button_search").on("click",function(e){
  $("#search_filter").submit();
});

$(".table")
    .delegate('.btn-destroy','click', function(){
    var url = $(this).data('url');
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this record.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#3085d6",   
            confirmButtonText: "Yes, delete it!"
        }).then(function(result) {
            window.location.href = url;
        });
});

  
</script>
@stop