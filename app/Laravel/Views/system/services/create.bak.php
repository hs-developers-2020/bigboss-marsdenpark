@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
	<div class="container" style="max-width: 1250px!important;">
		<div class="row">
			<div class="col-lg-6 mt-3">
                <h6 class="text-uppercase text-xsmall text-color-gray">Services (Cleaning)</h6>
            </div>
            <div class="col-lg-6 text-left">
                <form action="" method="GET" id="search_filter">
                    
                <div class="d-flex flex-row d-flex justify-content-end">
                    <p class="text-color-gray mt-1 font-semibold mr-2" style="font-size: 16px;">Choose Service Group</p>
                   
                     {!!Form::select('group',$service_group,old('group'),['class' => "form-control w-50 text-color-blue font-semibold pl-3 pr-3 mb-3 pb-2", 'id' => "select_group"])!!}
                </div>
               
                </form>
            </div>
		</div>
        <div class="bg-color-white p-4">
            <form action="" method="POST" id="form">
            {!! csrf_field() !!}
            </form>
            <table class="table bg-color-white  mb-0 services" id="table_data">
                <thead>
                    <tr>
                        <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" style="width: 20%;">Service Name</th>
                        <th class="p-3 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs" style="width: 35%;">Provides in</th>
                        <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm" style="width: 20%;">Active Days</th>
                          <th class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">SUN</th>
                        <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">MON</th>                                                                  
                        <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs sm">TUE</th>
                          <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">WED</th>
                          <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">THU</th>
                          <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">FRI</th>
                          <th  class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">SAT</th>
                          <th style="width: 50%;" class="p-3 pl-4 pr-4 text-uppercase text-xxsmall spacing-1 text-left text-color-blue font-weight-bold" data-breakpoints="xs">Action</th>
                    </tr>
                </thead>
                <tbody>
                    
                        @forelse($services as $index => $value)
                        <tr>
                            
                            <td>
                                <p class="text-color-blue font-semibold text-xsmall">{{$value->name}}</p>
                            </td>
                            <td>
                                {!!Form::select('regions',$reg,old('regions',$value->regions),['class' => "form-control input-sm select2"])!!}
                            </td>
                             <td>
                                <p class="text-color-blue font-semibold text-xsmall ml-2">Same Day </p>
                            </td>
                             <td>
                                <input type="checkbox" class="form-control" value="sun" name="days_available[]" 
                                {{in_array('sun',explode(",",$value->days_available)) ? 'checked="checked"' : NULL}}
                                style="width: 17px; height: 17px;">
                            </td>
                            <td>
                                <input type="checkbox" class="form-control" value="mon" name="days_available[]" style="width: 17px; height: 17px;"  {{in_array('mon',explode(",",$value->days_available)) ? 'checked="checked"' : NULL}}>
                            </td>
                            <td>
                                <input type="checkbox" class="form-control" value="tue" name="days_available[]" style="width: 17px; height: 17px;"  {{in_array('tue',explode(",",$value->days_available)) ? 'checked="checked"' : NULL}}>
                            </td>
                            <td>
                                <input type="checkbox" class="form-control" value="wed" name="days_available[]" style="width: 17px; height: 17px;"  {{in_array('wed',explode(",",$value->days_available)) ? 'checked="checked"' : NULL}}>
                            </td>
                            <td>
                                <input type="checkbox" class="form-control" value="thu" name="days_available[]" style="width: 17px; height: 17px;"  {{in_array('thu',explode(",",$value->days_available)) ? 'checked="checked"' : NULL}}>
                            </td>
                            <td>
                                <input type="checkbox" class="form-control" value="fri" name="days_available[]" style="width: 17px; height: 17px;" {{in_array('fri',explode(",",$value->days_available)) ? 'checked="checked"' : NULL}}>
                            </td>
                            <td>
                                <input type="checkbox" class="form-control" value="sat" name="days_available[]" style="width: 17px; height: 17px;"{{in_array('sat',explode(",",$value->days_available)) ? 'checked="checked"' : NULL}}>
                            </td>
                           
                            <td>
                                  <!-- <a href="{{route('system.services.save',[$value->id])}}" class="btn bg-color-blue text-white text-xsmall"><i class="fas fa-xs fa-save"></i></a> -->
                                  <button data-submit="{{route('system.services.save',[$value->id])}}" type="button" class="btn bg-color-blue text-white text-xsmall btn-save"><i class="fas fa-xs fa-save"></i></button>
                                <a href="{{route('system.services.edit',[$value->id])}}" class="btn bg-color-blue text-white text-xsmall"><i class="fas fa-xs fa-edit"></i></a>

                                <a  href="{{route('system.services.destroy',[$value->id])}}"class="btn bg-color-blue text-white text-xsmall" title="Remove Record"><i class="fas fa-xs fa-trash-alt"></i></a>
                            </td> 
                        </tr>

                    @empty
                    <tr>
                        <td>
                            <p class="text-color-blue font-semibold text-xsmall">Office-Once</p>
                        </td>
                        <td>
                            <select class="form-control text-xsmall">
                                <option>All Regions</option>
                            </select>
                        </td>
                         <td>
                            <p class="text-color-blue font-semibold text-xsmall ml-2">Same Day </p>
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="" style="width: 17px; height: 17px;">
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="" style="width: 17px; height: 17px;">
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="" style="width: 17px; height: 17px;">
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="" style="width: 17px; height: 17px;">
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="" style="width: 17px; height: 17px;">
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="" style="width: 17px; height: 17px;">
                        </td>
                        <td>
                            <input type="checkbox" class="form-control" name="" style="width: 17px; height: 17px;">
                        </td>
                        <td>
                            <button class="btn bg-color-blue text-white text-xsmall">Action</button>
                        </td>
                    </tr>
                    @endif
                   
                </tbody>
            </table>  
            <div class="d-flex justify-content-between mt-3 mb-3">
                <!-- <button data-submit="{{route('system.services.save')}}" class="btn bg-color-blue text-white text-xsmall ml-3 btn-save-all">Save Changes</button> -->
                <a href="{{route('system.services.create')}}">
                <button class="btn bg-color-green text-white text-xsmall mr-3"><i class="fas fa-plus text-color-white mr-2" style="font-size: 10px;"></i>Add Service</button>
                </a>
            </div>
        </div>                 
	</div>
</div>

@stop


@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
@endif


});

  $(function(){

    $("#action-delete").on("click",function(){
        
        var btn = $(this);
    $("#btn-confirm-delete").attr({"href" : btn.data('url')});

    });

    $("#table_data").delegate(".btn-save",'click',function(){
        var _form = $("#form");
        var btn = $(this);

        _form.attr('action',btn.data('submit'));

        $.each(btn.parents('tr').find("input,select"),function(index,value){
            var field = $(this);
            console.log($(field).attr('value'))
            console.log(field[0].type)
            var type = field[0].type;
            switch(type){
                case 'checkbox':
                    if(field.is(":checked")){
                        _form.append('<input type="hidden" name="'+$(field).attr('name')+'" value="'+$(field).attr('value')+'">')
                    }
                break;
                case 'select-one':
                    _form.append('<input type="hidden" name="'+$(field).attr('name')+'" value="'+$(field).val()+'">')
                break;
            }
        })

        _form.trigger('submit')
    })

    $('#btn-confirm-delete').on('click', function() {
        console.log("aaa")
        $('.btn-link').hide();
        $('.btn-loading').button('loading');
        $('#target').submit();
     });

 });

  $('#select_group').on('change', function(){
      $('#search_filter').get(0).submit();
    });

  
</script>
@stop

@section('page-styles')
<style type="text/css">
    footer {
        
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@stop