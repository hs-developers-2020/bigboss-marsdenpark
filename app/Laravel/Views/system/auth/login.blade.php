@extends('system._layouts.auth')

@section('content')

	<div class="background bg-dark">
		<img src="{{ asset('system/img/login-bg.jpg') }}" alt="" class="full-opacity">
	</div>
	<div class="wrapper">
		<!-- main container starts -->
		<div class="main-container">
  
			<!-- Begin page content -->
			<div class="container">
				<div class="card rounded-2 border-0 mb-3 pb-4 z3 signin-block">
					<div class="card-body">
						<div class="text-center">
						 	<img src="{{ asset('frontend/Logos/MainLogo.png') }}" style="width: 35%;" class="img-fluid" style="">
						</div>
						<p class="text-xsmall text-color-blue font-semibold text-center text-uppercase">Log-in to your account</p>
						@include('system._components.notifications')
						<div class="container ">
						 	<form action="" method="POST">
	              				{!!csrf_field()!!}
								<div class="form-group text-left float-label">
									<input id="username" name="username" type="text" autocomplete="off" class="form-control" value="{{old('username')}}">
									<label>Email address</label>
								</div>
								<div class="form-group text-left float-label">
									<input id="password" name="password" type="password" class="form-control">
									<label>Password</label>
								</div>
						{{-- 		<div class="text-center">
									<label class="text-color-blue text-xsmall mb-3 font-semibold pointer">Forgot your password? </label>
								</div> --}}
								<div>
									 <button data-dismiss="modal" type="submit" class="btn btn-success btn-xl text-uppercase w-100">Sign in</button>
									<br>
										<div class="text-center mt-3">
										<a class="text-xsmall font-semibold text-uppercase no-decoration" href="{{ route('frontend.main') }}">Go Back to Main Page</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<!-- main container ends -->
@stop

@section('page-styles')
<style type="text/css">
	@media only screen and (min-width: 1000px) {
	body{
		overflow: hidden;

	}
}
</style>
@stop