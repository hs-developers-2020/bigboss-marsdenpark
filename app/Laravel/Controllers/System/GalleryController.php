<?php


namespace App\Laravel\Controllers\System;

/**
 *
 * Models used for this controller
 */

use Illuminate\Http\Request;




/**
 *
 * Requests used for validating inputs
 */

use App\Laravel\Models\Gallery;
use App\Http\Requests\PageRequest;

use App\Laravel\Events\AuditTrailActivity;

use App\Laravel\Requests\System\AboutUsRequest;
use App\Laravel\Requests\System\GalleryRequest;
use Helper, Carbon, Str, Auth, ImageUploader, FileUploader, File, DB, AuditRequest, Event;


class GalleryController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data = [];
        parent::__construct();
        array_merge($this->data, parent::get_data());
    }

    public function index(PageRequest $request)
    {
        $this->data['keyword'] = $request->get('keyword', NULL);

        $this->data['about_us'] = Gallery::orderBy('created_at', "DESC")->paginate(10);

        return view('system.cleaning.gallery.index', $this->data);
    }

    public function create()
    {
        return view('system.cleaning.gallery.create', $this->data);
    }

    public function store (GalleryRequest $request) 
    {   
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$gallery = new Gallery;
			$gallery->fill($request->all());

            if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/gallery");
			    $gallery->path = $image['path'];
			    $gallery->directory = $image['directory'];
			    $gallery->filename = $image['filename'];
			}


			if($gallery->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "GALLERY_CREATED", 'remarks' => Auth::user()->full_name." has successfully created new gallery {$gallery->name}.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.gallery.index');
			}


			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function edit($id = NULL)
    {
        $gallery = Gallery::find($id);
	
		if (!$gallery) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.gallery.index');
		}

		$this->data['gallery'] = $gallery;

		return view('system.cleaning.gallery.edit',$this->data);
    }

    public function update (GalleryRequest $request, $id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$edit_gallery = Gallery::find($id);
			$edit_gallery->fill($request->all());

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/gallery");
			    $edit_gallery->path = $image['path'];
			    $edit_gallery->directory = $image['directory'];
			    $edit_gallery->filename = $image['filename'];
			}
			if (!$edit_gallery) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.gallery.index');
			}

			

			if($edit_gallery->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "GALLERY_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$edit_gallery->name} gallery.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.gallery.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$gallery = Gallery::find($id);

			if (!$gallery) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.gallery.index');
			}
			if($gallery->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "GALLERY_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$gallery->name} gallery.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.gallery.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
