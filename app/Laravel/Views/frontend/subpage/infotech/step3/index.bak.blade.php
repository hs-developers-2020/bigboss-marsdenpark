@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
@include('frontend._components.notifications')
{{ Session::put('percent',20) }}
<form action="{{ route('frontend.infotech.four') }}" onsubmit="return validateForm()" method="POST">
  {{ csrf_field() }}
   <div class="container" style="padding-top: 153px;padding-bottom:3%;">
     <div class="row">

      <div class="col-lg-12 text-right">
          <p class="text-color-white font-semibold spacing-1 text-uppercase">Service Quote for:  {{ Session::get('infotech.postcode') }}, {{ Session::get('infotech.sub') }}</p>
      </div>
      <div class="col-lg-12 text-center">
         <p class="text-white text-uppercase font-semibold spacing-1 mt-3">Select your development type</p>
      </div>
      <div class="col-lg-3"></div>
          <div class="col-lg-2 col-md-5 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="office-365-deployment"  name="property" value="office-365-deployment" >
                <label for="office-365-deployment" class="text-xxsmall text-uppercase pt-4 pb-4 text-mobile" style="color: white;">
                  <img src="{{ asset('frontend/button-icons/infotech/office_365.svg') }}" class="img-fluid"> <br>
                  <span class="mobile-text">Office 365 Deployment</span></label>
              </div>
            </div>
            <div class="col-lg-2 col-md-5 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="website-development"  name="property" value="website-development" >
                <label for="website-development" class="text-xxsmall text-uppercase pt-4 pb-4 text-mobile" style="color: white;">
                  <img src="{{ asset('frontend/button-icons/infotech/website_development.svg') }}" class="img-fluid"> <br>
                  <span class="mobile-text">Website Development</span></label>
              </div>
            </div>
        <div class="col-lg-2 col-md-5 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="mobile-app-development"  name="property" value="mobile-app-development">
                <label for="mobile-app-development" class="text-xxsmall text-uppercase pt-4 pb-4" style="color: white;">
                  <img src="{{ asset('frontend/button-icons/infotech/mobile _development.svg') }}" class="img-fluid"> <br>
                  <span  class="mobile-text">Mobile App Development</span>
                </label>
              </div>
              @if ($errors->first('property_type'))
                <span class="badge badge-danger">{{ $errors->first('property_type') }}</span>
              @endif
            </div>
            <div class="col-lg-3"></div>
            <div class="col-lg-3"></div>
            <div class="col-lg-2 col-md-5 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="digital-marketing"  name="property" value="digital-marketing" >
                <label for="digital-marketing" class="text-xxsmall text-uppercase pt-4 pb-4 text-mobile" style="color: white;">
                  <img src="{{ asset('frontend/button-icons/infotech/digital_marketing.svg') }}" class="img-fluid"> <br>
                  <span class="mobile-text">Digital Marketing</span></label>
              </div>
            </div>
            <div class="col-lg-2 col-md-5 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="graphics-design-multimedia"  name="property" value="graphics-design-multimedia" >
                <label for="graphics-design-multimedia" class="text-xxsmall text-uppercase pt-4 pb-4 text-mobile" style="color: white;">
                  {{-- <img src="{{ asset('frontend/button-icons/infotech/business selected.svg') }}" class="img-fluid"> <br> --}}
                  <span class="mobile-text">Graphics Design & Multimedia</span></label>
              </div>
            </div>
            <div class="col-lg-2 col-md-5 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="cloud-infrastracture"  name="property" value="cloud-infrastracture" >
                <label for="cloud-infrastracture" class="text-xxsmall text-uppercase pt-4 pb-4 text-mobile" style="color: white;">
                  {{-- <img src="{{ asset('frontend/button-icons/infotech/business selected.svg') }}" class="img-fluid"> <br> --}}
                  <span class="mobile-text">Cloud Infrastracture</span></label>
              </div>
            </div>
            <div class="col-lg-3"></div>
              <div class="col-lg-12 mt-2 text-center">
                  <button type="submit" id="next" class="next btn card-shadow font-semibold spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 " href="#"style="color: white !important;">Continue</button>
              </div>
        </div>
    </div>
  </div>
   </form>
   @include('frontend._components.footer-main')
 @stop
@section('scripts-content')
<script>

function validateForm() {
    var radios = document.getElementsByName("property");
    var formValid = false;

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked) formValid = true;
        i++;        
    }

    if (!formValid) Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Please select property Type!',
  // footer: '<a href>Please select </a>'
});
    return formValid;
}


</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 

@media only screen and (max-width: 1600px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
 .footer-bottom{
margin-top: 100px
}
}

@media only screen and (min-width: 1600px) {

   #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
  }
  .picture-size {
  height:5%; width: 7%;
  }
 .footer-bottom{
position: absolute;
bottom: 0%;
top: 100%;
}
  }

   @media only screen and (max-width: 992px) {
.mobile-text {
  font-size: 20px;
}
}
 

</style>
@stop