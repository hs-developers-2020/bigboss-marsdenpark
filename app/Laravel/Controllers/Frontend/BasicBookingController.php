<?php

namespace App\Laravel\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Laravel\Models\CleaningService;
use App\Laravel\Models\CustomerFeedback;

use App\Http\Requests\PageRequest;

use App\Laravel\Services\ResponseManager;

use Helper, Carbon, Session, Str, DB,Input,Event,PDF,Curl,Request,Signature,Log,Auth,Mail;

class BasicBookingController extends Controller
{
	protected $data;
	
	public function bookSubmitted(){
		return view ('frontend.page.booking-submitted.index');
	}

	public function bookApproved(){
		return view ('frontend.page.booking-approved.index');
	}

	public function bookingInvoice(){
		return view ('frontend.page.booking-invoice.index');
	}

	public function rating($id = null){
		$booking = CleaningService::where('booking_id', $id)->first();

		if(!$booking) {
			return view('frontend.page.booking-invoice.no-result');
		}

		$this->data['booking'] = $booking;
		$this->data['feedback'] = CustomerFeedback::where('booking_id', $id)->where('user_id', Auth::user()->id)->first();
		return view('frontend.page.rate-service.index', $this->data);
	}

	public function store_feedback(PageRequest $request){
		$_exist_feedback = CustomerFeedback::where('booking_id', $request->get('booking_id'))->where('user_id', Auth::user()->id)->first();

		if($_exist_feedback) {
			session()->flash('notification-status','warning');
			session()->flash('notification-msg','You already rated this booking.');
			return redirect()->back();
		}

		//dd($request->get('booking_id'));

		$feedback = new CustomerFeedback;
		$feedback->user_id = Auth::user()->id;
		$feedback->booking_id = $request->get('booking_id');
		$feedback->franchisee_id = $request->get('franchisee_id');
		$feedback->rate  = $request->get('rate');
		$feedback->feedback = $request->get('feedback');

		if($feedback->save()){
			session()->flash('notification-status','success');
			session()->flash('notification-msg','Feedback was successfully save.');
			return redirect()->route('frontend.main');
		}

		session()->flash('notification-status','failed');
		session()->flash('notification-msg','Something went wrong.');
		return redirect()->back();
	}

	public function create(Request $request){
		$current_page = $request->session()->put('postcode',$request->postcode);
		return $this->session->get('postcode');
	}
}
