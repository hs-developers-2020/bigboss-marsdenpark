<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader;
use App\Laravel\Models\Article;
use App\Laravel\Models\ArticleComment;
use Illuminate\Http\Request;
use App\Laravel\Requests\Api\ArticleCommentRequest;
use App\Laravel\Transformers\ArticleCommentTransformer;
use App\Laravel\Transformers\TransformerManager;

class ArticleCommentController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {
        $article = $request->get('article_data');
		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "List of comments";

        // $sort_by = $request->get('sort')

        $comments = ArticleComment::where('article_id',$article->id)->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_LIST";
        $this->response['has_morepages'] = $comments->hasMorePages();
        $this->response['data'] = $this->transformer->transform($comments, new ArticleCommentTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function store(ArticleCommentRequest $request, $format = '') {

        $user = $request->user();
        $article = $request->get('article_data');


        $comment = new ArticleComment;
        $comment->user_id = $user->id;
        $comment->article_id = $article->id;
        $comment->fill($request->only('content'));
        $comment->save();

        $this->response['msg'] = "Added new comment.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ARTICLE_CREATED";
        $this->response['data'] = $this->transformer->transform($comment, new ArticleCommentTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function show(Request $request, $format = '') {

        $comment = $request->get('comment_data');

        $this->response['msg'] = "Comment detail.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_DETAIL";
        $this->response['data'] = $this->transformer->transform($comment, new ArticleCommentTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update(ArticleCommentRequest $request, $format = '') {
        $comment = $request->get('comment_data');
        $article = $request->get('article_data');
        $user = $request->user();

        $comment->fill($request->only('content'));

        $comment->save();

        $this->response['msg'] = "Comment has been updated.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_MODIFIED";
        $this->response['data'] = $this->transformer->transform($comment, new ArticleCommentTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function destroy(Request $request, $format = '') {

        $comment = $request->get('comment_data');

        $comment->delete();

        $this->response['msg'] = "Comment has been removed.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_DELETED";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}