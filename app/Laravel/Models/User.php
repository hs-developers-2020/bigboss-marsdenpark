<?php

namespace App\Laravel\Models;

use Carbon, Helper,Str;
use App\Laravel\Models\Views\UserGroup;
use Illuminate\Notifications\Notifiable;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Database\Eloquent\SoftDeletes;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable 
{
    use Notifiable, SoftDeletes, DateFormatterTrait;

    protected $table = "user";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'firstname','lastname', 'username', 'email', 'contact_number',
        'password','abn',
        'job_title', 'business_type', 'company_name','entity_name','type',

        /*'description', */'path', 'directory', 'filename', 
        'last_notification_check','currency','fb_id', 'is_active','path', 'directory', 'filename'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    protected $appends = ['avatar','full_name'];


    public function getFullNameAttribute(){

        $name = "{$this->firstname} {$this->lastname}";

        
        
        return $name;
    }

    public function getAvatarAttribute(){

        if($this->filename){
            return "{$this->directory}/resized/{$this->filename}";
        }

        if($this->fb_id){
            return "https://graph.facebook.com/{$this->fb_id}/picture?width=310&height=310#v=1.0";
        }

        return asset('placeholder/user.jpg');
    }

    public function scopeKeyword($query,$keyword){
        if($keyword){
            $key = Str::lower($keyword);
            return $query->where(function($query) use ($key){
                    $query->whereRaw("firstname LIKE '%{$key}%'")
                        ->orWhereRaw("lastname LIKE '%{$key}%'")
                        ->orWhereRaw("username LIKE '%{$key}%'")
                        ->orWhereRaw("LOWER(type) LIKE '%{$key}%'");
                         
            });

        }
    }

    public function scopeType($query,$type){
        if($type){
            $type = Str::lower($type);
            return $query->where(function($query) use ($type){
                    $query->whereRaw("LOWER(type) = '{$type}'");
                        
                         
            });

        }
    }

    
    /**
     * Get the devices for this user.
     */
    public function devices() {
        return $this->hasMany("App\Laravel\Models\UserDevice", "user_id");
    }

    public function info() {
        return $this->belongsTo("App\Laravel\Models\UserInfo", "id" ,"user_id");
    }

    public function userimage() {
        return $this->belongsTo("App\Laravel\Models\UserImage", "id" ,"user_id");
    }

    public function police_check(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'police_check');
    }

    public function awareness_certificate(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'awareness_certificate');
    }

    public function first_aid_license(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'first_aid_license');
    }
    public function insurance(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'insurance');
    }

    public function whitecard(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'whitecard');
    }

    public function other_docs(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'other_documents');
    }

    public function business_avatar(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'business_avatar');
    }
    public function staff_uniform(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'staff_uniform');
    }
    public function utility_vehicle(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'utility_vehicle');
    }


    public function business_group() {
        return $this->belongsTo("App\Laravel\Models\BusinessGroup", "head_id" ,"user_id");
    }

    /**
     * Get the devices for this user.
     */
    public function specialty() {
        return $this->hasOne('App\Laravel\Models\Specialty','id','specialty_id');
    }

    /**
     * Get the facebook account for this user.
     */
    public function facebook() {
        return $this->hasOne("App\Laravel\Models\UserSocial", "user_id")->where('provider', "facebook");
    }

   
    /**
     * Search users that match a keyword.
     */
   /* public function scopeKeyword($query, $keyword = "") {
        return $query->where(function($query) use($keyword) {
            $query->WhereRaw("LOWER(email) = '{$keyword}'");
                // ->orWhere('email', 'like', "%{$keyword}%");
        });
    }*/

    public function scopeAccount_type($query,array $types){
        if(count($types) > 0){
            return $query->whereIn('type',$types);
        }
    }


    /**
     * Route notifications for the FCM channel.
     *
     * @return string
     */
    public function routeNotificationForFcm()
    {
        return $this->devices()->where('is_login', '1')->pluck('reg_id')->toArray() ?: 'user123';
    }

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return array
     */
    public function receivesBroadcastNotificationsOn()
    {
        // return [
        //     new PrivateChannel("USER.{$this->id}"),
        // ];

        return "user.{$this->id}";
    }

    /**
     * Get user's avatar
     */
    public function getAvatar() {

        if($this->fb_id){
            return "https://graph.facebook.com/{$this->fb_id}/picture?width=310&height=310#v=1.0";
        }

        return asset('assets/img/avatar4.png');
    }


    public function scopeTypes($query,array $types){
        if(count($types) > 0){
            return $query->whereIn('type',$types);
        }
    }


    // public function getNameAttribute(){
    //     return "{$this->fname} {$this->lname}";
    // }

    public function coordinates(){
        return $this->hasMany('App\Laravel\Models\UserTerritory', 'user_id', 'id');
    }

    public function territory(){
        return $this->hasMany('App\Laravel\Models\UserTerritory', 'user_id', 'head_id');
    }

}
