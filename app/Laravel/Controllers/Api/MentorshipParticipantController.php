<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader;
use App\Laravel\Models\User;
use App\Laravel\Models\Views\Mentorship;
use App\Laravel\Models\MentorshipParticipant;
use App\Laravel\Models\MentorshipConversation;
use Illuminate\Http\Request;
use App\Laravel\Requests\Api\MentorshipConversationRequest;
use App\Laravel\Transformers\MentorshipTransformer;
use App\Laravel\Transformers\MentorshipParticipantTransformer;
use App\Laravel\Transformers\MentorshipConversationTransformer;
use App\Laravel\Transformers\TransformerManager;

class MentorshipParticipantController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {
        $mentorship = $request->get('mentorship_data');
		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "{$mentorship->title} Participants";

        // $sort_by = $request->get('sort')

        $members = MentorshipParticipant::where('mentorship_id',$mentorship->id)->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CHAT_PARTICPANTS";
        $this->response['total'] = $members->total();
        $this->response['has_morepages'] = $members->hasMorePages();
        $this->response['data'] = $this->transformer->transform($members, new MentorshipParticipantTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function moderator(Request $request, $format = '') {
        $mentorship = $request->get('mentorship_data');
        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "{$mentorship->title} Moderators";

        // $sort_by = $request->get('sort')

        $members = MentorshipParticipant::where('mentorship_id',$mentorship->id)->where('role','moderator')->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CHAT_MODERATORS";
        $this->response['total'] = $members->total();
        $this->response['has_morepages'] = $members->hasMorePages();
        $this->response['data'] = $this->transformer->transform($members, new MentorshipParticipantTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }


    public function member(Request $request, $format = '') {
        $mentorship = $request->get('mentorship_data');
        $per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "{$mentorship->title} Members";

        // $sort_by = $request->get('sort')

        $members = MentorshipParticipant::where('mentorship_id',$mentorship->id)->where('role','member')->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CHAT_MEMBERS";
        $this->response['total'] = $members->total();
        $this->response['has_morepages'] = $members->hasMorePages();
        $this->response['data'] = $this->transformer->transform($members, new MentorshipParticipantTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function store(Request $request, $format = '') {

        $auth = $request->user();

        $mentorship = $request->get('mentorship_data');
        $user = $request->get('user_data');

        $is_moderator = MentorshipParticipant::where('mentorship_id',$mentorship->id)
                                        ->where('user_id',$auth->id)
                                        ->where('role','moderator')->first();

        if(!$is_moderator){
            $this->response['msg'] = "You don't have the permission to do the requested action. Unauthorized participant.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "UNAUTHORIZED_PARTICIPANT";
            $this->response_code = 403;
            goto callback;
        }

        $is_participant = MentorshipParticipant::where('mentorship_id',$mentorship->id)
                                        ->where('user_id',$user->id)->first();
        if($is_participant){
            $this->response['msg'] = "{$user->name} is already in the group.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "ALREADY_PARTICIPANT";
            $this->response_code = 400;
            goto callback;
        }

        $member = new MentorshipParticipant;
        $member->user_id = $user->id;
        $member->mentorship_id = $mentorship->id;
        $member->role = "member";
        $member->save();

        $this->response['msg'] = "Added new participant.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "NEW_PARTICIPANT";
        $this->response['data'] = $this->transformer->transform($member, new MentorshipParticipantTransformer, 'item');
        $this->response_code = 201;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function promote(Request $request, $format = '') {
        $auth = $request->user();
        $mentorship = $request->get('mentorship_data');
        $user = $request->get('user_data');

        if($request->get('user_id') == "1"){$user = new User; $user->id = 1;}

        $mentorship_participant = $request->get('mentorship_participant_data');


        if($mentorship->owner_user_id != $auth->id){
            $is_moderator = MentorshipParticipant::where('mentorship_id',$mentorship->id)
                                            ->where('user_id',$auth->id)
                                            ->where('role','moderator')->first();

            if(!$is_moderator){
                $this->response['msg'] = "You don't have the permission to do the requested action. Unauthorized participant.";
                $this->response['status'] = FALSE;
                $this->response['status_code'] = "UNAUTHORIZED_PARTICIPANT";
                $this->response_code = 403;
                goto callback;
            }
        }

        if(in_array($mentorship_participant->user_id,[$mentorship->owner_user_id,1]) OR $mentorship_participant->role == "moderator"){
            $this->response['msg'] = "No more action is required in the selected user.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "NO_ACTION_REQUIRED";
            $this->response_code = 400;
            goto callback;
        }   

        $mentorship_participant->role = "moderator";
        $mentorship_participant->save();

        $this->response['msg'] = "{$user->name} promoted as moderator in the group.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "NEW_MODERATOR";
        $this->response['data'] = $this->transformer->transform($mentorship_participant, new MentorshipParticipantTransformer, 'item');
        $this->response_code = 201;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function demote(Request $request, $format = '') {
        $auth = $request->user();
        $mentorship = $request->get('mentorship_data');
        $user = $request->get('user_data');
        $mentorship_participant = $request->get('mentorship_participant_data');

        if($request->get('user_id') == "1"){$user = new User; $user->id = 1;}


        if($mentorship->owner_user_id != $auth->id){
            $is_moderator = MentorshipParticipant::where('mentorship_id',$mentorship->id)
                                            ->where('user_id',$auth->id)
                                            ->where('role','moderator')->first();

            if(!$is_moderator){
                $this->response['msg'] = "You don't have the permission to do the requested action. Unauthorized participant.";
                $this->response['status'] = FALSE;
                $this->response['status_code'] = "UNAUTHORIZED_PARTICIPANT";
                $this->response_code = 403;
                goto callback;
            }
        }
        

        if(in_array($mentorship_participant->user_id,[$mentorship->owner_user_id,1]) OR $mentorship_participant->role == "member"){
            $this->response['msg'] = "No more action is required in the selected user.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "NO_ACTION_REQUIRED";
            $this->response_code = 400;
            goto callback;
        }   

        $mentorship_participant->role = "member";
        $mentorship_participant->save();

        $this->response['msg'] = "{$user->name} demoted to member role in the group.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "DEMOTED_MEMBER";
        $this->response['data'] = $this->transformer->transform($mentorship_participant, new MentorshipParticipantTransformer, 'item');
        $this->response_code = 201;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function show(Request $request, $format = '') {

        $comment = $request->get('comment_data');

        $this->response['msg'] = "Comment detail.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_DETAIL";
        $this->response['data'] = $this->transformer->transform($comment, new MentorshipConversationTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update(ChatConversationRequest $request, $format = '') {
        $comment = $request->get('comment_data');
        $article = $request->get('article_data');
        $user = $request->user();

        $comment->fill($request->only('content'));

        $comment->save();

        $this->response['msg'] = "Comment has been updated.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_MODIFIED";
        $this->response['data'] = $this->transformer->transform($comment, new ArticleCommentTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function destroy(Request $request, $format = '') {

        $comment = $request->get('comment_data');

        $comment->delete();

        $this->response['msg'] = "Comment has been removed.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_DELETED";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}