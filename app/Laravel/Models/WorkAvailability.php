<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class WorkAvailability extends Model
{
    use DateFormatterTrait,SoftDeletes;

    protected $table = "work_availability";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date','work_stated','max_leads','time_in','time_out'
    ];

    public $timestamps = true;


    public function info() {
        return $this->belongsTo("App\Laravel\Models\UserInfo", "id" ,"user_id");
    }
    public function user(){
        return $this->hasOne('App\Laravel\Models\UserImage', 'user_id', 'id')->where('type', 'police_check');
    }

}
