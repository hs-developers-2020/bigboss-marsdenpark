<nav class="navbar navbar-expand-xl navbar-dark pb-4 pt-3 bg-color-white fixed-top custom-nav">
    <div class="container">

@if(Request::segment(1) == 'cleaning')
  <a href="{{route('frontend.main')}}" class="navbar-brand" style="width:  150px !important; margin-right: 0px !important;"><img src="{{ asset('frontend/Logos/Cleaning.png') }}"  class="w-100"></a>
@elseif(Request::segment(1) == 'booking')
<a href="{{route('frontend.main')}}" class="navbar-brand" style="width:  150px !important; margin-right: 0px !important;"><img src="{{ asset('frontend/Logos/Cleaning.png') }}"  class="w-100"></a>
@elseif(Request::segment(1) == 'accounting') 
<a href="{{route('frontend.main')}}" class="navbar-brand" style="width:  150px !important; margin-right: 0px !important;"><img src="{{ asset('frontend/Logos/Accountants.png') }}"  class="w-100"></a>
@elseif(Request::segment(1) == 'infotech') 
<a href="{{route('frontend.main')}}" class="navbar-brand" style="width:  150px !important; margin-right: 0px !important;"><img src="{{ asset('frontend/Logos/Infotech.png') }}"  class="w-100"></a>
@else
  @if(Auth::check())
    <a href="{{route('frontend.main')}}" class="navbar-brand" style="width:  150px !important; margin-right: 0px !important;"><img src="{{ asset('frontend/images/main-logo.png') }}"  class="w-100"></a>
  @else
    <a href="http://bigbossgroup.com.au/" class="navbar-brand" style="width:  150px !important; margin-right: 0px !important;"><img src="{{ asset('frontend/images/main-logo.png') }}"  class="w-100"></a>
  @endif
@endif

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-list text-color-red"></i>
    </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item p-2 pr-3 pt-3 mt-1 d-flex flex-row">
        <a class="nav-link text-color-red text-small font-weight-bold" href="#"><i class=" fas fa-phone-square"></i>  Call 1800 131 599</a>
      </li>
      <li class="nav-item p-2 pr-3 pt-4">
        <a class="nav-link text-color-gray text-xsmall font-weight-bold "  href="{{route('frontend.how-it-works.index')}}">How it works</a>
      </li>
      <li class="nav-item p-2 pr-3 pt-4">
        <a class="nav-link text-color-gray text-xsmall font-weight-bold"  href="{{route('frontend.services.index')}}">Services</a>
      </li>
      <li class="nav-item p-2 pr-3 pt-4">
        <a class="nav-link text-color-gray text-xsmall font-weight-bold"  href="{{route('frontend.faqs')}}">FAQS</a>
      </li>
      <li class="nav-item p-2 pr-3 pt-4">
        <a class="nav-link text-color-gray text-xsmall font-weight-bold" href="mailto:info@bigbossgroup.com.au?Subject" target="_top">Support</a>
      </li>
       @if(Auth::check())
       @else
       <li class="nav-item pt-3 mr-3">
        <button type="button" id="track" class="btn bg-color-blue text-white font-semibold text-xxsmall" data-toggle="modal" data-target="#exampleModal">
          Track my booking
        </button>
      </li>
       @endif
     
      @if(Auth::check())
      <li class="nav-item dropdown pt-3 mr-3 ">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="text-color-red ml-1 font-semibold text-xsmall">Welcome Back! {{ Str::title(auth()->user()->firstname) }}</span>
          <img src="{{ asset('frontend/images/default.png') }}" style="border-radius: 50%; background-color: red;width: 42px" alt="Generic placeholder image" class="ml-2">
        </a>
        <div class="dropdown-menu pt-2 pb-3 bg-white shadow" aria-labelledby="dropdown01"  style="border: 0px solid white !important">
           <a class="dropdown-item font-semibold text-color-gray mt-2 text-xsmall" href="#" id="track" data-toggle="modal" data-target="#exampleModal">
          Track my booking
          </a>
          <a class="dropdown-item font-semibold text-color-gray mt-2 text-xsmall" href="{{ route('frontend.dashboard') }}">Dashboard</a>
          <a class="dropdown-item font-semibold text-color-gray mt-2 text-xsmall" href="{{ route('frontend.profile') }}">Profile</a>
         
          <a class="dropdown-item font-semibold text-color-gray mt-2 text-xsmall" href="{{route('frontend.clogout')}}">Logout</a>
        </div>
      </li>
   
        {{-- <button class="font-semibold btn my-2 my-sm-0 btn-color-red text-xxsmall" type="submit">LOGOUT</button> --}}
      @else
       <li class="nav-item pr-4 pt-3">
      <a href="{{route('frontend.login')}}" style="text-decoration: none;"> <button class="font-semibold btn my-2 my-sm-0 btn-color-red text-xxsmall" type="submit">Login</button></a>
       </li>
      @endif
     
     </ul>
  </div>
</div>
</nav>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="position: absolute;top:30%;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
       <div class="modal-header bg-color-blue w-100 h-100">
        <h5 class="modal-title text-color-white" id="exampleModalCenterTitle">Track my Booking</h5>
        <button type="button" class="close  text-color-white" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('frontend.tracker.index') }}" method="POST">
        {{ csrf_field() }}
      <div class="modal-body p-4 pb-5">
        <p class="text-color-gray font-semibold text-xsmall">To track your booking. Please type your booking ID</p>
        <div class="d-flex flex-row">
            <input type="text" id="booking_id" name="booking_id"  autocomplete="off" required="" class="form-control text-center">
            <button type="submit" id="search" class="btn bg-color-blue text-xxsmall text-white text-uppercase"><i class="fas fa-search"></i></button>
          </div>
      </div>
  
      </form>
    </div>
  </div>
</div>


