<?php


namespace App\Laravel\Controllers\System;

/**
 *
 * Models used for this controller
 */

use Illuminate\Http\Request;




/**
 *
 * Requests used for validating inputs
 */

use App\Laravel\Models\Gallery;
use App\Laravel\Models\Feedback;

use App\Http\Requests\PageRequest;
use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Requests\System\AboutUsRequest;
use App\Laravel\Requests\System\GalleryRequest;
use App\Laravel\Requests\System\FeedbackRequest;
use Helper, Carbon, Str, Auth, ImageUploader, FileUploader, File, DB, AuditRequest, Event;


class FeedbackController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data = [];
        parent::__construct();
        array_merge($this->data, parent::get_data());
    }

    public function index(PageRequest $request)
    {
        $this->data['keyword'] = $request->get('keyword', NULL);

        $this->data['feedback'] = Feedback::orderBy('created_at', "DESC")->paginate(10);

        return view('system.cleaning.feedback.index', $this->data);
    }

    public function create()
    {
        return view('system.cleaning.feedback.create', $this->data);
    }

    public function store (FeedbackRequest $request) 
    {   
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$feedback = new Feedback;
			$feedback->fill($request->all());

            if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/feedback");
			    $feedback->path = $image['path'];
			    $feedback->directory = $image['directory'];
			    $feedback->filename = $image['filename'];
			}


			if($feedback->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FEEDBACK_CREATED", 'remarks' => Auth::user()->full_name." has successfully created new feedback {$feedback->name}.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.feedback.index');
			}


			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function edit($id = NULL)
    {
        $feedback = Feedback::find($id);
	
		if (!$feedback) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.feedback.index');
		}

		$this->data['feedback'] = $feedback;

		return view('system.cleaning.feedback.edit',$this->data);
    }

    public function update(FeedbackRequest $request, $id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$edit_feedback = Feedback::find($id);
			$edit_feedback->fill($request->all());

			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/feedback");
			    $edit_feedback->path = $image['path'];
			    $edit_feedback->directory = $image['directory'];
			    $edit_feedback->filename = $image['filename'];
			}
			if (!$edit_feedback) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.feedback.index');
			}
			
			if($edit_feedback->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FEEDBACK_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$edit_feedback->name} feedback.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.feedback.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$feedback = Feedback::find($id);

			if (!$feedback) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.feedback.index');
			}

			if($feedback->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "FEEDBACK_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$feedback->name} feedback.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.feedback.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
