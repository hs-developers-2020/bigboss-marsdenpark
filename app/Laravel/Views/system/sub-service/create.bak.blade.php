@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
  <div class="container">
    <p class="text-color-gray font-semibold mb-3 text-uppercase">Add Sub Service For: <b>{{$service_info->name}}</b></p>
  </div>
  <form action="" class="with-confirmation" method="POST" enctype="multipart/form-data">
    {!!csrf_field()!!}
    <div class="container pb-5 pl-5 pr-5 pt-4 mt-2" style="background-color: white; ">       
      <div class="row">
        @if($service_info->group == "information_technology")
       <div class="col-lg-3 mt-2" id="service_type_container">
          <div class="text-color-gray mb-2 {{$errors->first('has_addon') ? 'has-error' : NULL}}" >
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold ">Has Add ons ?</span>
          </div>
          {!!Form::select('has_addon',$has_addons,old('has_addon'),["class" => "form-control", 'id' => 'addons_select'])!!}
          @if($errors->first('has_addon'))
          <span class="help-block" style="color: red;">{{$errors->first('has_addon')}}</span>
          @endif
        </div>
        @endif
        

        <div class="col-lg-12 mt-4">
          <p class="text-color-gray text-xsmall font-semibold text-uppercase">Service Details</p>
        </div>
        <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('name') ? 'has-error' : NULL}}">
             <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Sub Service Name
              <span class="text-color-red ml-1">*</span>
            </span>
            <input type="text" placeholder="Enter service name" class="form-control text-xxsmall" name="name" value="{{ old('name') }}">
            @if($errors->first('name'))
              <span class="help-block" style="color: red;">{{$errors->first('name')}}</span>
            @endif
          </div>
        </div>
        <div class="col-lg-4 mt-3" id="subservice_price">
          <div class="text-color-gray mb-2 {{$errors->first('price') ? 'has-error' : NULL}}">
             <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Service Price
              <span class="text-color-red ml-1">*</span>
            </span>
            <input type="text" placeholder="Enter service price" class="form-control text-xxsmall" name="price" value="{{ old('price') }}">
            @if($errors->first('price'))
              <span class="help-block" style="color: red;">{{$errors->first('price')}}</span>
            @endif
          </div>
        </div>
        <div class="col-lg-3 mt-2" id="service_type_container">
          <div class="text-color-gray mb-2 {{$errors->first('region') ? 'has-error' : NULL}}" >
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <i class="fas fa-chevron-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold ">Regions</span>
           
          </div>
          {!!Form::select('region[]',$reg,old('region'),["class" => "form-control custom-select", 'id' => 'basic' ,'multiple' => 'multiple'])!!}<br>
          @if($errors->first('region'))
            <span class="help-block" style="color: red;">{{$errors->first('region')}}</span>
          @endif
        </div>
        <div class="col-lg-12">
          <div class="text-color-gray mb-2 {{$errors->first('file') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Sub Service Icon 
              <span class="text-color-red">*</span>
            </span>
          </div>
          <div class="d-flex flex-row mb-3 mt-3">
            <input type="file" id="file" name="file"/> 
          </div>
          @if($errors->first('file'))
              <span class="help-block" style="color: red;">{{$errors->first('file')}}</span>
           @endif
        </div>
        @if($service_info->group == "information_technology")
        <div class="form-group form-repeat-risks col-lg-12 row" id="add_field">
          <input type="hidden" name="key_risks" />
          <div class="col-lg-12">
            <div id="existingGroupRisks1" class="repeat-input-group" style="margin-top: 0.5em;">        
              <button type="button" id="repeater_add_risks" class="btn" style="margin-top:10px;margin-right: 10px;">Add Field</button><br><br>                 
            </div>
          </div>
          <div class="col-lg-12" id="repeat_form">
            @forelse(range(1,count(old('addons')?:1 )) as $index => $value)
            <div class="row addons-item">
              <div class="col-lg-2">
                <div class="text-color-gray mb-2 {{$errors->first("addons_file.{$index}") ? 'has-error' : NULL}}">
                  <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                  <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Add Ons Icon 
                    <span class="text-color-red">*</span>
                  </span>
                  <input type="file" name="addons_file[]"/> 
                </div>
                @if($errors->first("addons_file.{$index}"))
                  <span class="help-block" style="color: red;">{{$errors->first("addons_file.{$index}")}}</span>
                @endif
              </div>
              <div class="col-lg-4">
                <div class="text-color-gray mb-2 {{$errors->first("addons.{$index}") ? 'has-error' : NULL}}">
                  <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                  <span class="ml-2 text-xxsmall font-semibold text-uppercase">Add Ons Name
                  <span class="text-color-red ml-1">*</span>
                  </span>
                  <input type="text" placeholder="Enter add on name" class="form-control text-xxsmall" name="addons[]" value="{{old("addons.{$index}")}}" >
                  @if($errors->first("addons.{$index}"))
                    <span class="help-block" style="color: red;">{{$errors->first("addons.{$index}")}}</span>
                  @endif
                </div>
              </div>
              <div class="col-lg-4">
                <div class="text-color-gray mb-2 {{$errors->first("addons_price.{$index}") ? 'has-error' : NULL}}">
                  <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                  <span class="ml-2 text-xxsmall font-semibold text-uppercase">Add Ons Price
                  <span class="text-color-red ml-1">*</span>
                  </span>
                  <input type="text" placeholder="Enter add on name" class="form-control text-xxsmall" name="addons_price[]" value="{{old("addons_price.{$index}")}}">
                  @if($errors->first("addons_price.{$index}"))
                    <span class="help-block" style="color: red;">{{$errors->first("addons_price.{$index}")}}</span>
                  @endif
                </div>
              </div>
              
              @if($index > 0)
              <div class="col-md-2"><button type="button" style="margin-top: 28px;" class="btn btn-sm btn-danger btn-remove">Remove</button></div>
              @endif
            </div>
            @empty
            <div class="row addons-item">
              <div class="col-lg-2">
                <div class="text-color-gray mb-2">
                  <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                  <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Add Ons Icon 
                    <span class="text-color-red">*</span>
                  </span>
                </div>
                <input type="file" name="addons_file[]"/> 
              </div>
              <div class="col-lg-4">
                <div class="text-color-gray mb-2">
                  <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                  <span class="ml-2 text-xxsmall font-semibold text-uppercase">Add Ons Name
                  <span class="text-color-red ml-1">*</span>
                  </span>
                  <input type="text" placeholder="Enter add on name" class="form-control text-xxsmall" name="addons[]" >
                </div>
              </div>
              <div class="col-lg-4">
                <div class="text-color-gray mb-2">
                  <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                  <span class="ml-2 text-xxsmall font-semibold text-uppercase">Add Ons Price
                  <span class="text-color-red ml-1">*</span>
                  </span>
                  <input type="text" placeholder="Enter add on name" class="form-control text-xxsmall" name="addons_price[]">
                </div>
              </div>
            </div>
            @endforelse
          </div>
        </div>

        
        @endif

        <div class="col-lg-12 mt-5">
          <button type="submit" class="btn bg-color-blue text-xxsmall text-color-white p-2 pl-3 pr-3 text-uppercase">Add Service</button>
          <a href="{{route('system.sub_service.index',[$main_id])}}" class="btn bg-color-red text-xxsmall text-color-white p-2 pl-3 pr-3 ml-2 text-uppercase">Cancel</a>
        
        </div> 
      </div>
    </div>
  </form>
</div>



@section('page-styles')

<style type="text/css">
  footer {
position: absolute;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
  }
body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@stop
@section('page-scripts')


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<script type="text/javascript">
  
$('#addons_select').on('change', function(){
  var val = $(this).val();

   if (val == 'yes') {       
        $('#add_field').show();
        $('#subservice_price').hide();
        
      }else{
         $('#add_field').hide();
        $('#subservice_price').show();
        $('select[name="price"]').val('');
      }

}).change();

$(function(){
  $('#basic').multiselect({

    templates: {
        li: '<li><a href="javascript:void(0);"><label class="pl-2"></label></a></li>'
    }
  });
});

  let id = 0;

  $("#repeat_form").delegate(".btn-remove","click",function(){
    var parent_div = $(this).parents(".addons-item");

    parent_div.fadeOut(500,function(){
      $(this).remove();
    })
  });

  $('#repeater_add_risks').on('click', function(){
    var repeat_item = $("#repeat_form .row").eq(0).prop('outerHTML');
    var main_holder = $(this).parents(".row").parent();

    $("#repeat_form").append(repeat_item).find("div[class^=col]:last").parent().append('<div class="col-md-2"><button type="button" style="margin-top: 28px;" class="btn btn-sm btn-danger btn-remove">Remove</button></div>')

  });
  // $('#repeater_add_risks').on('click', function(){
  //     id += 1
  //     let repeatFileInput = `<div id="repeatInputGroupRisks${id}" class="repeat-input-group col-lg-12" style="margin-top: 0.5em;">
  //                           <div class="row">
  //                             <div class="col-lg-4 mt-3 ">
  //                               <div class="text-color-gray mb-2 {{$errors->first('addons') ? 'has-error' : NULL}}">
  //                                  <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
  //                                 <span class="ml-2 text-xxsmall font-semibold text-uppercase">Add Ons Name
  //                                   <span class="text-color-red ml-1">*</span>
  //                                 </span>
  //                                 <input type="text" placeholder="Enter add on name" class="form-control text-xxsmall" name="addons[]">
  //                                 @if($errors->first('addons'))
  //                                   <span class="help-block" style="color: red;">{{$errors->first('addons')}}</span>
  //                                 @endif
  //                               </div>
  //                             </div>
  //                             <div class="col-lg-4 mt-3 ">
  //                               <div class="text-color-gray mb-2 {{$errors->first('addons_price') ? 'has-error' : NULL}}">
  //                                  <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
  //                                 <span class="ml-2 text-xxsmall font-semibold text-uppercase">Add Ons Price
  //                                   <span class="text-color-red ml-1">*</span>
  //                                 </span>
  //                                 <input type="text" placeholder="Enter Add On price" class="form-control text-xxsmall" name="addons_price[]">
  //                                 @if($errors->first('addons_price'))
  //                                   <span class="help-block" style="color: red;">{{$errors->first('addons_price')}}</span>
  //                                 @endif
  //                               </div>
  //                             </div>
  //                             <div class="col-md-4" style="margin-top: 3em">
  //                               <button type="button" data-id="repeatInputGroupRisks${id}" class="btn-danger">Delete</button>
  //                             </div>
  //                           </div>`
  //     $('.form-repeat-risks').append(repeatFileInput);
  // });

   $('.form-repeat-risks').on('click', '.btn-danger', function(){
     var id = $(this).data('id');
     $('#' + id).remove();
  });



</script>
@stop