@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
	<div class="container ">
		<div class="row">
    		<div class="col-lg-8">
                <div class="row">
                    <div class="col-md-2 pt-2">
                        <h6 class="text-xsmall spacing-1 text-color-gray">Task Details</h6>
                    </div>
                    <div class="col-md-10  mb-3" >
                        <button class="btn btn-warning rounded-3 text-color-white text-xxsmall">{{Str::title($jobs->status)}}</button>
                    </div>
                </div>
            </div>
            @if(in_array($jobs->status , ["ongoing","pending"]))
            <div class="col-lg-4 mb-3 text-right">
                @if(in_array($jobs->status , ["ongoing"]))
                <a href="{{ route('system.jobs.invoice',[$jobs->booking_id]) }}" class="btn bg-color-blue text-color-white text-xxsmall">Generate Invoice</a>
                @endif
                <a href="{{ route('system.jobs.cancel',[$jobs->id]) }}" class="btn bg-color-red text-color-white text-xxsmall">Cancel Quote</a>    
            </div>
            @endif
           
        </div>
        <div class="row">
            <div class="col-lg-12 text-right">
                <div id="map" style="width: 100%; height: 200px;"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 bg-color-white w-100 p-4" >
                <div class="row">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-between">
                            <p class="text-color-gray font-semibold">Job ID #{{$jobs->booking_id}}</p>
                                
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 mt-3">
                        <div class="d-flex flex-row">
                            <figure class="avatar150 rounded-circle border" style="min-width: 50px !important;height: 60px !important;width: 60px !important;">
                              <img src="{{ asset('frontend/images/user.jpg') }}" alt="user image">
                            </figure>
                            <p class="text-color-gray font-semibold ml-3 mt-2  w-75">{{Str::title($jobs->full_name)}} | +61 {{$jobs->contact}} | {{$jobs->email}} <br>#{{$jobs->location}}</p>
                        </div>
                     </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 mt-4">
                        <div class="row mb-3">
                            <div class="col-lg-4">
                                <span class="text-color-gray font-semibold">Service Type</span> 
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->property_type ?: 'No Data')}}</p>
                            </div>
                            <div class="col-lg-4 ">
                                <span class="text-color-gray font-semibold">Service Package</span>
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->service_type) ?: 'No Data'}}</p>
                            </div>
                            @if($jobs->services)
                             <div class="col-lg-4">
                                <span class="text-color-gray font-semibold">Extras Services</span>
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->services) ?: 'No Data'}}</p>
                            </div>
                            @endif
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-4 ">
                                <span class="text-color-gray font-semibold">Region</span>
                                <p class="text-color-blue font-semibold">{{$region}}</p>
                            </div>
                            <div class="col-lg-4 ">
                                <span class="text-color-gray font-semibold">Booking Date</span>
                                <p class="text-color-blue font-semibold">{{Helper::date_only($jobs->booking_date) }}</p>
                            </div>
                            <div class="col-lg-4 ">
                                <span class="text-color-gray font-semibold">Service Time</span>
                                @if($jobs->time_start)

                                    <p class="text-color-blue font-semibold">{{Helper::time_only($jobs->time_start) }} - {{Helper::time_only($jobs->time_end)}}</p>
                                @else
                                     <p class="text-color-blue font-semibold">{{$jobs->time_start ?: 'No Time Selected'}} - {{$jobs->time_end ?: 'No Time Selected'}}</p>
                                @endif

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 mb-3">
                                <span class="text-color-gray font-semibold">Estimated Total</span>
                                <p class="text-color-blue font-semibold">${{Helper::amount($jobs->amount + $jobs->service_type_amount)}}</p>                 
                            </div>
                            <div class="col-lg-4 mb-3">
                                <span class="text-color-gray font-semibold">Payment Method</span>
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->payment)}}</p>                 
                            </div>
                            <div class="col-lg-4 mb-3">
                                <span class="text-color-gray font-semibold">Booking Total</span>
                                <p class="text-color-blue font-semibold">${{Helper::amount($jobs->booking_total)}}</p>                 
                            </div>
                            <div class="col-lg-4 mb-3">
                                <span class="text-color-gray font-semibold">Service Group</span>
                                <p class="text-color-blue font-semibold">{{Str::title($jobs->type)}}</p>                 
                            </div>
                        </div>
                    </div> 
                </div>
              
                <div class="row">
                    
                    <div class="col-lg-12 mt-3">
                        @if($jobs->status == "pending")
                            <a href="{{ route('system.jobs.assigned',[$jobs->id]) }}" class="btn bg-color-blue text-white text-xsmall mr-3">Assign Staff</a>
                        @endif
                        @if($jobs->status == "ongoing")
                            <a href="{{ route('system.jobs.completed',[$jobs->id]) }}" class="btn bg-color-blue text-white text-xsmall" >Done Booking</a>
                        @endif
                        <a href="{{ route('system.jobs.show') }}" class="btn bg-color-red text-white text-xsmall">Cancel</a>
                    </div>

                </div>
                
            </div> 
            <div class="col-lg-1" style="flex: 0 0 3.33333%!important;"></div>

            <div class="col-lg-5 bg-color-white p-4" style="max-width: 45.666667%!important;flex: 0 0 44.66667%!important;">
                <form action="" method="POST">
                    {{ csrf_field() }}
                    @if($jobs->property_type == "house" || $jobs->property_type == "home")
                    <p class="text-color-gray font-semibold">SUB SERVICES</p>
                    <div class="row" style="margin-top: -30px;">
                        <div class="col-md-6" {{$errors->first('type') ? 'has-error' : NULL}}> 
                            {!!Form::select('room_type',$type,old('room_type',$jobs->room_type),['class' => "form-control input-sm select2 mt-3" ,'id' => "input_type"])!!}
                            @if($errors->first('type'))
                              <span class="help-block" style="color: red;">{{$errors->first('type')}}</span>
                            @endif
                        </div> 
                        <div class="col-md-6" {{$errors->first('unit') ? 'has-error' : NULL}}> 
                            {!!Form::select('unit',$sub_type,old('unit',$jobs->unit),['class' => "form-control input-sm select2 mt-3" ,'id' => "unit"])!!}
                            @if($errors->first('unit'))
                              <span class="help-block" style="color: red;">{{$errors->first('unit')}}</span>
                            @endif
                        </div> 
                        <div class="col-md-12" {{$errors->first('number_of_rooms') ? 'has-error' : NULL}}>
                            {!!Form::select('number_of_rooms',$rooms,old('number_of_rooms'),['class' => "form-control input-sm select2 mt-3" ,'id' => "room_number"])!!}
                             @if($errors->first('number_of_rooms'))
                              <span class="help-block" style="color: red;">{{$errors->first('number_of_rooms')}}</span>
                            @endif
                        </div>
                        <input type="hidden" name="input_rooms" value="{{$jobs->number_of_rooms}}">    
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-12 mt-3">
                            <p class="text-color-gray font-semibold">STANDARD SERVICES</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div id="existingGroupRisks1" class="repeat-input-group" style="margin-top: 0.5em;">        
                                <button type="button" id="repeater_add_risks" class="btn" style="margin-top:10px;margin-right: 10px;">Add Field</button><br><br>                 
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" id="repeat_form">
                            @forelse($others as $index => $value)
                            <div class="row service">
                                <div class="col-lg-5">
                                    <div class="text-color-gray mb-2 {{$errors->first("other_services.{$index}") ? 'has-error' : NULL}}">
                                        <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                        <span class="ml-2 text-xxsmall font-semibold text-uppercase">Other Services</span>
                                        <input type="text" placeholder="Enter service name" class="form-control text-xxsmall service-input" id="other_services" name="other_services[]" value="{{ old('other_services.{$index}',$value->services) }}">
                                        @if($errors->first("other_services.{$index}"))
                                          <span class="help-block" style="color: red;">{{$errors->first("other_services.{$index}")}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="text-color-gray mb-2 {{$errors->first("other_services_price.{$index}") ? 'has-error' : NULL}}">
                                        <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                        <span class="ml-2 text-xxsmall font-semibold text-uppercase">Other Services Price</span>
                                        <input type="text" placeholder="Enter total price" class="form-control text-xxsmall service-input" id="other_services_price"name="other_services_price[]" value="{{ old('other_services_price.{$index}',$value->price) }}">
                                        @if($errors->first("other_services_price.{$index}"))
                                          <span class="help-block" style="color: red;">{{$errors->first("other_services_price.{$index}")}}</span>
                                        @endif
                                    </div>
                                </div>
                                @if($index > 0)
                                  <div class="col-md-2"><button type="button" style="margin-top: 28px;" class="btn btn-sm btn-danger btn-remove">Remove</button></div>
                                @endif
                            </div>
                            @empty
                                @foreach(range(1,count(old('other_services')?:1 )) as $index => $value)
                                
                                    <div class="row service">
                                        <div class="col-lg-5">
                                            <div class="text-color-gray mb-2  {{$errors->first("other_services.{$index}") ? 'has-error' : NULL}}">
                                                <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                                <span class="ml-2 text-xxsmall font-semibold text-uppercase">Other Services</span>
                                                <input type="text" placeholder="Enter service name" class="form-control text-xxsmall" name="other_services[]" value="{{ old('other_services.{$index}') }}">
                                                @if($errors->first("other_services.{$index}"))
                                                  <span class="help-block" style="color: red;">{{$errors->first("other_services.{$index}")}}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <div class="text-color-gray mb-2 {{$errors->first("other_services_price.{$index}") ? 'has-error' : NULL}}">
                                                <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
                                                <span class="ml-2 text-xxsmall font-semibold text-uppercase">Other Services Price</span>
                                                <input type="text" placeholder="Enter total price" class="form-control text-xxsmall service-input" name="other_services_price[]" value="{{ old('other_services_price.{$index}') }}">
                                                @if($errors->first("other_services_price.{$index}"))
                                                  <span class="help-block" style="color: red;">{{$errors->first("other_services_price.{$index}")}}</span>
                                                @endif
                                            </div>
                                        </div>
                                        @if($index > 0)
                                          <div class="col-md-2"><button type="button" style="margin-top: 28px;" class="btn btn-sm btn-danger btn-remove">Remove</button></div>
                                        @endif
                                    </div>
                                @endforeach
                            @endforelse
                        </div>
                        
                    </div>
                    <input type="hidden" name="business_group" value="{{$jobs->type}}">
                    <div class="row">
                        <div class="col-lg-12 mt-2">
                            <button type="submit" class="btn bg-color-blue text-white text-xsmall mr-3 {{$jobs->status == "completed" ? 'disabled' : '' }}">Update Booking</button>
                        </div> 
                    </div>
                </form>
           </div>    
        </div>
        @if($jobs->franchisee_id)
        <div class="row mt-3">
            <div class="col-lg-6 bg-color-white">
                <div class="row">
                    <div class="col-lg-1 mb-3"> 
                        <figure class="avatar150 rounded-circle border mt-3" style="min-width: 50px !important;height: 60px !important;width: 60px !important;">
                            @if($jobs->staff->directory)
                            <img src="{{asset($jobs->staff->directory.'/'.$jobs->staff->filename)}}" alt="user image">
                            @else
                            <img src="{{ asset('frontend/images/default.png') }}" alt="Generic placeholder image" class="">
                            @endif
                        </figure>
                    </div>
                    <div class="col-lg-7 mt-3">
                        <p class="text-color-gray font-semibold ml-3 mt-3">{{$jobs->staff->full_name}} 
                            <span class="mr-2 ml-2">|</span>
                            <span class="text-color-light">Assigned Staff</span>
                        </p>
                    </div>
                    <div class="col-md-4" style="margin-top: 27px;">
                        <a href="{{ route('system.jobs.remove',[$jobs->id]) }}" class="btn bg-color-red text-white text-xsmall btn-sm"> <i class="fas fa-xs fa-trash-alt"></i> Remove Staff</a>
                       
                    </div>
                   
                </div>

            </div>
        </div>
        @endif
    </div>            
</div>          



@stop

@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script type="text/javascript">

function initMap() {
  // The location of Uluru
  var uluru = {lat: {{$jobs->latitude}}, lng: {{$jobs->longitude}}};


  // The map, centered at Uluru
  var map = new google.maps.Map(
      document.getElementById('map'), {zoom: 15, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
$(document).ready(function() {
    var max_fields = 3;
    var wrapper = $(".container--office");
    var add_button = $(".add_form_field");

    var x = 1;
    $(add_button).click(function(e) {
        e.preventDefault();
        if (x < max_fields) {
            x++;
            $(wrapper).append('<div class="d-flex flex-row"><input type="text" placeholder="Enter item name" class="form-control mr-2 mt-3" name="item[]"/><input type="number" placeholder="Enter item price" class="form-control mr-2 m-t3" name="price[]"/><a href="#" class=" mt-3 btn btn-danger delete text-color-white text-color-white text-uppercase"><i class="fas fa-trash-alt"></i></a></div>'); //add input box
        } else {  
            alert('You Reached the limits')
        }
    });

    $(wrapper).on("click", ".delete", function(e) {
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })
});
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOzUJWdUgmPii5xcVAsz2-mBj3V2zHQPM&callback=initMap"></script>

<script type="text/javascript">
    $(function(){   
        var rooms = $('input[name=input_rooms]').val()
       
        $('#unit').on('change', function(){
            var type = $('#input_type').val()
            var unit = $(this).val()
            console.log($(this).val())
            console.log(type)
            $.ajax({
                url : "{{route('system.jobs.get_rooms')}}",
                dataType : "json",
                data: {id : $(this).val(), 'input_type' : $('#input_type').val() },
                type: "GET",
                async: false,
                success : function(data){
                    if(data){
                        $('#room_number').empty()
                        $('#room_number').append('<option value="">--Select Rooms--</option>');
                        $.each(data,function(i, room_number){
                            if (!type || !unit) {
                                $('#room_number').empty()
                                $('#room_number').append('<option value="">--Select Rooms--</option>');
                            }else{
                                if(rooms == room_number.id){
                                  $('#room_number').append('<option value="' + room_number.id +'" selected>' + room_number.name +" - $"+  room_number.price + " </option>");
                                }
                                else{
                                  $('#room_number').append('<option value="' + room_number.id +'">' + room_number.name + " - $" + room_number.price +"</option>");
                                }
                            }
                            
                        });
                    }
                },  
                error : function(jqXHR,textStatus,thrownError){
                }
            });
        }).change();

        $('#input_type').on('change', function(){
            var type = $('#unit').val()
            var unit = $(this).val()
            console.log($(this).val())
            console.log(type)
            $.ajax({
                url : "{{route('system.jobs.get_rooms')}}",
                dataType : "json",
                data: {id : $('#unit').val(), 'input_type' : $(this).val() },
                type: "GET",
                async: false,
                success : function(data){
                    if(data){
                        $('#room_number').empty()
                        $('#room_number').append('<option value="">--Select Rooms--</option>');
                        $.each(data,function(i, room_number){
                            if (!type || !unit) {
                                $('#room_number').empty()
                                $('#room_number').append('<option value="">--Select Rooms--</option>');
                            }else{
                                if(rooms == room_number.id){
                                  $('#room_number').append('<option value="' + room_number.id +'" selected>' + room_number.name +" - $"+  room_number.price + " </option>");
                                }
                                else{
                                  $('#room_number').append('<option value="' + room_number.id +'">' + room_number.name + " - $" + room_number.price +"</option>");
                                }
                            }
                            
                        });
                    }
                },  
                error : function(jqXHR,textStatus,thrownError){
                }
            });
        }).change();

        $('[data-toggle="tooltip"]').tooltip(); 

        @if(Session::has('alert-type'))
        var type="{{Session::get('alert-type','info')}}"
        console.log(type)
        switch(type){
            case 'info':
                toastr.options = {
                "positionClass": "toast-top-center"
                };
                toastr.info("{{ Session::get('notification-msg') }}")
                break;
            case 'success':
                toastr.options = {
                "positionClass": "toast-top-center"
                };
                toastr.success("{{ Session::get('notification-msg') }}");
                break;
            case 'warning':
                toastr.options = {
                "positionClass": "toast-top-center"
                };
                toastr.warning("{{ Session::get('notification-msg') }}");
                break;
            case 'failed':
                toastr.options = {
                "positionClass": "toast-top-center"
                };
                toastr.error("{{ Session::get('notification-msg') }}");
                break;
            }
            @endif
    });

    $("#repeat_form").delegate(".btn-remove","click",function(){
        var parent_div = $(this).parents(".service");
        parent_div.fadeOut(500,function(){
          $(this).remove();
        })
    });

    $('#repeater_add_risks').on('click', function(){
        var repeat_item = $("#repeat_form .row").eq(0).prop('outerHTML');
        var main_holder = $(this).parents(".row").parent();

    $("#repeat_form").append(repeat_item).find("div[class^=col]:last").parent().append('<div class="col-md-2"><button type="button" style="margin-top: 28px;" class="btn btn-sm btn-danger btn-remove">Remove</button></div>').find(".service-input").val('')
    });
</script>
@stop

@section('page-styles')
<style type="text/css">
    footer {
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@stop