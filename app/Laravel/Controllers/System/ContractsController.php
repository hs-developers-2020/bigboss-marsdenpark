<?php

namespace App\Laravel\Controllers\System;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Service;
use App\Laravel\Models\BusinessGroup;
use App\Laravel\Models\BusinessGroupFile;


/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\System\BusinessGroupRequest;

use App\Http\Requests\PageRequest;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, Auth, ImageUploader,Input,PDF,FileUploader,File;

class ContractsController extends Controller
{
	public function index(){
		
		$month = Carbon::now()->addMonth()->format('Y-m-d');
		$startDate = Carbon::now()->format('Y-m-d');
		$this->data['contract_cleaning'] = BusinessGroupFile::whereIn('group_id', function($query){
											return $query->select('id')
													->where('group', 'cleaning')
													->from('business_group');
										})->count();
		$this->data['expiring_contract'] = BusinessGroupFile::whereBetween('date_expiry' , [$startDate ,$month])->where('date_expiry','>=',$startDate)->count();
		$this->data['high_contract'] = BusinessGroupFile::where('is_high_risk',"yes")->count();
		$this->data['expired_contract'] = BusinessGroupFile::where('date_expiry','<=',$startDate)->count();
		$this->data['total_contract'] = BusinessGroupFile::orderBy('created_at' , "DESC")->count();

		return view ('system.contract.index',$this->data);
	}
	public function create(){

		return view ('system.contract.create',$this->data);
	}
 
	
	public function store (BusinessGroupRequest $request) {
		try {
			
			$new_business_group = new BusinessGroup;
			$new_business_group->fill($request->all());


			if($request->hasFile('file')) {
			    $image = ImageUploader::upload($request->file('file'), "uploads/user_profile");
			    $new_business_group->path = $image['path'];
			    $new_business_group->directory = $image['directory'];
			    $new_business_group->filename = $image['filename'];
			}
			

			if($new_business_group->save()) {
				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.contract-group.index');
			}
			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}


	}
	

}
