<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ForgotRequest extends RequestManager{

	public function rules(){

		$rules = [
			'reset_email'		=> "required|email",
		
		];

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'reset_email.required'		=> "Please indicate your email address",
			'reset_email.email'	=> "Invalid email address format.",
			
		];
	}
}