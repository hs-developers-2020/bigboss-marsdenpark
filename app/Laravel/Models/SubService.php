<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubService extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "sub_service";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','price','type' ,'storey',
    ];

    public $timestamps = true;

     
}

