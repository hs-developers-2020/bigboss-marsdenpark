@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')

<div class="wrapper ">
  <form action="" method="POST">
    {{ csrf_field() }}
  	<div class="container">
  		<div class="row">
  			<div class="col-lg-5 pt-3 mb-3">
          <h6 class="text-xsmall spacing-1 text-color-gray text-uppercase">Assigned Staff</h6>
        </div>
        <div class="col-lg-7 mb-3 text-right d-flex flex-row">
            <p class="text-color-gray font-semibold text-nowrap pt-2 mr-3 text-uppercase">Select Business</p>
            <select class="form-control text-color-blue font-semibold h-100 mr-3 w-75">
              <option>Da Cleaners</option>
            </select>
            <input type="text" class="form-control h-100" placeholder="Search Business Name" name="">
        </div>    
        <div class="col-lg-12 bg-color-white w-100 pb-4 mt-2">
          <div class="row">
            <div class="col-lg-8 text-right" id="map" style="padding: 0px !important;height: 400px;">
            </div>    
            <div class="col-lg-4 p-4" style="max-height: 400px; overflow-y: scroll;">
              <div class="row">
                <div class="col-lg-12">
                  <p class="text-color-gray font-semibold text-uppercase">Nearby Staff (Da Cleaners)</p>
                </div>
                @forelse($assigned as $index => $value)

                <div class="col-lg-9 mt-3">
                  <div class="d-flex flex-row">
                    <figure class="avatar150 rounded-circle border" style="min-width: 50px !important;height: 60px !important;width: 60px !important;">
                      @if($value->user->directory)
                        <img src="{{asset($value->user->directory.'/'.$value->user->filename)}}" alt="Generic placeholder image" class="">
                      @else
                       <img src="{{ asset('frontend/images/default.png') }}" alt="Generic placeholder image" class="">
                      @endif
                    </figure>
                    <p class="text-color-gray font-semibold ml-3 mt-2  w-75">{{ Str::title($value->user->full_name) }} 
                      <br>
                      <span>{{ $value->user_distance($jobs->latitude, $jobs->longitude) }} KM</span>
                    </p>
                  </div>
                </div>

                <div class="col-lg-3">
                  <input type="radio" class="form-control" style="width: 30%;height: 30%; margin-top:35px;" name="staff_id" value="{{ $value->user_id }}">
                </div>
                <div class="col-lg-12">
                  <hr>
                </div>

                @empty

                <div class="col-lg-9 mt-3">
                  <div class="d-flex flex-row">
                    <h5>No Staff Available</h5>
                  </div>
                </div>

               

                @endforelse

                <div class="col-lg-9 mt-3">

                </div>     
              </div>
            </div>     
              
          <div class="col-lg-12 mt-3">
            <button type="submit" class="btn bg-color-blue text-white text-xsmall mr-3">Assign Staff</button>
            <a href="{{ route('system.jobs.details', [$jobs->id]) }}" class="btn bg-color-red text-white text-xsmall">Cancel</a>
          </div>
        
      </div>
    </div>
  </form>
</div>

@stop
@section('page-styles')
<style type="text/css">
  footer {
    position: absolute;
    bottom: 0;
    top: 100%;  
  }
  body {
    padding-top: 9% !important;
  }
  .f-10 {
    font-size: 8px;
  }

</style>
@stop
@section('page-scripts')
<script type="text/javascript">

 function initMap() {
  // The location of Uluru
  var uluru = {lat: {{$jobs->latitude}}, lng: {{$jobs->longitude}}};


  // The map, centered at Uluru
  var map = new google.maps.Map(
    document.getElementById('map'), {zoom: 8, center: uluru});
  // The marker, positioned at Uluru
  var marker = new google.maps.Marker({position: uluru, map: map});
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOzUJWdUgmPii5xcVAsz2-mBj3V2zHQPM&callback=initMap"></script>
@stop