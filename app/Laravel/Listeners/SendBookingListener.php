<?php 
namespace App\Laravel\Listeners;

use App\Laravel\Events\SendBooking;

class SendBookingListener{

	public function handle(SendBooking $email){
		$email->job();

	}
}