<?php

namespace App\Laravel\Controllers\System;

use App\Laravel\Services\ResponseManager;

use App\Laravel\Models\CustomerFeedback;
use App\Http\Requests\PageRequest;

use Curl,Str;

class CustomerFeedbackController extends Controller
{
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['service_group'] = [ ''=>"Choose Service",'cleaning' => "Cleaning","information_technology" => "Information Technology","accounting" => "Accounting"];
	}	

 	public function index(Pagerequest $request){

  		$this->data['keyword'] = $request->get('keyword', NULL);
 		$this->data['group'] = $request->get('group',false);
 		$this->data['feedbacks'] = CustomerFeedback::group($this->data['group'])->orderBy('created_at', 'DESC')->paginate(10);
		return view ('system.customer-feedback.index', $this->data);
	}

	public function show($id = NULL){
		$this->data['conversations'] = [];
		// $response = Curl::to("https://ronniecastro.freshdesk.com/api/v2/tickets/{$id}/conversations")
  //    	->withHeader('Authorization: Bearer ' . base64_encode('IUAoQZEL4IBYm5SKLOUs'))
  //    	->returnResponseObject()
  //    	->get();

  //    	if($response->status == 200) {
  //    		$this->data['conversations'] = json_decode($response->content);
  //    	}

		return view ('system.customer-feedback.show', $this->data);
	}
}
