@extends('system._layouts.main')
<style type="text/css">
footer {
		margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
	}
body {
	padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.font-small {
    font-size: 12px !important; 
}
</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
  
  <form action="" class="with-confirmation" method="POST" enctype="multipart/form-data">
    {!!csrf_field()!!}
    <div class="container">
      <div class="row page-title-row">
        <div class="col-lg-2 ">
          <h6 class="page-title text-color-gray font-semibold text-uppercase" style="font-size: 17px;">Add new user:</h6>
        </div>
        @if(in_array(Str::lower(Auth::user()->type), ['super_user']))
          <div class="col-lg-10  {{$errors->first('user_type') ? 'has-error' : NULL}}">
            {!!Form::select('user_type',$users_type,old('user_type'),['class' => "form-control w-25 text-color-blue font-semibold pl-3 pr-3", 'id' => "user_type"])!!}
            @if($errors->first('user_type'))
              <span class="help-block" style="color: red;">{{$errors->first('user_type')}}</span>
            @endif 
          </div>
          
        @endif           
      </div>
    </div>
    <div class="container pb-5 pl-5 pr-5 pt-4" style="background-color: white; ">       
      <div class="row">
        <div class="col-lg-12 mt-4 mb-3">
          <p class="text-color-gray text-xsmall font-weight-bold spacing-1 text-uppercase">Account Information</p>
        </div>
        <div class="col-lg-12">
          <div class="text-color-gray mb-2">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">User Profile 
              <span class="text-color-red">*</span>
            </span>
          </div>
          <div class="d-flex flex-row mb-3 mt-3 {{$errors->first('file') ? 'has-error' : NULL}}">
            <input type="file" id="file" name="file"/> 
          </div>
          @if($errors->first('file'))
            <span class="help-block" style="color: red;">{{$errors->first('file')}}</span>
          @endif
        </div>
        <div class="col-lg-6">
          <div class="text-color-gray mb-2 {{$errors->first('firstname') ? 'has-error' : NULL}}">      
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall text-uppercase font-semibold ">First Name</span>
              <input type="text" placeholder="Enter First name" class="form-control text-xsmall" name="firstname" value="{{ old('firstname') }}">
          </div>
          @if($errors->first('firstname'))
            <span class="help-block" style="color: red;">{{$errors->first('firstname')}}</span>
          @endif                           
        </div>
        <div class="col-lg-6 mt-3 mt-lg-0 mt-md-0">
          <div class="text-color-gray mb-2 {{$errors->first('lastname') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Last Name</span>
            <input type="text"  placeholder="Enter Last name" class="form-control text-xsmall" name="lastname" value="{{ old('lastname') }}">
          </div>
          @if($errors->first('lastname'))
            <span class="help-block" style="color: red;">{{$errors->first('lastname')}}</span>
          @endif 
        </div>
          
        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold">Contact number</span>
            <div class="input-icons"> 
              <i class="icon text-color-blue p-3" style="padding: 5px !important; padding-left: 10px !important;">
                <p class="text-color-blue font-weight-bold text-xxsmall text-uppercase" style="font-style: normal; margin-top: 2px;">+61 |</p>
              </i>
              <input type="text" placeholder="Enter Contact number" class="input-field form-control text-xxsmall text-uppercase" style="padding-left: 50px !important; padding-bottom: 8px !important; padding-top: 8px !important;" name="contact_number" value="{{ old('contact_number') }}">
              @if($errors->first('contact_number'))
                <span class="help-block" style="color: red;">{{$errors->first('contact_number')}}</span>
              @endif 
            </div>
          </div>
        </div>
        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('email') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Email address</span>
            <input type="text"  placeholder="Enter email address" class="form-control text-xsmall" name="email" value="{{ old('email') }}">
             @if($errors->first('email'))
            <span class="help-block" style="color: red;">{{$errors->first('email')}}</span>
          @endif 
          </div>
         
        </div>
        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('alternate_email') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Alternate Email address</span>
            <input type="text"  placeholder="Enter alternate email address" class="form-control text-xsmall" name="alternate_email" value="{{ old('alternate_email') }}">
          </div>
          @if($errors->first('alternate_email'))
            <span class="help-block" style="color: red;">{{$errors->first('alternate_email')}}</span>
          @endif
        </div>
        <div class="col-lg-4 mt-3 ">
          <div class="text-color-gray mb-2 {{$errors->first('username') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Username</span>
            <input type="text"  placeholder="Enter username" class="form-control text-xsmall" name="username" value="{{ old('username') }}">
          </div>
          @if($errors->first('username'))
            <span class="help-block" style="color: red;">{{$errors->first('username')}}</span>
          @endif 
        </div>
       
        
        <div class="col-lg-12 mt-3">
            <p class="text-xsmall font-semibold">Address</p>
        </div>
        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('street') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Street</span>
            <input type="text"  placeholder="Unit No / Street" class="form-control text-xsmall" name="street" value="{{ old('street') }}">
          </div>
          @if($errors->first('street'))
            <span class="help-block" style="color: red;">{{$errors->first('street')}}</span>
          @endif
        </div>
        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('city') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">City</span>
            <input type="text"  placeholder="City" class="form-control text-xsmall" name="city" value="{{ old('city') }}">
          </div>
          @if($errors->first('city'))
            <span class="help-block" style="color: red;">{{$errors->first('city')}}</span>
          @endif
        </div>
        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('state') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">State</span>
            {!!Form::select('state',$reg,old('state'),['class' => "form-control input-sm select2"])!!}
          </div>
          @if($errors->first('state'))
            <span class="help-block" style="color: red;">{{$errors->first('state')}}</span>
          @endif
        </div>
        <div class="col-lg-3 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('post_code') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Post Code</span>
            <input type="text"  placeholder="Post Code" class="form-control text-xsmall" name="post_code" value="{{ old('post_code') }}">
          </div>
          @if($errors->first('post_code'))
            <span class="help-block" style="color: red;">{{$errors->first('post_code')}}</span>
          @endif
        </div>
        <div class="col-lg-12 mt-3">
            <p class="text-xsmall font-semibold">Business Classification (Cleaning, Accounting, FX, Infotech)</p>
        </div>

        <div class="col-lg-4 mt-3">
          <div class="text-color-gray mb-2 {{$errors->first('business_group') ? 'has-error' : NULL}}">  
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Classification</span>
             {!!Form::select('business_group',$service_group,old('business_group'),['class' => "form-control text-xsmall input-sm select2", 'id' => "business_group"])!!}
          </div>
          @if($errors->first('business_group'))
            <span class="help-block" style="color: red;">{{$errors->first('business_group')}}</span>
          @endif
        </div>
        @if(in_array(Str::lower(Auth::user()->type), ['super_user','head']))
        <div class="col-lg-6 mt-3" id="user_head">
          <div class="text-color-gray mb-2 {{$errors->first('user_head') ? 'has-error' : NULL}}">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Select Big Boss Head</span>
            {!!Form::select('user_head',$business_name,old('user_head'),['class' => "form-control text-xsmall input-sm select2", 'id' => "user_head"])!!}
          </div>
          @if($errors->first('user_head'))
            <span class="help-block" style="color: red;">{{$errors->first('user_head')}}</span>
          @endif
        </div>
        @endif 



        {{-- Start Area Head --}}
        @if(in_array(Str::lower(Auth::user()->type), ['super_user','head']))
        <div class="col-lg-12" id="area_head_input">
          <div class="row">
            <div class="col-lg-12 mt-4" id="business_label">
              <p class="text-color-gray font-semibold">Business Details</p>
            </div>
            <div class="col-lg-12 mt-2" id="avatar_container">
              <div class="text-color-gray mb-2">
                
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Establishment Avatar</span>
              </div>
              <div class="d-flex flex-row mb-2 mt-1">
                <input type="file"  name="business_avatar[]"/> 
              </div>
            </div> 
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('business_name') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Business Name</span>
                <input type="text"  placeholder="Enter Establishment Name" class="form-control text-xsmall" name="business_name" value="{{ old('business_name') }}">
              </div>
              @if($errors->first('business_name'))
                <span class="help-block" style="color: red;">{{$errors->first('business_name')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('business_number') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Australian Business Number</span>
                <input type="text"  placeholder="Enter ABN" class="form-control text-xsmall" name="business_number" 
                value="{{ old('business_number') }}">
              </div>
              @if($errors->first('business_number'))
                <span class="help-block" style="color: red;">{{$errors->first('business_number')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('tax_number') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Tax File Number</span>
                <input type="text"  placeholder="Enter TFN" class="form-control text-xsmall" name="tax_number" value="{{ old('tax_number') }}">
              </div>
              @if($errors->first('tax_number'))
                <span class="help-block" style="color: red;">{{$errors->first('tax_number')}}</span>
              @endif
            </div>
            <div class="col-lg-12 mt-3">
              <p class="text-xsmall font-semibold">Business Address</p>
            </div>
            <div class="col-lg-4 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('business_street') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Street</span>
                <input type="text"  placeholder="Unit No / Street" class="form-control text-xsmall" name="business_street" value="{{ old('business_street') }}">
              </div>
              @if($errors->first('business_street'))
                <span class="help-block" style="color: red;">{{$errors->first('business_street')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('business_city') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">City</span>
                <input type="text"  placeholder="Unit No / Street" class="form-control text-xsmall" name="business_city" value="{{ old('business_city') }}">
              </div>
              @if($errors->first('business_city'))
                <span class="help-block" style="color: red;">{{$errors->first('business_city')}}</span>
              @endif
            </div>
            <div class="col-lg-4 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('business_state') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">State</span>
                {!!Form::select('business_state',$reg,old('business_state'),['class' => "form-control input-sm select2"])!!}
              </div> 
              @if($errors->first('business_state'))
                <span class="help-block" style="color: red;">{{$errors->first('business_state')}}</span>
              @endif
            </div>
            <div class="col-lg-3 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('business_post_code') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Post Code</span>
                <input type="text"  placeholder="Post Code" class="form-control text-xsmall" name="business_post_code" value="{{ old('business_post_code') }}">
              </div>
              @if($errors->first('business_post_code'))
                <span class="help-block" style="color: red;">{{$errors->first('business_post_code')}}</span>
              @endif         
            </div>
            <div class="col-lg-12 mt-4">
              <p class="text-color-gray font-semibold">Business Equipment Photos</p>
            </div>
            <div class="col-lg-6 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('file') ? 'has-error' : NULL}}">
                
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Utilitiy Vehicle</span>
              </div>
              <div class="d-flex flex-row mb-2 mt-1">
                <input type="file"  name="utility_vehicle[]"/> 
              </div>
              @if($errors->first('file'))
                <span class="help-block" style="color: red;">{{$errors->first('file')}}</span>
              @endif
            </div>
             <div class="col-lg-6 mt-2">
              <div class="text-color-gray mb-2 {{$errors->first('file') ? 'has-error' : NULL}}">
                
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Staff Uniform</span>
              </div>
              <div class="d-flex flex-row mb-2 mt-1">
                <input type="file"  name="staff_uniform[]"/> 
              </div>
              @if($errors->first('file'))
                <span class="help-block" style="color: red;">{{$errors->first('file')}}</span>
              @endif
            </div>
          </div>
        </div>
        @endif 
        {{-- End Area Head --}}    
        {{-- Start Franchisee --}}
        @if(in_array(Str::lower(Auth::user()->type), ['super_user','area_head']))
        <div class="col-lg-12" id="franchisee">
          <div class="row">
            <div class="col-lg-12 mt-3">
              <p class="text-xsmall font-semibold">Staff Classification</p>
            </div>
            <div class="col-lg-4 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('user_designation') ? 'has-error' : NULL}}">            
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">User Designation</span>
                <input type="text"  placeholder="Enter Designation" class="form-control text-xsmall" name="user_designation" value="{{ old('user_designation') }}">
              @if($errors->first('user_designation'))
                <span class="help-block" style="color: red;">{{$errors->first('user_designation')}}</span>
              @endif
              </div>
           
            </div> 
            <div class="col-lg-6 mt-3">
              <div class="text-color-gray mb-2 {{$errors->first('franchisee_head') ? 'has-error' : NULL}}">
                <i class="fas fa-angle-double-right font-small text-color-blue"></i>
                <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Select Establishment</span>
                @if(in_array(Auth::user()->type, ['area_head','head']))
                   {!!Form::select('franchisee_head',$business_area_head,old('franchisee_head',Auth::user()->id),['class' => "form-control text-xsmall input-sm select2", 'id' => "franchisee" , 'disabled' => "disabled"])!!}
                   <input type="hidden" name="franchisee_head" value="{{Auth::user()->id}}">
                @else
                  {!!Form::select('franchisee_head',$business_area_head,old('franchisee_head'),['class' => "form-control text-xsmall input-sm select2", 'id' => "franchisee_head"])!!}
                @endif
                
              </div>
              @if($errors->first('franchisee_head'))
                <span class="help-block" style="color: red;">{{$errors->first('franchisee_head')}}</span>
              @endif
             
            </div> 
          </div>
        </div>
        @endif 
        {{-- End Franchisee --}}         

        <div class="col-lg-12 mt-4">
          <p class="text-color-gray font-semibold">User Requirements</p>
        </div>
        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('police_check') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Police Check</span>
          </div>
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file"  name="police_check[]"/> 
          </div>
          @if($errors->first('police_check'))
            <span class="help-block" style="color: red;">{{$errors->first('police_check')}}</span>
          @endif
        </div> 
        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('awareness_certificate') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Asbestos Awareness Certificate</span>
          </div>
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="awareness_certificate[]" /> 
          </div>
          @if($errors->first('awareness_certificate'))
            <span class="help-block" style="color: red;">{{$errors->first('awareness_certificate')}}</span>
          @endif
        </div> 
        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('insurance') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Insurance</span>
          </div>
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="insurance[]"/> 
          </div>
          @if($errors->first('insurance'))
            <span class="help-block" style="color: red;">{{$errors->first('insurance')}}</span>
          @endif
        </div>
        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('first_aid_license') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">First Aid License</span>
          </div>
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file"  name="first_aid_license[]"/> 
          </div>
          @if($errors->first('first_aid_license'))
            <span class="help-block" style="color: red;">{{$errors->first('first_aid_license')}}</span>
          @endif
        </div>
        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('whitecard') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Whitecard</span>
          </div>
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="whitecard[]"/>
          </div>
          @if($errors->first('whitecard'))
            <span class="help-block" style="color: red;">{{$errors->first('whitecard')}}</span>
          @endif
        </div>
        <div class="col-lg-6 mt-2">
          <div class="text-color-gray mb-2 {{$errors->first('other_documents') ? 'has-error' : NULL}}">
            
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Other Qualifying Documents</span>
          </div>
          <div class="d-flex flex-row mb-2 mt-1">
            <input type="file" name="other_documents[]"/>
          </div>
          @if($errors->first('other_documents'))
            <span class="help-block" style="color: red;">{{$errors->first('other_documents')}}</span>
          @endif
        </div>
        <div class="col-lg-12 mt-3">
            <button type="submit" class="btn bg-color-blue text-xxsmall text-uppercase text-color-white p-2 pl-3 pr-3"> ADD USER<i class="ml-2 fas fa-chevron-right text-color-white" style="font-size: 12px;"></i><i class="fas fa-chevron-right text-color-white" style="font-size: 12px;"></i> </button>
        </div> 
      </div>
    </div>
  </form>
		<!-- main container ends -->
</div>
@stop
@section('page-styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css')}}"/>
@stop

@section('page-scripts')
<script src="{{asset('assets/lib/datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
    $('input.datepick').focus(function(){


        $(this).datetimepicker({autoclose: true,locale: 'en',
              pickTime: false,todayBtn : true,format : 'mm/dd/yyyy',minview:2});
        $(this).datetimepicker('show');
        $(this).on('changeDate', function(ev){
            // do stuff
        }).on('hide', function(){
                $(this).datetimepicker('remove');
            });
    });
 
     $('#user_type').on('change', function(){
      var val = $(this).val();

      if (val == 'area_head') {       
        $('#area_head_input').show();
        $('#franchisee').hide();
        $('#user_head').show();
        $('input[name="user_designation"]').val('');
        $('select[id="franchisee_head"]').val('');
      }else if (val == 'franchisee') {
          $('#area_head_input').hide();
          $('#franchisee').show();
          $('#user_head').hide();
          $('select[id="user_head"]').val('');
          
          $('#franchisee_head').show();
          $('input[name="business_name"]').val('');
          $('input[name="business_number"]').val('');
          $('input[name="tax_number"]').val('');
          $('input[name="business_street"]').val('');
          $('input[name="business_city"]').val('');
          $('input[name="business_post_code"]').val('');

      } else {
       
        $('#area_head_input').hide();
        $('#franchisee').hide();
        $('#user_head').hide();
        $('#franchisee_head').hide();
        $('select[id="user_head"]').val('');
        $('select[id="franchisee_head"]').val('');
        $('input[name="user_designation"]').val('');
        $('input[name="business_name"]').val('');
        $('input[name="business_number"]').val('');
        $('input[name="tax_number"]').val('');
        $('input[name="business_street"]').val('');
        $('input[name="business_city"]').val('');
        $('input[name="business_post_code"]').val('');
       
      }
    }).change();
     
  });
</script>
@stop