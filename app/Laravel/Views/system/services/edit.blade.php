@extends('system._layouts.main')

@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
  <div class="container">
    <p class="text-color-gray font-semibold mb-3 text-uppercase">Edit Service</p>
  </div>
  <div class="container pb-5 pl-5 pr-5 pt-4 mt-2" style="background-color: white; ">        
    <form action="" class="with-confirmation" method="POST" enctype="multipart/form-data">
        {!!csrf_field()!!}
        <div class="row">
          <div class="col-lg-12 mt-4 mb-2">
            <p class="text-color-gray text-xsmall font-semibold text-uppercase">Service Group</p>
          </div>  
        </div>
        <div class="row">
          <div class="col-lg-4">
            <div class="text-color-gray mb-2 {{$errors->first('group') ? 'has-error' : NULL}}">
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall font-semibold text-uppercase">Service Group
                <span class="text-color-red ml-1">*</span>
              </span>
              {!!Form::select('group',$service_group,old('group',$services->group),['class' => "form-control text-xsmall input-sm select2", 'id' => "group"])!!}
              @if($errors->first('group'))
                <span class="help-block">{{$errors->first('group')}}</span>
              @endif
            </div>
          </div>
          <div class="col-lg-8 mt-2 pt-4">
            <p class="text-color-light text-xsmall">Choose from: Cleaning, Accounting, FX and infoTech</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 mt-3 ">
            <div class="text-color-gray mb-2 {{$errors->first('name') ? 'has-error' : NULL}}">
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall font-semibold text-uppercase">Service Name
                <span class="text-color-red ml-1">*</span>
              </span>
              <input type="text" placeholder="Enter service name" class="form-control text-xxsmall" name="name" value="{{ old('name',$services->name) }}">
              @if($errors->first('name'))
                <span class="help-block">{{$errors->first('name')}}</span>
              @endif
            </div>
          </div>
          <div class="col-lg-4 mt-2" id="service_type_container">
            <div class="text-color-gray mb-2 {{$errors->first('service_type') ? 'has-error' : NULL}}" >
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall font-semibold ">Service Type</span>  
            </div>
             {!!Form::select('service_type[]',$type_service,old('service_type',explode(',', $services->service_type)),["class" => "form-control custom-select", 'id' => 'basic' ,'multiple' => 'multiple'])!!}
            @if($errors->first('service_type'))
              <span class="help-block" style="color: red;">{{$errors->first('service_type')}}</span>
            @endif
          </div>
          <div class="col-lg-4 mt-3" id="service_price_container">
            <div class="text-color-gray mb-2 {{$errors->first('price') ? 'has-error' : NULL}}">
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall font-semibold text-uppercase">Service Price
                <span class="text-color-red ml-1">*</span>
              </span>
              <input type="text" placeholder="Enter service price" class="form-control text-xxsmall" name="price" value="{{ old('price',$services->price) }}">
              @if($errors->first('price'))
                <span class="help-block">{{$errors->first('price')}}</span>
              @endif
            </div>
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-lg-12">
            <div class="form-group">
              <label class="control-label">Current Thumbnail</label>
              <br>
              <img src="{{asset($services->directory.'/'.$services->filename)}}" alt="" class="img-thumbnail" width="300">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 mt-2" id="service_type_container">
            <div class="text-color-gray mb-2 {{$errors->first('service_type') ? 'has-error' : NULL}}" >
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall font-semibold ">Service Icon Library</span>
            </div>
            {!!Form::select('media_type',$media_type,old('media_type',$services->icon_type),["class" => "form-control", 'id' => 'media_type_input'])!!}
          </div>
          <div class="col-lg-4 mt-2">
            <div class="text-color-gray mb-2">
              <i class="fas fa-angle-double-right font-small text-color-blue"></i>
              <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Service Icon 
                <span class="text-color-red">*</span>
              </span>
            </div>
            <div class="d-flex flex-row mb-3 mt-2">
              <input type="file" id="file" name="file"/>
              {!!Form::select('media_library',$media_lib,old('media_library',$services->icon_id),["class" => "form-control", 'id' => 'media_input' ])!!}
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 mt-5">
            <button type="submit" class="btn bg-color-blue text-xsmall text-color-white p-2 pl-3 pr-3">Update Service</button>
            <a href="{{route('system.services.index')}}" class="btn bg-color-red text-xsmall text-color-white p-2 pl-3 pr-3 ml-2">Cancel</a>
          </div> 
        </div>
    </form>
  </div>
</div>

@stop

@section('page-styles')
<style type="text/css">
  footer {
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
  }
body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.btn-group > .btn:first-child {
   
    padding: 0.375rem 4.75rem;
}
.font-small {
    font-size: 12px !important; 
}
</style>
@stop
@section('page-scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
<script type="text/javascript">


   
$('#group').on('change', function(){
  var val = $(this).val();

    if (val == 'accounting') {       
        $('#service_type_container').hide();
        $('#service_price_container').show();
        $('select[id="service_type_input"]').val('');
        $('select[name="price"]').val('');
      
    }else if(val == 'information_technology'){
      $('#service_type_container').hide();
      
      $('select[id="service_type_input"]').val('');
    }else{

      $('#service_type_container').show();
      $('#service_price_container').show();
      $('select[id="service_type_input"]').val('');
      $('select[name="price"]').val('');

    }

}).change();
$('#media_type_input').on('change', function(){
  var val = $(this).val();

    if (val == '1') {       
      $('#file').show();
      $('#media_input').hide();
       
    }else{
      $('#file').hide();
      $('#media_input').show();
    }

}).change();

$(function(){
    $('#basic').multiselect({

      templates: {
          li: '<li><label class="pl-2"></label></li>'
      }
  });
});


</script>
@stop

