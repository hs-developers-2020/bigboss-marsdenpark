@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
{{ Session::put('percent',50) }}
<form action="" onsubmit="return validateForm()" method="post" id="service_form">
  {{ csrf_field() }}
  <div class="container" style="padding-top: 123px;padding-bottom:7%;">
    <div class="row">
      <div class="col-lg-12 text-right">
        <p class="text-color-white font-semibold spacing-1">
          <span style="background-color: rgba(0,0,0,.5);" class="p-2">
          <strong>Service Quote for:</strong>  {{ Session::get('accounting.postcode') }}, {{ Session::get('accounting.sub') }}
          </span>
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-center">
        <p class="text-white font-semibold spacing-1 mt-3">Select your Development Type</i></p>
      </div>
    </div>
    <div class="row justify-content-center">
      @forelse($information_service as $index) 
      <div class="col-lg-2  text-center mt-3">
        <div class="styled-radio mt-2 mb-2">           
          <input type="checkbox" class="check"  id="{{$index->id}}" name="svc[]" value="{{$index->id}}">
          <label for="{{$index->id}}" class="text-xxsmall pt-4 pb-4 style-checkbox" style="color: white;letter-spacing: 2px !important;">
          <img src="{{asset($index->directory.'/'.$index->filename)}}" class="img-fluid pl-4 pr-4 mb-2"> <br><span class="mobile-text"><br> {{Str::title($index->name)}}</span></label>
        </div>
      </div>
      @empty
      @endforelse
    </div>
    <div class="row">
      <div class="col-lg-12 mt-2 text-center">
        <button  type="submit"  class="next btn font-semibold spacing-1 card-shadow text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 mr-3" href="#"style="color: white !important;">Continue</button>
        <input type="checkbox" class="check_skip hidden"  id="1" name="svc[]" value="1">
        <button type="submit"  class="next btn font-semibold spacing-1 card-shadow text-uppercase text-xsmall bg-color-red text-color-white pt-3 pl-3 pr-3 pb-3 btn_skip" href="#"style="color: white !important;">Skip</button>
      </div>
    </div>
  </div>
   
</form>
@include('frontend._components.footer-sub')
@stop
@section('scripts-content')
<script type="text/javascript">
  $(function(){
    var box_height = 0.0;
    var box_width = 0.0;
    $.each($(".style-checkbox"),function(index,value){

      if(box_height < parseFloat($(this).css('height'))){
        box_height = parseFloat($(this).css('height'));
        box_width = parseFloat($(this).css('width'))
      }

      if($(".style-checkbox").length -1 == index){
        $(".style-checkbox").css({ "height" : box_height+"px", "width" : box_width+"px"})
      }
    });

    $('#1').on('click', function(){
        if ( this.checked ) {
          $(".check").prop("checked", false);
          $("#service_form").submit();   
        } 
    });

    $('.check').on('click', function(){
        if ( this.checked ) {
          $("#1").prop("checked", false);
        }
    });

    @if ($errors->first('svc') ? 'has-error' : NULL) {
      Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Invalid or No Selected Service, Please Select One to Continue',
      })
        e.preventDefault()
      }
    @endif
    var clicked = false;
    $(".btn_skip").on("click", function() {
      $(".check_skip").prop("checked", !clicked);
      $(".check").prop("checked", false);
      clicked = !clicked;
    });
  })

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
@stop

@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 
@media only screen and (max-width: 1500px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
 .footer-bottom{
margin-top: 100px;
  
}
}

@media only screen and (min-width: 1500px) {

   #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
  }
  .picture-size {
  height:5%; width: 7%;
  }
  .footer-bottom{
  position: absolute;
  bottom:0;

}
  }
    @media only screen and (max-width: 992px) {
.mobile-text {
  font-size: 20px;
}
}
</style>
@stop