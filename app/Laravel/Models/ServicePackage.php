<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;

class ServicePackage extends Model
{
    protected $fillable = ['service_name','price'];
}
