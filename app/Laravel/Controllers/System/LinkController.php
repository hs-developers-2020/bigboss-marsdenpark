<?php


namespace App\Laravel\Controllers\System;

/**
 *
 * Models used for this controller
 */

use Illuminate\Http\Request;




/**
 *
 * Requests used for validating inputs
 */

use App\Laravel\Models\AboutUs;
use App\Http\Requests\PageRequest;

use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Models\Link;
use App\Laravel\Requests\System\IconRequest;

use App\Laravel\Requests\System\AboutUsRequest;
use App\Laravel\Requests\System\LinkRequest;
use Helper, Carbon, Str, Auth, ImageUploader, FileUploader, File, DB, AuditRequest, Event;


class LinkController extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data = [];
        parent::__construct();
        array_merge($this->data, parent::get_data());
    }

    public function create()
    {
		$this->data['links']  = Link::orderBy('created_at' , "DESC")->first();
        return view('system.cleaning.link.create', $this->data);
    }

    public function store (LinkRequest $request) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$link = new Link();
			$link->fill($request->all());

			if($link->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "LINK_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated links.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.link.create');
			}
			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
 
}
