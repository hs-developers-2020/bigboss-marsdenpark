@extends('system._layouts.main')
<style type="text/css">
    footer {
        margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
    <div class="container ">
    
        <form action="" id="search_filter" >
            <div class="row">
                <div class="col-lg-3 pt-2">
                    <h6 class="text-xsmall spacing-1 text-color-gray">Current Business</h6>
                </div>
                @if(in_array(Str::lower(Auth::user()->type), ['super_user','head']))
                <div class="col-lg-9 mb-2 d-flex flex-row ">   
                    <input type="text" class="searchTerm" name="keyword" value="{{$keyword}}" placeholder="Search keyword such as Business Name">
                    <a href="#"  class="btn bg-color-blue text-color-white searchButton" id="button_search">
                        <i class="fa fa-search"></i>
                    </a>
                   
                </div>
                @endif
            </div>
        </form>
      
            <table class="table bg-color-white  mb-0 footable-finance">
                <thead>
                    <tr class="text-nowrap">
                        <th class="p-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left">Avatar</th>
                        <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left">Business Name</th>
                        <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs">Recent File</th>
                        <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs">Date Last Update</th>
                        <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs sm">Files in group</th>
                        
                    </tr>
                </thead>
                <tbody>
                     @forelse($business_group as $index => $value)
                     <tr>
                        <td>
                            <div class="figure avatar40 border rounded-circle align-self-start">
                                @if($value->directory)
                                <img src="{{asset($value->directory.'/'.$value->filename)}}" alt="Generic placeholder image" class="">
                                @else
                                 <img src="{{ asset('frontend/images/default.png') }}" alt="Generic placeholder image" class="">
                                @endif
                            </div>
                        </td>
                        <td><a href="{{ route('system.contract-group.list',[$value->user_id]) }}" class="text-color-blue font-semibold p-3 text-xsmall">{{$value->business_name}}</a></td>
                        <td><p class="text-color-blue font-semibold p-3 text-xsmall">{{Helper::business_file($value->user_id)}}</p></td>
                        <td><p class="text-color-blue font-semibold p-3 text-xsmall">{{Helper::business_date($value->user_id)}}</p>
                        <td><p class="text-color-blue p-3 font-semibold text-xsmall">{{Helper::file_count($value->user_id)}}</p></td>
                     </tr>

                     @empty
                    <tr>
                        <td>No Record Available</td>
                    </tr> 
                    @endforelse 
                </tbody>
            </table>                    
                         
    </div>
</div> 
@stop

@section('page-scripts')
<script type="text/javascript">
$(document).ready(function(){


    $("#button_search").on("click",function(e){
      $("#search_filter").submit();
    });

});
</script>
@stop