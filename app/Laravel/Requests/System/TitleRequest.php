<?php

namespace App\Laravel\Requests\System;

use Session, Auth;
use App\Laravel\Requests\RequestManager;

class TitleRequest extends RequestManager
{

    public function rules()
    {
        return [
            'title'    => "required",
            'description' => "nullable",
        ];
    }

    public function messages()
    {
        return [
            'required'    => "Field is required.",
        ];
    }
}
