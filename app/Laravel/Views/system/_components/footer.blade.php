<footer class="container mb-3" style="background-color: transparent;">
  <div class="row">
      <div class="col-lg-6 col-sm-12">
        <h4 class="pt-5 mt-5 pt-lg-2 mt-lg-2 mobile-margin-footer-reserved text-xsmall">
        <script>document.write(new Date().getFullYear());</script> Big Boss Group. All rights reserved</h4>
      </div>
      <div class="col-lg-6 col-sm-12 text-lg-right text-right text-md-right d-flex flex-row justify-content-end">
        <h4 class="pt-lg-2 mt-lg-2 pt-md-5 mt-md-5 text-xsmall mr-4 pointer">Privacy</h4><h4 class="pt-lg-2 mt-lg-2 pt-md-5 mt-md-5 text-xsmall mr-4 pointer">Terms</h4><h4 class="pt-lg-2 mt-lg-2 pt-md-5 mt-md-5 text-xsmall mr-4 pointer">Help</h4>
      </div>
  </div>
</footer>