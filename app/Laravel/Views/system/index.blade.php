@extends('system._layouts.main')

@section('content')
<div class="main-content container-fluid">
  
  <div class="row">
    @include('system._components.notifications')

    {{-- <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-6">
        <div class="widget widget-tile">
            <div  class="chart sparkline icon-container">
              <div class="icon">
                <span class="mdi mdi-account-add"></span>
              </div>
            </div>
          <div class="data-info">
            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="{{$total_registered_voter}}" class="number">0</span>
            </div>
            <div class="desc">Total Registered Voter</div>
            <div>as of {{Helper::date_format(Carbon::now(), 'M d, Y @ h:i A')}}</div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-6 col-lg-6">
        <div class="widget widget-tile">
            <div  class="chart sparkline icon-container">
              <div class="icon">
                <span class="mdi mdi-account-add"></span>
              </div>
            </div>
          <div class="data-info">
            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="{{$total_registered_indigent}}" class="number">0</span>
            </div>
            <div class="desc">Total Registered Indigent</div>
            <div>as of {{Helper::date_format(Carbon::now(), 'M d, Y @ h:i A')}}</div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="widget widget-tile">
            <div  class="chart sparkline icon-container">
              <div class="icon">
                <span class="mdi mdi-account-add"></span>
              </div>
            </div>
          <div class="data-info">
            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="{{$total_permanent_resident}}" class="number">0</span>
            </div>
            <div class="desc">Total Number of Permanent Resident</div>
            <div>as of {{Helper::date_format(Carbon::now(), 'M d, Y @ h:i A')}}</div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="widget widget-tile">
            <div  class="chart sparkline icon-container">
              <div class="icon">
                <span class="mdi mdi-account-add"></span>
              </div>
            </div>
          <div class="data-info">
            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="{{$total_stayin_resident}}" class="number">0</span>
            </div>
            <div class="desc">Total Number of Stay-In Resident</div>
            <div>as of {{Helper::date_format(Carbon::now(), 'M d, Y @ h:i A')}}</div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-md-6 col-lg-4">
        <div class="widget widget-tile">
            <div  class="chart sparkline icon-container">
              <div class="icon">
                <span class="mdi mdi-account-add"></span>
              </div>
            </div>
          <div class="data-info">
            <div class="value"><span class="indicator indicator-equal mdi mdi-chevron-right"></span><span data-toggle="counter" data-end="{{$total_renting_resident}}" class="number">0</span>
            </div>
            <div class="desc">Total Number of Renting Resident</div>
            <div>as of {{Helper::date_format(Carbon::now(), 'M d, Y @ h:i A')}}</div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
         <div class="panel panel-default">
          <div class="panel-heading panel-heading-divider">
            <span class="title">Number of Business requested for Business Clearance Last 7 Days</span><span class="panel-subtitle">Business requested for Business Clearance Statistics from the last 7 days</span>
          </div>
          <div class="panel-body">
            <div id="weekly_business_clearance" style="height: 300px;"></div>
          </div>
        </div>
      </div>

      <div class="col-md-6">
         <div class="panel panel-default">
          <div class="panel-heading panel-heading-divider">
            <span class="title">Number of requests for Activity Clearance Last 7 Days</span><span class="panel-subtitle">Number of requests for Activity Clearance Statistics from the last 7 days</span>
          </div>
          <div class="panel-body">
            <div id="weekly_activity_clearance" style="height: 300px;"></div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-4">
         <div class="panel panel-default">
          <div class="panel-heading panel-heading-divider">
            <span class="title">Number of Residents requested for Personal Certificate Last 7 Days</span>
          </div>
          <div class="panel-body">
            <div id="weekly_personal" style="height: 300px;"></div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
         <div class="panel panel-default">
          <div class="panel-heading panel-heading-divider">
            <span class="title">Number of Residents requested for Residency Certificate Last 7 Days</span>
          </div>
          <div class="panel-body">
            <div id="weekly_residency" style="height: 300px;"></div>
          </div>
        </div>
      </div>

      <div class="col-md-4">
         <div class="panel panel-default">
          <div class="panel-heading panel-heading-divider">
            <span class="title">Number of Residents requested for Indigency Certificate Last 7 Days</span>
          </div>
          <div class="panel-body">
            <div id="weekly_indigency" style="height: 300px;"></div>
          </div>
        </div>
      </div>

    </div>

    <div class="row">
      <div class="col-xs-12 col-md-6 col-lg-6">
        <div class="widget widget-tile">
          <h5 class="desc" style="font-size: 16px;">Number of Business per Nature of Business</h5>
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Nature of Business</th>
                <th>Number of Business</th>
              </tr>
            </thead>
            <tbody>
              @foreach($nature_business as $business)
              <tr>
                <td>{{ $business->name }}
                </td>
                <td>{{ $business->business_count }}
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div> --}}
 
    </div>
  </div>
</div>
@stop



@section('page-scripts')
<script src="{{asset('assets/lib/countup/countUp.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.pie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.resize.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/plugins/curvedLines.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/jquery-flot/jquery.flot.tooltip.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/chartjs/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/raphael/raphael-min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/lib/morrisjs/morris.min.js')}}" type="text/javascript"></script>


<script type="text/javascript">
  $(function(){
    
  })
</script>
@stop