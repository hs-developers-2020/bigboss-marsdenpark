<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>

  <style type="text/css">

    *{
      text-transform: uppercase;
      font-family: 'Montserrat', sans-serif;

    }
    h4{

      font-weight: normal;

    }

    h3{
      text-transform: uppercase;
      font-size: 20px;
    }

    .code {
      text-decoration: underline;
      weight: bold;
    }

    .underline{
      border-bottom:1px solid #000000; 
      width:520px; 

    }

    .underline2{
      border-bottom:1px solid #000000; 
      width:200px; 

    }

    .underline3{
      border-bottom:1px solid #000000; 
      width:550px; 

    }

    .date{
      border-bottom:1px solid #000000; 
      width:150px; 

    }
    th {
      color: #323639;
    }
    p,span {
      color: #323639;
    }
  </style>



</head>
<body style="margin-left: 25px; margin-right: 25px">
  <img src="{{'frontend/images/main-logo.png'}}" style="position:absolute;top:3%;width:170px; height:0px;" >
  <h1 style="text-align: right;font-size: 40px;color: #446992;">Quote</h1>
  <p style="text-align: right;font-size: 14px;">Date :  {{ $booking->created_at->format("d/m/ Y") }}</p>
  <p style="text-align: right;font-size: 14px;">Booking Number :  {{ "#{$booking->booking_id}"  }}</p>
 <div style="position: absolute;top: 11%;">
    <a style="font-size: 12px;" href="http://www.bigbossgroup.com.au/">http://www.bigbossgroup.com.au/</a><br>
    <span style="font-size: 14px;font-weight: bold">T 1800 131 599</span><br>
    <span style="font-size: 12px;">Medan Pangalinan |<strong> Big Boss Cleaning Head</strong></span><br>
    <span style="font-size: 12px;">63 Marsedan Road St Marys NSW 2760</span><br>
    <span style="font-size: 12px;">Phone: 1800 131 599</span><br>
    <span style="font-size: 12px;">romedz@bigbosscleaning.com.au</span><br>
  </div>
  <div style="margin-top: 80px">
    <span style="font-size: 12px;">To</span>
    <span style="margin-left: 12px;font-size: 12px;">{{ "{$booking->full_name}"  }}</span><br>
    <span style="margin-left: 32px !important;font-size: 12px;"> {{ $booking->location  }}</span><br>
    <span style="margin-left: 32px !important;font-size: 12px;"> {{ $booking->postcode  }}</span><br>
    <span style="margin-left: 32px !important;font-size: 12px;">{{ $booking->payment  }}</span><br>
  </div>
  <p style="font-size: 15px;color: #323639;margin-top: 20px">Payment Details</p>

  <table width="100%"  cellpadding="3" cellspacing="0">
    {{-- Booking id: {{ $booking->booking_id  }} --}}
  <tr style="background-color: #DBE5F1; color: #323639 !important;">
    <th style="padding-top: 15px;padding-bottom: 15px;padding-left: 10px;font-size: 12px;">SERVICEs</th>
    <th style="text-align: right;font-size: 12px;">Service Price</th>
    <th style="text-align: right;padding-right: 10px;width: 30%;font-size: 12px;">Total Price</th>
  </tr>

@if($booking->type == "cleaning")
  <tr>
    <td style="width: 50%; margin-top: 20px !important;font-size: 12px;">SERVICE TYPE | {{ $booking->service_type }} | {{ $booking->property_type }} </td>
    <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">${{ $booking->service_type_amount }}</td>
    <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">${{ $booking->service_type_amount }}</td>
  </tr>
@endif
@forelse($services as $value)
<tr>
  <td style="width: 50%; margin-top: 20px !important;font-size: 12px;">{{$value->name}}</td>
  <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">$ {{$value->price}}</td>
  <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">$ {{$value->price}}</td>

</tr>
@empty
@endforelse
<hr>
@if($booking->type == "cleaning")
  @forelse($others as $value)
  <tr>
    <td style="width: 50%; margin-top: 20px !important;font-size: 12px;">{{$value->services}}</td>
    <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">$ {{$value->price}}</td>
    <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">$ {{$value->price}}</td>
  </tr>
  @empty
  @endforelse
  <tr>
    <td style="width: 50%; margin-top: 20px !important;font-size: 12px;">{{$subservice ? $subservice->name : " "}}</td>
    <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">$ {{$subservice ? $subservice->price : " "}}</td>
    <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">$ {{$subservice ? $subservice->price : " "}}</td>
  </tr>

  <tr style="background-color: #2047A4; color: white !important;">
    <th style="margin-top: 15px !important;padding-top: 15px;padding-bottom: 15px;padding-left: 10px;font-size: 12px;">SERVICES</th>
    <th style="text-align: right;margin-top: 15px !important;font-size: 12px;">Sub Service Price</th>
    <th style="text-align: right;margin-top: 15px !important;padding-right: 10px;width: 30%;font-size: 12px;">Total Price</th>
  </tr>
@endif

<tr>

  <td style="margin-top: 10px !important;font-size:20px;"></td>
  <td style="text-align: right;margin-top: 30px !important;font-size:13px;">
    Estimated Total
  </td>
  <td style="font-size:15px;text-align: right;margin-top: 25px !important;padding-right: 10px;">$ {{$booking->booking_total}}</td>
</tr>

</table>
<div style="text-align: right">
    <p style="font-size: 12px;text-transform: lowercase;text-align: right;font-style: italic;">
      <span style="text-transform: uppercase;font-style: normal;">Note:</span> Price subject to change upon actual site inspection.
    </p>
</div>
<p style="text-align: justify; margin-top: 30px;font-size: 12px;text-transform: lowercase;">
    At Big Boss Cleaning we provide the highest standard quality of cleaning and the best customer service experience because we value our
  clients. We only use environment friendly chemicals and quality equipment’s to perform our job. We use different color microfiber rugs to
  clean your office to avoid the spread of bacteria.
  <br><br>
{{--   To accept this quotation, sign here and return:
 --}}  <br>
  </p>

  <p style="text-align: center; margin-top: 10px;font-size: 16px;font-weight:bold;text-transform: uppercase;">
  THANK YOU FOR YOUR BUSINESS!
  </p>
</body>
</html>


