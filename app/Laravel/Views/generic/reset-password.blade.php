@extends('frontend._layout.main')

@section('content')
<div class="video-container" style="height: auto;">
    <video loop muted autoplay  width="1280" height="820">
        <source src="{{ asset('frontend/Video/main1.mp4') }}" type="video/mp4">
    </video>

    <div class="container d-flex justify-content-center" style="margin-top: 110px">
        <div class="row">
            <div class="col-lg-12 text-center pt-sm-0 pt-lg-3 pt-md-3 pb-lg-5 pb-md-5 pb-sm-2 bg-color-white">
                <img src="{{ asset('frontend/Logos/MainLogo.png') }}" class="img-fluid image-logo">
                <form action="" method = "POST">
                {!!csrf_field()!!}  
                    <div class="container padding-container">
                        <p class="text-color-blue font-semibold spacing-1 text-xsmall text-uppercase">Change your Password</p>
                        <h6 class="text-center text-muted font-weight-normal forgot-pass-txt">To complete the process please enter your new password.</h6>
                        @include('frontend._components.notifications')
                    
                        <input type="password" name="password" class="form-control" placeholder="New Password" >
                      
                        <input type="password" name="password_confirmation" class="form-control mt-2" placeholder="Confirm New Password">

                        <div class="row mt-2">
                            <div class="col-lg-12">
                                <button type="submit" class="btn bg-color-blue text-color-white w-100 text-uppercase">Submit</button>
                            </div> 
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

@stop

@section('style-script')
<style type="text/css">
    a:hover {
        

    }
    .bg-color-white {
        background-color: white !important;
    }

@media only screen and (max-width: 1000px) {
    .desktop-margin {
        padding-top: 30px !important;
        width: 90% !important;

    }
    .image-logo {
        width: 40% !important;
    }
    .padding-container {
        padding-left: 1rem !important;
        padding-right: 1rem !important;
    }

    .icon-size {
            font-size: 1em !important;
        }   
    .mobile-padding {
        padding-bottom: 20px;
    }
.hidden-sm {
        display: none;
    }
    .hidden-lg {
        display: block;
    }
}

@media only screen and (min-width: 1000px) {
    .desktop-margin {
        padding-top: 60px !important;
        width: 40% !important;
    }
    .image-logo {
        width: 25% !important;
    }

        .padding-container {
        padding-left: 3rem !important;
        padding-right: 3rem !important;
    }
        .icon-size {
            font-size: 2em !important;
        }
    .hidden-lg {
        display: none;
    }
    .hidden-sm {
        display: block;
    }
    }
</style>
@stop