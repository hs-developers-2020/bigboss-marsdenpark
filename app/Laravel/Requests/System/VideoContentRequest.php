<?php 

namespace App\Laravel\Requests\System;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class VideoContentRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;
	
		$rules = [
			'name'	=> "required",
			'file' =>"required",
		];
		
		if($id){
			$rules['file'] = "nullable|mimes:mp4,ogx,oga,ogv,ogg,webm";
		}

		return $rules;
	}

	public function messages(){
		return [
			
			'required'	=> "Field is required.",
			'file.mimes' => "Invalid File Type" 
		];
	}
}