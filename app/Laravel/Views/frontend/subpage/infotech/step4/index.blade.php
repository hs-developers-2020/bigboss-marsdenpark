@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
<style type="text/css">
  .hidden
  {
    display: none;
  }

</style>
{{ Session::put('percent',70) }}

<div class="container" style="padding-top: 125px;">
  <form action="" method="POST">
  {{ csrf_field() }}
  <div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-12" id="card-margin">
      <div class="panel-header hidden"></div>
        <div class="card card-shadow" style="border-radius: 0px !important;margin-bottom: 20px">
          <div class="card-body" style="padding: 0px !important;">
            <div class="row"  style="margin-left: 0px !important;margin-right: 0px !important;">
              <div class="col-lg-5 p-4" id="customer-details" style="" >
                <div class="row">
                  <div class="col-lg-12 text-center header-booking--details">   
                    <p class="text-color-blue font-semibold spacing-1 text-small text-uppercase">Booking <span class="text-color-red">Customer Details</span></p>
                  </div>
                  <div class="col-lg-12 text-center header-cancel--booking hidden mt-3">   
                    <p class="text-color-gray font-semibold spacing-1 text-xsmall">If you wanted us to call you to discuss the quotation and your need, Please fill up the following information.</p>
                  </div>
                  <div class="col-lg-12 mt-3">
                    <p class="text-color-gray font-semibold text-xsmall text-color-gray ">Enter your Details</p>
                  </div>
                  <div class="col-lg-12 mb-2">
                    <div class="text-color-gray mb-2 {{$errors->first('group') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">First Name<span class="text-danger">*</span></span>
                    </div>
                    <input type="text" class="form-control text-xsmall bg-gray--input text-white" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="fname" id="fname" value="{{ Auth::check() ? Auth::user()->firstname : old('lname') }}">
                    @if($errors->first('fname'))
                      <span class="help-block" style="color:red;">{{$errors->first('fname')}}</span>
                    @endif                       
                  </div>
                  <div class="col-lg-12 mb-2 mt-2">
                    <div class="text-color-gray mb-2 {{$errors->first('lname') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">Last Name<span class="text-danger">*</span></span>
                    </div>
                    <input type="text" class="form-control text-xsmall bg-gray--input text-white" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="lname" id="lname" value="{{ Auth::check() ? Auth::user()->lastname : old('lname') }}">
                    @if($errors->first('lname'))
                      <span class="help-block" style="color:red;">{{$errors->first('lname')}}</span>
                    @endif   
                  </div>
                  <div class="col-lg-12 mt-2">
                    <div class="text-color-gray mb-2 {{$errors->first('contact_number') ? 'has-error' : NULL}}">
                      <i class="fas fa-angle-double-right text-color-blue"></i>
                      <span class="ml-2 text-xxsmall font-semibold text-color-gray">Contact Number<span class="text-danger">*</span></span>
                    </div>
                    <div class="input-icons"> 
                      <i class="icon text-color-blue p-3" style="padding: 5px !important; padding-left: 10px !important;">
                      <p class="text-white font-weight-bold" style="font-style: normal;font-size: 14px; margin-top: 2px;">+61 |</p></i>
                      <input type="number" class="input-field form-control text-white text-xsmall bg-gray--input" style="padding-left: 50px !important; padding-bottom: 8px !important; padding-top: 8px !important;" name="contact_number" id="contact" value="{{ Auth::check() ? Auth::user()->contact_number :  old('contact_number') }}">
                    </div>
                    @if($errors->first('contact_number'))
                      <span class="help-block" style="color: red;">{{$errors->first('contact_number')}}</span>
                    @endif    
                  </div>
                    <div class="col-lg-12 mt-2">
                      <div class="text-color-gray mb-2 {{$errors->first('email') ? 'has-error' : NULL}}">
                        <i class="fas fa-angle-double-right text-color-blue"></i>
                        <span class="ml-2 text-xxsmall font-semibold text-color-gray ">Email Address<span class="text-danger">*</span></span>
                      </div>
                      <input type="email" class="form-control text-xsmall bg-gray--input text-white text-white" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="email" id="email" value="{{ Auth::check() ? Auth::user()->email : old('email') }}">
                      @if($errors->first('email'))
                        <span class="help-block" style="color:red;">{{$errors->first('email')}}</span>
                      @endif   
                    </div>
                    <div class="col-lg-6"></div>
                </div>
              </div>
              <div class="col-lg-7 pr-4" id="gradient-background" style="padding-top: 16px !important;">
                <div class="row p-3">
                  <div class="col-lg-12 text-left">
                    <p class="text-xsmall font-semibold spacing-1 text-color-white">You Selected</p>
                  </div>
                  @forelse($package as $index)   
                
                  <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 mt-2">
                    <div class="styled-radio">
                      <input type="checkbox" disabled checked="" id="regular" name="service-type">
                      <label for="regular" class="text-xxxsmall spacing-1 card-shadow  style-cbox" style="color: white;">
                      @if($index->id == "1")
                        <img src="{{ asset('frontend/images/skip.png') }}" class=" mb-2 img-fluid" style="height:50% !important;width:50% !important"> 
                      @else
                        <img src="{{asset($index->directory.'/'.$index->filename)}}" class=" mb-2 img-fluid" style="height:50% !important;width:60% !important"> 
                      @endif<br>
                      <span class="mobile-text">{{Str::title($index->name)}}</span></label>
                    </div>
                  </div>
                  @empty
                  <h1>No Selected Package</h1>
                  @endforelse
                  <div class="col-lg-12">
                    <div class="row">
                      <div class="col-lg-12 mt-3">
                        <span class="text-color-white font-semibold">Do you want to secure a spot now?</span>
                      </div>
                      <div class="col-md-6 col-12 text-left">
                        <button type="submit"  id="next" class="next card-shadow btn font-semibold text-uppercase text-xsmall text-color-white w-100 pt-3 pb-3 mt-3" style="background-color:#4472C4;color: white !important;">Yes</button>
                        <p class="text-white mt-2 text-xxxsmall spacing-1">Give me a quote and secure booking now. </p>
                      </div>
                      <div class="col-md-6 col-12 text-left">
                        <a  href="#modal_call" data-toggle="modal"  data-role="view" style="text-decoration: none;"><button class="btn text-color-white mt-3 w-100 font-semibold card-shadow text-uppercase text-xsmall  pt-3 pb-3" style="background-color: #972121;">No</button></a>
                        <p class="text-white mt-2 text-xxxsmall spacing-1">Call me and discuss price and schedule.</p>
                      </div>     
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div class="col-lg-3"></div>
  </div>
  </form>
  <form action="{{ route('frontend.infotech.cancel') }}" method="POST" id="cancel_form">
    <input type="hidden" name="fname" id="input_fname_cancel">
    <input type="hidden" name="lname" id="input_lname_cancel">
    <input type="hidden" name="email" id="input_email_cancel">
    <input type="hidden" name="contact_number" id="input_contact_cancel">
    {{ csrf_field() }}
   
  </form>
</div>



<div class="modal fade" id="modal_call" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top: 25% !important">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <div class="modal-content">
      <div class="modal-header d-flex justify-content-end">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       
      <div class="modal-body p-4 text-left">
        <p class="text-color-gray font-semibold"><span class="text-errors show">Are you sure you wanted to cancel your booking?<br>Please Make sure you fill up all the required fields</span>
        <div class="row justify-content-center">
          <button id ="proceeds" class="btn bg-color-blue w-50 text-white text-errors text-uppercase show">Proceed</button>
        </div>
      </div>
   
     
    </div>
  </div>
</div>
<!-- end Modal -->
@include('frontend._components.footer-main')

@stop
@section('scripts')
<script type="text/javascript">
  

$(document).ready(function(){

  $(document).on('click','a[data-role=view]',function(){
    var fname = $('#fname').val()
    var lname = $('#lname').val()
    var email = $('#email').val()
    var contact = $('#contact').val()

    $('#input_fname_cancel').val(fname)
    $('#input_lname_cancel').val(lname)
    $('#input_email_cancel').val(email)
    $('#input_contact_cancel').val(contact)

    $('.text-errors').addClass('show')
    $('.text').addClass('d-none')
  })


})

$('#proceeds').on('click',function(){
  var fname = $('#fname').val()
  var lname = $('#lname').val()
  var email = $('#email').val()
  var contact = $('#contact').val()

  $('#input_fname_cancel').val(fname)
  $('#input_lname_cancel').val(lname)
  $('#input_email_cancel').val(email)
  $('#input_contact_cancel').val(contact)
  $('#cancel_form').submit();

})

</script>
@stop
@section('scripts-content')
<script>



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
    padding: 10px !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
    padding: 10px !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 
 #gradient-background {
    background-color: #2047A4;
}

@media only screen and (min-width: 1501px) {

  .footer-bottom{
    position: absolute;
    bottom: 0%;
    top: 100% !important;
    }
  }
@media only screen and (max-width: 1500px) {

  .footer-bottom{
    margin-top: 50px
    }
  }


  @media only screen and (max-width: 992px) {
.mobile-text {
  font-size: 13px;
}
}
 @media only screen and (min-width: 992px) {
.mobile-text {
  font-size: 11px;
}
}
  a:hover{
    color: white !important;
  }
  .hidden {
    display: none;
  }
   .show {
    display: block !important;
  }


</style>
@stop