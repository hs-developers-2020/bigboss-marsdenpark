<?php

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
// use JWTAuth;

class ChatRequest extends ApiRequestManager
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        $rules = [
            'title'   => 'required',
        ];

        return $rules;
    }

    public function messages() {

        return [
            'title.required'  => "Group name is required.",
        ];
    }
}
