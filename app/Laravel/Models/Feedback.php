<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "feedback";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','stars','description','group','path', 'directory', 'filename'
    ];

    public $timestamps = true;
}
