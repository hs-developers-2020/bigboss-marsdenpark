<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkAvailabilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_availability', function (Blueprint $table) {
            $table->increments('id');
            $table->text('user_id')->nullable();
            $table->date('date')->nullable();
            $table->text('work_stated')->nullable();
            $table->text('max_leads')->nullable();
            $table->time('time_in')->nullable()->default('9:00');
            $table->time('time_out')->nullable()->default('17:00');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_availability');
    }
}
