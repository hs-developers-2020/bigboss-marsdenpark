<?php


namespace App\Laravel\Controllers\System;

/**
 *
 * Models used for this controller
 */

use Illuminate\Http\Request;




/**
 *
 * Requests used for validating inputs
 */

use App\Http\Requests\PageRequest;

use App\Laravel\Models\VideoContent;
use App\Laravel\Events\AuditTrailActivity;
use App\Laravel\Requests\System\AboutUsRequest;
use App\Laravel\Requests\System\GalleryRequest;
use App\Laravel\Requests\System\VideoContentRequest;
use App\Laravel\Services\FileUploader as ServicesFileUploader;
use Helper, Carbon, Str, Auth, ImageUploader, FileUploader, File, DB, AuditRequest, Event;


class VideoContentController  extends Controller
{
    protected $data;

    public function __construct()
    {
        $this->data = [];
        parent::__construct();
        array_merge($this->data, parent::get_data());
    }

    public function index(PageRequest $request)
    {
        $this->data['keyword'] = $request->get('keyword', NULL);

        $this->data['video_content'] = VideoContent::orderBy('created_at', "DESC")->paginate(10);

        return view('system.cleaning.video-content.index', $this->data);
    }

    public function create()
    {
        return view('system.cleaning.video-content.create', $this->data);
    }

    public function store (VideoContentRequest $request) 
    {   

		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$video_content = new VideoContent;
			$video_content->fill($request->all());


            if($request->has('file')) {
				dd($request->has('file'));
			    $image = FileUploader::upload($request->file('file'), "uploads/video_content");
			    $video_content->path = $image['path'];
			    $video_content->directory = $image['directory'];
			    $video_content->filename = $image['filename'];
			}

			if($video_content->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "VIDEO_CONTENT_CREATED", 'remarks' => Auth::user()->full_name." has successfully created new video content {$video_content->name}.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				session()->flash('alert-type','success');
				session()->flash('notification-msg',"New record has been added.");
				return redirect()->route('system.video-content.index');
			}


			session()->flash('notification-status','failed');
			session()->flash('alert-type','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			dd($e);
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function edit($id = NULL)
    {
        $gallery = VideoContent::find($id);
	
		if (!$gallery) {
			session()->flash('notification-status',"failed");
			Session()->flash('alert-type','failed');
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.gallery.index');
		}

		$this->data['video_content'] = $gallery;

		return view('system.cleaning.video-content.edit',$this->data);
    }

    public function update (VideoContentRequest $request, $id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();

		try {
			$edit_video = VideoContent::find($id);
			$edit_video->fill($request->all());

			if($request->hasFile('file')) {
			    $image = FileUploader::upload($request->file('file'), "uploads/video_content");
			    $edit_video->path = $image['path'];
			    $edit_video->directory = $image['directory'];
			    $edit_video->filename = $image['filename'];
			}
			if (!$edit_video) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.video-content.index');
			}

			

			if($edit_video->save()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "VIDEO_CONTENT_UPDATED", 'remarks' => Auth::user()->full_name." has successfully updated {$edit_video->name} video content.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been modified successfully.");
				return redirect()->route('system.video-content.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');
			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

    public function destroy ($id = NULL) {
		$ip = AuditRequest::header('X-Forwarded-For');
		if(!$ip) $ip = AuditRequest::getClientIp();
		try {
			$video_content = VideoContent::find($id);

			if (!$video_content) {
				session()->flash('notification-status',"failed");
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.video-content.index');
			}

			if($video_content->delete()) {
				$log_data = new AuditTrailActivity(['user_id' => Auth::user()->id,'process' => "VIDEO_CONTENT_DELETED", 'remarks' => Auth::user()->full_name." has successfully deleted {$video_content->name} video content.",'ip' => $ip,'franchisee_id' => NULL]);
				Event::fire('log-activity', $log_data);

				session()->flash('notification-status','success');
				Session()->flash('alert-type','success');
				session()->flash('notification-msg',"Record has been deleted.");
				return redirect()->route('system.video-content.index');
			}

			session()->flash('notification-status','failed');
			session()->flash('notification-msg','Something went wrong.');

			return redirect()->back();

		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}
