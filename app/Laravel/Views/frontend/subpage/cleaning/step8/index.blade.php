@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
<div class="container pb-5" style="padding-top: 155px; padding-bottom: 7%;">
        <div class="card card-shadow">
          <div class="card-body" style="padding: 0px !important">
          {{ Session::put('percent',90) }}
          <form action="" method="POST">
            {{ csrf_field() }}

            <div class="row" style="margin-right: 0px !important; margin-left: 0px !important;">
                  <div class="col-lg-6 p-5">
                    <div class="row">
                      <div class="col-lg-12">                        
                        <p class="text-color-blue font-semibold text-uppercase text-small">Confirm Booking <span class="font-semibold text-color-red"> Summary</span></p>

                      </div>
                       <div class="col-lg-12 mt-3">
                        <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-list-alt mr-2 text-color-blue"></i>Service Details</p>
                      </div>
                      <div class="col-lg-12">
                        <div class="p-2" style="background-color: #F4F4F4;"> 
                          <div class="row">
                            <div class="col-lg-5 col-md-6 mt-lg-0 mt-md-0 mt-sm-2 mt-xs-2">
                              <span class="text-color-gray font-semibold text-xsmall">Service Type :</span>
                            </div>
                            <div class="col-lg-7 col-md-6 text-first-letter">
                              <span  id="service_type"class="text-color-blue text-xsmall ">{{ Session::get('clean.type') ?: 'No data' }}</span>
                            </div>
                            <div class="col-lg-5 col-md-6 mt-lg-0 mt-md-0 mt-sm-2 mt-xs-2">
                              <span class="text-color-gray font-semibold text-xsmall">Property Type :</span>
                            </div>
                            <div class="col-lg-7 col-md-6 text-first-letter">
                              <span  id="service_type"class="text-color-blue text-xsmall ">{{ Session::get('clean.property_type') ?: 'No data' }}</span>
                            </div>
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <span class="text-color-gray font-semibold text-xsmall">Extras :</span>
                            </div>
                             <div class="col-lg-7 col-md-6 mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1"> 
                              <span class="text-color-blue text-xsmall">{{ implode(", ",Session::get('clean.svc_name'))  }}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 mt-4">
                        <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-calendar-alt text-color-blue mr-2"></i>Booking Time/Date</p>
                      </div>
                       <div class="col-lg-12">
                        <div class="p-2" style="background-color: #F4F4F4;"> 
                          <div class="row">
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <span class="text-color-gray font-semibold text-xsmall">Booking Date :</span>
                            </div>
                            <div class="col-lg-7 col-md-6 mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <span class="text-color-blue text-uppercase text-xsmall">{{ Session::get('clean.booking_date')   }}</span>
                            </div>
                      
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <p class="text-color-gray font-semibold text-xsmall">Service Time :</p>
                            </div>
                            <div class="col-lg-7 col-md-6 mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <p class="text-color-blue text-uppercase text-xsmall font-semibold">                      
                                     {{   Session::get('clean.time_start') && Session::get('clean.time_end') == '08:00 ' ? '' : Helper::time_only(Session::get('clean.time_start'))  }} - {{ Session::get('clean.time_start') && Session::get('clean.time_end') == '08:00 ' ? '' : Helper::time_only(Session::get('clean.time_end'))}}       
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    
                       <div class="col-lg-12 mt-4">
                        <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-map-marker-alt mr-2 text-color-blue"></i>Service Location </p>
                      </div>
                      <div class="col-lg-12">
                        <div class="p-2" style="background-color: #F4F4F4;"> 
                          <div class="row">
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <span class="text-color-gray font-semibold text-xsmall">Address :</span>
                            </div>
                            <div class="col-lg-7 col-md-6  mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <span class="text-color-blue text-xsmall">{{ Session::get('clean.location') }}</span>
                            </div>
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <span class="text-color-gray font-semibold text-xsmall">Postcode :</span>
                            </div>
                            <div class="col-lg-7 col-md-6  mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <span class="text-color-blue text-uppercase text-xsmall">{{ Session::get('clean.postcode') }}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 mt-4 col-md-12 col-sm-12 col-xs-12">
                           <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-user text-color-blue mr-2"></i>Customer Details</p>
                        </div>
                        <div class="col-lg-12">
                          <div class="p-2" style="background-color: #F4F4F4;"> 
                            <div class="row">
                               <div class="col-lg-5 col-md-6 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                                  <span class="text-color-gray font-semibold text-xsmall">Name :</span>
                              </div>
                              <div class="col-lg-7 col-md-6 mt-lg-2 mt-md-2 mt-sm-1 mt-xs-1">
                                  <span class="text-color-blue text-left font-semibold text-xsmall">{{ Str::title(Session::get('clean.fname')) ." ".Str::title(Session::get('clean.lname')) }}</span>
                              </div>
                              <div class="col-lg-5 col-md-6 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                                  <span class="text-color-gray font-semibold text-xsmall">Contact Number :</span>
                              </div>
                              <div class="col-lg-7 col-md-6 text-sm-left text-lg-left mt-lg-2 mt-md-2 mt-sm-1 mt-xs-1">
                                <span class="text-color-blue text-xsmall text-left font-semibold text-uppercase">+61 {{ Session::get('clean.contact') }}</span>
                              </div>
                              <div class="col-lg-5 col-md-6 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                                <span class="text-color-gray font-semibold text-xsmall">Email Address :</span>
                              </div>
                              <div class="col-lg-7 col-md-6 text-sm-left mt-lg-2 mt-md-2 mt-sm-1 mt-xs-1 ">
                                <span class="text-color-blue text-left font-semibold text-xsmall">{{ Session::get('clean.email') }}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-6 p-5" id="gradient-background1">
                     <div class="row">     
                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-lg-right text-md-right text-sm-left text-xs-left">
                            <p class="text-color-white font-semibold text-uppercase spacing-1 text-xsmall">Booking ID : {{ Session::get('booking_info.booking_id') }}</p>
                        </div>
                       
                        <div class="col-lg-12 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                          <p class="text-color-white text-xsmall font-semibold text-uppercase"><i class="fas fa-user text-color-white mr-2"></i>Payment Summary</p><hr class="bg-white">
                        </div>

                        <div class="col-lg-12 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                          <table class="text-xsmall " style="width: 100%">
                          <th style="color:#fff;padding-bottom: 15px;" class="font-semibold text-xxsmall">SERVICE TYPE</th>
                          <th style="color:#fff;padding-bottom: 15px;" class="font-semibold  text-xxsmall">PROPERTY TYPE</th>
                          <th style="color:#fff;text-align: right;padding-bottom: 15px;" class="font-semibold  text-xxsmall">PRICE</th>

                          <tr>
                          <td style="color:#fff;padding-bottom: 15px;" class="text-first-letter"><span>{{ Session::get('clean.type') }}</span></td>
                          <td style="color:#fff;padding-bottom: 15px;" class="text-first-letter"><span>{{ Session::get('clean.property_type') }}</span></td>
                          @php
                            $service_type = 0;
                            if(Session::get('clean.type') == "regular" && Session::get('clean.property_type') == "home")
                            {
                                $service_type = 90;
                            }
                            if(Session::get('clean.type') == "once-off" && Session::get('clean.property_type') == "home")
                            {
                                $service_type = 250;
                            }
                            if(Session::get('clean.type') == "lease" && Session::get('clean.property_type') == "home")
                            {
                                $service_type = 350;
                            }

                            // office

                            if(Session::get('clean.type') == "regular" && Session::get('clean.property_type') == "office")
                            {
                                $service_type = 100;
                            }
                            if(Session::get('clean.type') == "once-off" && Session::get('clean.property_type') == "office")
                            {
                                $service_type = 250;
                            }
                            if(Session::get('clean.type') == "lease" && Session::get('clean.property_type') == "office")
                            {
                                $service_type = 350;
                            }
                          @endphp

                          <td style="text-align:right;color:#fff;padding-bottom: 10px;">${{ $service_type }}</td>
                        </tr>
                          </table>
                        </div>
                       
                      {{--   <div class="col-lg-6 col-md-6">
                          <p class="text-color-white font-semibold text-xsmall text-uppercase">{{ implode(" ,",Session::get('clean.services.svc'))  }}</p>
                        </div> --}}
                        <div class="col-lg-12">

                          @php
                          $total = 0;
                          $price = 0;
                          $servcices = Session::get('clean.services.svc');
                          
                          
                          @endphp
                          <table class="text-xsmall mt-3" style="width: 100%">
                            <th style="color:#fff;padding-bottom: 15px;" class="text-uppercase text-xxsmall font-semibold spacing-1">Extras</th>
                            <th style="color:#fff;text-align: right;padding-bottom: 15px;"></th>

                          
                             <tr>
                              <td style="color:#fff; padding-bottom: 10px;">{{ implode(", ",Session::get('clean.svc_name'))  }}</td>
                              <td style="text-align:right;color:#fff;padding-bottom: 10px;">${{ Session::get('clean.svc_total') }}</td>
                            </tr>
                           
                            
                          </table>
                        </div>
                        <div class="col-lg-12">
                          <hr class="text-color-white" style="height: 2px; background-color: white;">
                        </div>
                        <div class="col-lg-6 col-md-6 col-8">
                          <p class="text-color-white font-semibold text-uppercase mobile-text-small">Estimated Total</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-4 text-right">
                          <p class="text-color-white font-semibold text-uppercase text-small">${{ Session::get('clean.svc_total')+ $service_type }}</p>
                          {{ Session::put('clean.service_price', Session::get('clean.svc_total') + $service_type ) }}
                          {{ Session::put('clean.service_type_amount', $service_type ) }}
                        </div>
                        <div class="col-lg-12">
                          <p class="text-xxsmall text-color-gray "><i style="background-color: #FFFF82;" class="p-1 rounded">Note: Price subject to change upon actual site inspection.</i></p>
                        </div>
                        <div class="col-lg-12 mt-3">
                          <div class="row">
                             <div class="col-lg-6 col-md-6 col-6">
                                  <button  type="submit" name="next" onclick="myFunction()" id="next" class="next btn font-semibold spacing-1 text-uppercase w-100 text-xsmall card-shadow bg-color-blue text-color-white pt-3 pb-3 mt-3" href="#"style="color: white !important;background-color: #4472C4">Continue</button>
                              </div>
                              <div class="col-lg-6 col-md-6 col-6">
                                <div  class="">
                                  <div class="">    
                                  {{--   <a name="prev" href="{{ route('frontend.cleaning.index') }}" class="spacing-1" style="text-decoration: none;"> --}}<a  href="#" id="cancel" class="btn card-shadow text-color-white mt-3 font-semibold w-100 spacing-1 text-uppercase text-xsmall pt-3 pb-3" id="cancel" style="background-color: #972121;">CANCEL</a>{{-- </a> --}}
                                  </div>
                                </div>
                              </div>     
                          </div>
                        </div>
                      </div>
                    </div>          
                </div>   
            </div>
        </div>
        </form>
   </div>



 @include('frontend._components.footer-cleaning')
 @stop
 @section('scripts-content')
 <script>

$(document).ready(function(){
$('#cancel').on('click',function(){
      Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, cancel it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "{{ route('frontend.cleaning.last_cancel') }}";
      }
    })

  })
   
})

 </script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/cleaning bg.png') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 

@media only screen and (max-width: 1000px) {

 #gradient-background1 {
   /* width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
     background-color: #2047A4;
}

.picture-size {
height: 15%; width: 30%;
}
.mobile-text-small {
  font-size: 18px;
}
}

@media only screen and (min-width: 1000px) {

 #gradient-background1 {
   /* width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
    background-color: #2047A4;
}
.picture-size {
height:5%; width: 7%;
}
.mobile-text-small {
  font-size: 20px;
}
  }
  a:hover{
    color: white !important;
  }
span::first-letter {
  text-transform:uppercase !important;
}
</style>
@stop