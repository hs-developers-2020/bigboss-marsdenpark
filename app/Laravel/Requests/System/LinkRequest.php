<?php

namespace App\Laravel\Requests\System;

use Session, Auth;
use App\Laravel\Requests\RequestManager;

class LinkRequest extends RequestManager
{

    public function rules()
    {
        return [
            'ask_me_how'    => "nullable",
            'own_this_business'    => "nullable",
            'facebook'    => "nullable",
            'instagram'    => "nullable",
            'twitter'    => "nullable",
        ];
    }

    public function messages()
    {
        return [
            'required'    => "Field is required.",
        ];
    }
}
