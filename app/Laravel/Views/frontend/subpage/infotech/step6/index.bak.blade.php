@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
<style type="text/css">
  .hidden
  {
    display: none;
  }

</style>
{{ Session::put('percent',70) }}
<form action="{{ route('frontend.infotech.seven') }}" method="POST">
  {{ csrf_field() }}
<div class="container" style="padding-top: 175px;padding-bottom:4.3%;">
  <div class="card card-shadow">
    <div class="card-body" style="padding: 0px !important;">
        <div class="row"  style="margin-left: 0px !important;margin-right: 0px !important;">
            <div class="col-lg-5 p-5 {{-- {{ Auth::check() ? 'hidden' :'' }} --}}" style="" >
                <div class="row">
                    <div class="col-lg-12 text-center header-booking--details">   
                            <p class="text-color-blue font-semibold spacing-1 text-small text-uppercase">Booking <span class="text-color-red">Customer Details</span></p>
                        </div>
                        <div class="col-lg-12 text-center header-cancel--booking hidden mt-3">   
                            <p class="text-color-gray font-semibold spacing-1 text-xsmall text-uppercase">If you wanted us to call you to discuss the quotation and your need, Please fill up the following information.</p>
                        </div>
                    
                    {{-- <div class="col-lg-6 text-right">
                     <p class="text-uppercase text-small font-semibold spacing-2 text-color-blue">BOOKING ID :<span name="booking_id" value="" class="text-color-red"> {{ Session::get('booking_info.booking_id')  }} </span>  </p>
                   </div> --}}
                        <div class="col-lg-12 mt-3">
                        <p class="text-color-gray font-semibold text-xsmall text-uppercase text-color-gray ">User Details</p>
                    </div>
                    <div class="col-lg-12 mb-2">
                        <div class="text-color-gray mb-2">
                          <i class="fas fa-angle-double-right text-color-blue"></i>
                          <span class="ml-2 text-xxsmall font-semibold text-color-gray text-uppercase">First Name<span class="text-danger">*</span></span>
                        </div>
                        <input type="text" required  placeholder="First name" class="form-control text-xsmall" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="fname" id="fname" value="{{ Auth::check() ? Auth::user()->firstname : '' }}">
                       
                      </div>
                      <div class="col-lg-12 mb-2 mt-2">
                          <div class="text-color-gray mb-2">
                            <i class="fas fa-angle-double-right text-color-blue"></i>
                            <span class="ml-2 text-xxsmall font-semibold text-color-gray text-uppercase">Last Name<span class="text-danger">*</span></span>
                          </div>
                            <input type="text" required  placeholder="Last Name" class="form-control text-xsmall" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="lname" id="lname" value="{{ Auth::check() ? Auth::user()->lastname : '' }}">
                           
                      </div>
                      <div class="col-lg-12 mt-2">
                          <div class="text-color-gray mb-2">
                            <i class="fas fa-angle-double-right text-color-blue"></i>
                            <span class="ml-2 text-xxsmall font-semibold text-color-gray text-uppercase">Contact Number<span class="text-danger">*</span></span>
                          </div>
                         <div class="input-icons"> 
                            <i class="icon text-color-blue p-3" style="padding: 5px !important; padding-left: 10px !important;">

                            <p class="text-color-blue font-weight-bold" style="font-style: normal;font-size: 14px; margin-top: 2px;">+61 |</p></i>

                            <input type="number" required placeholder="Contact number" class="input-field form-control text-xsmall" style="padding-left: 50px !important; padding-bottom: 8px !important; padding-top: 8px !important;" name="contact" id="contact" value="{{ Auth::check() ? Auth::user()->contact_number : '' }}">

                          </div>
                        
                      </div>
                        <div class="col-lg-12 mt-2">
                          <div class="text-color-gray mb-2">
                            <i class="fas fa-angle-double-right text-color-blue"></i>
                            <span class="ml-2 text-xxsmall font-semibold text-color-gray text-uppercase">Email Address<span class="text-danger">*</span></span>
                          </div>

                          <input type="email" required placeholder="Email Address" class="form-control text-xsmall" style="padding-bottom: 8px !important; padding-top: 8px !important;" name="email" id="email" value="{{ Auth::check() ? Auth::user()->email : '' }}">

                        
                        </div>
                        <div class="col-lg-6"></div>
                    </div>
                  </div>
                  <div class="col-lg-7 pr-4 pb-4" id="gradient-background" style="padding-top: 36px !important;">
                    <div class="row p-3">
                      
                        <div class="col-lg-12 text-left">
                           <p class="text-small spacing-1 text-color-white text-uppercase">You Selected</p>
                        </div>
                      
                  
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.property_type.property') == "office-365-deployment" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                                 <img src="{{ asset('frontend/button-icons/infotech/office_365.svg') }}" class="img-fluid"> <br>

                                <span class="mobile-text">Office 365 Deployment</span></label>
                            </div>
                        </div>
                         <div class="col-lg-4 col-xl-4 col-md-3 col-sm-6 col-12 col-xs-6 {{ Session::get('infotech.property_type.property') =="website-development" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                                <img src="{{ asset('frontend/button-icons/infotech/website_development.svg') }}" class="img-fluid"> <br>

                                <span class="mobile-text">Website <br> Development</span></label>
                            </div>
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.property_type.property') =="mobile-app-development" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                                <img src="{{ asset('frontend/button-icons/infotech/mobile _development.svg') }}" class="img-fluid"> <br>

                                <span class="mobile-text">Mobile App Development</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.property_type.property') =="digital-marketing" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                                <img src="{{ asset('frontend/button-icons/infotech/digital_marketing.svg') }}" class="img-fluid"> <br>

                                <span class="mobile-text">Digital Marketing</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.property_type.property') =="graphics-design-multimedia" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                               {{--  <img src="{{ asset('frontend/button-icons/end off lease selected.svg') }}" class="img-fluid"> <br>
                                --}}
                                <span class="mobile-text">Graphics Design Multimedia</span></label>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.property_type.property') =="cloud-infrastracture" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                                {{-- <img src="{{ asset('frontend/button-icons/end off lease selected.svg') }}" class="img-fluid"> <br>
                                 --}}
                                <span class="mobile-text">Cloud Infrastracture</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "website-development​" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                                <img src="{{ asset('frontend/button-icons/infotech/website_development.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Website Development​</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "web-application" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">

                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/web application.svg') }}" class="img-fluid"><br>

                                <span class="mobile-text">Web Application</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "office-365-migration" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                 <img src="{{ asset('frontend/button-icons/infotech/office365/office 365 integration.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Office 365 Migration</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "office-365-emails-and-apps" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/office365/office 365 email and apps.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Office 365 Emails and Apps</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "office-365-training" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                 <img src="{{ asset('frontend/button-icons/infotech/office365/office 365 training.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Office 365 Training</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "office-365" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/cloud/365.png') }}" class="img-fluid"><br>
                                 <span class="mobile-text">Office <br> 365</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "microsoft-azure" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/cloud/azure.png') }}" class="img-fluid"><br>
                            <span class="mobile-text">Microsoft Azure</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "starter-application" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/mobile/starter application.svg') }}" class="img-fluid"><br>
                              <span class="mobile-text">Starter Application</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "business-application" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/mobile/business application.svg') }}" class="img-fluid"><br>
                            <span class="mobile-text">Business Application</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "custom-application" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/mobile/custom application.svg') }}" class="img-fluid"><br>
                            <span class="mobile-text">Custom Application</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "SEO" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/digital/seo/seo.svg') }}" class="img-fluid"><br>
                              <span class="mobile-text">SEO</span>
                            </div>
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{ Session::get('infotech.type.type') == "SEM" ? : "hidden" }}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
{{--                                 <img src="{{ asset('frontend/button-icons/regular selected.svg') }}" class="img-fluid"> <br>
 --}}                                <span class="mobile-text">SEM</span></label>
                            </div>
                        </div>


                        <div class="col-lg-12 text-left mt-2">
                           <p class="text-small spacing-1 text-color-white text-uppercase">You Selected (Extras)</p>
                        </div>      
                       <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('landing-page',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/landing page.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Landing <br> Page</span></label>
                            </div>
                        </div>
                         <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('info-site',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/infosite.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Info <br> Site</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6{{--  {{ in_array('creative-website',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/creative website.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Creative <br> Website</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6{{--  {{ in_array('financial-management',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/financial management.svg') }}" class="img-fluid"><br>
                                 <span class="mobile-text">Financial Management</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('inventory',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/inventory.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text"> Inventory System</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('e-commerce-site',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/e commerce site.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">E-Commerce Site</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('purchasing',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/purchasing.svg') }}" class="img-fluid"><br>
                                 <span class="mobile-text">Purchasing System</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 ">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/portal web application.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Portal Web Application</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 ">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/animated web application.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Animated Web Applications</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 ">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/human resource management.svg') }}" class="img-fluid"><br>
                              <span class="mobile-text mobile-text--HR">Human Resource Management</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('others',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/websiteDevelopment/others.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Other <br> System</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('website-audit',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/digital/seo/website audit.svg') }}" class="img-fluid"><br>
                               <span class="mobile-text">Website <br> Audit</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('keyword-research',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/digital/seo/keyword search.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Keyword Research</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('technical-optimization',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/digital/seo/technical optimization.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Technical Optimization</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 ">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/digital/seo/content optimization.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Content Optimization</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('onsite-seo',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/digital/seo/onsite seo.svg') }}" class="img-fluid"><br>
                                <span class="mobile-text">Onsite <br> SEO</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6{{--  {{ in_array('web-design',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/web design.png') }}" class="img-fluid pl-2 pr-2"><br>
                                <span class="mobile-text">Web <br> Design</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('e-commerce-design',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                 <img src="{{ asset('frontend/button-icons/infotech/graphics/ecommerce.png') }}" class="img-fluid pl-2 pr-2"><br>
                              <span class="mobile-text">E-commerce <br> design</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('website-posters-and-banners-design',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/website_poster.png') }}" class="img-fluid pl-2 pr-2"><br>
                              <span class="mobile-text">Posters and <br>Banners Design</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/social_media.png') }}" class="img-fluid pl-2 pr-2"><br>
                              <span class="mobile-text">Social Media <br> Postings</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('avp-for-any-events',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                 <img src="{{ asset('frontend/button-icons/infotech/graphics/avp.png') }}" class="img-fluid pl-2 pr-2"><br>
                            <span class="mobile-text">AVP for any <br> events</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('mobile-app-design',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/mobile_app.png') }}" class="img-fluid pl-2 pr-2"><br>
                              <span class="mobile-text">Mobile App <br> Design</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('catalog-design',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/catalog.png') }}" class="img-fluid pl-2 pr-2"><br>
                              <span class="mobile-text">Catalog <br> design</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('business-cards',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/business_card.png') }}" class="img-fluid pl-2 pr-2"><br>
                              <span class="mobile-text">Business <br> Cards</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6{{--  {{ in_array('other-marketing-materials',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/other_marketing.png') }}" class="img-fluid pl-2 pr-2"><br>
                              <span class="mobile-text mobile-text--hr">Marketing <br> Materials</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('company-logos',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
                                <img src="{{ asset('frontend/button-icons/infotech/graphics/company logo.png') }}" class="img-fluid pl-2 pr-2"><br>
                            <span class="mobile-text">Company <br> Logos</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('social-media-calendar',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
{{--                                 <img src="{{ asset('frontend/button-icons/rubbish removal selected.svg') }}" class="img-fluid">
 --}}                               <br><span class="mobile-text">Social Media Calendar</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6 {{-- {{ in_array('regular-media-calendar',Session::get('infotech.services.svc')) ? : "hidden" }} --}}">
                          <div class="styled-radio mt-2 mb-2 ">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
{{--                                 <img src="{{ asset('frontend/button-icons/rubbish removal selected.svg') }}" class="img-fluid">
 --}}                               <br><span class="mobile-text">Regular Media Calendar</span></label>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 col-6">{{--  {{ in_array('social-media-advertisements',Session::get('infotech.services.svc')) ? : "hidden" }}" --}}
                          <div class="styled-radio mt-2 mb-2">
                            <input type="checkbox" disabled checked="" id="regular" name="service-type">
                              <label for="regular" class="text-xxxsmall text-uppercase spacing-1" style="color: white;">
{{--                                 <img src="{{ asset('frontend/button-icons/rubbish removal selected.svg') }}" class="img-fluid">
 --}}                               <br><span class="mobile-text">Social Media Advertisements</span></label>
                            </div>
                        </div>
                        <div class="col-lg-12">
                          <div class="row">
                            <div class="col-lg-12 mt-3">
                              <span class="text-color-white font-semibold">Do you want to secure a spot now?</span>
                            </div>
                            <div class="col-md-6 col-12 text-left">
                              <button type="submit"  id="next" class="next  card-shadow btn font-semibold text-uppercase text-xsmall text-color-white w-100 pt-3 pb-3 mt-3" style="background-color:#4472C4;color: white !important;">Yes</button>
                              <p class="text-white mt-2 text-xxxsmall text-uppercase spacing-1">Give me a quote and secure booking now. </p>
                            </div>
                            <div class="col-md-6 col-12 text-left">
                              <a  href="#modal_call" data-toggle="modal"  data-role="view" style="text-decoration: none;"><button class="btn text-color-white mt-3 w-100 font-semibold text-uppercase text-xsmall  card-shadow pt-3 pb-3" style="background-color: #972121;">No</button></a>
                              <p class="text-white mt-2 text-xxxsmall text-uppercase spacing-1">Call me and discuss price and schedule</p>
                            </div>     
                          </div>
                        </div>

                       
                    </div>
                  </div>
                 
             </div>
          </div>
        </div>
        
   </div>
</form>


<!-- Modal -->
<div class="modal fade" id="modal_call" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="top: 25% !important">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <div class="modal-content">
      <div class="modal-header d-flex justify-content-end">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    
      <form action="{{ route('frontend.booking.complete') }}" method="POST">
          <input type="hidden" name="fname" id="name">
          <input type="hidden" name="lname" id="lastname">
          <input type="hidden" name="email" id="mail">
          <input type="hidden" name="contact" id="contacts">
        {{ csrf_field() }}
      <div class="modal-body p-4 text-left">
        <p class="text-color-gray font-semibold"><span class="text-errors show">Are you sure you wanted to cancel your booking?<br></span>
        <span class="text text-danger d-none pt-3 text-xxsmall"> Please fill up the required fields for future appointment</span></p>
        <div class="d-flex flex-row ">
          <button type="submit" id ="proceeds" class="btn bg-color-blue text-white w-100 text-errors text-uppercase show">Yes</button>
          <button class="btn bg-color-red text-white w-100 text-uppercase ml-2 text-errors show" type="button" data-dismiss="modal" aria-label="Close">No</button>
        </div>
      </div>
   
      </form>
    </div>
  </div>
</div>
<!-- end Modal -->
@include('frontend._components.footer-main')
@stop
@section('scripts')
<script type="text/javascript">
  

$(document).ready(function(){

  $(document).on('click','a[data-role=view]',function(){
    var fname = $('#fname').val()
    var lname = $('#lname').val()
    var email = $('#email').val()
    var contact = $('#contact').val()


    $('#name').val(fname)
    $('#lastname').val(lname)
    $('#mail').val(email)
    $('#contacts').val(contact)

    $('.text').addClass('d-none')
  })


$('#proceeds').on('click',function(e){
  var fname = $('#fname').val()
  var lname = $('#lname').val()
  var email = $('#email').val()
  var contact = $('#contact').val()
    var x = 0;
if( fname == "" || lname == "" || email == "" || contact == "")
{
    
    // $('.text').removeClass('d-none')
    // $('.text-errors').removeClass('show')
    //  x = 1;
    // $('.text-errors').addClass('hidden')

    // if(x == 1){
    //     setInterval(function(){
    
    // $('.text').addClass('d-none')
    //    window.location.reload()
    // },1000)
    // }
    
    //  e.preventDefault()
      Swal.fire({
  type: 'error',
  title: 'Error',
  text: 'Please fill up the required fields for future appointment',
  
})
$('#modal_call').modal('hide');

    e.preventDefault()
}

})

})





</script>
@stop
@section('scripts-content')
<script>



</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
    padding: 0px !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
    padding: 10px !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 

@media only screen and (max-width: 1000px) {

 #gradient-background {
   /* width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
     background-color: #2047A4;
}
.picture-size {
height: 15%; width: 30%;
}

}
@media only screen and (max-width: 767px) {
.mobile-text {
  font-size: 12px;
}

}
@media only screen and (max-width: 999px) {
.mobile-text {
  font-size: 12px;
}

}

@media only screen and (min-width: 1000px) {
.mobile-text {
  font-size: 12px;
}
.img-fluid {
    max-width: 68% !important;
    height: auto;
}
 #gradient-background {
  /*  width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
     background-color: #2047A4;
}
.picture-size {
height:5%; width: 7%;
}

  }
  a:hover{
    color: white !important;
  }
  .hidden {
    display: none;
  }

</style>
@stop