@extends('frontend._layout.main') @section('style-script')
<style type="text/css">
    body {
        background-color: #F4F4F4 !important;
    }
    
    @media only screen and (max-width: 1000px) {
        #gradient-background {
            width: 100%;
            padding-top: 70px;
            height: 100%;
            background: linear-gradient(-90deg, transparent, transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center;
            background-size: cover;
            \ background-position: center;
        }
        .picture-size {
            height: 15%;
            width: 30%;
        }
    }
    
    @media only screen and (min-width: 1000px) {
        #gradient-background {
            width: 100%;
            padding-top: 70px;
            height: 100%;
            background: linear-gradient(-90deg, transparent, transparent, transparent, transparent), url("frontend/background/bg5.jpg") no-repeat center;
            background-size: cover;
            background-position: center;
        }
        .picture-size {
            height: 5%;
            width: 7%;
        }
    }
    
    a:hover {
        color: white !important;
    }
    
    .prev_btn {
        text-align: right;
        margin-right: 35px
    }
    
    .fa-caret-square-right {
        position: absolute;
        right: 0;
        top: 0;
    }
</style>
@stop @section('content') @include('frontend._components.topnav-main')

<div class="container mt-4 text-center" id="gradient-background" style="margin-top: 170px !important; padding:100px;">
    <h3 class="text-white text-uppercase">Services</h3>
</div>

<div class="container mt-2" style="background-color: white; ">
    <div class="contentz">
        <div class="">
            <div class="card p-4" style="border: 0px solid rgba(0,0,0,.125);">
                <div class="card-body p-3">
                    <div class="row">
                        <div class="col-lg-12 mt-2 text-left">
                            <h4 class="text-color-red">Cleaning Services</h4>
                            <hr>
                        </div>
                        <div class="col-lg-12 text-center" style="background-color: white;">

                            <div class="services">

                                <div class="slider--services mt-3 text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                    <div class="d-flex justify-content-center">
                                        <img src="{{ asset('frontend/button-icons/main/cleaning/business_selected.png') }}" style="height:100px;width:auto;" />
                                    </div>
                                    <p class="pt-2 font-weight-bold text-uppercase">Office Cleaning</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/home_selected.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Home Cleaning</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/carpet_selected.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Carpet Cleaning</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/house_care.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">House Care</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/gardening_selected.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Lawns and gardening</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/rubbish_selected.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Rubish removal</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/endoff_selected.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Vacate</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/strip_polish.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Strips and Polish</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/cleaning">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/cleaning/pressure_wash.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Pressure Wash</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-5 text-left">

                                <h4 class="text-color-red">Accounting Services</h4>
                                <hr>
                            </div>
                            <div class="services">
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/accounting">
                                        <div class="d-flex justify-content-center">
                                           <img src="{{  asset('frontend/button-icons/main/accounting/tax.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Tax</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/accounting">
                                        <div class="d-flex justify-content-center">
                                           <img src="{{  asset('frontend/button-icons/main/accounting/small_business.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Small Business</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/accounting">
                                        <div class="d-flex justify-content-center">
                                           <img src="{{  asset('frontend/button-icons/main/accounting/smsf.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Superannuation</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-5 text-left">
                                <h4 class="text-color-red">Foreign Exchange Services</h4>
                                <hr>
                            </div>
                            <div class="services">
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/forex">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/forex/globa_license.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Global License</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/forex">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/forex/interbank.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Interbank Liquidity</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/forex">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/forex/support.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Personalised Support</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/forex">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/forex/smart_trading.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Smart Trading</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/forex">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/main/forex/forex.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Forex Trading Environment</p>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-12 mt-5 text-left">
                                <h4 class="text-color-red">Infotech Services</h4>
                                <hr>
                            </div>
                            <div class="services">
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/infotech">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/infotech/office_365.svg')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Office 365</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/infotech">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/infotech/website_development.svg')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Website Development</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/infotech">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/infotech/cloud/365.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Cloud Infrastructure</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/infotech">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/infotech/mobile _development.svg')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Mobile Development</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/infotech">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/infotech/graphics/company logo.png')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Graphics Design & Multimedia</p>
                                    </a>
                                </div>
                                <div class="slider--services text-center w-75 mt-3" style="border:1px solid #5f6769;padding:10px">
                                    <a href="http://booking.bigbossgroup.com.au/infotech">
                                        <div class="d-flex justify-content-center">
                                            <img src="{{  asset('frontend/button-icons/infotech/digital_marketing.svg')  }}" style="height:100px;width:auto;" />
                                        </div>
                                        <p class="pt-2 font-weight-bold text-uppercase">Digital Marketing</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@include('frontend._components.footer') @stop