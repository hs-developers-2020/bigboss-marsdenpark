@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
 <div class="container" style="padding-top: 140px;padding-bottom:7%;">
{{ Session::put('percent',100) }}
  <form action="" onsubmit="return validateForm()" method="POST">
    {{ csrf_field() }}

     <div class="row">   
        <div class="col-lg-12 text-center">    
           <p class="text-white font-semibold spacing-1 mt-3">How are you going to pay for our services ?</p>
        </div>
      <div class="col-lg-3"></div>
        <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="cod" name="payment" value="cash" data-toggle="modal" data-target=".bd-example-modal-xl">
                <label for="cod" class="text-xxsmall font-weight-bold spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/cash on service selected.svg') }}" class="img-payment--size"> <br><span class="mobile-text">Cash</span></label>
              </div>
            </div>
            
            <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="visa" name="payment" value="bank transfer" data-toggle="modal" data-target=".bd-example-modal-xl">
                <label for="visa" class="text-xxsmall font-weight-bold spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/payment/bank_transfer.png') }}" class="img-payment--size"> <br><span class="mobile-text">Bank Transfer</span></label>
              </div>
            </div>

         {{--    <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="amex" name="payment" value="amex" data-toggle="modal" data-target=".bd-example-modal-xl"> 
                <label for="amex" class="text-xxsmall font-weight-bold spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/amex seelcted.svg') }}" class="img-payment--size"> <br>AMEX</label>
              </div>
            </div> --}}
            <div class="col-lg-2 col-6  text-center mt-3">
            <div class="styled-radio mt-2 mb-2">
              <input type="radio" id="card" name="payment" value="card" data-toggle="modal" data-target=".bd-example-modal-xl">
                <label for="card" class="text-xxsmall spacing-1 pt-4 pb-4" style="color: white;"> <img src="{{ asset('frontend/button-icons/payment/Artboard 36.png') }}" class="img-payment--size"> <br><span class="mobile-text" style="word-wrap: break-word;">Card</span></label>
              </div>
            </div>
           {{--  <div class="col-lg-2 col-6  text-center mt-3">
            <div class="styled-radio mt-2 mb-2">
              <input type="radio" id="paypal" name="payment" value="paypal" data-toggle="modal" data-target=".bd-example-modal-xl">
                <label for="paypal" class="text-xxsmall text-uppercase spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/paypal selected.svg') }}" class="img-payment--size"> <br>PayPal</label>
              </div>
            </div> --}}
            <div class="col-lg-3"></div>
            <div class="col-lg-12 text-center">
              <p class="text-color-gray mt-3 font-semibold text-xsmall spacing-1"><span class="p-1 rounded" style="background-color: #FFFF82;">We do not collect any of your financial details. The selected payment method is for information only.</span></p>
            </div>
            <div class="col-lg-12 text-center">
              <div id="continue" class="">
                <a id="next" href="{{route('frontend.book-submitted')}}" class="spacing-1 text-color-white" style="text-decoration: none;">
              <button  type="submit"class="btn card-shadow text-color-white mt-3 font-semibold spacing-1 text-uppercase text-xsmall pl-5 pr-5 pt-3 pb-3" style="background-color: #1F46A3;" id="continues">FINISH BOOKING 
                </button>
                </a>
              </div>
            </div>
        
      </div>

    </form>
   </div>
@include('frontend._components.footer-cleaning')
@stop
@section('scripts-content')
<script>

@if ($errors->first('payment') ? 'has-error' : NULL) {
  Swal.fire({
    type: 'error',
    title: 'Oops...',
    text: 'Invalid or No Selected Payment Method, Please Select One to Continue',
  })
    e.preventDefault()
  }
@endif
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">

  .styled-radio input[type=radio]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
  }
  .styled-radio input[type=checkbox]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
  }
  .styled-radio input[type=checkbox] {
      position: absolute;
      visibility: hidden;
      display: none;
  }
  a:hover{
    color: white !important;
  }
  p{
    font-size: 17px;
    font-weight: 600;
  }
  body {
    background-image: url(" {{ asset('frontend/background/cleaning bg.png') }}") !important;
    background-repeat:no-repeat !important;
    background-size:cover !important;
    overflow-x: hidden !important;
  } 

  @media only screen and (max-width: 1600px) {

  #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
  }

  .picture-size {
    height: 15%; width: 30%;
  }

  }
  @media only screen and (min-width: 1800px) {
    .footer-bottom{
      position: absolute;
      bottom: 0%;

    }
  }

  @media only screen and (min-width: 1600px) {
    #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
    }
    .picture-size {
      height:5%; width: 7%;
    }
  }

</style>
@stop