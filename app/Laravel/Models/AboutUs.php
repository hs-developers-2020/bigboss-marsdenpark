<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class AboutUs extends Model
{
    use DateFormatterTrait,SoftDeletes;

	protected $table = "about_us";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description','title'
    ];

    public $timestamps = true;
}
