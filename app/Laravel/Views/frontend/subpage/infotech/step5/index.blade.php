@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
 <div class="container" style="padding-top: 153px;padding-bottom:3%;">
   <form action="" method="post">
    {{ csrf_field() }}
    <div class="row">
  
      <div class="col-12 text-center text-xs-center text-md-right text-lg-right text-right">
            <p class="text-color-white font-semibold text-uppercase spacing-1">Service Quote for: {{ Session::get('infotech.postcode') }}, {{ Session::get('infotech.sub') }}</p>
        </div>
     <div class="col-lg-12 text-center">
        <p class="text-white font-semibold spacing-1 mt-3">Select your booking date</p>
     </div>
     <div class="col-lg-3"></div>
       <div class="col-lg-6 text-center mt-3">
           <div class="card p-4 card-shadow">
             <div class="card-body">
               <div class="row">
                 <div class="col-lg-12 text-left">
                   <p class="font-weight-bold text-xsmall text-color-blue">Select Booking Date<span class="text-danger">*</span></p>
                 </div>
                 <div class="col-lg-12">
                   {{-- <input type="date" id="booking_date" required class="form-control" name="booking_date"> --}}
                   <input type="text" required id="booking_date" class="form-control" autocomplete="off" name="booking_date" placeholder="Select your booking date" value="{{ Session::has('infotech.booking_date') ? Session::get('infotech.booking_date') : Session::get('infotech.booking_date') }}">
                   {{-- <input type="text" class="form-control datepicker" name="date_hired" value="{{old('date_hired')}}" autocomplete="off"> --}}
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-xxsmall">Time Start (Optional)</label>
                  <input type="text" id="t1" class="form-control text-uppercase" name="time_start" />
                   {{-- <input type="time" class="form-control time_picker" name="time_start"  min="07:00" max="19:00"> --}}
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-xxsmall">Time End (Optional)</label>
                   <input type="text" class="form-control text-uppercase" id="t2" name="time_end"/>
                  
                 </div>
               </div>
             </div>
           </div>
       </div>

    {{--   <div class="col-lg-5 text-center mt-3">
           <div class="card p-4">
             <div class="card-body">
               <div class="row">
                 <div class="col-lg-12 text-left">
                   <p class="font-semibold text-uppercase text-xsmall text-color-blue">Select Secondary Booking Date</p>
                 </div> 
                 <div class="col-lg-12">
                   <input  placeholder="Select your secondary booking date"  value="{{ old('booking_date',Session::get('infotech.booking_date_secondary')) }}" type="text" required id="booking_date_secondary" class="form-control" autocomplete="off" name="booking_date_secondary">
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-uppercase text-xxsmall">Time Start (Optional)</label>
                   <input type="time" id="time_start" min="07:00" max="19:00"   class="form-control time_picker" name="secondary_time_start">
                 </div>
                 <div class="col-lg-6 mt-3 text-left">
                   <label class="text-color-gray font-semibold text-uppercase  text-xxsmall">Time End (Optional)</label>
                   <input type="time" min="07:00" max="19:00"  class="form-control time_picker" name="secondary_time_end">
                 </div>
               </div>
             </div>
           </div>
       </div> --}}
       <div class="col-lg-3"></div>
           <div id="continue" class="col-lg-12 text-center mt-2">
             <div class="">
              <button class="btn text-color-white mt-3 text-xxsmall card-shadow pl-5 pr-5 pt-3 pb-3" style="background-color: #1F46A3;" id="continues" >
              <a name="next" class="spacing-1" style="text-decoration: none;">CONTINUE</a>
            </button>
             </div>
           </div>
        
     </div>
      </form>
  </div>
@include('frontend._components.footer-main')
@stop
@section('scripts-content')
<script>

$(document).ready(function(){

$('#continues').on('click',function(e){

  var booking_date = $('#booking_date').val()
  var booking_date_secondary = $('#booking_date_secondary').val()
  var currentDate = new Date()
  var day = currentDate.getDate()
var month = currentDate.getMonth() + 1
var year = currentDate.getFullYear()

  var date =  year + "/" + month + "/0" + day
  if(booking_date > booking_date_secondary)
  {
   
    Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Secondary booking date must be ahead than primary booking date!',
  
})
    e.preventDefault()
  }
  // else if(booking_date+1 < date  )
  // {
  //   alert(date)
  //   Swal.fire({
  //       type: 'error',
  //       title: 'Oops...',
  //       text: 'Primary booking Date is not available!',
  
  //         })
  //   e.preventDefault()
  // }
  else
  {
    return true;
  }

    
})

    


})

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 
@media only screen and (max-width: 720px) {
  .popover {
    display: none !important;
  }
}

@media only screen and (max-width: 1500px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
 .footer-bottom{
margin-top: 100px;
}
}

@media only screen and (min-width: 1500px) {

   #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
  }
  .picture-size {
  height:5%; width: 7%;
  }
 .footer-bottom{
position: absolute;
top: 100%;
  bottom: 0%;
  
}
  }
</style>
@stop