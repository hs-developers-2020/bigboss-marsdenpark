@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
<div class="container pb-5" style="padding-top: 155px; padding-bottom: 7%;">
        <div class="card card-shadow">
          <div class="card-body" style="padding: 0px !important">
          {{ Session::put('percent',90) }}
          <form action="" method="POST">
            {{ csrf_field() }}

            <div class="row" style="margin-right: 0px !important; margin-left: 0px !important;">
                  <div class="col-lg-6 p-5">
                    <div class="row">
                      <div class="col-lg-12">                        
                        <p class="text-color-blue font-semibold text-uppercase text-small">Confirm Booking <span class="font-semibold text-color-red"> Summary</span></p>

                      </div>
                       <div class="col-lg-12 mt-3">
                        <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-list-alt mr-2 text-color-blue"></i>Service Details</p>
                      </div>
                      <div class="col-lg-12">
                        <div class="p-2" style="background-color: #F4F4F4;"> 
                          <div class="row">
                            <div class="col-lg-5 col-md-6 mt-lg-0 mt-md-0 mt-sm-2 mt-xs-2">
                              <span class="text-color-gray font-semibold text-xsmall">Service Type :</span>
                            </div>
                            <div class="col-lg-7 col-md-6 text-first-letter">
                              <span  id="service_type"class="text-color-blue text-xsmall ">{{ Str::title(implode(",", session::get('infotech.svc_name'))) }}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 mt-4">
                        <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-calendar-alt text-color-blue mr-2"></i>Booking Time/Date</p>
                      </div>
                       <div class="col-lg-12">
                        <div class="p-2" style="background-color: #F4F4F4;"> 
                          <div class="row">
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <span class="text-color-gray font-semibold text-xsmall">Booking Date :</span>
                            </div>
                            <div class="col-lg-7 col-md-6 mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <span class="text-color-blue text-uppercase text-xsmall">{{ Session::get('infotech.booking_date')   }}</span>
                            </div>
                      
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <p class="text-color-gray font-semibold text-xsmall">Service Time :</p>
                            </div>
                            <div class="col-lg-7 col-md-6 mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <p class="text-color-blue text-uppercase text-xsmall font-semibold">                      
                                     {{   Session::get('infotech.time_start') && Session::get('infotech.time_end') == '08:00 ' ? '' : Helper::time_only(Session::get('infotech.time_start'))  }} - {{ Session::get('infotech.time_start') && Session::get('infotech.time_end') == '08:00 ' ? '' : Helper::time_only(Session::get('infotech.time_end'))}}       
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    
                       <div class="col-lg-12 mt-4">
                        <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-map-marker-alt mr-2 text-color-blue"></i>Service Location </p>
                      </div>
                      <div class="col-lg-12">
                        <div class="p-2" style="background-color: #F4F4F4;"> 
                          <div class="row">
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <span class="text-color-gray font-semibold text-xsmall">Address :</span>
                            </div>
                            <div class="col-lg-7 col-md-6  mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <span class="text-color-blue text-xsmall">{{ Session::get('infotech.location') }}</span>
                            </div>
                            <div class="col-lg-5 col-md-6 mt-lg-1 mt-md-1 mt-sm-3 mt-xs-3">
                              <span class="text-color-gray font-semibold text-xsmall">Postcode :</span>
                            </div>
                            <div class="col-lg-7 col-md-6  mt-lg-1 mt-md-1 mt-sm-1 mt-xs-1">
                              <span class="text-color-blue text-uppercase text-xsmall">{{ Session::get('infotech.postcode') }}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-12 mt-4 col-md-12 col-sm-12 col-xs-12">
                           <p class="text-color-blue font-weight-bold text-uppercase text-xsmall"><i class="fas fa-user text-color-blue mr-2"></i>Customer Details</p>
                        </div>
                        <div class="col-lg-12">
                          <div class="p-2" style="background-color: #F4F4F4;"> 
                            <div class="row">
                               <div class="col-lg-5 col-md-6 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                                  <span class="text-color-gray font-semibold text-xsmall">Name :</span>
                              </div>
                              <div class="col-lg-7 col-md-6 mt-lg-2 mt-md-2 mt-sm-1 mt-xs-1">
                                  <span class="text-color-blue text-left font-semibold text-xsmall">{{ Str::title(Session::get('infotech.fname')) ." ".Str::title(Session::get('infotech.lname')) }}</span>
                              </div>
                              <div class="col-lg-5 col-md-6 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                                  <span class="text-color-gray font-semibold text-xsmall">Contact Number :</span>
                              </div>
                              <div class="col-lg-7 col-md-6 text-sm-left text-lg-left mt-lg-2 mt-md-2 mt-sm-1 mt-xs-1">
                                <span class="text-color-blue text-xsmall text-left font-semibold text-uppercase">+61 {{ Session::get('infotech.contact') }}</span>
                              </div>
                              <div class="col-lg-5 col-md-6 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                                <span class="text-color-gray font-semibold text-xsmall">Email Address :</span>
                              </div>
                              <div class="col-lg-7 col-md-6 text-sm-left mt-lg-2 mt-md-2 mt-sm-1 mt-xs-1 ">
                                <span class="text-color-blue text-left font-semibold text-xsmall">{{ Session::get('infotech.email') }}</span>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-lg-6 p-5" id="gradient-background1">
                     <div class="row">     
                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-lg-right text-md-right text-sm-left text-xs-left">
                            <p class="text-color-white font-semibold text-uppercase spacing-1 text-xsmall">Booking ID : {{ Session::get('booking_info.booking_id') }}</p>
                        </div>
                       
                        <div class="col-lg-12 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                          <p class="text-color-white text-xsmall font-semibold text-uppercase"><i class="fas fa-user text-color-white mr-2"></i>Payment Summary</p><hr class="bg-white">
                        </div>

                        <div class="col-lg-12 mt-lg-2 mt-md-2 mt-sm-3 mt-xs-3">
                          <table class="font-semibold text-xsmall " style="width: 100%">
                            <th style="color:#fff;padding-bottom: 15px;">Service Type</th>
                            <th style="color:#fff;padding-bottom: 15px;">Price</th>
                            @foreach($package as $value)
                            <tr>
                              <td style="color:#fff;padding-bottom: 15px;">{{ $value->name }}</td>
                              <td style="color:#fff;padding-bottom: 15px;">${{ $value->price }}</td>
                            </tr>
                            @endforeach

                          </table>
                        </div>
                        <div class="col-lg-12">
                          <hr class="text-color-white" style="height: 2px; background-color: white;">
                        </div>
                        <div class="col-lg-6 col-md-6 col-8">
                          <p class="text-color-white font-semibold text-uppercase mobile-text-small">Estimated Total</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-8">
                          <p  class="text-color-white font-semibold text-uppercase mobile-text-small float-right"> ${{ session()->get('infotech.svc_total')}}</p>
                        </div>
                        <div class="col-lg-12">
                          <p class="text-xxsmall text-color-gray "><i style="background-color: #FFFF82;" class="p-1 rounded">Note: Price subject to change upon actual site inspection.</i></p>
                        </div>
                        <div class="col-lg-12 mt-3">
                          <div class="row">
                             <div class="col-lg-6 col-md-6 col-6">
                                  <button  type="submit" name="next" onclick="myFunction()" id="next" class="next btn font-semibold spacing-1 text-uppercase w-100 text-xsmall card-shadow bg-color-blue text-color-white pt-3 pb-3 mt-3" href="#"style="color: white !important;background-color: #4472C4">Continue</button>
                              </div>
                              <div class="col-lg-6 col-md-6 col-6">
                                <div  class="">
                                  <div class="">    
                                  {{--   <a name="prev" href="{{ route('frontend.cleaning.index') }}" class="spacing-1" style="text-decoration: none;"> --}}<a  href="#" id="cancel" class="btn card-shadow text-color-white mt-3 font-semibold w-100 spacing-1 text-uppercase text-xsmall pt-3 pb-3" id="cancel" style="background-color: #972121;">CANCEL</a>{{-- </a> --}}
                                  </div>
                                </div>
                              </div>     
                          </div>
                        </div>
                      </div>
                    </div>          
                </div>   
            </div>
        </div>
        </form>
   </div>



 @include('frontend._components.footer-cleaning')
 @stop
 @section('scripts-content')
 <script>

$(document).ready(function(){
$('#cancel').on('click',function(){
      Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, cancel it!'
    }).then((result) => {
      if (result.value) {
        window.location.href = "{{ route('frontend.infotech.last_cancel') }}";
      }
    })

  })
   
})

 </script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: #2047A4 !important;
    border: 2px solid white !important;
    background-color: white !important;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/Infotech.jpg') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 

@media only screen and (max-width: 1000px) {

 #gradient-background1 {
   /* width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
     background-color: #2047A4;
}

.picture-size {
height: 15%; width: 30%;
}
.mobile-text-small {
  font-size: 18px;
}
}

@media only screen and (min-width: 1000px) {

 #gradient-background1 {
   /* width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; */
    background-color: #2047A4;
}
.picture-size {
height:5%; width: 7%;
}
.mobile-text-small {
  font-size: 20px;
}
  }
  a:hover{
    color: white !important;
  }
span::first-letter {
  text-transform:uppercase !important;
}
</style>
@stop