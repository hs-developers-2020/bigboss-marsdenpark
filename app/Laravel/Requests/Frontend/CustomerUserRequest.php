<?php namespace App\Laravel\Requests\Frontend;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class CustomerUserRequest extends RequestManager{

	public function rules(){

		$id = $this->route('id')?:0;

		$rules = [
			'username'		=> "required|unique:user,username,{$id},id,deleted_at,NULL",
			'email'	=> "required|unique:user,email,{$id},id,deleted_at,NULL",
			// 'description'	=> "required",
			// 'birthdate'	=> "required|date",
			//'name'		=> "required",
			'password'	=> "required|confirmed",
			//'type'	=> "required",
			// 'street' => "required",
			// 'city' => "required",
			// 'state' => "required",
			// 'post_code' => "required",
			// 'alternate_email' => "required",

			'contact_number' => "required|max:10|phone:AU",
			'firstname' => "required",

			'lastname' => "required",
			// 'lname' => "required",
			

		];

		if($id != 0){
			$rules['password'] = "confirmed";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required'	=> "Field is required.",
			'contact.phone'	=> "Please provide a valid PH mobile number.",
			'birthdate.date'	=> "Birthdate must be a valid date.",
			'website.url'	=> "Invalid URL format. Adding http:// or https:// is also needed.",
		];
	}
}