	<script src="{{asset ('system/vendor/jquery-3.2.1.min.js')}} "></script>
	<script src="{{asset ('system/vendor/popper.min.js')}} "></script>
	<script src="{{asset ('system/vendor/bootstrap-4.1.1/bootstrap.min.js')}} "></script>

	<!--Cookie js for theme chooser and applying it --> 
	<script src="{{asset ('system/vendor/cookie/jquery.cookie.js')}} "></script> 

	<!-- Circular progress -->
	<script src="{{asset ('system/vendor/jquery-custom-scrollbar-0.5.5/jquery-custom-scrollbar-0.5.5/jquery.custom-scrollbar.min.js')}} "></script>

	<!-- Circular progress -->
	<script src="{{asset ('system/vendor/cicular_progress/circle-progress.min.js')}} "></script>

	<!-- Sparklines chartsw -->
	<script src="{{asset ('system/vendor/sparklines/jquery.sparkline.min.js')}} "></script>

	<!-- Other JavaScript -->
	<script src="{{asset ('system/js/main.js')}} "></script>
    <script src="{{asset('system/vendor/sweet-alert2/sweetalert2.min.js')}}"></script>

		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	

	<!--Cookie js for theme chooser and applying it --> 
	<script src="{{asset ('system/vendor/cookie/jquery.cookie.js')}}"></script> 

	<!-- Circular progress -->
	<script src="{{asset ('system/vendor/jquery-custom-scrollbar-0.5.5/jquery-custom-scrollbar-0.5.5/jquery.custom-scrollbar.min.js')}}"></script>	

	<!-- Circular progress -->
	<script src="{{asset ('system/vendor/cicular_progress/circle-progress.min.js')}}"></script>

	<!-- Sparklines chartsw -->
	<script src="{{asset ('system/vendor/sparklines/jquery.sparkline.min.js')}}"></script>
    

	<!-- Footable  -->
	<script src="{{asset ('system/vendor/footable-bootstrap.latest/js/footable.min.js')}}"></script>
    <!-- chart js -->
	<script src="{{asset ('system/vendor/chartjs/Chart.bundle.min.js')}}"></script>
	<script src="{{asset ('system/vendor/chartjs/utils.js')}}"></script>
    
	<!-- jvectormap JavaScript -->
	<script src="{{asset ('system/vendor/jquery-jvectormap/jquery-jvectormap.js')}}"></script>
	<script src="{{asset ('system/vendor/jquery-jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
	<script src="{{asset ('system/vendor/jquery-jvectormap/jquery-jvectormap-asia-mill.js')}}"></script>
	<script src="{{asset ('system/vendor/jquery-jvectormap/jquery-jvectormap-africa-mill.js')}}"></script>
	<script src="{{asset ('system/vendor/jquery-jvectormap/jquery-jvectormap-us-aea.js')}}"></script>
	<script src="{{asset ('system/vendor/jquery-jvectormap/jquery-jvectormap-in-mill.js')}}"></script>
    @yield('page-scripts')
    
	<!-- Other JavaScript -->
	<script src="{{asset ('system/js/main.js')}}"></script>

       <script>
        "use strict";

        var pagination_exist = $(".pagination");

        if(typeof pagination_exist != "undefined"){
            pagination_exist.find("li").addClass("page-link")

            $(".page-link").on("click",function(){
                var link_exist = $(this).children('a')

                if(typeof link_exist != "undefined" && typeof link_exist.attr("href") != "undefined"){
                    window.location.href = link_exist.attr("href")
                }
            })
        }

        /* Footable */
        $('.footable').footable({
            "paging": {
                "enabled": true,
                "size": 4
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });

    
        "use strict";
        /* Footable */
        $('.footable-finance').footable({
            "paging": {
                "enabled": true,
                "size": 10
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });
        
        "use strict";
        $('.footable-history').footable({
            "paging": {
                "enabled": true,
                "size": 5
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });
        "use strict";
        $('.jobs-show').footable({
            "paging": {
                "enabled": true,
                "size": 10
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });
        "use strict";
        $('.footable-availability').footable({
            "paging": {
                "enabled": true,
                "size": 3
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });
        "use strict";
        $('.footable-service').footable({
            "paging": {
                "enabled": true,
                "size": 10
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });

        "use strict";
        $('.footable-media').footable({
            "paging": {
                "enabled": true,
                "size": 10
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });
        "use strict";
        $('.footable-dashboard').footable({
            "paging": {
                "enabled": true,
                "size": 5
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });
        "use strict";
        $('.footable-audit').footable({
            "paging": {
                "enabled": true,
                "size": 10
            }
        }, function(ft){
            $('.checkbox-user-check').on('change',function(){                
                $(this).closest('.media').toggleClass("selected");
            });
            $('.selectallticket').on('change',function(){
                if( $(this).is(":checked") == true){
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', true).closest('.media').addClass("selected");
                 }else{
                    $('.checkbox-user-check input[type="checkbox"]').attr('checked', false).closest('.media').removeClass("selected");
                 }
            });
        });
       
    </script>