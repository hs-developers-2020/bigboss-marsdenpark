<?php

namespace App\Laravel\Models;


use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon, Helper,Str;

class MediaLibrary extends Model
{
    use DateFormatterTrait,SoftDeletes;

    protected $table = "media_library";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','group','path', 'directory', 'filename'
    ];

    public $timestamps = true;

    public function scopeGroup($query,$group = NULL){
        $key = Str::lower($group);
        if ($key) {
            return $query->where(function($query) use ($key){
                    $query->where('group', $key);
            });
        }
       

    }

}
