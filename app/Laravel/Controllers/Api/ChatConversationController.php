<?php 

namespace App\Laravel\Controllers\Api;

use Helper, Str, DB,ImageUploader,FileUploader;
use App\Laravel\Models\Views\Chat;
use App\Laravel\Models\ChatConversation;
use App\Laravel\Models\ChatParticipant;

use Illuminate\Http\Request;
use App\Laravel\Requests\Api\ChatConversationRequest;
use App\Laravel\Requests\Api\ChatConversationFileRequest;

use App\Laravel\Transformers\ChatTransformer;
use App\Laravel\Transformers\ChatParticipantTransformer;
use App\Laravel\Transformers\ChatConversationTransformer;
use App\Laravel\Transformers\TransformerManager;

class ChatConversationController extends Controller{

	protected $response = array();

	public function __construct(){
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "BAD_REQUEST"
			);
		$this->response_code = 400;
		$this->transformer = new TransformerManager;
	}

	public function index(Request $request, $format = '') {
        $chat = $request->get('chat_data');
		$per_page = $request->get('per_page', 10);
        $page = $request->get('page', 1);
        // $unit = $request->get('unit_data');
        $user = $request->user();

        $this->response['msg'] = "Conversation thread";

        // $sort_by = $request->get('sort')

        $thread = ChatConversation::where('chat_id',$chat->id)->orderBy('created_at',"DESC")->paginate($per_page);

        $this->response['status'] = TRUE;
        $this->response['status_code'] = "CONVERSATION_THREAD";
        $this->response['has_morepages'] = $thread->hasMorePages();
        $this->response['chat_status'] = $chat->status; //inactive - closed 
        $this->response['data'] = $this->transformer->transform($thread, new ChatConversationTransformer, 'collection');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function upload(ChatConversationFileRequest $request, $format = ''){

        $user = $request->user();
        $chat = $request->get('chat_data');

        $mime_type = explode("/", $request->file('file')->getMimeType());
        $size = $request->file('file')->getSize();
        $type = "file";
        $orig_filename = $request->file('file')->getClientOriginalName();
        if(is_array($mime_type) AND Str::lower($mime_type[0]) == "image"){
            $type = "image";
        }

        $message = new ChatConversation;
        $message->sender_user_id = $user->id;
        $message->chat_id = $chat->id;
        $message->content = $orig_filename;
        $message->type = $type;
        $message->size = $size;

        if($type == "image"){
            $file = ImageUploader::upload($request->file('file'), "uploads/gc/{$chat->id}");
            $message->path = $file['path'];
            $message->directory = $file['directory'];
            $message->filename = $file['filename'];
        }else{
            $file = FileUploader::upload($request->file('file'), "uploads/gc/{$chat->id}");
            $message->path = $file['path'];
            $message->directory = $file['directory'];
            $message->filename = $file['filename'];
        }
        $message->save();

        $this->response['msg'] = "Added a new attachment.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "ATTACHMENT_ADDED";
        $this->response['data'] = $this->transformer->transform($message, new ChatConversationTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function store(ChatConversationRequest $request, $format = '') {

        $user = $request->user();
        $chat = $request->get('chat_data');

        $messsage = new ChatConversation;
        $messsage->sender_user_id = $user->id;
        $messsage->chat_id = $chat->id;
        $messsage->content = $request->get('content');
        $messsage->type = "message";
        $messsage->save();

        $this->response['msg'] = "Added new message.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "MESSAGE_ADDED";
        $this->response['data'] = $this->transformer->transform($messsage, new ChatConversationTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function show(Request $request, $format = '') {

        $comment = $request->get('comment_data');

        $this->response['msg'] = "Comment detail.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_DETAIL";
        $this->response['data'] = $this->transformer->transform($comment, new ChatConversationTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function update(ChatConversationRequest $request, $format = '') {
        $comment = $request->get('comment_data');
        $article = $request->get('article_data');
        $user = $request->user();

        $comment->fill($request->only('content'));

        $comment->save();

        $this->response['msg'] = "Comment has been updated.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "COMMENT_MODIFIED";
        $this->response['data'] = $this->transformer->transform($comment, new ArticleCommentTransformer, 'item');
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }

    public function destroy(Request $request, $format = '') {

        $user = $request->user();
        $chat = $request->get('chat_data');
        $msg = $request->get('msg_data');
        // $chat_participant = $request->get('chat_participant_data');

        $chat_participant = ChatParticipant::where('chat_id',$request->get('chat_id'))->where('user_id',$user->id)->first();

        if(!$chat_participant){
            $this->response['msg'] = "Unable to process request. You are not part of the conversation.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "UNAUTHORIZED";
            $this->response_code = 403;
            goto callback;
        }

        $msg_date = $msg->created_at;

        //if owned by the user
        if($msg->sender_user_id == $user->id){
            $msg->sender_is_deleted = 'yes';
            $msg->save();
            goto flag;

        }

        if($chat_participant->role != "moderator"){
            $this->response['msg'] = "You are not allowed to delete message you don't own.";
            $this->response['status'] = FALSE;
            $this->response['status_code'] = "UNAUTHORIZED";
            $this->response_code = 403;
            goto callback;
        }

        $msg->delete();

        $message = new ChatConversation;
        $message->chat_id = $request->get('chat_id');
        $message->type = "announcement";
        $message->sender_user_id = 1;
        $message->content = "{$user->name} deleted a message";
        $message->created_at = $msg_date;
        $message->save();

        // $comment->delete();

        flag:
        $this->response['msg'] = "Message has been removed.";
        $this->response['status'] = TRUE;
        $this->response['status_code'] = "MESSAGE_DELETED";
        $this->response_code = 200;

        callback:
        switch(Str::lower($format)){
            case 'json' :
                return response()->json($this->response, $this->response_code);
            break;
            case 'xml' :
                return response()->xml($this->response, $this->response_code);
            break;
        }
    }
}