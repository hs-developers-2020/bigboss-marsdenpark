@extends('system._layouts.main')
<style type="text/css">
footer {
    margin-top: 25px !important;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1030;
    }
body {
    padding-top: 9% !important;
}
.f-10 {
    font-size: 8px;
}

</style>
@section('content')
@include('system._components.topnav')
  
<div class="wrapper ">
    <div class="container ">
        <form action="" id="search_filter" >
            <div class="row">
                <div class="col-lg-5 pt-2">
                    <h6 class="text-xsmall text-color-gray text-uppercase pt-2" >File Directory: <b>{{$contract->business_name}}</b></h6>
                </div>
                <div class="col-lg-7 mb-2 d-flex flex-row ">   
                    <input type="text" class="searchTerm" name="keyword" value="{{$keyword}}" placeholder="Search keyword such as name">
                    <a href="#"  class="btn bg-color-blue text-color-white searchButton" id="button_search">
                        <i class="fa fa-search"></i>
                    </a>
                    <a  href="{{ route('system.contract-group.create',[$contract->user_id]) }}" style="padding-left: 10px;" class="no-decoration"><button class="btn bg-color-blue text-xxxsmall text-color-white text-uppercase" type="button"><i class="fas fa-plus text-color-white mr-2"></i>Add File</button></a>
                </div>          
            </div>
        </form>        
    
    
          <table class="table bg-color-white  mb-0 footable-finance">
            <thead>
                <tr class="text-nowrap">
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left">Reference #</th>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left">File Name(<i>file type</i>)</th>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs">Updated by</th>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs sm">Date Last Updated</th>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs sm">Date Expiry</th>
                    <th class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs sm">Status</th>
                    <th  class="p-4 pl-4 pr-4 text-uppercase text-xxsmall text-color-blue font-semibold text-left" data-breakpoints="xs sm">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($business_group as $index => $value)
                 <tr>
                    <td><p class="text-color-blue font-semibold p-3 text-xsmall">{{$value->reference}}</p></td>
                    <td><a href="{{ route('system.contract-group.show',[$value->id]) }}" class="text-color-blue font-semibold p-3 text-xsmall">{{Str::title($value->name)}}(<i>{{str_replace("_", " ", $value->file_type)}}</i>)</a></td>
                    
                    <td><p class="text-color-blue font-semibold p-3 text-xsmall">{{Str::title($value->user->full_name)}}</p></td>
                    <td><p class="text-color-blue p-3 font-semibold text-xsmall">{{Helper::date_format($value->updated_at)}}</p></td>
                    <td><p class="text-color-blue p-3 font-semibold text-xsmall">{{Helper::date_only($value->date_expiry)}}</p></td>
                    <td class="text-center"> <button class="btn btn-info rounded-3 text-color-white text-xxsmall">{{$value->is_approved == "yes" ? "Complied" :"Pending"}} Contract</button></td>
                    <td>
                        <div class="dropdown">
                          <span>
                            <i class="fas fa-circle f-10"></i>
                            <i class="fas fa-circle f-10"></i>
                            <i class="fas fa-circle f-10"></i></span>
                          <div class="dropdown-content">
                            <div class="d-flex flex-column mt-1">
                                {{-- <a href="{{ route('system.contract-group.edit',[$value->id]) }}" style="font-size: 16px;" class="mb-2 text-color-light font-semibold">Edit</a> --}}
                                <a href="{{ route('system.contract-group.destroy',[$value->id]) }}" style="font-size: 16px;" class="mb-2 text-color-light font-semibold">Delete</a>
                            </div>
                          </div>
                        </div>
                    </td>      
                </tr>
                @empty
                <tr>
                    <td><i>No Record</i></td>
                </tr>
                @endforelse        
            </tbody>
        </table>                                       
    </div>
</div> 
@stop


@section('page-scripts')
<script src="{{asset('assets/lib/moment.js/min/moment.min.js')}}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('system/js/toastr.min.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('system/css/toastr.css')}}">
<script>
$(function(){
    $('[data-toggle="tooltip"]').tooltip(); 

    @if(Session::has('alert-type'))
    var type="{{Session::get('alert-type','info')}}"
    console.log(type)
    switch(type){
        case 'info':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.info("{{ Session::get('notification-msg') }}")
             break;
        case 'success':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.success("{{ Session::get('notification-msg') }}");
            break;
        case 'warning':
            toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.warning("{{ Session::get('notification-msg') }}");
            break;
        case 'failed':
             toastr.options = {
              "positionClass": "toast-top-center"
            };
            toastr.error("{{ Session::get('notification-msg') }}");
            break;
    }
    @endif
    $("#action-delete").on("click",function(){
        var btn = $(this);
        $("#btn-confirm-delete").attr({"href" : btn.data('url')});
    });


});

$('#btn-confirm-delete').on('click', function() {
    console.log("aaa")
    $('.btn-link').hide();
    $('.btn-loading').button('loading');
    $('#target').submit();
 });


$('#select_group').on('change', function(){
    $('#search_filter').get(0).submit();
});
$("#button_search").on("click",function(e){
  $("#search_filter").submit();
});

$(".table")
    .delegate('.btn-destroy','click', function(){
    var url = $(this).data('url');
        swal({   
            title: "Are you sure?",   
            text: "You will not be able to recover this record.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#3085d6",   
            confirmButtonText: "Yes, delete it!"
        }).then(function(result) {
            window.location.href = url;
        });
});

  
</script>
@stop