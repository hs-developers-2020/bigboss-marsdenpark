<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<form id="redirect_form" action="{{route($route_name)}}" action="POST">
		{!!csrf_field()!!}
	</form>
	<script src="{{asset('frontend/js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
		$(function(){
			$("#redirect_form").submit();
		})
	</script>
</body>
</html>