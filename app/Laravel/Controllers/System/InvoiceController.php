<?php

namespace App\Laravel\Controllers\System;
use App\Laravel\Models\Invoice;
use App\Laravel\Models\CleaningService;
use App\Laravel\Models\SubService;
use App\Laravel\Models\Service;
use App\Laravel\Models\Finance;
use App\Laravel\Models\OtherService;
use App\Laravel\Events\SendEmailPdf;
//use App\Http\Controllers\Controller;

use App\Laravel\Services\ResponseManager;

use App\Http\Requests\PageRequest;
use Helper, Carbon, Session, Str, Auth, ImageUploader,Input,PDF,FileUploader,File,DB,Event,Excel;
class InvoiceController extends Controller
{
    protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['invoice_status'] = [ ''=>"Select Status",'paid' => "Paid","cancelled" => "Cancelled"];
		$this->data['payment_status'] = [ ''=>"Select Status",'paid' => "Paid",'pending' => "Pending","cancelled" => "Cancelled"];
		
	}

  	public function index(PageRequest $request){
  		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['status'] = $request->get('status',false);

  		if (!$this->data['from']) {
			$from = Invoice::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
			$this->data['to'] = Carbon::now()->format("Y-m-d");
		}
		

  		$this->data['invoice'] = Invoice::status($this->data['status'])->dateRange($this->data['from'],$this->data['to'])->orderBy("created_at" , "DESC")->get();
		return view ('system.invoice.index',$this->data);
	}

	public function show($id = NULL){

		$this->data['invoice_details'] = Invoice::find($id);
		
		
		return view ('system.invoice.show',$this->data);
	}

	public function print($id = NULL){

		$this->data['invoice'] = Invoice::find($id);

		$this->data['gst'] = $this->data['invoice']->amount / 11;

		$this->data['gst_total'] = $this->data['gst'] + $this->data['invoice']->amount;

		$this->data['booking_data'] = CleaningService::where('booking_id' , $this->data['invoice']->booking_id)->first();

		$this->data['others'] = OtherService::where('booking_id', $this->data['invoice']->booking_id)->get();

		$this->data['services'] = Service::whereIn('id', explode(',', $this->data['booking_data']->service_id))->get();

		$pdf = PDF::loadView('pdf.invoice', $this->data)->setPaper('letter', 'portrait');
		return $pdf->stream('invoice.pdf');
	}

	public function download($id = NULL){

		$this->data['invoice'] = Invoice::find($id);

		$this->data['booking_data'] = CleaningService::where('booking_id' , $this->data['invoice']->booking_id)->first();

		$this->data['others'] = OtherService::where('booking_id', $this->data['invoice']->booking_id)->get();

		$this->data['services'] = Service::whereIn('id', explode(',', $this->data['booking_data']->service_id))->get();

		$pdf = PDF::loadView('pdf.invoice', $this->data)->setPaper('letter', 'portrait');
		return $pdf->download('invoice.pdf');
	}

	public function send_invoice($id = NULL){
		
		try {

			$sendmail = CleaningService::where('booking_id' , $id)->first();
			$invoice_id = Invoice::where('booking_id' , $id)->first();

			if (!$sendmail) {
			session()->flash('notification-status',"failed");
			session()->flash('notification-msg',"Record not found.");
			return redirect()->route('system.invoice.index');
			}
			
			if ($sendmail) {

				$insert[] = [
		                'email' => $sendmail->email,
		                'firstname' => $sendmail->name,
		                'lastname' => $sendmail->lastname,
		                'name' => $sendmail->name . " " . $sendmail->lastname,
		                'id' => $invoice_id->id
		                ];	
				$notification_data = new SendEmailPdf($insert);
				Event::fire('send-pdf', $notification_data);

				session()->flash('notification-status','success');
				Session::flash('alert-type','success');
				session()->flash('notification-msg',"Invoice Email has successfully sent");
				return redirect()->back();
			}
			

				
			
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function update_status(PageRequest $request,$id= NULL){
		$user_id = Auth::user()->id;
		

		try {
			$invoice = Invoice::find($id);

			$booking_details = CleaningService::where('booking_id' , $invoice->booking_id)->first();
			

			if (!$invoice) {
				session()->flash('notification-status',"failed");
				Session::flash('alert-type','failed');
				session()->flash('notification-msg',"Record not found.");
				return redirect()->route('system.invoice.index');
			}
			$invoice->status = $request->get('status');
			$invoice->updated_by = $user_id;

			if ($request->get('status') == "paid") {
				$new_finance = new Finance;
				$new_finance->booking_id = $booking_details->booking_id;
				$new_finance->franchisee_id = $booking_details->franchisee_id;
				$new_finance->group = $booking_details->type;
				$new_finance->details = $booking_details->service_type;
				$new_finance->property_type = $booking_details->property_type;
				$new_finance->type = "Income";
				$new_finance->user_id = Auth::user()->id;
				$new_finance->amount = $booking_details->booking_total;
				$new_finance->save();

			}

		if ($invoice->save()) {
			
			session()->flash('notification-status','success');
			Session::flash('alert-type','success');
			session()->flash('notification-msg',"Invoice has successfully updated");
			return redirect()->route('system.invoice.index');
		}
			
			
		} catch (Exception $e) {
			session()->flash('notification-status','failed');
			session()->flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function excel (PageRequest $request,$id = NULL){
		$this->data['from'] = $request->get('from',false);
		$this->data['to'] = $request->get('to',false);
		$this->data['status'] = $request->get('status',NULL);

		
		if (!$this->data['from']) {
			$from = Invoice::select('created_at')->orderBy('created_at','asc')->take(1)->pluck('created_at')->toArray();
		}

		if(isset($from) AND count($from)==1){
			$this->data['from'] = $from[0]->format("Y-m-d");
			$this->data['to'] = Carbon::now()->format("Y-m-d");
		}

		$header = array_merge([
				"Invoice #", "Booking ID", "Date", "Customer Name", "Amount","Status" , "Franchisee"
			]);

		Excel::create("Exported Invoice Data", function($excel) use($header){
				$excel->setTitle("Exported Finance Data")
			    		->setCreator('HSI')
			          	->setCompany('Highly Suceed Inc.')
			          	->setDescription('Exported Invoice data.');
		$excel->sheet('Invoice', function($sheet) use($header){
			    	$sheet->freezeFirstRow();

			    	$sheet->setOrientation('landscape');

			    	  
			       	$sheet->row(1, $header);

			       	$counter = 2;
		$invoice = Invoice::status($this->data['status'])
								->dateRange($this->data['from'],$this->data['to'])
								->orderBy('created_at',"DESC")
								->chunk(20000, function($chunk) use($sheet, $counter) {
							foreach ($chunk as $invoice) {
								$data = [$invoice->invoice_number, $invoice->booking_id ?: "N/A" , Helper::date_format($invoice->created_at),$invoice->invoice_to ,$invoice->amount,$invoice->status,$invoice->user ? $invoice->user->full_name : "N/A"
									];

								$sheet->row($counter++, $data);
								}	
						});
				  });
			})->export('xls');

	}
}
