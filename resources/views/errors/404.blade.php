 @extends('frontend._layout.main')
 @section('content')



 <div class="content" style="padding-bottom:7%;">


    <div class="container" style="padding-top: 115px;">
     
        <div class="col-lg-12">
        <div class="text-center">
          <img src="{{ asset('frontend/Icons/emptyData.svg')}}" class=" img-fluid w-25 p-3 rounded">
          <h2 class="mt-3">Page not found</h2>
          <p class="mt-4">Try adjusting your search or filter to find what you're looking for.</p>
        </div>
      </div>
      </div>
    </div>
  </div>
  @stop