@extends('frontend._layout.main')
@section('content')

@include('frontend._components.topnav-main')
{{ Session::put('percent',40) }}
<form action="" onsubmit="return validateForm()" method="post">
  {{ csrf_field() }}
   <div class="container" style="padding-top: 133px;padding-bottom:7%;">
     <div class="row">
       <div class="col-lg-12 text-right">
          <p class="text-color-white font-semibold spacing-1">
            <span style="background-color: rgba(0,0,0,.5);" class="p-2">
            <strong>Service Quote for:</strong>  {{ Session::get('clean.postcode') }}, {{ Session::get('clean.sub') }}
          </span>
        </p>
      </div>
      <div class="col-lg-12 text-center">
       
         <p class="text-white font-semibold spacing-1 mt-3 ">What kind of Cleaning Service do you want ?</p>
      </div>
      <div class="col-lg-3"></div>
        <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="regular" name="type" value="regular" >
                <label for="regular" class="text-xxsmall spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/regular_selected.png') }}" class="img-fluid pl-4 pr-4 mb-2"> <br><span class="mobile-text">Regular</span></label>
              </div>
            </div>
            <div class="col-lg-2 col-6 text-center mt-3">
            <div class="styled-radio mt-2 mb-2 ">
              <input type="radio" id="Once-Off1" name="type" value="once-off">
                <label for="Once-Off1" class="text-xxsmall spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/onceoff_selected.png') }}" class="img-fluid pl-4 pr-4 mb-2"> <br><span class="mobile-text">Once-Off</span></label>
              </div>
            </div>
            <div class="col-lg-2 col-6  text-center mt-3">
            <div class="styled-radio mt-2 mb-2">
              <input type="radio" id="End-of-Lease" name="type" value="lease">
                <label for="End-of-Lease" class="text-xxsmall spacing-1 pt-4 pb-4" style="color: white;"><img src="{{ asset('frontend/button-icons/endoff_selected.png') }}" class="img-fluid pl-4 pr-4 mb-2"> <br><span class="mobile-text">End of Lease</span></label>
              </div>
            </div>
            <div class="col-lg-3"></div>
             <div class="col-lg-12 mt-2 text-center">
                <button type="submit"  id="next" class="next btn font-semibold card-shadow spacing-1 text-uppercase text-xsmall bg-color-blue text-color-white pt-3 pl-3 pr-3 pb-3 " href="#"style="color: white !important;">Continue</bjtton>
            </div>
      </div>
   </div>
   </form>
@include('frontend._components.footer-cleaning')
@stop
@section('scripts-content')
<script>
@if ($errors->first('type') ? 'has-error' : NULL) {
 Swal.fire({
  type: 'error',
  title: 'Oops...',
  text: 'Invalid or No Selected Type of Service, Please Select One to Continue',
})
e.preventDefault()
}
@endif

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfrSiksfDRMSa5DxhE7KHwcNHxC0y5R_I&libraries=drawing&callback=initMap"></script>
 @stop
@section('style-script')
<style type="text/css">
  .styled-radio input[type=radio]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
  .styled-radio input[type=checkbox]:checked+label {
    color: white !important;
    border: 2px solid #2047A4  !important;
    background-color: #2047A4  !important;
    -webkit-box-shadow: 3px 3px 3px #212121;
    -moz-box-shadow:    3px 3px 3px #212121;
    box-shadow:         3px 3px 3px #212121;
}
.styled-radio input[type=checkbox] {
    position: absolute;
    visibility: hidden;
    display: none;
}
a:hover{
  color: white !important;
}
p{
  font-size: 17px;
  font-weight: 600;
}
body {
  background-image: url(" {{ asset('frontend/background/cleaning bg.png') }}") !important;
        background-repeat:no-repeat !important;
       background-size:cover !important;
       overflow-x: hidden !important;
} 
@media only screen and (max-width: 1500px) {

 #gradient-background {
    width: 100%;
    padding-top: 70px; 
    height: 100%; 
    background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
    background-size: cover; 
}
.picture-size {
height: 15%; width: 30%;
}
 .footer-bottom{
margin-top: 100px;
  
}
}

@media only screen and (min-width: 1500px) {

   #gradient-background {
      width: 100%;
      padding-top: 70px; 
      height: 100%; 
      background: linear-gradient(-90deg,transparent,transparent, transparent, transparent), url("frontend/background/blue bg.png") no-repeat center; 
      background-size: cover; 
  }
  .picture-size {
  height:5%; width: 7%;
  }
  .footer-bottom{
  position: absolute;
  bottom:0;

}
  }
    @media only screen and (max-width: 992px) {
.mobile-text {
  font-size: 20px;
}
}
</style>
@stop