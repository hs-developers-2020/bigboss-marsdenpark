@extends('system._layouts.main')

</style>@section('content')
@include('system._components.topnav')
  
  <div class="container">
    <p class="text-color-gray font-semibold mb-3 text-uppercase">Update Gallery Details</p>
  </div>
   
    <div class="container pb-5 pl-5 pr-5 pt-4 mt-2 " style="background-color: white; ">
       <form action="" class="with-confirmation" method="POST" enctype="multipart/form-data">
    {!!csrf_field()!!}
    <div class="row">
        <div class="col-lg-12 mt-4">
          <p class="text-color-gray text-xsmall font-semibold text-uppercase">Gallery Details</p>
        </div>
      </div>       
      <div class="row">
        <div class="col-lg-4">
          <div class="text-color-gray mb-2 {{$errors->first('name') ? 'has-error' : NULL}}">
             <i class="fas fa-angle-double-right text-xsmall text-color-blue"></i>
            <span class="ml-2 text-xxsmall font-semibold text-uppercase">Image Name
              <span class="text-color-red ml-1">*</span>
            </span>
            <input type="text" placeholder="Enter service name" class="form-control text-xxsmall" name="name" value="{{ old('name',$gallery->name) }}">
            @if($errors->first('name'))
              <span class="help-block" style="color: red;">{{$errors->first('name')}}</span>
            @endif
          </div>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col-lg-12">
          <div class="form-group">
            <label class="control-label">Current Thumbnail</label>
            <br>
            <img src="{{asset($gallery->directory.'/'.$gallery->filename)}}" alt="" class="img-thumbnail" width="300">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="text-color-gray mb-2">
            <i class="fas fa-angle-double-right font-small text-color-blue"></i>
            <span class="ml-2 text-xxsmall text-uppercase font-semibold ">Gallery image
              <span class="text-color-red">*</span>
            </span>
          </div>
          <div class="d-flex flex-row mb-3 mt-3">
            <input type="file" id="file" name="file"/> 
          </div>
        </div> 
      </div>
      <div class="row">
        <div class="col-lg-12 mt-5">
          <button type="submit" class="btn bg-color-blue text-xxsmall text-color-white p-2 pl-3 pr-3 text-uppercase">Update Gallery</button>
          <a href="{{route('system.gallery.index')}}" class="btn bg-color-red text-xxsmall text-color-white p-2 pl-3 pr-3 ml-2 text-uppercase">Cancel</a>
        </div> 
      </div>
      </form>
    </div>
  

  




@section('page-styles')
<style type="text/css">

body {
  padding-top: 9% !important;
}
input[type=file] {
  font-size: 12px;
  position: ;
  left: 0;
  top: 0;
  
}
.btn-group > .btn:first-child {
   
    padding: 0.375rem 4.75rem;
}
.font-small {
    font-size: 12px !important; 
}
</style>
@stop
@section('page-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
$(function(){
  $('#service_type_input').multiselect({

    templates: {
        li: '<li><a href="javascript:void(0);"><label class="pl-2"></label></a></li>'
    }
  });
});
$('#group').on('change', function(){
  var val = $(this).val();

    if (val == 'accounting') {       
        $('#service_type_container').hide();
        $('#service_price_container').show();
        $('select[id="service_type_input"]').val('');
        $('select[name="price"]').val('');
      
    }else if(val == 'information_technology'){
      $('#service_type_container').hide();
      $('#service_price_container').hide();
      $('select[id="service_type_input"]').val('');
    }else{

      $('#service_type_container').show();
      $('#service_price_container').show();
      $('select[id="service_type_input"]').val('');
      $('select[name="price"]').val('');

    }

}).change();

</script>
@stop