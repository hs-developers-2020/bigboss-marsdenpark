<?php

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use App\Laravel\Traits\DateFormatterTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth,Helper,Input,Request,GeoIP;

class UserTerritory extends Model
{
    use DateFormatterTrait;

    protected $table = "user_territory";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'latitude', 'longitude', 'user_id'
    ];
    
    public $timestamps = true;

    public function user() {
        return $this->belongsTo("App\Laravel\Models\User", "user_id", "id");
    }
     public function userimage() {
        return $this->belongsTo("App\Laravel\Models\UserImage", "user_id", "id");
    }

    public function user_distance($lat = null, $lng = null){
        if($lat AND $lng){
            if($lat == "0.0" AND $lng == "0.0"){
                $ip = Request::header('X-Forwarded-For');
                if(!$ip){
                    $ip = Request::getClientIp();
                }
                $location = GeoIP::getLocation($ip);

 

                $lat = $location['lat'];
                $lng = $location['lon'];
            }
        }else{
            $ip = Request::header('X-Forwarded-For');
            if(!$ip){
                $ip = Request::getClientIp();
            }
            $location = GeoIP::getLocation($ip);

 

            $lat = $location['lat'];
            $lng = $location['lon'];
        }
        
        return number_format(((acos(sin(($lat*pi()/180)) * 
                                            sin(($this->latitude*pi()/180))+cos(($lat*pi()/180)) * 
                                            cos(($this->latitude*pi()/180)) * cos((($lng- $this->longitude)* 
                                            pi()/180))))*180/pi())*60*1.1515,2,'.','');
    }



    public function scopeDistance($query,$range,$lat,$lng){

        if($lat AND $lng){
            if($lat == "0.0" AND $lng == "0.0"){
                $ip = Request::header('X-Forwarded-For');
                if(!$ip){
                    $ip = Request::getClientIp();
                }
                $location = GeoIP::getLocation($ip);

 

                $lat = $location['lat'];
                $lng = $location['lon'];

            }
        }else{
            $ip = Request::header('X-Forwarded-For');
            if(!$ip){
                $ip = Request::getClientIp();
            }
            $location = GeoIP::getLocation($ip);

 

            $lat = $location['lat'];
            $lng = $location['lon'];

        }

 
      
            /*return $query->whereRaw("(((acos(sin((".$lat."*pi()/180)) * 
                                    sin(([geo_lat]*pi()/180))+cos((".$lat."*pi()/180)) * 
                                    cos(([geo_lat]*pi()/180)) * cos(((".$lat."- [geo_long])* 
                                    pi()/180))))*180/pi())*60*1.1515) BETWEEN '{$range[0]}' AND '{$range[1]}'");*/

    

            if(env('TRANSACT_DB_DRIVER','mysql') == "sqlsrv"){
                 
                return $query->whereRaw("(((acos(sin((".$lat."*pi()/180)) * 
                                    sin(([latitude]*pi()/180))+cos((".$lat."*pi()/180)) * 
                                    cos(([latitude]*pi()/180)) * cos(((".$lng."- [longitude])* 
                                    pi()/180))))*180/pi())*60*1.1515) BETWEEN '{$range[0]}' AND '{$range[1]}'");
            }else{
                
                return $query->whereRaw("(((acos(sin((".$lat."*pi()/180)) * 
                                        sin((`latitude`*pi()/180))+cos((".$lat."*pi()/180)) * 
                                        cos((`latitude`*pi()/180)) * cos(((".$lng."- `longitude`)* 
                                        pi()/180))))*180/pi())*60*1.1515) BETWEEN '{$range[0]}' AND '{$range[1]}'");
            }

        
        
    }

    public function scopePolygon($point, $polygon, $pointOnVertex = true){
        $this->pointOnVertex = $pointOnVertex;
 
        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = array(); 
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex); 
        }
 
        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            return "vertex";
        }
    }
}
