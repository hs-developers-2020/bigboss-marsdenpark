<?php

namespace App\Laravel\Controllers\Frontend;

use App\Laravel\Models\cleaningService;
use App\Laravel\Models\ServicePackage;
use App\Laravel\Models\OtherService;
use App\Laravel\Models\Service;
use App\Laravel\Models\ExtraService;
use App\Laravel\Models\Postcode;
use App\Laravel\Models\Addons;
use App\Laravel\Models\User;
use App\Laravel\Models\Finance;
use App\Laravel\Requests\Frontend\InfoRequest;
use App\Mail\BookingNumber;
use App\Laravel\Events\SendBooking;
use App\Http\Controllers\Controller;

use App\Laravel\Services\ResponseManager;
use Illuminate\Http\Request;

use Helper, Carbon, Session, Str, DB,Input,Event,PDF,Curl,Signature,Log,Auth,Mail;


class SubpageInfotechController extends Controller
{
	protected $data;
	function __construct($foo = null)
	{
		$this->data = [];
	}
	public function infotech(Request $request){
		
		$request->session()->forget('booking_info');
		$request->session()->forget('current_step');
		$request->session()->forget('infotech');
		$request->session()->forget('percent');
		$this->data['message_alert'] =  '';
		return view ("frontend.subpage.infotech.step1.index",$this->data);
	}

	
	// public function infotech(){
	// 	return view ('frontend.subpage.infotech.index');
	// }
	public function forex(){
		return view ('frontend.subpage.forex.index');
	}
	public function itService(){

		return view ('frontend.subpage.it-service.index');
	}

	public function back(Request $request)
	{
		$current_step = $request->session()->get('current_step');
		$request->session()->get('current_step') - 2;
		 // return $request->session()->get('current_step');
		return redirect()->route('frontend.infotech.index');
	}

	public function create(BookingRequest $request){
		
		
	}
	public function search(Request $request)
	{
		$postcode = $request->get('term');

		$result = Postcode::where('place_name','LIKE','%'.$postcode."%")
		->orWhere('postcode','LIKE','%'.$postcode."%")	
		->get();

		return $x = json_decode($result,true);

	}


	public function infotech_index(Request $request)
	{

		$current_progress = $request->session()->get('current_progress');
		//dd($current_progress);
		switch ($current_progress) {
			case '1':
				return view("frontend.subpage.infotech.step1.index",$this->data);
				break;
			case '2':
				return view("frontend.subpage.infotech.step{$current_progress}.index",$this->data);
				break;
			case '3':
				$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your Big Boss IT Experts",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';
				$this->data['information_service'] = Service::where('group',"information_technology")->get();
				return view("frontend.subpage.infotech.step{$current_progress}.index",$this->data);
				break;
			case '4':

				$this->data['package'] = Service::whereIn('id', session()->get('infotech.services')['svc'])->get();
				return view("frontend.subpage.infotech.step{$current_progress}.index",$this->data);
				break;
			case '5':
				return view("frontend.subpage.infotech.step{$current_progress}.index",$this->data);
				break;
			case '6':
				$this->data['package'] = Service::whereIn('id', session()->get('infotech.services')['svc'])->get();
				return view("frontend.subpage.infotech.step{$current_progress}.index",$this->data);
				break;
			case '7':
				return view("frontend.subpage.infotech.step{$current_progress}.index",$this->data);
				break;
			default:
				return view("frontend.subpage.infotech.step1.index",$this->data);
				break;
		}
		
		$this->data['message_alert'] =  "";
		
	}
	public function infotech_store(InfoRequest $request)
	{
		$svc_total = 0;
		$current_progress = $request->session()->get('current_progress');
		//dd("!23");
		switch ($current_progress) {
			case '1':
				$validate = Postcode::where('postcode',$request->get('postcode'))->count();
				if($validate > 0)
				{
					$request->session()->put('infotech.postcode',$request->get('postcode'));
					$request->session()->put('infotech.longitude1',$request->get('longitude'));
					$request->session()->put('infotech.latitude2',$request->get('latitude'));
				}
				else
				{
					session()->flash('notification-status','error');
					session()->flash('notification-msg','no post code found Please try again.');
					return redirect()->back();
				}
				if ($request->get('postcode')) {
					session()->put('current_progress',$current_progress + 1) ;
				}

				break;
			case '2':
				
				$request->session()->put('infotech.location',$request->location);
				$request->session()->put('infotech.longitude', $request->get('geo_long'));
				$request->session()->put('infotech.latitude', $request->get('geo_lat'));
				$request->session()->put('infotech.state', $request->get('state'));
				$request->session()->put('infotech.sub', $request->get('sub'));
				
				if ($request->get('location')) {
					session()->put('current_progress',$current_progress + 1) ;
					$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your Big Boss IT Experts",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';
				}
				break;
				
			case '3':
				$this->data['message_alert'] =  '<script>
					Swal.fire({
					position: "center",
					type: "success",
					title: "We found your Big Boss IT Experts",
					showConfirmButton: false,
					timer: 3000
					})
					</script>
					';

				if ($request->get('svc')) {
					$this->data['package'] = Service::whereIn('id' , $request->only('svc')['svc'])->get();
					foreach ($this->data['package'] as $key => $value) {
						$svc_name[] = $value->name;
						$svc_price[] = $value->price;
						$svc_total += $value->price;
					}
					$request->session()->put('infotech.services',$request->only('svc'));
					$request->session()->put('infotech.svc_name',$svc_name);
					$request->session()->put('infotech.svc_price',$svc_price);
					$request->session()->put('infotech.svc_total',$svc_total);
					
					session()->put('infotech.svc',$request->only('svc'));
					session()->put('current_progress',$current_progress + 1) ;
				}
				break;
			case '4':			
				if ($request->get('fname')) {
					$request->session()->put('booking_info.booking_id',uniqid());
					$request->session()->put('infotech.fname', $request->get('fname'));
					$request->session()->put('infotech.lname', $request->get('lname'));
					$request->session()->put('infotech.email', $request->get('email'));
					$request->session()->put('infotech.contact', $request->get('contact_number'));
					session()->put('current_progress',$current_progress + 1) ;
				}
				break;
			case '5':
				if ($request->get('booking_date')) {
					$request->session()->put('infotech.booking_date', $request->get('booking_date'));
					$request->session()->put('infotech.time_start', Helper::time_only_24hour($request->get('time_start')));
					$request->session()->put('infotech.time_end',  Helper::time_only_24hour($request->get('time_end')));
					session()->put('current_progress',$current_progress + 1) ;
				}
				break;
			case '6':
				if($request->session()->has('infotech.postcode')){
					$services =  new cleaningService;
					$services->postcode = $request->session()->get('infotech.postcode');
					$services->location = $request->session()->get('infotech.location');
					$services->service_id = implode(', ',$request->session()->get('infotech.svc')['svc']);
					$services->service_type =  implode(', ',$request->session()->get('infotech.svc_name'));
					$services->name = $request->session()->get('infotech.fname');
					$services->lastname = $request->session()->get('infotech.lname');
					$services->email = $request->session()->get('infotech.email');
					$services->contact = $request->session()->get('infotech.contact');
					$services->booking_id = $request->session()->get('booking_info.booking_id');
					$services->booking_date = Carbon::parse($request->session()->get('infotech.booking_date'));
					$services->time_start =  $request->session()->get('infotech.time_start');
					$services->time_end =  $request->session()->get('infotech.time_end');
					$services->booking_date_secondary =   $request->session()->get('infotech.booking_date_secondary');
					$services->secondary_time_end =    $request->session()->get('infotech.secondary_time_end');
					$services->secondary_time_start =    $request->session()->get('infotech.secondary_time_start');
					$services->payment =  $request->session()->get('infotech.payment');
					$services->longitude =  $request->session()->get('infotech.longitude');
					$services->latitude =  $request->session()->get('infotech.latitude');
					$services->region =  $request->session()->get('infotech.state');
					$services->amount =   $request->session()->get('infotech.svc_total');
					$services->booking_total =   $request->session()->get('infotech.svc_total');
					$services->status =   "pending";
					$services->type =   "information_technology";
				   	if ($services->save()) {
						$insert[] = [
						                'email' => $services->email,
						                'firstname' => $services->name,
						                'lastname' => $services->lname,
						                'name' =>  $services->name . " " . $services->lname,
						                'booking_id' => $services->booking_id
						            ];	
						$notification_data = new SendBooking($insert);
					    Event::fire('send-booking', $notification_data);
						$request->session()->forget('current_progress');
						$request->session()->forget('infotech');
						$request->session()->forget('percent');
					}
					Helper::send_sms();
					$this->data['message_alert'] =  '<script>
						Swal.fire({
						position: "center",
						type: "success",
						title: "Book Successfully",
						showConfirmButton: false,
						timer: 3000
						})
						</script>
						';
							// $request->session()->put('current_step',9);
						return view('frontend.page.booking-submitted.infotech',$this->data);
				}else{
					return redirect()->route('frontend.infotech.index');
				}
				
				
				break;
		
			
			default:
				
				break;
		}
		
		return redirect()->route("frontend.infotech.index");
		
	
		
	} 

	public function revert (Request $request){
		
		$current_progress = $request->session()->get('current_progress');
		if ($current_progress > 1) {
			session()->put('current_progress',$current_progress - 1);
			
		}else{
			session()->put('current_progress', 1);
		}

		return redirect()->route("frontend.infotech.index",$this->data);
	}


	public function destroy($value='')
	{
		$request->session()->flush();
		return redirect()->route('frontend.infotech.index');
	}


	public function cancel(InfoRequest $request)
	{	
		

		if(!$request->session()->has('infotech.postcode'))
		{	
			return redirect()->route('frontend.infotech.index');
		}
        $request->session()->put('infotech.payment', $request->payment);
		$services =  new cleaningService;
 		$services->postcode = $request->session()->get('infotech.postcode');
		$services->location = $request->session()->get('infotech.location');
		$services->service_type =  implode(', ',$request->session()->get('infotech.svc_name'));
		$services->service_id = implode(', ',$request->session()->get('infotech.svc')['svc']);
		$services->name =$request->get('fname');
		$services->lastname = $request->get('lname');
		$services->email = $request->get('email');
		$services->contact = $request->get('contact_number');
		$services->booking_id = $request->session()->get('infotech.booking_id');
		$services->booking_date = Carbon::parse($request->session()->get('clean.booking_date'));
		$services->time_start =  $request->session()->get('infotech.time_start');
		$services->time_end =  $request->session()->get('infotech.time_end');
		$services->booking_date_secondary =   $request->session()->get('infotech.booking_date_secondary');
		$services->secondary_time_end =    $request->session()->get('infotech.secondary_time_end');
		$services->secondary_time_start =    $request->session()->get('infotech.secondary_time_start');
		$services->payment =  $request->session()->get('infotech.payment');
		$services->longitude =  $request->session()->get('infotech.longitude');
		$services->latitude =  $request->session()->get('infotech.latitude');
		$services->region =  $request->session()->get('infotech.state');
		$services->amount =   $request->session()->get('infotech.svc_total');
		$services->booking_total =   $request->session()->get('infotech.svc_total');
		$services->status =   "cancelled";
		$services->type =   "information_technology";


		if ($services->save()) {
			$request->session()->forget('current_progress');
			$request->session()->forget('infotech');
			$request->session()->forget('percent');
		}
			
		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "Thank you for leaving your details. Our local Big Boss Expert will call you in a while.",
			showConfirmButton: false,
			timer: 3000
			})
			</script>';
			$this->data["message"] = "Your local Big Boss Information Technology expert will be in touch with you shortly.";
			return view('frontend.page.cancel-booking.index',$this->data);
			// return redirect()->route('frontend.infotech.index',$this->data);
		
	}
	public function last_cancel(InfoRequest $request)
	{	
		

		if(!$request->session()->has('infotech.postcode'))
		{	
			return redirect()->route('frontend.infotech.index');
		}
        $request->session()->put('infotech.payment', $request->payment);
	

		$services =  new cleaningService;
 		$services->postcode = $request->session()->get('infotech.postcode');
		$services->location = $request->session()->get('infotech.location');
		$services->service_type =  implode(', ',$request->session()->get('infotech.svc_name'));
		$services->service_id = implode(', ',$request->session()->get('infotech.svc')['svc']);
		$services->name = $request->session()->get('infotech.fname');
		$services->lastname = $request->session()->get('infotech.lname');
		$services->email = $request->session()->get('infotech.email');
		$services->contact = $request->session()->get('infotech.contact');
		$services->booking_id = $request->session()->get('infotech.booking_id');
		$services->booking_date = Carbon::parse($request->session()->get('clean.booking_date'));
		$services->time_start =  $request->session()->get('infotech.time_start');
		$services->time_end =  $request->session()->get('infotech.time_end');
		$services->booking_date_secondary =   $request->session()->get('infotech.booking_date_secondary');
		$services->secondary_time_end =    $request->session()->get('infotech.secondary_time_end');
		$services->secondary_time_start =    $request->session()->get('infotech.secondary_time_start');
		$services->payment =  $request->session()->get('infotech.payment');
		$services->longitude =  $request->session()->get('infotech.longitude');
		$services->latitude =  $request->session()->get('infotech.latitude');
		$services->region =  $request->session()->get('infotech.state');
		$services->amount =   $request->session()->get('infotech.svc_total');
		$services->booking_total =   $request->session()->get('infotech.svc_total');
		$services->status =   "cancelled";
		$services->type =   "information_technology";


		if ($services->save()) {
		

			$request->session()->forget('current_progress');
			$request->session()->forget('infotech');
			$request->session()->forget('percent');
		}
			
	
		
		$this->data['message_alert'] =  '<script>
			Swal.fire({
			position: "center",
			type: "success",
			title: "Thank you for leaving your details. Our local Big Boss Expert will call you in a while.",
			showConfirmButton: false,
			timer: 3000
			})
			</script>';
			$this->data["message"] = "Your local Big Boss Information Technology expert will be in touch with you shortly.";
			return view('frontend.page.cancel-booking.index',$this->data);
			// return redirect()->route('frontend.infotech.index',$this->data);
		
	}
}



